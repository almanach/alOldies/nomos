#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
import re
import sqlite3
from time import time
from xml.dom.minidom import *
import math
import numpy as np

####################################################################################
# GENERIC FUNCTIONS
####################################################################################

def getError(the_exception):
    return unicode(type(the_exception))+"("+" ".join(map(lambda a: unicode(a), the_exception.args))+")"

def check_error(error, line=0):
    if error is not None and error != "":
        sys.exit("nomos-afp (%d): %s"%(line, error))

def getException(the_exception):
    return str(type(the_exception))+"("+" ".join(map(lambda a: str(a), the_exception.args))+")"

def verbose(verbose_flag, message):
    if verbose_flag:
        print >> sys.stderr, message

def dumpInstance(instance):
    print >> sys.stderr, instance.__dict__
    raw_input()

def debugf(string):
    sys.stderr.write(string.encode('utf-8'))
    raw_input()

def debug(string, level, programm_level):
    if level <= programm_level:
        sys.stderr.write(string.encode('utf-8'))
        #if level > 1:
        raw_input()

def printerr(var):
    sys.stderr.write(var.encode('utf-8')+'\n')

def writeLog(string, LOG=None):
    if LOG != None: LOG.write(string+'\n')
    else: printerr(string)

def stdin(string=None):
    if string != None:
        raw_input(string)
    else:
        raw_input()

def replaceDiacr(string):
    DIACR = {"à":"a", "â":"a", "ä":"a", "é":"e", "è":"e", "ê":"e", "ë":"e", "î":"i", "ï":"i", "ì":"i", "í":"i", "ô":"o", "ö":"o", "ó":"o", "ò":"o", "û":"u", "ü":"u", "ù":"u", "ç":"c"}
    UDIACR = {}
    for car in DIACR.keys():
        ucar = unicode(car, "utf8")
        UDIACR[ucar] = DIACR[car]
    for car in UDIACR.keys():
        # if re.search(car, string): print 'seen car '+car+' in string '+string
        string = re.sub(car, UDIACR[car], string)
        # DO NOT ENCODE/DECODE ANYTHING TO UTF-8!!
    return string

def old_replaceDiacr(string):
    DIACR = {"à":"a", "â":"a", "ä":"a", "é":"e", "è":"e", "ê":"e", "ë":"e", "î":"i", "ï":"i", "ì":"i", "í":"i", "ô":"o", "ö":"o", "ó":"o", "ò":"o", "û":"u", "ü":"u", "ù":"u", "ç":"c"}
    UDIACR = {}
    for car in DIACR.keys():
        ucar = unicode(car, "utf8")
        UDIACR[ucar] = DIACR[car]
    for car in UDIACR.keys():
        string = re.sub(car, UDIACR[car], string)
    string.encode("utf-8")
    return string

def normalizeCase(s):
    import string
    import re
    if re.search('-', s) == None:
        return string.capwords(s)
    else:
        return '-'.join(map((lambda x: x.capitalize()), s.split('-')))

def mergeLists(listOfLists):
    mergedList = []
    for list2merge in listOfLists:
        mergedList.extend(list2merge)
    return mergedList

def getTime():
    return time()

####################################################################################
# XML FUNCTIONS
####################################################################################

def isTextNode(node):
    return node.nodeType == node.TEXT_NODE

def isElementNode(node):
    return node.nodeType == node.ELEMENT_NODE

def getPreviousElement(node):
    if node is None: return None
    if node.previousSibling is None:
        return None
    if isElementNode(node.previousSibling):
        return node.previousSibling
    if isTextNode(node.previousSibling):
        return getPreviousElement(node.previousSibling)
    # return node.previousSibling

def getNextElement(node):
    if node is None: return None
    if node.nextSibling is None:
        return None
    if isElementNode(node.nextSibling):
        return node.nextSibling
    if isTextNode(node.nextSibling):
        return getNextElement(node.nextSibling)
    # return node.previousSibling

# selectNodes(dom.getElementsByTagName('token'), 'attr_value', ('id', 'E'+sent+'F'+str(frank-2)))[0]
def selectNodes(nodes_list, select_mode, select_data):
    if select_mode == 'node_type':
        if select_data[0] == 'text':
            return filter(isTextNode, nodes_list)
        elif select_data[0] == 'element':
            return filter((lambda node: isElementNode(node) and (select_data[1] == None or node.tagName in select_data[1])), nodes_list)
    elif select_mode == 'attr_value':
        return filter(lambda node: node.getAttribute(select_data[0]) == select_data[1], nodes_list)
    else:
        return nodes_list

def getNodeData(node):
    data = ""
    if node.nodeType == node.TEXT_NODE:
        data += node.data
    else:
        for child in node.childNodes:
            if child.nodeType == child.TEXT_NODE:
                data += child.data
            elif child.nodeType == child.ELEMENT_NODE:
                for sub_child in child.childNodes:
                    data += getNodeData(sub_child)
    return data

def getData(node):
    data = None
    try:
        for child in node.childNodes:
            if child.nodeType == child.TEXT_NODE:
                data = child.data
                break
    except:
        pass
    return data

def createElementNode(dom, tag_name, attributes_and_values=None, data=None, sorted_attributes=None):
    new_element = dom.createElement(tag_name)
    if attributes_and_values:
        if sorted_attributes != None: attributes = sorted_attributes
        else: attributes = attributes_and_values.keys()
        for att_name in attributes:
            if attributes_and_values[att_name] != None and attributes_and_values[att_name] != '':
                addAttribute2Element(dom, new_element, att_name, attributes_and_values[att_name])
    if data:
        data = re.sub('&amp;', '&', data)
        data = re.sub('&', '&amp;', data)
        addData2Element(dom, new_element, data)
    return new_element

def addAttribute2Element(dom, node, att_name, att_value):
    att = dom.createAttribute(att_name)
    att_node = node.setAttributeNode(att)
    try:
        node.setAttribute(att_name, str(att_value))
    except UnicodeEncodeError:
        node.setAttribute(att_name, att_value)

def addData2Element(dom, node, data):
    text = dom.createTextNode(data)
    new_node = node.appendChild(text)

def convertXmlEntities(string, mode='full'):
    new_string = re.sub("&amp;", "&", string)
    new_string = re.sub("&", "&amp;", new_string)
    if mode == 'full':
        new_string = re.sub('<', '&lt;', new_string)
        new_string = re.sub('>', '&gt;', new_string)
        new_string = re.sub("&quot;", '"', new_string)
        new_string = re.sub("&apos;", "'", new_string)
        new_string = re.sub('"', "&quot;", new_string)
        new_string = re.sub("'", "&apos;", new_string)
    return new_string

def getAttributeDefault(node, attribute, defaultvalue):
    if node.getAttribute(attribute) == "": return defaultvalue
    return node.getAttribute(attribute)

def getAttributeTest(node, attribute, testvalue):
    if node.getAttribute(attribute) == testvalue: return True
    return False
    
####################################################################################
####################################################################################
# SQLITE FUNCTIONS + CLASS
####################################################################################

def initTables(gotTables):
    tables = {}
    for (name, columns, index, columns_values_list) in gotTables:
        table = SqliteTable(name, columns)
        tables[table.name] = table
    return tables

def testCommit(insert_count, db, force=False):
    if insert_count >= 10000 or force:
        db.commit()
        print >> sys.stderr, "Commit %d insertions\n"%(insert_count)
        return 0
    else:
        return insert_count

class DBConnection():
    def __init__(self, db, tables=None):
        self.connection = sqlite3.connect(db)
        self.connection.row_factory = sqlite3.Row
        self.cursor = self.connection.cursor()
        self.dbname = db
        self.tables = {}
        if tables != None:
            self.addTables(tables)
        self.addTable('sqlite_master')

    def addTable(self, name):
        self.tables[name] = SqliteTable(name)
        # self.tables[name].cursor = self.cursor

    def addTables(self, names):
        for name in names:
            self.addTable(name)

    def addTablesInstances(self, tables):
        # for table in tables:
        #     self.tables[table.name] = table
        self.tables = tables

    def createTables(self):
        for table in self.tables.values():
            table.createTable(self.cursor)

    def commit(self):
        self.connection.commit()

    def close(self):
        self.cursor.close()
        self.connection.close()

    def check(self, table_name_to_check, column_names_to_check=None):
        self.tables['sqlite_master'].selectFromTable(self.cursor, ['name', 'sql'], [('type', '=', 'table')])
        data = self.cursor.fetchall()
        table2sql = dict(zip(map((lambda t: t[0]), data), map((lambda t: re.sub('^[^\(]+', '', t[1])), data)))
        if table_name_to_check in table2sql.keys():
            if column_names_to_check == None:
                return True
            columns = map((lambda s: s.split(' ')[0]), re.sub('[\(\)]', '', table2sql[table_name_to_check]).split(', '))
            for c in column_names_to_check:
                if c not in columns:
                    return False
            else:
                return True
        else:
            return False
            


class SqliteTable:
    def __init__(self, name, columns=None):
        self.name = name
        self.columns = columns

    def createTable(self, cursor):
        query = """CREATE TABLE IF NOT EXISTS %s ("""%(self.name)
        query += ", ".join(columns2create(self.columns))
        query += """)"""
        cursor.execute(query)
        # print >> sys.stderr, "Creating table %s with columns: "%(self.name)
        # print >> sys.stderr, self.columns

    def createIndex(self, cursor, column, index_name):
        query = """CREATE INDEX IF NOT EXISTS %s ON %s(%s)"""%(index_name, self.name, column)
        print >> sys.stderr, 'Creating index %s in %s...'%(index_name, self.name)
        cursor.execute(query)
        print >> sys.stderr, 'Done.'

    def selectFromTable(self, cursor, columns, conditions=None, limit=None):
        query = """SELECT %s FROM %s"""%(", ".join(columns), self.name)
        values = []
        if conditions != None:
            query += """ WHERE """
            for i in range(len(conditions)):
                (column_condition, operator, value) = conditions[i]
                values.append(value)
                query += """%s %s ?"""%(column_condition, operator)
                if i < len(conditions)-1: query += " and "
        if limit != None:
            query += """ limit %d"""%(limit)
        if values != []:
            symbols = tuple(values)
            cursor.execute(query, symbols)
        else:
            cursor.execute(query)

    def selectFromTableOr(self, cursor, columns, conditions=None, limit=None):
        query = """SELECT %s FROM %s"""%(", ".join(columns), self.name)
        values = []
        if conditions != None:
            query += """ WHERE """
            for i in range(len(conditions)):
                (column_condition, operator, value) = conditions[i]
                values.append(value)
                query += """%s %s ?"""%(column_condition, operator)
                if i < len(conditions)-1: query += " or "
        if limit != None:
            query += """ limit %d"""%(limit)
        if values != []:
            symbols = tuple(values)
            cursor.execute(query, symbols)
        else:
            cursor.execute(query)

    def selectFromTableOrder(self, cursor, columns, sort_column, direction, conditions=None, limit=None):
        query = """SELECT %s FROM %s"""%(", ".join(columns), self.name)
        values = []
        if conditions != None:
            query += """ WHERE """
            for i in range(len(conditions)):
                (column_condition, operator, value) = conditions[i]
                values.append(value)
                query += """%s %s ?"""%(column_condition, operator)
                if i < len(conditions)-1: query += " and "
        if limit != None:
            query += """ limit %d"""%(limit)
        query += """ order by %s %s"""%(sort_column, direction)
        if values != []:
            symbols = tuple(values)
            cursor.execute(query, symbols)
        else:
            cursor.execute(query)
        
    def deleteFromTable(self, cursor, conditions):
        values = []
        query = """DELETE FROM %s ("""%(self.name)
        query += "WHERE "
        for i in range(len(conditions)):
            (column_condition, operator, value) = conditions[i]
            values.append(value)
            query += """%s %s ?"""%(column_condition, operator)
            if i < len(conditions)-1: query += " and "
        symbols = tuple(values)
        cursor.execute(query+','.join('?' for x in values)+')', symbols)
            
    def insertIntoTable(self, cursor, columns2values, debug=False):
        (columns, values) = columns2values2query(columns2values)
        query = """INSERT INTO %s ("""%(self.name)
        query += ", ".join(columns)
        query += """) VALUES("""
        symbols = tuple(values)
        if debug:
            print >> sys.stderr, query+','.join('?' for x in values)+')'+' | Symbols:',; print >> sys.stderr, symbols
        else:
            # try:
            cursor.execute(query+','.join('?' for x in values)+')', symbols)
        
    def insertOrReplaceIntoTable(self, cursor, columns2values):
        # tables['entities2swords'].insertOrReplaceIntoTable(entities_cursor, {'id':entity_id, 'sword':sword, 'count':count })
        (columns, values) = columns2values2query(columns2values)
        query = """INSERT OR REPLACE INTO %s ("""%(self.name)
        query += ", ".join(columns)
        query += """) VALUES("""
        symbols = tuple(values)
        cursor.execute(query+','.join('?' for x in values)+')', symbols)
            
    def updateTable(self, cursor, columns2values, conditions=None):
        query = """UPDATE %s SET """%(self.name)
        (columns, values) = columns2values2query(columns2values)
        query += ", ".join(map((lambda col: " = ".join([col, "?"])), columns))
        if conditions != None:
            query += """ WHERE """
            for i in range(len(conditions)):
                (column_condition, operator, value) = conditions[i]
                values.append(value)
                query += """%s %s ?"""%(column_condition, operator)
                if i < len(conditions)-1: query += " and "
        symbols = tuple(values)
        cursor.execute(query, symbols)
        
    def updateTableCountRow(self, cursor, columns2values, columns, conditions):
        self.selectFromTable(cursor, columns, conditions)
        res = cursor.fetchone()
        if res != None:
            old_value = res[0]
            new_value = columns2values[columns[0]] + old_value
            columns2values.update({columns[0]:new_value})
        self.insertOrReplaceIntoTable(cursor, columns2values)

    def dropTable(self, cursor):
        query = """DROP TABLE IF EXISTS %s"""%(self.name)
        cursor.execute(query)

def columns2values2query(columns2values):
    columns = columns2values.keys()
    columns.sort()
    values = map( (lambda col: columns2values[col]), columns)
    return columns, values
                           
def columns2create(columns):
    l = []
    for column, value in columns.items():
        if len(value.split("#")) == 2 and value.split("#")[1] == "unique":
            value = value.split("#")[0]; unique = " unique"
        else:
            unique = ""
        l.append("%s %s%s"%(column, value, unique))
    return l


####################################################################################
# OTHER
####################################################################################
def convertIptcCodes(full_code):
    table = {'01':'CLT', '02':'CLJ', '03':'DIS', '04':'ECO', '05':'EDU', '06':'ENV', '07':'HTH', '08':'HUM', '09':'SOC', '10':'LIF', '11':'POL', '12':'REL', '13':'SCI', '14':'SOI', '15':'SPO', '16':'UNR', '17':'WEA'}
    topic = None
    for code in table.keys():
        if full_code.startswith(code):
            topic = table[code]
            break
    if topic:
        return topic
    else:
        return 'NULL'

def computeSalience(words2frequency, total_words_population, dbconnection):
    rho = 0.0
    z = 0.0
    words2saillance = {}
    dbconnection.cursor.execute('PRAGMA case_sensitive_like(words2frequency)')
    sub_population = float(sum(words2frequency.values()))
    for word in words2frequency:
        frequency = words2frequency[word]
        dbconnection.tables['words2frequency'].selectFromTable(dbconnection.cursor, ['frequency'], [('word', '=', word)])
        try: temp_positive_in_population = float(dbconnection.cursor.fetchone()[0])
        except: temp_positive_in_population = 0.0
        # NORMALISATION DES POPULATIONS (TEST BEN)
        positive_in_population = temp_positive_in_population * sub_population / float(total_words_population)
        new_population = sub_population
        positive_in_sub_population = float(frequency)
        rho = (positive_in_population + positive_in_sub_population) / (new_population + sub_population)
        truc = ((rho * (1-rho)) / sub_population) + (rho * (1-rho) / total_words_population)
        z = ((positive_in_sub_population / sub_population) - (positive_in_population / new_population)) / math.sqrt(((rho * (1-rho)) / sub_population) + (rho * (1-rho) / new_population))
        words2saillance[word] = z
    ranked_words2saillance = sorted(zip(words2saillance.keys(), words2saillance.values()), key=lambda x: x[1], reverse=True)
    return ranked_words2saillance

def computeSalienceLocal(words2frequency, total_words_population, total_words2frequency):
    rho = 0.0
    z = 0.0
    words2saillance = {}
    sub_population = float(sum(words2frequency.values()))
    for word in words2frequency:
        frequency = words2frequency[word]
        # try:
        temp_positive_in_population = total_words2frequency[word]
        # except KeyError: temp_positive_in_population = 0.0
        # NORMALISATION DES POPULATIONS (TEST BEN)
        positive_in_population = temp_positive_in_population * sub_population / float(total_words_population)
        new_population = sub_population
        positive_in_sub_population = float(frequency)
        rho = (positive_in_population + positive_in_sub_population) / (new_population + sub_population)
        truc = ((rho * (1-rho)) / sub_population) + (rho * (1-rho) / total_words_population)
        try:
            z = ((positive_in_sub_population / sub_population) - (positive_in_population / new_population)) / math.sqrt(((rho * (1-rho)) / sub_population) + (rho * (1-rho) / new_population))
        except ZeroDivisionError:
            z  = 0
        words2saillance[word] = z
    ranked_words2saillance = sorted(zip(words2saillance.keys(), words2saillance.values()), key=lambda x: x[1], reverse=True)
    return ranked_words2saillance

# def computeSalience(doc_words2frequency, words_population=123000000, words_connection=None):
#     # arg1 = dict{w:frq}; if words_connection: needs table 'words2frequency' with 'word', 'frequency' fields
#     rho = 0.0
#     z = 0.0
#     words2salience = {}
#     # words_connection.cursor.execute("PRAGMA case_sensitive_like(words2frequency)")
#     sub_population = float(sum(doc_words2frequency.values()))
#     for (word, frequency) in doc_words2frequency.items():
#         # words_connection.tables['words2frequency'].selectFromTable(words_connection.cursor, ['frequency'], [('word', '=', word)])
#         # try: temp_positive_in_population = float(words_connection.cursor.fetchone()[0])
#         # except: temp_positive_in_population = 0.0
#         temp_positive_in_population = float(words2frequency[word])
#         print "word: %s"%(word)
#         # NORMALISATION DES POPULATIONS (TEST BEN)
#         positive_in_population = temp_positive_in_population * sub_population / words_population
#         print "pos in pop: %f / %f"%(temp_positive_in_population, positive_in_population)
#         new_population = sub_population
#         # print "pop: %f"%(new_population)
#         # print "Found: "+str(positive_in_population)+" in population"
#         positive_in_sub_population = float(frequency)
#         # print "pos in sub pop: %f"%(positive_in_sub_population)
#         # try:
#             # print "Found: "+str(positive_in_sub_population)+" in sub population"
#             # rho = (positive_in_population + positive_in_sub_population) / (population + sub_population)
#         rho = (positive_in_population + positive_in_sub_population) / (new_population + sub_population)
#         # print "rho = ("+str(positive_in_population)+" + "+str(positive_in_sub_population)+") / ("+str(new_population)+" + "+str(sub_population)+")"
#         # print "rho: %f"%(rho)
#         truc = ((rho * (1-rho)) / sub_population) + (rho * (1-rho) / words_population)
#         # print "truc: %f"%(truc)
#             # z = ((positive_in_sub_population / sub_population) - (positive_in_population / population)) / math.sqrt(((rho * (1-rho)) / sub_population) + (rho * (1-rho) / population))
#         print >> sys.stderr, "z = ((%f / %f) - (%f / %f)) / math.sqrt(((%f * (1-%f)) / %f) + (%f * (1-%f) / %f))"%(positive_in_sub_population, sub_population, positive_in_population, new_population, rho, rho, sub_population, rho, rho, new_population)
#         z = ((positive_in_sub_population / sub_population) - (positive_in_population / new_population)) / math.sqrt(((rho * (1-rho)) / sub_population) + (rho * (1-rho) / new_population))
#         # print "z: %f"%(z)
#         # except Exception, error:
#         #     printerr('Error in computeSalience: %s'%(Exception))
#         #     print >> sys.stderr, error.args
#         words2salience[word] = z
#         # print "Population: "+str(new_population)+"\nPositive in population: "+str(positive_in_population)
#         # print "News population: "+str(sub_population)+"\nPositive in news: "+str(positive_in_sub_population)
#         # print "Rho: "+str(rho)
#         # print "Truc: "+str(truc)
#         # print "Z for this word/entity: "+str(z); stdin()
#     ranked_words2salience = sorted(zip(words2salience.keys(), words2salience.values()), key=lambda x: x[1], reverse=True)
#     # end = time.time()
#     # time_message = "Get words saliences in news item: %.2fs"%(end-start)
#     # printerr(time_message)
#     return ranked_words2salience

def getStopWords():
    stop = {"&quot;":True, "&apos;":True, "!":True, "!!":True, "!!!":True, "!?":True,
"!?!?":True, "&":True, "&amp;":True, "&lt;-":True, "&lt;-&gt;":True, "'":True, "(":True,
    "(...)":True, ")":True, "*":True, "+":True, "++":True, ",":True, "-":True, "-&gt;":True, "->":True,
    "-ce":True, "-elle":True, "-elles":True, "-en":True, "-il":True, "-ils":True, "-je":True,
    "-la":True, "-le":True, "-les":True, "-leur":True, "-lui":True, "-m'":True, "-moi":True,
    "-nous":True, "-on":True, "-t'":True, "-t-elle":True, "-t-elles":True, "-t-en":True, "-t-il":True,
    "-t-ils":True, "-t-on":True, "-t-y":True, "-toi":True, "-tu":True, "-vous":True, "-vs":True,
    "-y":True, ".":True, "...":True, "....":True, "/":True, ":":True, ";":True, "<-":True, "<->":True,
    "?":True, "?!":True, "?!?!":True, "???":True, "@":True, "@+":True, "O.K..":True, "OK":True,
    "[":True, "[...]":True, "]":True, "_-_":True, "_ACC_F":True, "_ACC_O":True, "_UNDERSCORE":True,
    "a":True, "a+":True, "absence":True, "accord":True, "admettant":True, "afin":True, "agissant":True,
    "aglagla":True, "ah":True, "ai":True, "aie":True, "aient":True, "aies":True, "aille":True,
    "aillent":True, "ailles":True, "aima":True, "aimai":True, "aimaient":True, "aimais":True,
    "aimait":True, "aimant":True, "aimas":True, "aimasse":True, "aimassent":True, "aimasses":True,
    "aimassiez":True, "aimassions":True, "aime":True, "aiment":True, "aimer":True, "aimera":True,
    "aimerai":True, "aimeraient":True, "aimerais":True, "aimerait":True, "aimeras":True, "aimerez":True,
    "aimeriez":True, "aimerions":True, "aimerons":True, "aimeront":True, "aimes":True, "aimez":True,
    "aimiez":True, "aimions":True, "aimons":True, "aimâmes":True, "aimât":True, "aimâtes":True,
    "aimèrent":True, "aimé":True, "ainsi":True, "ait":True, "alla":True, "allaient":True, "allais":True,
    "allait":True, "allant":True, "allas":True, "allassent":True, "allasses":True, "allassiez":True,
    "allassions":True, "aller":True, "allez":True, "alliez":True, "allions":True, "allo":True,
    "allons":True, "allâmes":True, "allât":True, "allâtes":True, "allèrent":True, "allé":True,
    "allé-":True, "allés":True, "alors":True, "amont":True, "an":True, "and":True, "app'":True,
    "appétit":True, "apr.":True, "après":True, "arrière":True, "as":True, "assez":True, "atchoum":True,
    "attendu":True, "au":True, "au-delà":True, "au-dessous":True, "au-dessus":True, "au-devant":True,
    "aucun":True, "aucune":True, "aune":True, "auprès":True, "aura":True, "aurai":True, "auraient":True,
    "aurais":True, "aurait":True, "auras":True, "aurez":True, "auriez":True, "aurions":True,
    "aurons":True, "auront":True, "aussi":True, "aussitôt":True, "autant":True, "autour":True,
    "autre":True, "autres":True, "autrui":True, "aux":True, "av.":True, "avaient":True, "avais":True,
    "avait":True, "aval":True, "avant":True, "avec":True, "avez":True, "aviez":True, "avions":True,
    "avis":True, "avoir":True, "avons":True, "avt":True, "ayant":True, "ayez":True, "ayons":True,
    "aïe":True, "bah":True, "basta":True, "bcp":True, "beau":True, "beaucoup":True, "because":True,
    "ben":True, "beurk":True, "bicause":True, "bien":True, "bientôt":True, "bip":True, "bof":True,
    "bof-bof":True, "bon":True, "bonjour":True, "bonne":True, "bons":True, "bonsoir":True, "bord":True,
    "bouh":True, "bout":True, "buenas":True, "but":True, "bye":True, "c'":True, "c-à-d":True,
    "c.-à.-d.":True, "car":True, "carpe":True, "cas":True, "cause":True, "ce":True, "ceci":True,
    "cela":True, "celle":True, "celle-ci":True, "celle-là":True, "celles":True, "celles-ci":True,
    "celles-là":True, "celui":True, "celui-ci":True, "celui-là":True, "centre":True, "cependant":True,
    "certain":True, "certaine":True, "certaines":True, "certains":True, "ces":True, "cet":True,
    "cette":True, "ceux":True, "ceux-ci":True, "ceux-là":True, "cf":True, "cf.":True, "ch'":True,
    "chacun":True, "chacune":True, "chaqu'":True, "chaque":True, "chers":True, "chez":True,
    "chose":True, "ci-devant":True, "ciao":True, "combien":True, "comble":True, "comme":True,
    "comment":True, "compris":True, "compte":True, "compter":True, "concernant":True, "condition":True,
    "conf.":True, "confer":True, "confins":True, "conformément":True, "consequent":True,
    "considérant":True, "considéré":True, "contre":True, "contrebas":True, "cotés":True, "coucou":True,
    "coup":True, "cours":True, "crainte":True, "croie":True, "croient":True, "croies":True,
    "croira":True, "croirai":True, "croiraient":True, "croirais":True, "croirait":True, "croiras":True,
    "croire":True, "croirez":True, "croiriez":True, "croirions":True, "croirons":True, "croiront":True,
    "crois":True, "croit":True, "croyaient":True, "croyais":True, "croyait":True, "croyant":True,
    "croyez":True, "croyiez":True, "croyions":True, "croyons":True, "cru":True, "crurent":True,
    "crus":True, "crusse":True, "crussent":True, "crusses":True, "crussiez":True, "crussions":True,
    "crut":True, "crûmes":True, "crût":True, "crûtes":True, "ct'":True, "càd":True, "côté":True,
    "d'":True, "d'après":True, "dam":True, "damned":True, "dans":True, "de":True, "debut":True,
    "dedans":True, "dehors":True, "delà":True, "depuis":True, "derrière":True, "des":True,
    "dessous":True, "dessus":True, "devaient":True, "devais":True, "devait":True, "devant":True,
    "devers":True, "devez":True, "deviez":True, "devions":True, "devoir":True, "devons":True,
    "devra":True, "devrai":True, "devraient":True, "devrais":True, "devrait":True, "devras":True,
    "devrez":True, "devriez":True, "devrions":True, "devrons":True, "devront":True, "deçà":True,
    "diantre":True, "diem":True, "dieu":True, "dira":True, "dirai":True, "diraient":True, "dirais":True,
    "dirait":True, "diras":True, "dire":True, "dirent":True, "direz":True, "diriez":True,
    "dirions":True, "dirons":True, "diront":True, "dis":True, "disaient":True, "disais":True,
    "disait":True, "disant":True, "dise":True, "disent":True, "dises":True, "disiez":True,
    "disions":True, "disons":True, "disse":True, "dissent":True, "disses":True, "dissiez":True,
    "dissions":True, "dit":True, "dites":True, "dixit":True, "dois":True, "doit":True, "doive":True,
    "doivent":True, "doives":True, "donc":True, "donna":True, "donnai":True, "donnaient":True,
    "donnais":True, "donnait":True, "donnant":True, "donnas":True, "donnasse":True, "donnassent":True,
    "donnasses":True, "donnassiez":True, "donnassions":True, "donne":True, "donnent":True,
    "donner":True, "donnera":True, "donnerai":True, "donneraient":True, "donnerais":True,
    "donnerait":True, "donneras":True, "donnerez":True, "donneriez":True, "donnerions":True,
    "donnerons":True, "donneront":True, "donnes":True, "donnez":True, "donniez":True, "donnions":True,
    "donnons":True, "donnâmes":True, "donnât":True, "donnâtes":True, "donnèrent":True, "donné":True,
    "dont":True, "droit":True, "drôle":True, "du":True, "durant":True, "durent":True, "dus":True,
    "dusse":True, "dussent":True, "dusses":True, "dussiez":True, "dussions":True, "dut":True,
    "dès":True, "début":True, "défaut":True, "déjà":True, "dépens":True, "dépit":True, "déplaise":True,
    "dîmes":True, "dît":True, "dîtes":True, "dû":True, "dûmes":True, "dût":True, "dûtes":True,
    "effet":True, "eh":True, "eight":True, "elle":True, "elle-même":True, "elles":True,
    "elles-même":True, "elles-mêmes":True, "en":True, "en-deçà":True, "encontre":True, "encore":True,
    "endéans":True, "enseigne":True, "entre":True, "entremise":True, "envers":True, "environ":True,
    "es":True, "espoir":True, "est":True, "est-à-dire":True, "et":True, "et-ou":True, "et/ou":True,
    "etc.":True, "et|ou":True, "eu":True, "eue":True, "eues":True, "euh":True, "eurent":True,
    "eus":True, "eusse":True, "eussent":True, "eusses":True, "eussiez":True, "eussions":True,
    "eut":True, "eux":True, "eux-même":True, "eux-mêmes":True, "excepté":True, "exemple":True,
    "exemples":True, "eûmes":True, "eût":True, "eûtes":True, "face":True, "facon":True, "faille":True,
    "faire":True, "fais":True, "faisaient":True, "faisais":True, "faisait":True, "faisant":True,
    "faisiez":True, "faisions":True, "faisons":True, "fait":True, "faites":True, "fallait":True,
    "falloir":True, "fallu":True, "fallut":True, "fallût":True, "fasse":True, "fassent":True,
    "fasses":True, "fassiez":True, "fassions":True, "faudra":True, "faudrait":True, "faut":True,
    "faute":True, "faveur":True, "façon":True, "fera":True, "ferai":True, "feraient":True,
    "ferais":True, "ferait":True, "feras":True, "ferez":True, "feriez":True, "ferions":True,
    "ferons":True, "feront":True, "fin":True, "finallementthe":True, "firent":True, "fis":True,
    "fisse":True, "fissent":True, "fisses":True, "fissiez":True, "fissions":True, "fit":True,
    "five":True, "fois":True, "fond":True, "font":True, "force":True, "forme":True, "four":True,
    "fur":True, "furent":True, "fus":True, "fusse":True, "fussent":True, "fusses":True, "fussiez":True,
    "fussions":True, "fut":True, "fîmes":True, "fît":True, "fîtes":True, "fûmes":True, "fût":True,
    "fûtes":True, "geht'":True, "glagla":True, "grace":True, "grand":True, "grand-chose":True,
    "grrr":True, "grâce":True, "gré":True, "guise":True, "guère":True, "hasta":True, "haut":True,
    "hauteur":True, "hein":True, "heure":True, "hey":True, "hihihi":True, "histoire":True, "hola":True,
    "hommage":True, "hop":True, "hormis":True, "hors":True, "hypothèse":True, "hé":True, "hélas":True,
    "i.e.":True, "icelle":True, "icelles":True, "icelui":True, "iceux":True, "ici":True, "id":True,
    "idée":True, "ie":True, "il":True, "ils":True, "importe":True, "instance":True, "instant":True,
    "instar":True, "insu":True, "intention":True, "interessant":True, "interessante":True,
    "interessantes":True, "interessants":True, "io":True, "ira":True, "iraient":True, "irais":True,
    "irait":True, "iras":True, "irez":True, "iriez":True, "irions":True, "irons":True, "iront":True,
    "j'":True, "j'aille":True, "j'allai":True, "j'allais":True, "j'allasse":True, "j'irai":True,
    "j'étais":True, "jamais":True, "je":True, "jour":True, "journée":True, "jusqu'":True, "jusque":True,
    "l'":True, "la":True, "ladite":True, "laquelle":True, "large":True, "le":True, "ledit":True,
    "lequel":True, "les":True, "lesdites":True, "lesdits":True, "lesquelles":True, "lesquels":True,
    "leur":True, "leurs":True, "lez":True, "lieu":True, "loin":True, "long":True, "longtemps":True,
    "lors":True, "lorsqu'":True, "lorsque":True, "luego":True, "lui":True, "lui-même":True, "là":True,
    "lès":True, "m'":True, "ma":True, "maint":True, "mainte":True, "maintenant":True, "maintes":True,
    "maints":True, "mais":True, "mal":True, "malgré":True, "manière":True, "manque":True,
    "matière":True, "mauvais":True, "mauvaise":True, "mauvaises":True, "me":True, "meilleur":True,
    "meilleurs":True, "mes":True, "mesure":True, "met":True, "mets":True, "mettaient":True,
    "mettais":True, "mettait":True, "mettant":True, "mette":True, "mettent":True, "mettes":True,
    "mettez":True, "mettiez":True, "mettions":True, "mettons":True, "mettra":True, "mettrai":True,
    "mettraient":True, "mettrais":True, "mettrait":True, "mettras":True, "mettre":True, "mettrez":True,
    "mettriez":True, "mettrions":True, "mettrons":True, "mettront":True, "meuh":True, "mhh":True,
    "miam":True, "mien":True, "mienne":True, "miennes":True, "miens":True, "mieux":True, "milieu":True,
    "mirent":True, "mis":True, "misse":True, "missent":True, "misses":True, "missiez":True,
    "missions":True, "mit":True, "mmh":True, "mmm":True, "moi":True, "moi-même":True, "moins":True,
    "moment":True, "mon":True, "monde":True, "mot":True, "mots":True, "mouais":True, "moui":True,
    "moyen":True, "moyennant":True, "même":True, "mêmes":True, "mîmes":True, "mît":True, "mîtes":True,
    "n'":True, "nan":True, "ne":True, "ni":True, "niark":True, "nine":True, "no":True, "nom":True,
    "non":True, "nonobstant":True, "nos":True, "notre":True, "nous":True, "nous-même":True,
    "nous-mêmes":True, "nuit":True, "nul":True, "nulle":True, "nullement":True, "nulles":True,
    "nuls":True, "nôtre":True, "nôtres":True, "of":True, "okay":True, "oki":True, "okidoki":True,
    "on":True, "one":True, "ont":True, "oops":True, "or":True, "ordre":True, "ou":True, "ou/et":True,
    "ouah":True, "ouais":True, "oublier":True, "oups":True, "outre":True, "où":True, "pac'":True,
    "par":True, "par-delà":True, "par-dessous":True, "par-dessus":True, "parbleu":True, "parc'":True,
    "parce":True, "pareil":True, "pareille":True, "pareilles":True, "pareils":True, "parla":True,
    "parlai":True, "parlaient":True, "parlais":True, "parlait":True, "parlant":True, "parlas":True,
    "parlasse":True, "parlassent":True, "parlasses":True, "parlassiez":True, "parlassions":True,
    "parle":True, "parlent":True, "parler":True, "parlera":True, "parlerai":True, "parleraient":True,
    "parlerais":True, "parlerait":True, "parleras":True, "parlerez":True, "parleriez":True,
    "parlerions":True, "parlerons":True, "parleront":True, "parles":True, "parlez":True, "parliez":True,
    "parlions":True, "parlons":True, "parlâmes":True, "parlât":True, "parlâtes":True, "parlèrent":True,
    "parlé":True, "parmi":True, "part":True, "partant":True, "particulier":True, "particuliere":True,
    "particulieres":True, "particuliers":True, "partir":True, "pas":True, "pask'":True, "paske":True,
    "passé":True, "pcq":True, "pdt":True, "pendant":True, "pensa":True, "pensai":True, "pensaient":True,
    "pensais":True, "pensait":True, "pensant":True, "pensas":True, "pensasse":True, "pensassent":True,
    "pensasses":True, "pensassiez":True, "pensassions":True, "pense":True, "pensent":True,
    "penser":True, "pensera":True, "penserai":True, "penseraient":True, "penserais":True,
    "penserait":True, "penseras":True, "penserez":True, "penseriez":True, "penserions":True,
    "penserons":True, "penseront":True, "penses":True, "pensez":True, "pensiez":True, "pensions":True,
    "pensons":True, "pensâmes":True, "pensât":True, "pensâtes":True, "pensèrent":True, "pensé":True,
    "personne":True, "perte":True, "peu":True, "peur":True, "peut":True, "peuvent":True, "peux":True,
    "pfff":True, "pfiou":True, "pire":True, "pires":True, "pkoi":True, "plein":True, "plouche":True,
    "plupart":True, "plus":True, "plusieurs":True, "plutôt":True, "point":True, "pouah":True,
    "pour":True, "pourquoi":True, "pourra":True, "pourrai":True, "pourraient":True, "pourrais":True,
    "pourrait":True, "pourras":True, "pourrez":True, "pourriez":True, "pourrions":True, "pourrons":True,
    "pourront":True, "pourvu":True, "pouvaient":True, "pouvais":True, "pouvait":True, "pouvant":True,
    "pouvez":True, "pouviez":True, "pouvions":True, "pouvoir":True, "pouvons":True, "pr":True,
    "premièrement":True, "prenaient":True, "prenais":True, "prenait":True, "prenant":True, "prend":True,
    "prendra":True, "prendrai":True, "prendraient":True, "prendrais":True, "prendrait":True,
    "prendras":True, "prendre":True, "prendrez":True, "prendriez":True, "prendrions":True,
    "prendrons":True, "prendront":True, "prends":True, "prenez":True, "preniez":True, "prenions":True,
    "prenne":True, "prennent":True, "prennes":True, "prenons":True, "preuve":True, "prirent":True,
    "pris":True, "prisse":True, "prissent":True, "prisses":True, "prissiez":True, "prissions":True,
    "prit":True, "proche":True, "proie":True, "proportion":True, "propos":True, "prorata":True,
    "prou":True, "près":True, "précédemment":True, "présent":True, "prétexte":True, "prîmes":True,
    "prît":True, "prîtes":True, "pu":True, "puis":True, "puisqu'":True, "puisque":True, "puisse":True,
    "puissent":True, "puisses":True, "puissiez":True, "puissions":True, "purent":True, "pus":True,
    "pusse":True, "pussent":True, "pusses":True, "pussiez":True, "pussions":True, "put":True,
    "putain":True, "pô":True, "pûmes":True, "pût":True, "pûtes":True, "qd":True, "qq":True, "qqc":True,
    "qqch":True, "qqchose":True, "qqes":True, "qqn":True, "qqs":True, "qu'":True, "quand":True,
    "quant":True, "que":True, "quel":True, "quelle":True, "quelles":True, "quelqu'":True,
    "quelque":True, "quelques":True, "quelques-unes":True, "quelques-uns":True, "quels":True,
    "qui":True, "quiconque":True, "quitte":True, "quoi":True, "quoiqu'":True, "quoique":True,
    "quête":True, "raison":True, "rapport":True, "ras":True, "regard":True, "regarda":True,
    "regardai":True, "regardaient":True, "regardais":True, "regardait":True, "regardant":True,
    "regardas":True, "regardasse":True, "regardassent":True, "regardasses":True, "regardassiez":True,
    "regardassions":True, "regarde":True, "regardent":True, "regarder":True, "regardera":True,
    "regarderai":True, "regarderaient":True, "regarderais":True, "regarderait":True, "regarderas":True,
    "regarderez":True, "regarderiez":True, "regarderions":True, "regarderons":True, "regarderont":True,
    "regardes":True, "regardez":True, "regardiez":True, "regardions":True, "regardons":True,
    "regardâmes":True, "regardât":True, "regardâtes":True, "regardèrent":True, "regardé":True,
    "relation":True, "reste":True, "rev'":True, "revoici":True, "revoilà":True, "revoir":True,
    "rien":True, "règlement":True, "réserve":True, "s":True, "s'":True, "sa":True, "sachant":True,
    "sache":True, "sachent":True, "saches":True, "sachez":True, "sachiez":True, "sachions":True,
    "sachons":True, "sais":True, "sait":True, "sangrebleu":True, "sans":True, "sauf":True, "saura":True,
    "saurai":True, "sauraient":True, "saurais":True, "saurait":True, "sauras":True, "saurez":True,
    "sauriez":True, "saurions":True, "saurons":True, "sauront":True, "savaient":True, "savais":True,
    "savait":True, "savent":True, "savez":True, "saviez":True, "savions":True, "savoir":True,
    "savons":True, "se":True, "secondement":True, "sein":True, "selon":True, "sens":True, "sera":True,
    "serai":True, "seraient":True, "serais":True, "serait":True, "seras":True, "serez":True,
    "seriez":True, "serions":True, "serons":True, "seront":True, "ses":True, "seule":True, "seven":True,
    "si":True, "sien":True, "sienne":True, "siennes":True, "siens":True, "signe":True, "sinon":True,
    "sitôt":True, "six":True, "snif":True, "soi":True, "soi-même":True, "soient":True, "soins":True,
    "sois":True, "soit":True, "sommes":True, "son":True, "sont":True, "sorry":True, "sorte":True,
    "sortir":True, "sous":True, "soyez":True, "soyons":True, "su":True, "suis":True, "suite":True,
    "suivant":True, "sujet":True, "supposant":True, "supposer":True, "supposé":True, "sur":True,
    "surent":True, "surtout":True, "sus":True, "susse":True, "sussent":True, "susses":True,
    "sussiez":True, "sussions":True, "sut":True, "sûmes":True, "sût":True, "sûtes":True, "t'":True,
    "ta":True, "tandis":True, "tant":True, "tardes":True, "tchuss":True, "tchô":True, "te":True,
    "tel":True, "telle":True, "telles":True, "tels":True, "temps":True, "ten":True, "tenaient":True,
    "tenais":True, "tenait":True, "tenant":True, "tenez":True, "teniez":True, "tenions":True,
    "tenir":True, "tenons":True, "tenu":True, "terme":True, "termes":True, "tes":True, "three":True,
    "tien":True, "tiendra":True, "tiendrai":True, "tiendraient":True, "tiendrais":True,
    "tiendrait":True, "tiendras":True, "tiendrez":True, "tiendriez":True, "tiendrions":True,
    "tiendrons":True, "tiendront":True, "tienne":True, "tiennent":True, "tiennes":True, "tiens":True,
    "tient":True, "tinrent":True, "tins":True, "tinsse":True, "tinssent":True, "tinsses":True,
    "tinssiez":True, "tinssions":True, "tint":True, "titre":True, "to":True, "toi":True,
    "toi-même":True, "ton":True, "touchant":True, "toujours":True, "tous":True, "tout":True,
    "toute":True, "toutes":True, "train":True, "travers":True, "trouva":True, "trouvai":True,
    "trouvaient":True, "trouvais":True, "trouvait":True, "trouvant":True, "trouvas":True,
    "trouvasse":True, "trouvassent":True, "trouvasses":True, "trouvassiez":True, "trouvassions":True,
    "trouve":True, "trouvent":True, "trouver":True, "trouvera":True, "trouverai":True,
    "trouveraient":True, "trouverais":True, "trouverait":True, "trouveras":True, "trouverez":True,
    "trouveriez":True, "trouverions":True, "trouverons":True, "trouveront":True, "trouves":True,
    "trouvez":True, "trouviez":True, "trouvions":True, "trouvons":True, "trouvâmes":True,
    "trouvât":True, "trouvâtes":True, "trouvèrent":True, "trouvé":True, "tréfonds":True, "tu":True,
    "twentyà":True, "two":True, "tînmes":True, "tînt":True, "tîntes":True, "un":True, "une":True,
    "unes":True, "uns":True, "v'":True, "va":True, "vais":True, "vas":True, "venaient":True,
    "venais":True, "venait":True, "venant":True, "venez":True, "veniez":True, "venions":True,
    "venir":True, "venons":True, "venu":True, "venus":True, "verra":True, "verrai":True,
    "verraient":True, "verrais":True, "verrait":True, "verras":True, "verrez":True, "verriez":True,
    "verrions":True, "verrons":True, "verront":True, "vers":True, "versus":True, "vertu":True,
    "veuille":True, "veuillent":True, "veuilles":True, "veulent":True, "veut":True, "veux":True,
    "veux/veuille":True, "via":True, "viendra":True, "viendrai":True, "viendraient":True,
    "viendrais":True, "viendrait":True, "viendras":True, "viendrez":True, "viendriez":True,
    "viendrions":True, "viendrons":True, "viendront":True, "vienne":True, "viennent":True,
    "viennes":True, "viens":True, "vient":True, "vinrent":True, "vins":True, "vinsse":True,
    "vinssent":True, "vinsses":True, "vinssiez":True, "vinssions":True, "vint":True, "virent":True,
    "vis":True, "vis-à-vis":True, "visse":True, "vissent":True, "visses":True, "vissiez":True,
    "vissions":True, "vit":True, "voici":True, "voie":True, "voient":True, "voies":True, "voili":True,
    "voilou":True, "voilà":True, "voir":True, "voire":True, "vois":True, "voit":True, "vont":True,
    "vos":True, "vot'":True, "votre":True, "voudra":True, "voudrai":True, "voudraient":True,
    "voudrais":True, "voudrait":True, "voudras":True, "voudrez":True, "voudriez":True, "voudrions":True,
    "voudrons":True, "voudront":True, "voulaient":True, "voulais":True, "voulait":True, "voulant":True,
    "voulez":True, "voulez/veuillez":True, "vouliez":True, "voulions":True, "vouloir":True,
    "voulons":True, "voulu":True, "voulurent":True, "voulus":True, "voulusse":True, "voulussent":True,
    "voulusses":True, "voulussiez":True, "voulussions":True, "voulut":True, "voulûmes":True,
    "voulût":True, "voulûtes":True, "vous":True, "vous-même":True, "vous-mêmes":True, "voyaient":True,
    "voyais":True, "voyait":True, "voyant":True, "voyez":True, "voyiez":True, "voyions":True,
    "voyons":True, "vraiment":True, "vroum":True, "vs":True, "vs.":True, "vu":True, "vue":True,
    "vîmes":True, "vînmes":True, "vînt":True, "vîntes":True, "vît":True, "vîtes":True, "vôtre":True,
    "vôtres":True, "wie":True, "wik":True, "y":True, "youpi":True, "zyva":True, "|":True, " !":True,
    " !!":True, " !!!":True, " !?":True, " !?!?":True, " :":True, " ;":True, " ?":True, " ?!":True,
    " ?!?!":True, " ???":True, " »":True, "«":True, "« ":True, "¯":True, "»":True, "à":True, "âge":True,
    "ça":True, "écrira":True, "écrirai":True, "écriraient":True, "écrirais":True, "écrirait":True,
    "écriras":True, "écrire":True, "écrirez":True, "écririez":True, "écririons":True, "écrirons":True,
    "écriront":True, "écris":True, "écrit":True, "écrivaient":True, "écrivais":True, "écrivait":True,
    "écrivant":True, "écrive":True, "écrivent":True, "écrives":True, "écrivez":True, "écriviez":True,
    "écrivions":True, "écrivirent":True, "écrivis":True, "écrivisse":True, "écrivissent":True,
    "écrivisses":True, "écrivissiez":True, "écrivissions":True, "écrivit":True, "écrivons":True,
    "écrivîmes":True, "écrivît":True, "écrivîtes":True, "égard":True, "époque":True, "étaient":True,
    "étais":True, "était":True, "étant":True, "étiez":True, "étions":True, "été":True, "êtes":True,
    "être":True}
    return stop

def accept(word, stop):
    if len(word) < 3: return False
    if word in stop: return False
    if re.match('^\s*$', word): return False
    if re.search('\W', word): return False
    if re.match('^\d+$', word) and len(word) != 4: return False
    return True

def clean(word):
    return re.sub('^\(', '\)$', re.sub("'", "_", word))

def truncate(s):
    s2 = re.sub('(e|s|t|nt|x)$', '', s)
    if s2 == s:
        return s
    else:
        return truncate(s2)


#######################
# KBWP UTILS FUNCTIONS
def selectTokens(tokenslist, stop=None):
    if stop == None:
        stop = getStopWords()
    return map(lambda t:re.sub(':', '', t), filter(lambda t: accept(t, stop), map(lambda t: truncate(clean(t.lower())), tokenslist)))


def getTokens2counts(tokenslist):
    d = defaultdict(integer)
    for t in tokenslist:
        d[t] += 1
    return d

def getWordsDBtables():
    return ['words2frequency', 'bow', 'swords']

def getKBDBtables():
    return ['main', 'bow', 'swords', 'categories', 'slugs',
            'iptc', 'related', 'peers', 'secpeers', 'secswords',
            'seccategories', 'secslugs', 'seciptc', 'mentions',
            'surrwords', 'countries', 'mentions2frequency', 'frequencies']


def normalizeTitle(title):
    return re.sub("&apos;", "'", re.sub('&quot;', '', re.sub('&amp;quot;', '&quot;', title)))
###########################

def computeCosSim(iter1, iter2):
    v1, v2 = iterables2vectors(iter1, iter2)
    sim = np.dot(v1, v2) / (np.sqrt(np.dot(v1, v1)) * np.sqrt(np.dot(v2, v2)))
    if math.isnan(sim): sim = 0.0
    return sim

def iterables2vectors(iter1, iter2):
    s1 = set(iter1)
    s2 = set(iter2)
    u = s1.union(s2)
    v1 = []
    v2 = []
    for x in u:
        if x in s1: v1.append(1.0)
        else: v1.append(0.0)
        if x in s2: v2.append(1.0)
        else: v2.append(0.0)
    return v1, v2
    


# TO BE REPLACED
def acceptWordInDB(word, charmin=None):
    if charmin and len(word) < charmin: return False
    if re.match('^\s+$', word): return False
    if word in [",", ".", ";", ":", "(", ")", "!", "?", "|", "%", "/", '"', "+", "-", "...", "=", "", "&quot;", "&apos;", "&amp;", "$"]:
        return False
    for x in [",", ".", ";", ":", "(", ")", "!", "?", "|", "%", "/", '"', "+", "-", "...", "=", "", "&quot;", "&apos;", "&amp;", "$"]:
        if x in word:
            return False
    return True

def fixStringForDB(arg):
    if isinstance(arg, str) or isinstance(arg, unicode):
        arg = re.sub("'", "_", arg)
    return arg
