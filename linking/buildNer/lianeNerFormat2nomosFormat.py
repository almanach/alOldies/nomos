#!/usr/bin/python
# -*- coding: utf-8 -*-
import codecs
import re
import os
import sys
from collections import defaultdict
from optparse import *
from xml.dom.minidom import *
from subprocess import *
sys.path.append('@common_src@')
from utils import *

def main():
    options, args = getOptions()
    try: os.mkdir(options.outputdir)
    except OSError: pass
    dom = parse(options.nefile)
    files2nes = defaultdict(dict)
    nes = filter(lambda ne:ne.getAttribute('conf')!='0.00', dom.getElementsByTagName('ne'))
    global token_id2token_content; token_id2token_content = defaultdict(dict)
    print '%d NEs in file'%(len(nes))
    if options.disamb:
        # SXPIPE NES SHOULD ALREADY BE DISAMBIGUATED
        # ONLY DISAMB LIANE NES, *BEFORE* DOING REMOVE DOUBLES!!
        nes = disamb(nes)
        print '%d NEs after disamb'%(len(nes))
    nes = removeDoubles(nes, dom, options.intersect)
    print '%d NEs after removing doubles'%(len(nes))
    origins = set([])
    # RECORD <NE> TAGS BY FILE, TOKENS, ORIGIN files2nes{file_id: {tokens: {origin: [<ne>, <ne>, ...]}}}
    for ne in nes:
        tokens = tuple(ne.getAttribute('tokens').split(' '))
        file_id = ne.getAttribute('file')
        if getNodeData(ne) == '': continue
        origin = ne.getAttribute('origin'); origins.add(origin)
        if not files2nes[file_id].has_key(tokens): files2nes[file_id][tokens] = {}
        if not files2nes[file_id][tokens].has_key(origin): files2nes[file_id][tokens][origin] = []
        files2nes[file_id][tokens][origin].append(ne)
        token_content = filter((lambda x: x != ''), getNodeData(ne).split(' '))
        i = 0
        for token in tokens:
            if not token_id2token_content[file_id].has_key(token):
                token_id2token_content[file_id][token] = token_content[i]
            i += 1
            
    for file_id, tokens2nes in files2nes.items():
        file_groups = defaultdict(set); i = 0
        file_tokens = map((lambda t: set(t)), tokens2nes.keys())
        i = 0
        # GROUP <NE> TOKENS BY INTERSECTION IN EACH FILE
        #file_tokens = [set([1,2]), set([2,3]), set([2,3,4]), set([3,4])]
        #file_groups = {i: set of tokens tuples}
        i = 0
        while(len(file_tokens) != 0):
            file_groups[i] = groupTokens(file_tokens)
            i += 1
        a = 0; c = 0

        global local_eid; local_eid = 0
        global alt_id; alt_id = 0
        
        # try: gold_dom = parse(options.golddir+'/'+file_id)
        # except: gold_dom = parse(options.golddir+'/'+convertAfpRef(file_id))
        try: gold_dom = parse(options.golddir+'/'+file_id+'.gold.xml')
        except IOError: continue
        new_dom = parseString(cleanGoldDom(gold_dom.toxml('utf-8')))
        # output = options.outputdir+'/'+re.sub('(\.gold)?\.xml', '.'+options.nerid+'.xml', convertAfpRef(file_id))
        output = options.outputdir+'/'+file_id+'.'+options.nerid+'.xml'
        G = codecs.open(output, 'w', 'utf-8')

        # FIND MIN AND MAX TOKENS IN EACH GROUP TO GET EXTEND OF ALTS
        for group_id, group in file_groups.items():
            new_group = []
            for tokens in group:
                sorted_tokens = sorted(list(tokens), lambda x,y:mysort(x,y))
                new_group.append(sorted_tokens)
            # LOOK FOR MIN AND MAX TOKEN IN WHOLE GROUP
            new_group_tokens = set([])
            for x in new_group:
                for y in x:
                    new_group_tokens.add(int(re.sub('^T', '', y)))
            min_token = min(new_group_tokens)
            max_token = max(new_group_tokens)            
            extend = range(min_token, max_token+1)
            prev_was_enamex = False
            # GET <NE> FOR CURRENT ALT
            nes = []
            for tokens in new_group:
                for o in origins:
                    try: nes.extend(files2nes[file_id][tuple(tokens)][o])
                    except KeyError: continue
            # IF DISAMB, CHECK FOR REMAINING AMBS IN LIANE NES
            # => TOO COMPLICATED IF MERGE; UNAMB NER IN MERGE WILL NEVER BE REALLY UNAMB FOR LIANE
            if options.disamb and origins == set(['liane']) and len(nes) > 1:
                lne = sorted(nes, key=lambda lne: float(lne.getAttribute('conf')), reverse=True)[0]
                nes = [lne]
            # IF ONLY ONE <NE> IN THIS GROUP, WRITE <ENAMEX>
            if len(nes) == 1: string = writeEnamex(nes[0])
            # ELSE WRITE <ALT>
            else: string = writeAlt(nes, extend, file_id)
            try: node = parseString(string.encode('utf-8')) # <ENAMEX> AND <ALT>
            except xml.parsers.expat.ExpatError: print >> sys.stderr, "!!!!!!!!!!!"; print >> sys.stderr, file_id; print >> sys.stderr, string; sys.exit()
            insertNode(node.documentElement, new_dom)

        print >> sys.stderr, "Writing %d ALTS\t%d ENAMEX\tin %s"%(alt_id, len(filter((lambda e: e.parentNode.tagName=='para'), new_dom.getElementsByTagName('ENAMEX'))), output)
        xmllint = Popen(['xmllint', '--format', '--encode', 'UTF-8', '-'], stdin=PIPE, stdout=PIPE)
        valid_output = xmllint.communicate(new_dom.toxml('utf-8'))[0]
        G.write(valid_output.decode('utf-8'))
        G.close()


def cleanGoldDom(gold_xml):
    gold_xml = re.sub('</?(ALT|CHOICE|ENAMEX|TIMEX|NUMEX|EMAIL|TEL|URL|ADDRESS|ADRESSE|ADRESSE|DIMENSION|EMAIL|ETR|NP|ROMNUM|SMILEY|SPECWORD)[^<>]*>', '', gold_xml)
    return gold_xml

def groupTokens(tokensetlist):
    group = set([])
    group.add(tuple(min(tokensetlist)))
    inter = filter(lambda tokenset: atLeastOneInter(tokenset, group), tokensetlist)
    while(len(inter) != 0):
        for twin_tokenset in inter:
            group.add(tuple(twin_tokenset))
            tokensetlist.remove(twin_tokenset)
        inter = filter(lambda tokenset: atLeastOneInter(tokenset, group), tokensetlist)
    return group

def atLeastOneInter(tokenset, tokensetlist):
    for t in tokensetlist:
        if len(set(t).intersection(tokenset)) > 0:
            return True
    return False


def writeAlt(nes, extend, file_id):
    global alt_id; alt_id += 1
    global local_eid
    global token_id2token_content
    string = '<ALT id="%d">\n'%(alt_id)
    choice_id = 0

    for ne in nes:
        local_eid += 1; choice_id += 1
        ne_token_ids = ne.getAttribute('tokens').split(' ')
        ne_token_content = filter((lambda x: x != ''), getNodeData(ne).split(' '))
        ids2tokens = dict(zip(ne_token_ids, ne_token_content))
        # ne_form = re.sub(' ', '_', re.sub('^ +', '', re.sub(' +$', '', getNodeData(ne))))
        ne_type = ne.getAttribute('type')
        ne_origin = ne.getAttribute('origin')
        if ne_origin == 'liane' and not re.search('_', ne_type):
            ne_type = lianeType2nomosType(ne_type)
        conf = getAttributeDefault(ne, 'conf', None)
        gender = getAttributeDefault(ne, 'gender', None)

        string += '<CHOICE id="%d" type="ENAMEX">\n'%(choice_id)
        seen_tokens = []
        for token_id in map((lambda t: 'T'+str(t)), extend):
            token_content = token_id2token_content[file_id][token_id]
            if token_id in ne_token_ids:
                # IF CURRENT TOKEN IN <NE>
                if token_id == ne_token_ids[0]:
                    # IF CURRENT TOKEN IS FIRST OF <NE>: OPEN <ENAMEX>, WRITE ALL ENAMEX TOKENS, FLAG THEM AS SEEN
                    string += '<ENAMEX local_eid="%d" type="%s"'%(local_eid, ne_type)
                    if gender != None: string += ' gender="%s"'%(gender)
                    if conf != None: string += ' conf="%s"'%(conf)
                    # string += ' origin="%s">\n<form value="%s">\n'%(ne_origin, ne_form)
                    string += ' origin="%s">\n'%(ne_origin)
                    for tokenid in sorted(ne_token_ids, lambda x,y:mysort(x,y)):
                        string += '<token id="%s">%s</token>\n'%(tokenid, ids2tokens[tokenid])
                        seen_tokens.append(tokenid)
                    # string += '</form>\n</ENAMEX>\n'
                    string += '</ENAMEX>\n'
                # IF CURRENT TOKEN IS IN <NE> BUT NOT FIRST, IT IS ALREADY WRITTEN (IN ENAMEX)
            # elif token_id not in seen_tokens:
            else:
                # IF CURRENT TOKEN IS NOT IN <NE>, WRITE IT IN A <FORM> => <TOKEN>
                # string += '<form value="%s"><token id="%s">%s</token></form>\n'%(token_content, token_id, token_content)
                string += '<token id="%s">%s</token>\n'%(token_id, token_content)
        string += '</CHOICE>\n'
    string += '</ALT>\n'
    return string

def noInter(tokens, tokenslist):
    nointer = []
    for t in tokenslist:
        if len(set(tokens).intersection(set(t))) == 0:
            nointer.append(t)
    return nointer

def insertNode(node, new_dom):
    node_first_token = int(re.sub('^T', '', node.getElementsByTagName('token')[0].getAttribute('id')))
    node_last_token = int(re.sub('^T', '', node.getElementsByTagName('token')[-1].getAttribute('id')))
    token_nodes2delete = filter((lambda t: int(re.sub('^T', '', t.getAttribute('id'))) in range(node_first_token, node_last_token+1)), new_dom.getElementsByTagName('token'))
    parent = token_nodes2delete[0].parentNode
    next_node = token_nodes2delete[-1].nextSibling
    if next_node != None:
        while next_node != None and isTextNode(next_node):
            if next_node.nextSibling != None:
                next_node = next_node.nextSibling
            else:
                next_node = None
    for token in token_nodes2delete:
        # form = token.parentNode
        # print 'Deleting token %s'%(token.getAttribute('id'))
        # form.removeChild(token); token.unlink()
        # try: parent.removeChild(form); print "removed form:",; print form.toxml()
        # except xml.dom.NotFoundErr: continue
        try: parent.removeChild(token)#; print "removed token:",; print token.toxml()
        except xml.dom.NotFoundErr: continue

    if next_node != None:
        # print 'Inserting node %d to %d in parent "%s" before next node %s (%s)'%(node_first_token, node_last_token, parent.tagName, next_node.toxml(), next_node.nodeType)
        try: parent.insertBefore(node, next_node)
        except xml.dom.NotFoundErr: print parent.toxml(); print node.toxml(); sys.exit()
    else:
        # print 'Inserting node %d to %d in parent "%s" at end of child nodes'%(node_first_token, node_last_token, parent.tagName)
        parent.appendChild(node)
    # stdin()

    # min_token = int(re.sub('^T', '', node.getElementsByTagName('token')[0].getAttribute('id')))
    # max_token = int(re.sub('^T', '', node.getElementsByTagName('token')[-1].getAttribute('id')))
    # try: first_token = filter((lambda t: t.getAttribute('id') == 'T'+str(min_token)), new_dom.getElementsByTagName('token'))[0]
    # except IndexError: sys.exit('!!!!!!!!!!! NE token not in Gold')
    # anchor_form = first_token.parentNode
    # anchor_form_tokens = map((lambda t: t.getAttribute('id')), anchor_form.getElementsByTagName('token'))
    # anchor_para = anchor_form.parentNode
    # if int(re.sub('^T', '', anchor_form_tokens[-1])) > max_token:
    #     node_tokens = map((lambda t: t.getAttribute('token')), node.getElementsByTagName('token'))
    #     for token in filter((lambda t: t in node_tokens), anchor_form_tokens):
    #         anchor_form.removeChild(filter((lambda t: t.getAttribute('id') == token), anchor_form_tokens)[0])
    #     anchor_para.insertBefore(node.documentElement, anchor_form)
    # else:    
    #     anchor_para.replaceChild(node.documentElement, anchor_form)
    # first_token_id = getFirstFormToken(anchor_form)
    # for form in filter((lambda f: getFirstFormToken(f) > min_token and getLastFormToken(f) <= max_token), new_dom.getElementsByTagName('form')):
    #     try: anchor_para.removeChild(form)
    #     except xml.dom.NotFoundErr: continue
        
        # REORDER ALTS AND ENAMEX IDS
        # a = 0
        # for alt in new_dom.getElementsByTagName('ALT'):
        #     a += 1
        #     alt.removeAttribute('id')
        #     addAttribute2Element(new_dom, alt, 'id', str(a))
        # e = 0
        # for enamex in new_dom.getElementsByTagName('ENAMEX'):
        #     e += 1
        #     enamex.removeAttribute('local_eid')
        #     addAttribute2Element(new_dom, enamex, 'local_eid', str(e))

def writeEnamex(ne):
    string = ""
    global local_eid
    ne_token_ids = ne.getAttribute('tokens').split(' ')
    ne_token_content = filter((lambda x: x != ''), getNodeData(ne).split(' '))
    ids2tokens = dict(zip(ne_token_ids, ne_token_content))
    # ne_form = re.sub(' ', '_', re.sub('^ +', '', re.sub(' +$', '', getNodeData(ne))))
    ne_type = ne.getAttribute('type')
    ne_origin = ne.getAttribute('origin')
    if ne_origin == 'liane' and not re.search('_', ne_type):
        ne_type = lianeType2nomosType(ne_type)
    conf = getAttributeDefault(ne, 'conf', None)
    gender = getAttributeDefault(ne, 'gender', None)
    local_eid += 1
    string += '<ENAMEX local_eid="%d" type="%s"'%(local_eid, ne_type)
    if gender != None: 
        string += ' gender="%s"'%(gender)
    if conf != None:
        string += ' conf="%s"'%(conf)
    # string += ' origin="%s">\n<form value="%s">\n'%(ne_origin, ne_form)
    string += ' origin="%s">\n'%(ne_origin)
    for tokenid in sorted(ne_token_ids, lambda x,y:mysort(x,y)):
        string += '<token id="%s">%s</token>\n'%(tokenid, ids2tokens[tokenid])
    # string += '</form>\n</ENAMEX>\n'
    string += '</ENAMEX>\n'
    return string

def mysort(x,y):
    if int(re.sub('^T', '', x)) > int(re.sub('^T', '', y)): return 1
    if int(re.sub('^T', '', x)) < int(re.sub('^T', '', y)): return -1
    return 0

# def getFirstFormToken(form_node):
#     return int(re.sub('^T', '', form_node.getElementsByTagName('token')[0].getAttribute('id')))

# def getLastFormToken(form_node):
#     return int(re.sub('^T', '', form_node.getElementsByTagName('token')[-1].getAttribute('id')))
                           
def lianeType2nomosType(string):
    d = {'LOC':'Location', 'ORG':'Organization', 'PERS':'Person', 'COM': 'Company', 'W':'Work', 'PRO':'Product'}
    # 'Location':'Location', 'Organization':'Organization', 'Person':'Person', 'Company': 'Company', 'Work':'Work', 'Product':'Product'}
    return d[string]

def disamb(nes):
    newnes = []; tokens2nes = defaultdict(dict)
    # SXPIPE <NES> SHOULD ALREADY BE DISAMBIGUATED
    newnes.extend(filter(lambda ne: ne.getAttribute('origin') == 'sxpipe', nes))
    for ne in filter(lambda n:n.getAttribute('origin')=='liane', nes):
        file_id = ne.getAttribute('file')
        tokens = tuple(ne.getAttribute('tokens').split(' '))
        try: tokens2nes[file_id][tokens].append(ne)
        except KeyError: tokens2nes[file_id][tokens] = [ne]
    for file_id in tokens2nes:
        for tokens, group in filter((lambda i: len(i[1]) > 1), tokens2nes[file_id].items()):
            # FOR LIANE <NES>, SORT BY CONF AND TAKE BEST CONF
            # THIS ONLY DISAMBIGUATES BETWEEN SAME SPAN MENTIONS
            liane_nes = filter(lambda ne: ne.getAttribute('origin') == 'liane', group)
            newnes.append(sorted(liane_nes, key=lambda ne: float(ne.getAttribute('conf')), reverse=True)[0])
        for tokens, group in filter((lambda i: len(i[1]) == 1), tokens2nes[file_id].items()):
            newnes.append(group[0])
    return newnes

def removeDoubles(nes, dom, intersect=False):
    tokens2nes = defaultdict(dict)
    newnes = []
    for ne in nes:
        file_id = ne.getAttribute('file')
        # CHECK IF M. or Dr PERSON NAME
        # <ne file="afp.com-20090521T121110ZTX-PAR-FYJ80.gold.xml" type="PERS" conf="0.67" tokens="T119 T120"> M. Biden </ne>
        # TODO: BETTER HANDLE THESE CASES - CAN CREATE A DOUBLE (PERSON) WHICH IS THEN REMOVED, SO INFO OF MUTLIPLE ORIGINS IS LOST
        if re.match('^\s*(M\.|Dr|Mme) +', getNodeData(ne)):
            # REMOVE FIRST TOKEN ID AND REMOVE M. FROM DATA
            new_ne = parseString(re.sub('="T\d+ ', '="', re.sub('> *(M\.|Dr|Mme) *', '>', ne.toxml('utf-8'))))
            ne = new_ne.documentElement
        tokens = tuple(ne.getAttribute('tokens').split(' '))
        try: tokens2nes[file_id][tokens].append(ne)
        except KeyError: tokens2nes[file_id][tokens] = [ne]
    for file_id in tokens2nes:
        for tokens, group in filter((lambda i: len(i[1]) == 1), tokens2nes[file_id].items()):
            # IF ONLY ONE <NE> WITH THESE TOKENS: APPEND IT, UNLESS INTERSECT OPTION
            if not intersect:
                newnes.append(group[0])
        for tokens, group in filter((lambda i: len(i[1]) > 1), tokens2nes[file_id].items()):
            origins = ';'.join(list(set(map((lambda ne: ne.getAttribute('origin')), group))))
            # IF ONLY ONE ORIGIN FOR THESE DIFFERENT <NE> AND INTERSECT OPTION, DONT APPEND IT
            if len(origins.split(';')) == 1 and intersect:
                continue
            types = list(set(map((lambda ne: getType(ne)), group)))
            valid = True
            for ne in group:
                valid = True
                netype = getType(ne)
                conf = getAttributeDefault(ne, 'conf', None)
                if conf != None:
                    try: types[types.index(netype)] = netype+'_'+conf
                    except ValueError: valid = False; continue
            if not valid: continue
            group[0].removeAttribute('origin')
            addAttribute2Element(dom, group[0], 'origin', origins)        
            group[0].removeAttribute('type')
            addAttribute2Element(dom, group[0], 'type', ';'.join(types))
            if 'Person' in types:
                gender = ';'.join(set(filter((lambda g: g != ''), map((lambda ne: ne.getAttribute('gender')), group))))
                try: group[0].removeAttribute('gender')
                except xml.dom.NotFoundErr: pass
                addAttribute2Element(dom, group[0], 'gender', gender)
            newnes.append(group[0])
    return newnes
            
def getType(ne):
    t = ne.getAttribute('type')
    if ne.getAttribute('origin') == 'liane': return lianeType2nomosType(t)
    return t

def getOptions():
    usage = '%s [options]\n'%sys.argv[0]
    parser = OptionParser(usage)
    parser.add_option('--file', action='store', dest='nefile', default=None, help='NE file')
    parser.add_option('--golddir', action='store', dest='golddir', default=None, help='Gold directory')
    parser.add_option('--outputdir', action='store', dest='outputdir', default=None, help='Output dir')
    parser.add_option('--nerid', action='store', dest='nerid', default=None, help='NER ID')
    parser.add_option('--intersect', action='store_true', dest='intersect', default=False, help='With NER=liane_sxpipe, write only intersection')
    # DISAMB IS FOR LIANE <NES>; SXPIPE <NES> ALREADY DISAMBIGUATED WHEN PRODUCING FILE <NES>
    parser.add_option('--disamb', action='store_true', dest='disamb', default=False, help='With NER=liane, write only 1best')
    try: (options, args) = parser.parse_args()
    except OptionError: sys.stderr.write('OptionError'); parser.print_help(); sys.exit()
    except: sys.stderr.write('An error occured during options parsing\n'); parser.print_help(); sys.exit()
    if options.nefile == None: parser.print_help(); sys.exit()
    if options.golddir == None: parser.print_help(); sys.exit()
    # if options.intersect and options.nerid != "liane_sxpipe": print >> sys.stderr, 'Incompatible NER "%s" and intersect option'%(options.nerid); parser.print_help(); sys.exit()
    return options, args 

def convertAfpRef(f):
    # urn:newsml:afp.com:20090520T164214Z:TX-PAR-FVU98:1
    #                      urn:newsml:afp.com:20130103T112922Z:TX-PAR-IBN39:1
    fpattern = re.compile('^urn:newsml:afp\.com:(\d{8}T\d{6}Z):TX-PAR-([A-Z]{3}\d{2}):\d$')
    datetime, refid = fpattern.search(f).groups()
    # afp.com-20090520T164214ZTX-PAR-FVU98.gold.xml
    return "afp.com-"+datetime+"TX-PAR-"+refid+".nomos.xml"

        
if __name__ == '__main__':
    main()

