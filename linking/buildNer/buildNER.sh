#!/bin/bash
#==========================================================================================
# ACTIONS AND ARGS

golddir=""
newsinputdir=""
lianenesfile=""
nerdir="./nerdir"
installscripts="/usr/local/share/nomos/build/linking/resolution"
localscripts="/Users/triantafillo/dev/alpc/nomos/linking/resolution"

echousage(){
echo "Options and arguments: 
--golddir <golddir>
--newsinputdir <newsinputdir>
--lianenesfile <lianenesfile>
--nerdir <nerdir> (default ./nerdir/)
--installscripts <install scripts path> (default /usr/local/share/nomos/build/linking/resolution)
--localscripts <local scripts path> (default /Users/triantafillo/dev/alpc/nomos/linking/resolution)
"
}

echoargs(){
echo "Options and arguments: 
--golddir $golddir
--newsinputdir $newsinputdir
--lianenesfile $lianenesfile
--nerdir $nerdir
--installscripts $installscripts
--localscripts $localscripts
"
}


if [ "$1" = "" ] || [ "$1" = "--help" ] || [ "$1" = "-help" ] || [ "$1" = "-h" ]; then 
    echousage
    exit
fi

# GET OPTIONS
while [ "$1" != "" ]
do
    case $1 in
	--golddir )
	    shift
	    golddir=$1
	    ;;
	--newsinputdir )
	    shift
	    newsinputdir=$1
	    ;;
	--lianenesfile )
	    shift
	    lianenesfile=$1
	    ;;
	--nerdir )
	    shift
	    nerdir=$1
	    ;;
    esac
    shift
done

mkdir -v $nerdir
mkdir -v $nerdir/nomosDocuments

########################################################################################
# 1 # GOLD NER
########################################################################################
echo "===== GOLD NER ====="
# echo "Conversion of gold dir $golddir to [Gold NER] files in $nerdir/goldNerParas/"
# mkdir -v $nerdir/goldNerParas/
# for gold in $golddir/*.xml
# do
#     filename=$(basename $gold)
#     output=$nerdir/goldNerParas/$filename
#     output=${output/.gold.xml/.goldner.xml}
#     echo "Running xsltproc $installscripts/gold2goldner.xsl $file > $output ..."
#     xsltproc $installscripts/gold2goldner.xsl $gold > $output
# done
# echo "Done."
echo "Building Nomos documents from Gold dir $golddir/*.xml to $nerdir/nomosDocuments/goldNerNomos/"
mkdir -v $nerdir/nomosDocuments/goldNerNomos/
read
for goldner in $golddir/*.xml
do
    filename=$(basename $goldner)
    news=${filename/.gold.xml/.xml}
    news=$newsinputdir/$news
    output=$nerdir/nomosDocuments/goldNerNomos/$filename
    output=${output/.goldner.xml/.nomos.goldner.xml}
    echo "Running python $installscripts/buildNomos.py --goldner $file --nerid goldner $news > $output ..."
    #python $installscripts/buildNomos.py --goldner $goldner --nerid goldner $news > $output
done
echo "Done."
read
########################################################################################
# 2 # SxPipe NER Ambiguous
########################################################################################
echo "===== SXPIPE AMBIGUOUS NER ====="
echo "Annotation of news files in $newsinputdir with sxpipe-afp-nomos (ner) written to $nerdir/nomosDocuments/sxpipeAmbNerNomos/"
mkdir -v $nerdir/nomosDocuments/sxpipeAmbNerNomos/
read
for news in $newsinputdir/*.xml
do
    filename=$(basename $news)
    output=$nerdir/nomosDocuments/sxpipeAmbNerNomos/$filename
    output=${output/.xml/.nomos.sxpipener.xml}
    echo "Running python $installscripts/buildNomos.py $news > $output ..."
    # python $installscripts/buildNomos.py --nerid sxpipener $news > $output
done
echo "Done."
read

########################################################################################
# 3 # SxPipe NER Disambiguated
########################################################################################
echo "===== SXPIPE DISAMBIGUATED NER ====="
echo "Annotation of news files in $newsinputdir with sxpipe-afp-nomos (ner) + np_normalizer (disamb) written to $nerdir/nomosDocuments/sxpipeDsbNerNomos/"
read
mkdir -v $nerdir/nomosDocuments/sxpipeDsbNerNomos/
for news in $newsinputdir/*.xml
do
    filename=$(basename $news)
    output=$nerdir/nomosDocuments/sxpipeDsbNerNomos/$filename
    output=${output/.xml/.nomos.sxpipenerdsb.xml}
    echo "Running python $installscripts/buildNomos.py --disamb $news > $output ..."
    # python $installscripts/buildNomos.py --disamb --nerid sxpipener_dsb $news > $output
done
echo "Done."
read

########################################################################################
# 4 # Liane NER n-best
########################################################################################
echo "===== LIANE N-BEST NER ====="
echo "Conversion of Liane <nes> $lianenesfile to [Liane n-best NER] files in $nerdir/lianeNbestNerParas/"
mkdir -v $nerdir/lianeNbestNerParas/
echo "Running python $localscripts/lianeNerFormat2nomosFormat.py --golddir $golddir --file $lianenesfile --outputdir $nerdir/lianeNbestNerParas/ --nerid lianener ..."
read
python $localscripts/lianeNerFormat2nomosFormat.py --golddir $golddir --file $lianenesfile --outputdir $nerdir/lianeNbestNerParas/ --nerid lianener
echo "Done."
echo "Building Nomos documents from $nerdir/lianeNbestNerParas/*.xml to $nerdir/nomosDocuments/lianeNbestNerNomos/"
mkdir -v $nerdir/nomosDocuments/lianeNbestNerNomos/
read
for lianener in $nerdir/lianeNbestNerParas/*.xml
do
    filename=$(basename $lianener)
    news=${filename/.lianener.xml/.xml}
    news=$newsinputdir/$news
    output=$nerdir/nomosDocuments/lianeNbestNerNomos/$filename
    output=${output/.lianener.xml/.nomos.lianener.xml}
    echo "Running python $installscripts/buildNomos.py --nosxpipener --lianener $lianener --nerid lianener $news > $output ..."
    python $installscripts/buildNomos.py --nosxpipener --lianener $lianener --nerid lianener $news > $output
done
echo "Done."
read

########################################################################################
# 5 # Liane NER 1-best
########################################################################################
echo "===== LIANE 1-BEST NER ====="
echo "Conversion of Liane <nes> $lianenesfile to [Liane 1-best NER] files in $nerdir/lianeNer1bestParas/"
mkdir -v $nerdir/lianeNer1bestParas/
echo "Running python $localscripts/lianeNerFormat2nomosFormat.py --golddir $golddir --file $lianenesfile --disamb --outputdir $nerdir/lianeNer1bestParas/ --nerid lianener_dsb ..."
read
python $localscripts/lianeNerFormat2nomosFormat.py --golddir $golddir --file $lianenesfile --disamb --outputdir $nerdir/lianeNer1bestParas/ --nerid lianener_1best
echo "Done."
echo "Building Nomos documents from $nerdir/lianeNer1bestParas/*.xml to $nerdir/nomosDocuments/lianeNer1bestNomos/"
mkdir -v $nerdir/nomosDocuments/lianeNer1bestNomos/
read
for lianener in $nerdir/lianeNer1bestParas/*.xml
do
    filename=$(basename $lianener)
    news=${filename/.lianener_1best.xml/.xml}
    news=$newsinputdir/$news
    output=$nerdir/nomosDocuments/lianeNer1bestNomos/$filename
    output=${output/.lianener_1best.xml/.nomos.lianener_1best.xml}
    echo "Running python $installscripts/buildNomos.py --nosxpipener --lianener $lianener --nerid lianener_1best $news > $output ..."
    python $installscripts/buildNomos.py --nosxpipener --lianener $lianener --nerid lianener_1best $news > $output
done
echo "Done."
read

########################################################################################
# 6 # Sxpipe NER Ambiguous + Liane NER n-best
########################################################################################
echo "===== SXPIPE AMBIGUOUS NER + LIANE N-BEST NER (UNION) ====="
sxpipenesfile=$nerdir/sxpipe-nes-amb.xml
lianesxpipenesfileamb=$nerdir/liane-sxpipe-nes-amb.xml
echo "Running python $localscripts/news2sxpipeNerFormat.py --newsdir $newsinputdir/ --output $sxpipenesfile --nerorigin sxpipe ..."
read
python $localscripts/news2sxpipeNerFormat.py --newsdir $newsinputdir/ --output $sxpipenesfile --nerorigin sxpipe
echo "Done."
read
echo "Merge $lianenesfile and $sxpipenesfile into $lianesxpipenesfileamb"
echo "<nes>
" > $lianesxpipenesfileamb
grep '<ne ' $lianenesfile >> $lianesxpipenesfileamb
grep '<ne ' $sxpipenesfile >> $lianesxpipenesfileamb
echo "</nes>
" >> $lianesxpipenesfileamb
read
echo "Conversion of Liane and SxPipe <nes> $lianesxpipenesfileamb to [Liane n-best + SxPipe Ambiguous NER] files in $nerdir/lianeNbestSxpipeAmbNerParas/"
mkdir -v $nerdir/lianeNbestSxpipeAmbNerParas/
echo "Running python $localscripts/lianeNerFormat2nomosFormat.py --golddir $golddir --file $lianesxpipenesfileamb --outputdir $nerdir/lianeNbestSxpipeAmbNerParas/ --nerid lianener_sxpipener_amb ..."
python $localscripts/lianeNerFormat2nomosFormat.py --golddir $golddir --file $lianesxpipenesfileamb --outputdir $nerdir/lianeNbestSxpipeAmbNerParas/ --nerid lianener_sxpipener_amb
echo "Done."
read
echo "Building Nomos documents from $nerdir/lianeNbestSxpipeAmbNerParas/ to $nerdir/nomosDocuments/lianeNbestSxpipeAmbNerNomos/"
mkdir -v $nerdir/nomosDocuments/lianeNbestSxpipeAmbNerNomos/
read
for lianesxpipener in $nerdir/lianeNbestSxpipeAmbNerParas/*.xml
do
    filename=$(basename $lianesxpipener)
    news=${filename/.lianener_sxpipener_amb.xml/.xml}
    news=$newsinputdir/$news
    output=$nerdir/nomosDocuments/lianeNbestSxpipeAmbNerNomos/$filename
    output=${output/.lianener_sxpipener_amb.xml/.nomos.lianener_sxpipener_amb.xml}
    echo "Running python $installscripts/buildNomos.py --mergener $lianesxpipener --nerid lianener_sxpipener_amb $news > $output ..."
    python $installscripts/buildNomos.py --mergener $lianesxpipener --nerid lianener_sxpipener_amb $news > $output
done
echo "Done."
read

########################################################################################
# 7 # Sxpipe NER Ambiguous + Liane NER n-best Intersection
########################################################################################
echo "===== SXPIPE AMBIGUOUS NER + LIANE N-BEST NER (INTERSECTION) ====="
echo "Conversion of Liane and SxPipe <nes> $lianesxpipenesfileamb to [Liane n-best + SxPipe Ambiguous NER Intersection] files in $nerdir/lianeNbestSxpipeAmbNerParas/"
mkdir -v $nerdir/lianeNbestSxpipeAmbInterNerParas/
echo "Running python $localscripts/lianeNerFormat2nomosFormat.py --golddir $golddir --file $lianesxpipenesfileamb --outputdir $nerdir/lianeNbestSxpipeAmbNerParas/ --intersect --nerid lianener_sxpipener_amb_inter ..."
read
python $localscripts/lianeNerFormat2nomosFormat.py --golddir $golddir --file $lianesxpipenesfileamb --outputdir $nerdir/lianeNbestSxpipeAmbInterNerParas/ --intersect --nerid lianener_sxpipener_amb_inter
echo "Done."
read
echo "Building Nomos documents from $nerdir/lianeNbestSxpipeAmbInterNerParas/ to $nerdir/nomosDocuments/lianeNbestSxpipeAmbInterNerNomos/"
mkdir -v $nerdir/nomosDocuments/lianeNbestSxpipeAmbInterNerNomos/
read
for lianesxpipener in $nerdir/lianeNbestSxpipeAmbInterNerParas/*.xml
do
    filename=$(basename $lianesxpipener)
    news=${filename/.lianener_sxpipener_amb_inter.xml/.xml}
    news=$newsinputdir/$news
    output=$nerdir/nomosDocuments/lianeNbestSxpipeAmbInterNerNomos/$filename
    output=${output/.lianener_sxpipener_amb_inter.xml/.nomos.lianener_sxpipener_amb_inter.xml}
    echo "Running python $installscripts/buildNomos.py --mergener $lianesxpipener --nerid lianener_sxpipener_amb_inter $news > $output ..."
    python $installscripts/buildNomos.py --mergener $lianesxpipener --nerid lianener_sxpipener_amb_inter $news > $output
done
echo "Done."
read

########################################################################################
# 8 # Sxpipe NER Disambiguated + Liane NER 1-best
########################################################################################
echo "===== SXPIPE DISAMBIGUATED NER + LIANE 1-BEST NER (UNION) ====="
sxpipenesfile=$nerdir/sxpipe-nes-dsb.xml
lianesxpipenesfiledsb=$nerdir/liane-sxpipe-nes-dsb.xml
echo "Running python $localscripts/news2sxpipeNerFormat.py --newsdir $newsinputdir/ --disamb --output $sxpipenesfile --nerorigin sxpipe ..."
read
python $localscripts/news2sxpipeNerFormat.py --newsdir $newsinputdir/ --disamb --output $sxpipenesfile --nerorigin sxpipe
echo "Done."
read
echo "Merge $lianenesfile and $sxpipenesfile into $lianesxpipenesfiledsb"
echo "<nes>
" > $lianesxpipenesfiledsb
grep '<ne ' $lianenesfile >> $lianesxpipenesfiledsb
grep '<ne ' $sxpipenesfile >> $lianesxpipenesfiledsb
echo "</nes>
" >> $lianesxpipenesfiledsb
read
echo "Conversion of Liane and SxPipe <nes> $lianesxpipenesfiledsb to [Liane 1-best + SxPipe Disambiguated NER] files in $nerdir/liane1bestSxpipeDsbNerParas/"
mkdir -v $nerdir/liane1bestSxpipeDsbNerParas/
echo "Running python $localscripts/lianeNerFormat2nomosFormat.py --golddir $golddir --file $lianesxpipenesfiledsb --disamb --outputdir $nerdir/liane1bestSxpipeDsbNerParas/ --nerid lianener_sxpipener_dsb ..."
python $localscripts/lianeNerFormat2nomosFormat.py --golddir $golddir --file $lianesxpipenesfiledsb --disamb --outputdir $nerdir/liane1bestSxpipeDsbNerParas/ --nerid lianener_sxpipener_dsb
echo "Done."
echo "Building Nomos documents from $nerdir/liane1bestSxpipeDsbNerParas/ to $nerdir/nomosDocuments/liane1bestSxpipeDsbNerNomos/"
mkdir -v $nerdir/nomosDocuments/liane1bestSxpipeDsbNerNomos/
for lianesxpipener in $nerdir/liane1bestSxpipeDsbNerParas/*.xml
do
    filename=$(basename $lianesxpipener)
    news=${filename/.lianener_sxpipener_dsb.xml/.xml}
    news=$newsinputdir/$news
    output=$nerdir/nomosDocuments/liane1bestSxpipeDsbNerNomos/$filename
    output=${output/.lianener_sxpipener_dsb.xml/.nomos.lianener_sxpipener_dsb.xml}
    echo "Running python $installscripts/buildNomos.py --mergener $lianesxpipener --nerid lianener_sxpipener_dsb $news > $output ..."
    python $installscripts/buildNomos.py --mergener $lianesxpipener --nerid lianener_sxpipener_dsb $news > $output
done
echo "Done."
read

########################################################################################
# 9 # Sxpipe NER Disambiguated + Liane NER 1-best Intersection
########################################################################################
echo "===== SXPIPE DISAMBIGUATED NER + LIANE 1-BEST NER (INTERSECTION) ====="
echo "Conversion of Liane and SxPipe <nes> $lianesxpipenesfiledsb to [Liane 1-best + SxPipe Disambiguated NER Intersection] files in $nerdir/liane1bestSxpipeDsbInterNerParas/"
mkdir -v $nerdir/liane1bestSxpipeDsbInterNerParas/
echo "Running python $localscripts/lianeNerFormat2nomosFormat.py --golddir $golddir --file $lianesxpipenesfiledsb --outputdir $nerdir/liane1bestSxpipeDsbInterNerParas/ --intersect --nerid lianener_sxpipener_dsb_inter ..."
read
python $localscripts/lianeNerFormat2nomosFormat.py --golddir $golddir --file $lianesxpipenesfiledsb --outputdir $nerdir/liane1bestSxpipeDsbInterNerParas/ --intersect --nerid lianener_sxpipener_dsb_inter
echo "Done."
read
echo "Building Nomos documents from $nerdir/liane1bestSxpipeDsbInterNerParas/ to $nerdir/nomosDocuments/liane1bestSxpipeDsbInterNerNomos/"
mkdir -v $nerdir/nomosDocuments/liane1bestSxpipeDsbInterNerNomos/
read
for lianesxpipener in $nerdir/liane1bestSxpipeDsbInterNerParas/*.xml
do
    filename=$(basename $lianesxpipener)
    news=${filename/.lianener_sxpipener_dsb_inter.xml/.xml}
    news=$newsinputdir/$news
    output=$nerdir/nomosDocuments/liane1bestSxpipeDsbInterNerNomos/$filename
    output=${output/.lianener_sxpipener_dsb_inter.xml/.nomos.lianener_sxpipener_dsb_inter.xml}
    echo "Running python $installscripts/buildNomos.py --mergener $lianesxpipener --nerid lianener_sxpipener_dsb_inter $news > $output ..."
    python $installscripts/buildNomos.py --mergener $lianesxpipener --nerid lianener_sxpipener_dsb_inter $news > $output
done
echo "Done."

