<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="xml"/>
    <xsl:template match="paras">
        <paras>
            <xsl:apply-templates select="para"/>
        </paras>
    </xsl:template>
    <xsl:template match="para">
        <para>
            <xsl:attribute name="rank"><xsl:value-of select="./@rank"/></xsl:attribute>
            <xsl:apply-templates/>
        </para>
    </xsl:template>
    <!--<xsl:template match="para/form|para/TIMEX|para/NUMEX|para/URL|para/EMAIL|para/TEL|para/ADDRESS|para/ADRESSE|para/HEURE">-->
    <!--<xsl:template match="para/form">
        <form>
            <xsl:attribute name="value"><xsl:value-of select="./@value"/></xsl:attribute>
            <xsl:apply-templates/>
        </form>
        </xsl:template>-->
    <xsl:template match="token">
        <token>
            <xsl:attribute name="id"><xsl:value-of select="./@id"/></xsl:attribute>
            <xsl:value-of select="."/>
        </token>
    </xsl:template>
    <!--<xsl:template match="form">
        <form>
            <xsl:attribute name="value"><xsl:value-of select="./@value"/></xsl:attribute>
            <xsl:apply-templates/>
        </form>
    </xsl:template>-->
    <!--<xsl:template match="para/TIMEX|para/NUMEX|para/URL|para/EMAIL|para/TEL|para/ADDRESS|para/ADRESSE|para/HEURE|para/SMILEY">-->
        <!--<xsl:apply-templates select="./form"></xsl:apply-templates>-->
        <!--<xsl:apply-templates select="./token"></xsl:apply-templates>
    </xsl:template>-->
    <xsl:template match="para//ENAMEX">
      <!--<xsl:variable name="forms_value">
        <xsl:for-each select="./form">
            <xsl:value-of select="./@value" />
            <xsl:if test="position() != last()">_</xsl:if>
        </xsl:for-each>
      </xsl:variable>-->        
        <ALT>
            <xsl:attribute name="id"><xsl:value-of select="./@local_eid"/></xsl:attribute>
            <CHOICE>
                <xsl:attribute name="id"><xsl:value-of select="./@local_eid"/></xsl:attribute>
                <xsl:attribute name="type">ENAMEX</xsl:attribute>
                <ENAMEX>
                    <xsl:attribute name="local_eid"><xsl:value-of select="./@local_eid"/></xsl:attribute>
                    <xsl:attribute name="type"><xsl:value-of select="./@type"/></xsl:attribute>
                    <xsl:if test="./@type2"><xsl:attribute name="type2"><xsl:value-of select="./@type2"/></xsl:attribute></xsl:if>
                    <xsl:if test="./@gender"><xsl:attribute name="gender"><xsl:value-of select="./@gender"/></xsl:attribute></xsl:if>
                    <!--<form>
                        <xsl:attribute name="value"><xsl:value-of select="$forms_value"/></xsl:attribute>
                        <xsl:apply-templates select=".//token"></xsl:apply-templates>
                        </form>-->
                    <xsl:apply-templates select=".//token"></xsl:apply-templates>
                </ENAMEX>
            </CHOICE>
        </ALT>
    </xsl:template>
</xsl:stylesheet>