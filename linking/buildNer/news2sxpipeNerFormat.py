#!/usr/bin/python
# -*- coding: utf-8 -*-
import codecs
import re
import os
import sys
from collections import defaultdict
from optparse import *
from xml.dom.minidom import *
from subprocess import *
sys.path.append('@common_src@')
from utils import *


def main():
    options, args = getOptions()
    G = codecs.open(options.outputfile, 'w', 'utf-8')
    G.write('<nes>\n')
    # for root, dirs, files in os.walk(options.newsdir):
    #     for file in files:
    F = codecs.open(options.newsfile, 'r', 'utf-8')
    docidpattern = re.compile('ref="([^"]+)"')
    initem = False
    for line in F:
        if re.search('<news_item ', line):
            file_id = docidpattern.search(line).group(1)
            # filename = os.path.join(root, file)
            # if not filename.endswith('.xml'): continue
            # file_id = re.sub('\.xml', '.gold.xml', file)
            string = line
            initem = True
        elif re.search('</news_item>', line) and initem:
            # print >> sys.stderr, 'Parse %s'%(filename)
            # dom = parse(filename)
            string += line
            dom = parseString(string.encode('utf-8'))
            sxpipe_ner_dom = getSxpipeNerDom('\n'.join(filter((lambda p: isValid(p)), map((lambda p: p.toxml()), dom.getElementsByTagName('para')))), options.disamb)
            dom.unlink()
            nes = convert2nes(sxpipe_ner_dom, file_id, options.nerorigin)
            G.write('\n'.join(nes))
            G.write('\n')
            initem = False
        elif initem:
            string += line
    G.write('\n</nes>\n')
    F.close()
    G.close()

def convert2nes(dom, file_id, nerorigin):
    # <ne file="afp.com-20090520T220037ZTX-PAR-FWV92.gold.xml" type="LOC" conf="0.11" tokens="T160 T161 T162"> collège de Cannes </ne>
    enamex_nodes = dom.getElementsByTagName('ENAMEX')
    nes = []
    for e in enamex_nodes:
        gender = getAttributeDefault(e, 'gender', None)
        if gender != None: gattr = ' gender="%s"'%(gender)
        else: gattr = ""
        tokensids = ' '.join(map((lambda t: t.getAttribute('id')), e.getElementsByTagName('token')))
        tokensdata = ' '.join(map((lambda t: getNodeData(t)), e.getElementsByTagName('token')))
        # TYPE: REMOVE TRAILING NUMBER IN TYPE (PERSON2)
        nes.append('<ne file="%s" type="%s"%s tokens="%s" origin="%s">%s</ne>'%(file_id, re.sub('_?\d+$', '', e.getAttribute('type')), gattr, tokensids, nerorigin, tokensdata))
    return nes

def isValid(para):
    signature_pattern = re.compile("^\s*<p>\s*[a-zé&A-Z]{2,4}((-|/)[a-zé&A-Z]{2,4}/?)+( +[a-z\.]{1,7})?\s*</p>\s*$")
    if signature_pattern.search(para):
        return False
    return True

def getSxpipeNerDom(paras, disamb=False):
    format_text = Popen(['xml2sxpipe'], stdin=PIPE, stdout=PIPE)
    formatted_text = format_text.communicate(paras.encode('utf-8'))[0]
    sxpipe = Popen(['sxpipe-afp-nomos'], stdin=PIPE, stdout=PIPE)    
    sxpipe_ner_dags = sxpipe.communicate(formatted_text)[0]
    if disamb:
        np_normalizer = Popen(['@sxpipebin@/np_normalizer.pl', '-d', '@aledalibdir@', '-l', 'fr'], stdin=PIPE, stdout=PIPE)
        sxpipe_ner_dags = np_normalizer.communicate(sxpipe_ner_dags)[0]
        dag2ddag = Popen(['dag2ddag'], stdin=PIPE, stdout=PIPE)
        sxpipe_ner_dags = dag2ddag.communicate(sxpipe_ner_dags)[0]
    if not disamb:
        dag2pdag = Popen(['dag2pdag'], stdin=PIPE, stdout=PIPE)
        sxpipe_ner_pdags = dag2pdag.communicate(sxpipe_ner_dags)[0]
        # sxpipe2xml = Popen(['sxpipe2xml', '--pdag', '--t', '--form', '--sent'], stdin=PIPE, stdout=PIPE)
        sxpipe2xml = Popen(['sxpipe2xml', '--pdag', '--t'], stdin=PIPE, stdout=PIPE)
        output = sxpipe2xml.communicate(sxpipe_ner_pdags)[0]
    else:
        # sxpipe2xml = Popen(['sxpipe2xml', '--t', '--form', '--sent'], stdin=PIPE, stdout=PIPE)
        sxpipe2xml = Popen(['sxpipe2xml', '--t'], stdin=PIPE, stdout=PIPE)
        output = sxpipe2xml.communicate(sxpipe_ner_dags)[0]
    sxpipe_ner_dom = orderTokens(parseString('<root>'+output+'</root>'))
    if disamb:
        sxpipe_ner_dom = buildAlts(sxpipe_ner_dom)
    return sxpipe_ner_dom

def orderTokens(sxpipe_ner_dom):
    # PUT BACK NICE AND ORDERED TOKEN IDS
    seen_tokens = {}
    for token in sxpipe_ner_dom.getElementsByTagName('token'):
        token_id = token.getAttribute('id')
        if not seen_tokens.has_key(token_id):
            token_count = len(seen_tokens.keys())+1
            seen_tokens[token_id] = token_count
        else:
            token_count = seen_tokens[token_id]
        token.setAttribute('id', 'T'+str(token_count))
    return sxpipe_ner_dom

def buildAlts(dom):
    altid = 0
    enamex_id = 0
    for enamex in dom.getElementsByTagName('ENAMEX'):
        parent = enamex.parentNode
        altid += 1
        alt = createElementNode(dom, 'ALT', {'id':str(altid)})
        choice = createElementNode(dom, 'CHOICE')
        enamex_child = enamex.cloneNode(True)
        choice.appendChild(enamex_child)
        alt.appendChild(choice)
        parent.replaceChild(alt, enamex)
    return dom
        

def getOptions():
    usage = '%s [options]\n'%sys.argv[0]
    parser = OptionParser(usage)
    # parser.add_option('--newsdir', action='store', dest='newsdir', default=None, help='News input dir')
    parser.add_option('--newsfile', action='store', dest='newsfile', default=None, help='News input file')
    parser.add_option('--output', '-o', action='store', dest='outputfile', default=None, help='Output file for <nes>')
    parser.add_option('--nerorigin', action='store', dest='nerorigin', default="sxpipe", help='NER ID')
    parser.add_option('--disamb', action='store_true', dest='disamb', default=False, help='Disambiguate NER')
    try: (options, args) = parser.parse_args()
    except OptionError: sys.stderr.write('OptionError'); parser.print_help(); sys.exit()
    except: sys.stderr.write('An error occured during options parsing\n'); parser.print_help(); sys.exit()
    if options.newsfile == None: parser.print_help(); sys.exit()
    if options.outputfile == None or options.nerorigin == None: parser.print_help(); sys.exit()
    return options, args 

if __name__ == '__main__':
    main()
