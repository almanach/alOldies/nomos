<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:output indent="yes" method="html" encoding="utf-8"
    doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"
    doctype-public="-//W3C//DTD XHTML 1.1//EN"/>

    <xsl:template match="/child::*">
        <html>
          <head>
            <title> Nomos Resolution </title>
          </head>
          <style type="text/css">
            span.enamex_token{color:#15317E}            
          </style>
          <body>
            <xsl:apply-templates select="./module[@type='resolution']"/>    
          </body>          
        </html>                
    </xsl:template>
    
    <xsl:template match="module[@type='resolution']">
        <h3><xsl:value-of select=".//news_head/@text"/></h3>
        <xsl:apply-templates select=".//news_text"/>
    </xsl:template>
    
    <xsl:template match="module[@type='resolution']//news_text">
        <xsl:apply-templates select="./para"/>
    </xsl:template>
    
    <xsl:template match="module[@type='resolution']//news_text/para">
        <p><xsl:apply-templates select=".//token"/></p>
    </xsl:template>
    
    <xsl:template match="module[@type='resolution']//ENAMEX//token">
        <span class="enamex_token"><xsl:value-of select="concat(., ' ')"/></span>
    </xsl:template>
    
    <xsl:template match="module[@type='resolution']//para/form/token">
        <span class="token"><xsl:value-of select="concat(., ' ')"/></span>
    </xsl:template>
      
</xsl:stylesheet>
