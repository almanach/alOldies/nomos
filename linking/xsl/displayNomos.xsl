<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Nov 19, 2011</xd:p>
            <xd:p><xd:b>Author:</xd:b> stern</xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:output encoding="UTF-8" indent="yes" method="html"/>
    <xsl:template match="/child::*">
        <html>
            <head>
                <meta content="text/html; charset=utf-8"/>
                <title><xsl:value-of select="./module[@type='news_analytics']/meta/@doc_id"/></title>
                <style type="text/css">
                    body{font-family:Arial}
                    table.zone{border-collapse:collapse}
                    tr.first_rank td{background-color:#66CCCC}
                    tr.not_first_rank td{background-color:#E8E8E8}
                    td.invalid{font:italic;font-size:small}
                    td.linking_type{font-variant:small-caps}
                    td.token{font:Verdana}
                    a.candidate{font-size:small}
                    a.gold_candidate{font-size:small;color:maroon}
                </style>

            </head>
            <body>
                <h3>News Analytics</h3>
                <xsl:apply-templates select="./module[@type='news_analytics']"/>  
                <h3>Linking</h3>
                <xsl:apply-templates select="./module[@type='resolution']"/>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="module[@type='news_analytics']">
        <xsl:apply-templates select="./context"/>
        <xsl:apply-templates select="./category"/>
        <xsl:apply-templates select="./salient_words"/>
    </xsl:template>
    <xsl:template match="module[@type='news_analytics']/context">
        <div><xsl:value-of select="concat(./spatial_context/@city, ' (', ./spatial_context/@country, ') @', ./temporal_context/@date, '/', ./temporal_context/@time)"/></div>
    </xsl:template>
    <xsl:template match="module[@type='news_analytics']/category">
        <xsl:apply-templates select="./iptc"/>
        <xsl:apply-templates select="./domain"/>
        <xsl:apply-templates select="./keyword"/>
    </xsl:template>
    <xsl:template match="salient_words">
        <xsl:apply-templates select="./salient_word"></xsl:apply-templates>
    </xsl:template>
    <xsl:template match="iptc|domain|keyword|salient_word">
        <div><xsl:value-of select="concat(local-name(), ': ', ./@value)"/></div>
    </xsl:template>
    <xsl:template match="module[@type='resolution']">
        <xsl:apply-templates select="./news_content"/>
    </xsl:template>
    <xsl:template match="module[@type='resolution']/news_content">
        <h4><xsl:value-of select="./news_head/@text"/></h4>
        <xsl:apply-templates select="./news_text"/>
    </xsl:template>
    <xsl:template match="module[@type='resolution']//news_text">
        <xsl:apply-templates select="./para"/>
    </xsl:template>
    <xsl:template match="module[@type='resolution']//para">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="module[@type='resolution']//para/SENT/ALT|module[@type='resolution']//para/ALT">
        <h5>ZONE</h5>
        <table border="1" class="zone">
            <tr class="table_head"><th>Path Rank</th><th>Path Score</th><th>Linking Type</th><th>Forms</th></tr>
            <!--<xsl:attribute name="RULES">NONE</xsl:attribute>
            <xsl:attribute name="FRAME">BOX</xsl:attribute>-->
            <xsl:apply-templates select="./CHOICE"/>
        </table>
    </xsl:template>
    <xsl:template match="module[@type='resolution']//para/SENT/ENAMEX|module[@type='resolution']//para/ENAMEX">
    <!--
        <ENAMEX cand_type="ALEDA" candidate_name="Les" candidate_score="-3.13001039508" candidate_type="Null" gender="m" local_eid="1" path_score="-3.13001039508" rank="0" resolution_id="Les" type="Person" types="Person" valid="true">
        <form value="Les">
        <token id="T1">Les</token>
        </form>
        <candidate href="http://nomos/linking/anomymousAFPentities/Les.not" id="Les" name="Les" rank="1" score="-3.13001039508" type="Null"/>
        <candidate href="http://nomos/linking/discardedEntities/Les.not" id="Les" name="Les" rank="2" score="-3.13001039508" type="Null"/>
        <candidate href="http://fr.wikipedia.org/wiki/Jim Les" id="1000000003377853" name="Jim Les" rank="3" score="-5.98076367262" type="Person"/>
        </ENAMEX>
    -->
    </xsl:template>
    <xsl:template match="module[@type='resolution']//para/SENT/ALT/CHOICE[@type='ENAMEX']|module[@type='resolution']//para/ALT/CHOICE[@type='ENAMEX']">
        <tr>
            <xsl:choose>
                <xsl:when test="./@rank='1'"><xsl:attribute name="class">first_rank</xsl:attribute></xsl:when>
                <xsl:when test="./@rank='0'"><xsl:attribute name="class">first_rank</xsl:attribute></xsl:when>  
                <xsl:otherwise><xsl:attribute name="class">not_first_rank</xsl:attribute></xsl:otherwise>
            </xsl:choose>    
            <xsl:choose>
                <xsl:when test="./@valid='true'">
                    <td><xsl:value-of select="./@rank"/></td>
                    <td><xsl:value-of select="./@path_score"/></td>
                </xsl:when>
                <xsl:otherwise>
                    <td class="invalid">#null</td><td class="invalid">invalid</td>
                </xsl:otherwise>
            </xsl:choose>
            <td class="linking_type">Entity</td>
            <td class="token"><xsl:apply-templates select="./token|./ENAMEX//token"/></td>
            <xsl:apply-templates select="./ENAMEX"/>
        </tr>
    </xsl:template>
    <xsl:template match="module[@type='resolution']//para/SENT/ALT/CHOICE[@type='NOT_ENAMEX']|module[@type='resolution']//para/ALT/CHOICE[@type='NOT_ENAMEX']">
        <tr>
            <xsl:choose>
                <xsl:when test="./@rank='1'"><xsl:attribute name="class">first_rank</xsl:attribute></xsl:when>
                <xsl:otherwise><xsl:attribute name="class">not_first_rank</xsl:attribute></xsl:otherwise>
            </xsl:choose>
            <xsl:choose>    
                <xsl:when test="./@valid='true'">
                    <td><xsl:value-of select="./@rank"/></td>
                    <td><xsl:value-of select="./@path_score"/></td>
                </xsl:when>
                <xsl:otherwise>
                    <td class="invalid">#null</td><td class="invalid">invalid</td>
                </xsl:otherwise>
            </xsl:choose>
            <td class="linking_type">not an entity</td>
            <td class="token"><xsl:apply-templates select="./token"/></td>
        </tr>
    </xsl:template>
    <xsl:template match="module[@type='resolution']//para//ALT/CHOICE/token">
        <!--<div> <xsl:apply-templates select="./token"></xsl:apply-templates></div>-->
        <div><xsl:value-of select="."/></div>
    </xsl:template>
    <xsl:template match="module[@type='resolution']//para//ALT/CHOICE/ENAMEX">
        <td>
            <xsl:variable name="form"><xsl:call-template name="form"/></xsl:variable>
                <xsl:if test="count(./candidate) > 0">
                <table>
                    <tr><th colspan="2"><xsl:value-of select="concat('Candidates for Enamex ', ./@local_eid, ' (', ./token,')')"/></th></tr>
                    <xsl:apply-templates select="./candidate"/>
                </table>
                </xsl:if>   
        </td>
    </xsl:template>
    <xsl:template match="module[@type='resolution']//para//ALT/CHOICE/ENAMEX/candidate">
        <tr>
            <td><a>
                <xsl:choose>
                    <xsl:when test="./@id=following-sibling::gold/@eid"><xsl:attribute name="class">gold_candidate</xsl:attribute></xsl:when>
                    <xsl:otherwise><xsl:attribute name="class">candidate</xsl:attribute></xsl:otherwise>
                </xsl:choose>
                <xsl:attribute name="href"><xsl:value-of select="./@href"/></xsl:attribute>
                <xsl:attribute name="target">blank</xsl:attribute>
                <xsl:value-of select="concat('#', ./@rank, '::', ./@name, ' (', ./@id, ')', ' (', ./@known, ')')"/>
            </a></td>
        <td><xsl:value-of select="./@score"/></td>
        </tr>
    </xsl:template>
    <xsl:template match="module[@type='resolution']//para//ALT/CHOICE/ENAMEX/form">
        <!--<div style="color:#330099"> <xsl:apply-templates select="./token"/></div>-->
        <div style="color:#330099"><xsl:value-of select="./@value"/></div>
    </xsl:template>
    <xsl:template name="form">
        <!--<div style="color:#330099"> <xsl:call-template name="token"/></div>-->
        <div style="color:#330099"><xsl:value-of select="./@value"/></div>
    </xsl:template>
    <xsl:template name="token">
        <span><xsl:value-of select="concat('[', ., ']')"/></span>
    </xsl:template>
    <xsl:template match="module[@type='resolution']//para//ALT/CHOICE//form/token">
        <span><xsl:value-of select="concat('[', ., ']')"/></span>
    </xsl:template>
    <xsl:template match="module[@type='resolution']//para/SENT/form|module[@type='resolution']//para/form">
        <span><xsl:apply-templates select="./token"/></span>
    </xsl:template>
    <xsl:template match="module[@type='resolution']//para/SENT/form/token|module[@type='resolution']//para/form/token">
        <span><xsl:value-of select="concat('[', ., ']')"/></span>
    </xsl:template>
</xsl:stylesheet>
