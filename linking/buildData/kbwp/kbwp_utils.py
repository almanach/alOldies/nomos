#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
import re
sys.path.append('@common_src@')
from utils import *
from collections import defaultdict

# utils fonctions common to KB population from frwiki scripts

# read frwiki articles (20 dirs of 500 files)
# select articles: entity articles with map wptitle>aledaid, articles with at least 1 wikilink with wptitle in map wptitle>aledaid
# open and close DBs: in main utils.py)
# access table names and structure in KbwpDB and frwikiWordsDB
# compute Salience: in main utils.py

# !!! NOW IN UTILS.PY !!!
# def selectTokens(tokenslist, stop):
#     return map(lambda t:re.sub(':', '', t), filter(lambda t: accept(t, stop), map(lambda t: truncate(clean(t.lower())), tokenslist)))


# def getTokens2counts(tokenslist):
#     d = defaultdict(integer)
#     for t in tokenslist:
#         d[t] += 1
#     return d

# def getWordsDBtables():
#     return ['words2frequency', 'bow', 'swords']

# def getKBDBtables():
#     return ['main', 'bow', 'swords', 'categories', 'slugs',
#             'iptc', 'related', 'peers', 'secpeers', 'secswords',
#             'seccategories', 'secslugs', 'seciptc', 'mentions',
#             'surrwords', 'wordclusters']


# def normalizeTitle(title):
#     return re.sub("&apos;", "'", re.sub('&quot;', '', re.sub('&amp;quot;', '&quot;', title)))
