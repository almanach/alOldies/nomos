#!/usr/bin/perl -w

use strict;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";

my $slugfile = shift or die "$0 <slugs> <wp cats>\n";
my $catfile = shift or die "$0 <slugs> <wp cats>\n";

my %slugs = ();
my %slugs2cats = ();

open(S, "<:encoding(utf-8)", $slugfile) or die "Can't open $slugfile: $!\n";
while(<S>)
  {
    chomp;
    my $slug = $_;
    $slug =~ s/^\s*//g; $slug =~ s/\s*$//g; $slug =~ s/ +/_/g;
    $slug = lc($slug);
    $slugs{$slug}++;
  }
close(S);

my $slugs_total = scalar(keys(%slugs));
print STDERR "$slugs_total slugs.\n";
open(C, "<:encoding(utf-8)", $catfile) or die "Can't open $catfile: $!\n";
while(<C>)
  {
    chomp;
    my $cat = $_;
    $cat =~ s/^\s*//g; $cat =~ s/\s*$//g; $cat =~ s/ +/_/g;
    $cat = lc($cat);
    my $short_cat = substr($cat, 0, 5);
    if(exists($slugs{$cat}))
      {
	$slugs2cats{$cat}{$cat}++;
      }
    elsif(exists($slugs{$short_cat}))
      {
	$slugs2cats{$short_cat}{$cat}++;
      }
  }
close(C);

my $slugs2cats_total = scalar(keys(%slugs2cats));
print STDERR "$slugs2cats_total matches.\n";

foreach my $slug(sort(keys(%slugs2cats)))
  {
    # print "SLUG#$slug#CATS#".join('|', keys(%{$slugs2cats{$slug}}))."\n";
    print "$slug#".join('|', keys(%{$slugs2cats{$slug}}))."\n";
  }

