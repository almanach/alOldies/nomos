#!/usr/bin/perl -w

use strict;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";

# my $file = shift or die "$0 <wikipedia xml+token+nicewl file> <slugs2cats data>\n";
# my $data = shift or die "$0 <wikipedia xml+token+nicewl file> <slugs2cats data>\n";

# input: frwiki xml files with tokens and nicewl
# data: slugs2wpCats - format:
## architecture#architecture|...|... => slug#cat|cat|...
# for each line <category title="..." lang=""/>
## if normalize rule applies, transform title => normalized title
### add attribute normalized_title
### search match title => slug
### if match: add attribute slug
## else: add attribute normalized_title = null
# normalize rule:
## title does not start with number or - or...
## title does not contain a year =~ /(^|^.+?)\d{4}.+$/
## title has one word
## title has more than one word, words after first are not cap =~ /^([A-Z]\S+( |_)+[^A-Z].+)$/; keep first word

# my %slugs2cats = ();
my %cats2slugs = ();
# open(D, "<:encoding(utf-8)", $data) or die "Can't open $data: $!\n";
while(<DATA>)
  {
    chomp;
    # if(/^([^#]+)#([^\|]+(\|[^\|]+)*)$/)
    #{
    # my $slug = $1;
    # my $cats = $2;
    my($slug, $cats) = split(":");
    my @cats = split('\|', $cats);
    foreach my $cat(@cats)
      {
	# $slugs2cats{$slug}{$cat}++;
	$cats2slugs{$cat} = $slug;
      }
  #}
  }
# close(D);

# open(F, "<:encoding(utf-8)", $file) or die "Can't open $file: $!\n";
# while(<F>)
while(<>)
  {
    chomp;
    if(/^\s*(<category title=")([^"]+)"( lang="[^"]+"\/>)\s*$/)
      {
	my $start = $1; my $end = $3; my $title = $2;
	my $normalized_title = "null";
	my $slug = "null";
	if($title !~ /^\d/ and $title !~ /\d{4}/)
	  {
	    if($title =~ /^([A-Z][^ _]+)$/)
	      {
		$normalized_title = $1;
		$normalized_title = lc($normalized_title);
	      }
	    if($title =~ /^([A-Z][^ _]+)( |_)+[^A-Z].+$/)
	      {
		$normalized_title = $1;
		$normalized_title = lc($normalized_title);
	      }
	    if(exists($cats2slugs{$normalized_title}))
	      {
		$slug = $cats2slugs{$normalized_title};
	      }
	  }
	print $start.$title.'" normalized_title="'.$normalized_title.'" afp_slug="'.$slug.'"'.$end."\n";
      }
    else
      {
	print $_."\n";
      }
  }
# close(F);


__DATA__
accident:accident
accusation:accusation
acquisition:acquisition
actualité:actualité
administration:administration
adolescence:adolescence
adoption:adoption
agriculture:agriculture
aide:aide
alimentation:alimentation
alpinisme:alpinisme
anglican:anglican
anglicanisme:anglicanisme
animal:animal
animaux:animaux
anthropologie:anthropologie
antisémitisme:antisémitisme
apprentissage:apprentissage
architecture:architecture
archéologie:archéologie
armes:armes
armée:armée
art:art
arts:arts
assemblée:assemblée
assurance:assurance
astronomie:astronomie
athlétisme:athlétisme
attentat:attentat
attentats:attentats
audiovisuel:audiovisuel
audit:audit
automobile:automobile
avalanche:avalanche
aviron:aviron
avortement:avortement
aéronautique:aéronautique
baccalauréat:baccalauréat
badminton:badminton
ban:ban
banque:banque
banque_centrale:banque_centrale
baseball:baseball
beach-volley:beach-volley
biathlon:biathlon
bible:bible
billard:billard
biologie:biologie
biométrie:biométrie
biotechnologie:biotechnologie
bobsleigh:bobsleigh
bombardements:bombardements
botanique:botanique
bouddhisme:bouddhisme
bouddhiste:bouddhiste
bourse:bourse
bowling:bowling
boxe:boxe
brevet:brevet
btp:btp
cancer:cancer
canicule:canicule
canoë-kayak:canoë-kayak
cap:cap
cardiologie:cardiologie
carnaval:carnaval
casino:casino
catastrophe:catastrophe
catastrophes:catastrophes
catholicisme:catholicisme
catholique:catholique
cgt:cgt
chantier_naval:chantier_naval
charbon:charbon
chasse:chasse
chien:chien
chimie:chimie
choléra:choléra
christianisme:christianisme
chrétiens:chrétiens
chômage:chômage
ciment:ciment
cinéma:cinéma
circulation:circulation
climat:climat
clinique:clinique
clonage:clonage
coentreprise:coentreprise
combiné:combiné
commande:commande
commerce:commerce
commémoration:commémoration
comptabilité:comptabilité
conflit:conflit
conflits:conflits
consommation:consommation
constitution:constitution
construction:construction
contentieux:contentieux
contraception:contraception
contrat:contrat
coran:coran
corruption:corruption
coup:coup
courtage:courtage
cricket:cricket
criminalité:criminalité
crise:crise
croissance:croissance
crosse:crosse
croyance:croyance
croyances:croyances
culture:culture
curling:curling
cyclisme:cyclisme
cyclone:cyclone
danse:danse|danseuse|danseur
diamant:diamant
diplomatie:diplomatie
direction:direction
dirigeant:dirigeant
discrimination:discrimination
distribution:distribution
dopage:dopage
dos:dos
drogue:drogue
droits:droits
défense:défense
délinquance:délinquance
démographie:démographie
désarmement:désarmement
eau:eau
education:education
emballage:emballage
endurance:endurance
enfance:enfance
enfant:enfant
enlèvement:enlèvement
enseignement:enseignement
entreprise:entreprise
environnement:environnement
esclavage:esclavage
escrime:escrime
espace:espace
espionnage:espionnage
euthanasie:euthanasie
exploration:exploration
explosion:explosion
exposition:exposition
fai:fai
famille:famille
famine:famine
festival:festival
finance:finance
fiscalité:fiscalité
fond:fond
foot:foot
formation:formation
forêt:forêt
franc-maçonnerie:franc-maçonnerie
fus:fus
fusion:fusion
g20:g20
gastronomie:gastronomie
gay:gay
gaz:gaz
gendarmerie:gendarmerie
gens:gens
golf:golf
gouvernement:gouvernement
grippe:grippe
grève:grève
guerre:guerre
guérilla:guérilla
gymnastique:gymnastique
génocide:génocide
génétique:génétique
géologie:géologie
haltérophilie:haltérophilie
handicap:handicap
handisport:handisport
hindouisme:hindouisme
hindouiste:hindouiste
histoire:histoire
hockey:hockey
homicide:homicide
homosexualité:homosexualité
hôpital:hôpital
hôtellerie:hôtellerie
immigration:immigration
immobilier:immobilier
incendie:incendie
indicateur:indicateur
industrie:industrie
infirmière:infirmière
inflation:inflation
informatique:informatique
inondation:inondation
internet:internet
islam:islamisme|islamabad|islam|islamologue|islamologie|islamiste
jeux:jeux
judaïsme:judaïsme
judo:judo
juif:juif
juifs:juifs
justice:justice
karaté:karaté
langage:langage
laïcité:laïcité
littérature:littérature
livre:livre-jeu|livre
livres:livres
logiciel:logiciel
logistique:logistique
loi:loi
luge:luge
lutte:lutteur|lutte|lutteuse
luxe:luxe
mafia:mafia
maladie:maladie
maladies:maladies
manifestation:manifestation
marathon:marathon
marketing:marketing
massacre:massacre
mathématiques:mathématiques
matin:matinale
meurtre:meurtre
migration:migration
mine:mine
mines:mines
ministre:ministre
ministres:ministres
missile:missile
mode:mode
motonautisme:motonautisme
municipalité:municipalité
musique:musique
musulman:musulman
musée:musée_ou_galerie_photographique|musée_chinois|musée
mécanique:mécanique
médecin:médecin
médecine:médecine
médecins:médecins
média:médiateur|médiation|média-participations|média_sans_publicité|média
médias:médias
médicament:médicament
métallurgie:métallurgie
météo:météorite|météorologie|météo-france|météoroloque|météoroïde|météorologue
nanotechnologie:nanotechnologie
natation:natation
nature:nature
naufrage:naufrage
netball:netball
notation:notation
note:note
obésité:obésité
océanographie:océanographie
omc:omc
ong:ong
opéra:opéra-ballet|opérateur|opéra|opération|opéra-comique
orientation:orientation
orthodoxe:orthodoxe
orthodoxie:orthodoxie
oscars:oscars
otage:otage
ouragan:ouragan
paix:paix
paléontologie:paléontologie
pape:pape
parachutisme:parachutisme
parlement:parlement
partis:partis
patrimoine:patrimoine
pauvreté:pauvreté
peinture:peinture
pentathlon:pentathlon
pharmacie:pharmacie
photo:photographe_français|photographe_du_xxe_siècle|photo-guide|photokinésiste|photojournaliste|photométrie|photographe_plasticien|photochimie|photosynthèse|photographe|photojournalisme
photographie:photographie
physique:physique
piraterie:piraterie
piste:piste
plongeon:plongeon
police:police
politique:politique
pollution:pollution
polo:polo
pornographie:pornographie
port:port
poste:poste
prison:prison
procès:procès
prostitution:prostitution
protestant:protestant
protestantisme:protestantisme
psychologie:psychologie
publicité:publicité
pédophilie:pédophilie
pétrole:pétrole
pêche:pêche
racisme:racisme
radar:radar
radio:radiotélescope|radiogalaxie|radioisotope|radiophonie|radiodiffusion|radioactivité|radiofréquence|radiocristallographie|radiotéléphonie|radiologue|radiomessagerie|radio-oncologie|radionavigation|radiologie|radio_française|radioamateurisme|radiochimie|radio|radiobiologie|radiocommunications|radiohead
recherche:recherche
religion:religion
restauration:restauration
retraite:retraite
royauté:royauté
rugby:rugby|rugbyman
rébellion:rébellion
santé:santé
satellite:satellite
science:science
science-fiction:science-fiction
sciences:sciences
scientologie:scientologie
sculpture:sculpture
secte:secteur|secteurs|secte
services:services
sexualité:sexualité
short-track:short-track
sida:sida
sidérurgie:sidérurgie
skeleton:skeleton
ski:ski
snooker:snooker
snowboard:snowboard
sociologie:sociologie
société:société
softball:softball
sommet:sommet
spectacle:spectacle
sport:sportif|sports|sportive|sport_dans_la_province_de_liège|sport
suicide:suicide
sumo:sumo
surf:surf
syndicat:syndicat
séisme:séisme
sénat:sénatrice|sénateurs|sénateur|sénat
taekwondo:taekwondo
tauromachie:tauromachie
taux:taux
technologie:technologie
tennis:tennis
terrorisme:terrorisme
tex:tex
textile:textile
théâtre:théâtre
tir:tir
tourisme:tourisme
tradition:tradition
transport:transport
transports:transports
travail:travail
triathlon:triathlon
troubles:troubles
tsunami:tsunami
typhon:typhon
télécommunications:télécommunications
télévision:télévision
universiade:universiade
université:université
vaccin:vaccin
vatican:vatican
vieillesse:vieillesse
ville:villepinte|ville|villes|villeurbanne|villecresnes|villetaneuse|villeneuve-d&apos;ascq|villefranche-de-panat|villejuif|villeneuve-la-garenne|ville_de_pologne|villemomble
vin:vin
viol:viol
violences:violences
viticulture:viticulture
voile:voile
vol:vol
volcan:volcan
vtt:vtt
water-polo:water-polo
xénophobie:xénophobie
zoologie:zoologie
