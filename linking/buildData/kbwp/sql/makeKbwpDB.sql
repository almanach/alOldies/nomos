CREATE TABLE main(aledaid integer unique, artid integer unique, wptitle text unique, titlepar text);
CREATE TABLE bow(aledaid integer unique, bow text);
CREATE TABLE swords(aledaid integer unique, swords text);
CREATE TABLE categories(aledaid integer unique, categories text);
CREATE TABLE slugs(aledaid integer unique, slugs text);
CREATE TABLE iptc(aledaid integer unique, iptc text);
CREATE TABLE related(aledaid integer unique, related text);
CREATE TABLE peers(aledaid integer unique, peers text);
CREATE TABLE secpeers(aledaid integer unique, secpeers text);
CREATE TABLE secswords(aledaid integer unique, secswords text);
CREATE TABLE seccategories(aledaid integer unique, seccategories text);
CREATE TABLE secslugs(aledaid integer unique, secslugs text);
CREATE TABLE seciptc(aledaid integer unique, seciptc text);
CREATE TABLE mentions(aledaid integer unique, mentions text);
CREATE TABLE surrwords(aledaid integer unique, surrwords text);
CREATE TABLE wordclusters(word text unique, cluster text);
CREATE TABLE mentions2frequency(mention text unique, frequency integer);
CREATE TABLE countries (afpcc text unique, geonamescc text unique, aledaid integer unique);

