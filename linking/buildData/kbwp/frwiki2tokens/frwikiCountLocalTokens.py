#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
import re
sys.path.append('@common_src@')
sys.path.append('@kb_src@')
from utils import *
from kbwp_utils import *
from collections import defaultdict

# STDIN: GREP <TOKEN> LINES FROM FRWIKI-SEG-NORM
# VORSICHT!!! HOMONYMY CONTENT WAS APPENDED BETWEEN ARTICLES => BAD XML + DATA TO BE LEFT OUT FROM COUNTING
# >> STDIN: GREP<ARTICLES AND <TOKEN> LINES FROM FRWIKI-SEG-NORM, CONTROL IF INART


stop = getStopWords()
uniq_tokens = 0
total_tokens = 0

words2counts = defaultdict(int)
# badlines = [];
# linepattern = re.compile('^\s*(\d+)\s+(.+)$')
tokenline = re.compile('<token')
artinline = re.compile('<article')
artoutline = re.compile('</article')
tokenpattern = re.compile('<token[^<>]+>([^<>]+)</token>')

message = ""
articles = 0
inart = False
print >> sys.stderr, "Reading Frwiki..."
for line in sys.stdin:
    if artinline.search(line):
        inart = True
    elif artoutline.search(line):
        articles += 1; inart = False
        message = "Articles %d"%(articles)
        sys.stderr.write(message+'\r'*(len(message)+1)) 
    elif inart and tokenline.search(line):
        tokens = selectTokens(re.findall(tokenpattern, line), stop)
        for t in tokens:
            words2counts[t] += 1
print >> sys.stderr, "Done."
sys.stderr.write(message+" read.\n")

total_tokens = 0
uniq_tokens = 0

print >> sys.stderr, "Write local counts..."
for w in words2counts:
    count = words2counts[w]
    sys.stdout.write("%d:%s\n"%(count, w))
    total_tokens += count
    uniq_tokens += 1

print >> sys.stderr, "Done."

print >> sys.stderr, "Total tokens: %d"%(total_tokens)
print >> sys.stderr, "Unique tokens: %d"%(uniq_tokens)
         

