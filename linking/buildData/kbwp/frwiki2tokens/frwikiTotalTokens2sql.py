#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
import re
sys.path.append('@common_src@')
from utils import *
from collections import defaultdict

stop = getStopWords()
uniq_tokens = 0
total_tokens = 0

words2counts = defaultdict(int)
# badlines = [];
# linepattern = re.compile('^\s*(\d+)\s+(.+)$')
linepattern = re.compile('^\s*(\d+):(.+)$')
print >> sys.stderr, "Reading local tokens2counts..."
for line in sys.stdin:
    line = line.rstrip()
    m = linepattern.match(line)
    if m is None:
        # badlines.append(line)
        continue
    word = m.group(2).lower()
    # MODIF TRUNCATE FUNCTION AFTER LOCAL PROCESSING: REDO ACCEPT (REMOVE FOR NEXT PROCESS)
    word = truncate(word)
    if accept(word, stop): # ALREADY DONE AT LOCAL COUNTS
        count = int(m.group(1))
        words2counts[word] += count
print >> sys.stderr, "Done."
    
# sys.stderr.write("%d bad lines\n"%(len(badlines)))
# sys.stderr.write('\n'.join(badlines))

print >> sys.stderr, "Merge counts and write insert queries..."
for w in words2counts:
    count = words2counts[w]
    if count > 1:
	 sys.stdout.write("INSERT INTO words2frequency (word, frequency) VALUES ('%s', %d);\n"%(w, count))
         total_tokens += count
         uniq_tokens += 1

print >> sys.stderr, "Done."

print >> sys.stderr, "Total tokens: %d"%(total_tokens)
print >> sys.stderr, "Unique tokens: %d"%(uniq_tokens)
         

