#!/usr/bin/python
# -*- coding: utf-8 -*-
#import codecs
import re
import os
import sys
sys.path.append('@common_src@')
sys.path.append('@kb_src@')
from utils import *
from kbwp_utils import *
from collections import defaultdict
#from subprocess import *
from optparse import *
import codecs

def main():
    usage = 'cat <input> | %s --db <words DB>'%sys.argv[0]
    parser = OptionParser(usage)
    # parser.add_option("--stop", action="store", dest="stop", default=None, help="stoplist file")
    parser.add_option("--wordsdb", action="store", dest="db", default=None, help="words DB")
    try: (options, args) = parser.parse_args()
    except OptionError: sys.stderr.write('OptionError'); parser.print_help(); sys.exit()
    except: sys.stderr.write('An error occured during options parsing\n'); parser.print_help(); sys.exit()
    if options.db == None: parser.print_help(); sys.exit()

    global db; global dbc
    db = DBConnection(options.db, getWordsDBtables())

    sys.stdout = codecs.getwriter('utf8')(sys.stdout)
    sys.stdin = codecs.getreader("utf-8")(sys.stdin)
    for line in sys.stdin:
        # id248982 C doter:1 rom:5 vill:1
        line = line.rstrip()
        elements = line.split(' ')
        docid = elements[0]
        bow = elements[2:len(elements)]
        words2frequency = dict(map(lambda i:tuple([i.split(':')[0], int(i.split(':')[1])]), bow))
        swords = computeSalience(words2frequency, 175000000, db)
        sys.stdout.write(docid+'#'+'#'.join(map(lambda i:i[0]+':'+str(i[1]), swords))+"\n")

    db.close()
        



if __name__ == '__main__':
    main()
