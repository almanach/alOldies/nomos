#!/usr/bin/python
# -*- coding: utf-8 -*-
#import os
import sys
#import re
import sqlite3
import codecs
from optparse import *
sys.path.append('@common_src@')
sys.path.append('@kb_src@')
from utils import *
from kbwp_utils import *
from collections import defaultdict


def main():
    options, args = getOptions()

    global stop; stop = getStopWords()

    # VORSICHT: SUB NON ALPHANUM CHARS
    print >> sys.stderr, "Loading mapping from wptitles to aleda ids..."
    wptitlesInAleda = {}
    F = codecs.open(options.wptitlesInAleda, "r", "utf-8")
    for line in F:
        line = line.rstrip()
        wptitlesInAleda[line] = True
    F.close()
    print >> sys.stderr, " Done."
    
    global db; global dbc
    db = DBConnection(options.db, getWordsDBtables())
    dbc = db.cursor

    # <article id="3" title="Antoine Meillet" wikipedia_title="Antoine Meillet">
    art_pattern_in1 = re.compile('<article id="\d+"')
    art_pattern_in2 = re.compile('<article id="(\d+)" title="[^"]+" wikipedia_title="([^"]+)">')
    art_pattern_out = re.compile('</article>')
    wl_pattern1 = re.compile('<nicewl')
    wl_pattern2 = re.compile('<nicewl title="([^"]+)"')
    token_pattern1 = re.compile('<token')
    token_pattern2 = re.compile('<token[^<>]+>([^<>]+)</token>')
    inart = False

    articles_all = 0
    articles_ok = 0
    global bow_cpt; bow_cpt = 0
    global sw_cpt; sw_cpt = 0 
    global insert_count
    insert_count = 0
    global insert_count_total
    insert_count_total = 0
    
    for line in sys.stdin: # STDIN: GREP <article> and <token> lines only in frwiki-xml-seg
        line = line.rstrip()
        if art_pattern_in1.search(line):
            articles_all += 1; message = "Articles total: %d "%(articles_all)
            artid, wptitle = art_pattern_in2.search(line).groups()
            wptitle = normalizeTitle(wptitle)
            inart = True
            tokens2counts = defaultdict(int)
            entities_wl = {}
        elif inart and wl_pattern1.search(line):
            # VORSICHT: SUB NON ALPHANUM CHARS
            local_entities_wl = filter(lambda wl: normalizeTitle(wl) in wptitlesInAleda, re.findall(wl_pattern2, line))
            for entity_wl in local_entities_wl:
                entities_wl[normalize(entity_wl)] = True
        elif inart and token_pattern1.search(line):
            tokens = selectTokens(re.findall(token_pattern2, line), stop)
            for t in tokens:
                # t = t.lower()
                # if accept(clean(t), stop):
                db.tables['words2frequency'].selectFromTable(dbc, ['frequency'], [('word', '=', t)])
                res = dbc.fetchone()
                if res != None and res != '':
                    wpfreq = res[0]
                    # if wpfreq > 1:
                    if wpfreq > 2:
                        tokens2counts[t] += 1
        elif inart and art_pattern_out.search(line):
            # VORSICHT: SUB NON ALPHANUM CHARS
            inaleda = wptitle in wptitlesInAleda
            if inaleda or entities_wl != {}:
                insertBowSwords(artid, tokens2counts, int(inaleda))
                inart = False
            else:
                inart = False
            articles_ok += 1; message += "Articles OK %d"%(articles_ok)
            sys.stderr.write(message+'\r'*(len(message)+1))

    db.commit()
    db.close()
    
    message = "Articles total: %d Articles OK: %d"%(articles_all, articles_ok)
    sys.stderr.write(message+"\n")
    sys.stdout.write(message+"\n")
    message = "BOW non empty: %d Swords non empty: %d"%(bow_cpt, sw_cpt)
    sys.stderr.write(message+"\n")
    sys.stdout.write(message+"\n")
    sys.stderr.write("Commit %d insertions\n"%(insert_count_total))
    sys.stdout.write("Commit %d insertions\n"%(insert_count_total))
                
def insertBowSwords(artid, tokens2counts, inaleda):
    if tokens2counts != {}:
        bow = '#'.join(map(lambda i: i[0]+':'+str(i[1]), sorted(tokens2counts.items(), key=lambda i:i[1], reverse=True)))
        db.tables['bow'].insertIntoTable(dbc, {'artid':artid, 'inaleda':inaleda, 'bow':bow})
        global insert_count; global insert_count_total; global bow_cpt
        insert_count += 1; insert_count_total += 1
        insert_count = testCommit(insert_count, db)
        insertSwords(artid, computeSalience(tokens2counts, 175000000, db), inaleda)
        bow_cpt += 1
        

def insertSwords(artid, swords2scores, inaleda):
    # VORSICHT: COMPUTESALIENCE RETURNS A LIST OF TUPLES ALREADY SORTED
    # STORE WITHOUT SCORE, JUST RANKED
    if swords2scores != []:
        swords = '#'.join(map(lambda i: i[0], swords2scores))
        db.tables['swords'].insertIntoTable(dbc, {'artid':artid, 'inaleda':inaleda, 'swords':swords})
        global insert_count; global insert_count_total; global sw_cpt
        insert_count += 1; insert_count_total += 1
        insert_count = testCommit(insert_count, db)
        sw_cpt += 1

def getOptions():
    usage = '\n\n\t%prog [options] <articles>\n'
    parser = OptionParser(usage)
    # parser.add_option("--artids2links", action="store", dest="artids2links", default=None, help="mapping articles ids to wp links")
    parser.add_option("--wptitlesInAleda", action="store", dest="wptitlesInAleda", default=None, help="mapping wptitles to aleda ids")
    parser.add_option("--db", action="store", dest="db", default=None, help="DB words (to get word frequencies and to populate)")
    try: (options, args) = parser.parse_args()
    except OptionError: parser.print_help(); sys.exit('OptionError')
    except: parser.print_help(); sys.exit('An error occured during options parsing\n')
    if options.db == None: parser.print_help(); sys.exit()
    # if options.artids2links == None: parser.print_help(); sys.exit()
    if options.wptitlesInAleda == None: parser.print_help(); sys.exit()
    return options, args

if __name__ == "__main__":
    main()    

