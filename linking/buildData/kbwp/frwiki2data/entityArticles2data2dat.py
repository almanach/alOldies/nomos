#!/usr/bin/python
# -*- coding: utf-8 -*-
#import os
import sys
#import re
#import sqlite3
from optparse import *
import codecs
sys.path.append('@common_src@')
sys.path.append('@kb_src@')
from utils import *
from kbwp_utils import *
import codecs

def main():
    options, args = getOptions()
     
    print >> sys.stderr, "Loading mapping from wptitles to aleda ids..."
    wptitles2aledaids = {}
    F = codecs.open(options.wptitles2aledaids, "r", "utf-8")
    for line in F:
        line = line.rstrip()
        (aledaid, wptitle) = line.split('|')
        wptitles2aledaids[wptitle] = aledaid
    F.close()
    print >> sys.stderr, " Loaded %d wptitles from Aleda"%(len(wptitles2aledaids))
    print >> sys.stderr, " Done."

    

    global wordsdb; global wordsdbc
    wordsdb = DBConnection(options.wordsdb, getWordsDBtables())
    wordsdbc = wordsdb.cursor

    global kbdb; global kbdbc
    kbdb = DBConnection(options.kbdb, getKBDBtables())
    kbdbc = kbdb.cursor


    global insert_count; global insert_count_total
    insert_count = 0; insert_count_total = 0
    global insert_main; insert_main = 0

    # READ MAPPING TITLE => RELATED TITLES
    # SELECT ALEDA TITLES IN TITLE AND RELATED TITLES
    # DO ONLY ONCE => TEST IF TABLE IS EMPTY OR NOT
    kbdb.tables['related'].selectFromTable(kbdbc, ['count(*)'])
    res = kbdbc.fetchone()[0]
    if res == 0:
        print >> sys.stderr, "Loading and inserting mapping from wptitles to related..."
        related_count = 0
        F = codecs.open(options.wptitles2related, "r", "utf-8")
        for line in F:
            line = line.rstrip()
            elements = line.split('###')
            wptitle = normalizeTitle(elements[0])
            related = map(lambda e:wptitles2aledaids[e], filter(lambda e:e in wptitles2aledaids, elements[1:len(elements)]))
            if wptitle in wptitles2aledaids:
                aledaid = wptitles2aledaids[wptitle]
                insertRelated(aledaid, related)
                related_count += 1
        F.close()
        print >> sys.stderr, "Inserted %d wptitles with related"%(related_count)
        print >> sys.stderr, "Done."

    
    # <article id="3" title="Antoine Meillet" wikipedia_title="Antoine Meillet">
    art_pattern_in1 = re.compile('<article id="\d+"')
    art_pattern_in2 = re.compile('<article id="(\d+)" title="[^"]+" wikipedia_title="([^"]+)">')
    art_pattern_out = re.compile('</article>')
    # <category title="Algorithmique" normalized_title="algorithmique" afp_slug="null" lang="fr"/>
    cat_pattern1 = re.compile('<category title="[^"]+" normalized_title="[^"]+" afp_slug="[^"]+"')
    cat_pattern2 = re.compile('<category title="[^"]+" normalized_title="([^"]+)" afp_slug="([^"]+)"')
    wl_pattern1 = re.compile('<nicewl')
    wl_pattern2 = re.compile('<nicewl title="([^"]+)"[^<>]*>')
    inart = False
    
    articles_all = 0
    articles_ok = 0
    inserted = False

    titlepar_pattern = re.compile('\(([^\(\)]+)\)$')

    for line in sys.stdin:
        line = line.rstrip()
        if art_pattern_in1.search(line):
            # GET ARTICLE WPTITLE
            articles_all += 1; message = "Articles total: %d "%(articles_all)
            artid, wptitle = art_pattern_in2.search(line).groups()
            wptitle = normalizeTitle(wptitle.decode('utf-8'))
            # IF WPTITLE IN ALEDA
            # VORSICHT: SUB NON ALPHANUM CHARS
            if wptitle in wptitles2aledaids:
                inart = True
                articles_ok += 1 # ; message += "Articles / Entities %d"%(articles_ok)
                # sys.stderr.write(message+'\r'*(len(message)+1))
                titlepar = getTitlepar(wptitle, titlepar_pattern)
                aledaid = wptitles2aledaids[wptitle]
                # INSERT MAIN TABLE
                inserted = insertMain(aledaid, artid, wptitle, titlepar)
                if inserted:
                    # INSERT BOW, SWORDS
                    insertBow(aledaid, artid)
                    insertSwords(aledaid, artid)
                # INIT MEMORY FOR CATS-SLUGS AND PEERS
                cats = {}; slugs = {}; peers = {}
            else:
                # sys.stdout.write(wptitle.encode('utf-8')+"\n")
                inart = False
                continue
        elif inart and cat_pattern1.search(line):
            # INSERT CATEGORIES AND SLUGS
            cat, slug = cat_pattern2.search(line).groups()
            if cat != "null": cats[cat.decode('utf-8')] = True
            if slug != "null": slugs[slug.decode('utf-8')] = True
        elif inart and wl_pattern1.search(line):
            # VORSICHT: SUB NON ALPHANUM CHARS
            local_peers = map((lambda p: wptitles2aledaids[p]), filter((lambda p: p in wptitles2aledaids), re.findall(wl_pattern2, line)))
            for peer in local_peers:
                peers[peer] = True
        elif inart and art_pattern_out.search(line) and inserted:
            insertCatsSlugs(aledaid, cats, slugs)
            insertPeers(aledaid, peers)
            inart = False; inserted = False

    sys.stderr, "\n"
    message = "Articles Total: %d Articles Entities: %d"%(articles_all, articles_ok)
    sys.stderr.write(message+"\n")
    # sys.stdout.write(message+"\n")
    sys.stderr.write("Total inserts: %d\n"%(insert_count_total))
    # sys.stdout.write("Total inserts: %d\n"%(insert_count_total))
    sys.stderr.write("Total inserts main table: %d\n"%(insert_main))
    # sys.stdout.write("Total inserts main table: %d\n"%(insert_main))

    kbdb.commit()
    wordsdb.close(); kbdb.close()

def insertRelated(aledaid, related):
    global kbdb; global kbdbc
    global insert_count; global insert_count_total
    # print >> sys.stderr, "kbdb.tables['related'].insertIntoTable(kbdbc, {'aledaid':%s, 'related':%s})"%(aledaid, '#'.join(related))
    kbdb.tables['related'].insertIntoTable(kbdbc, {'aledaid':int(aledaid), 'related':'#'.join(related)})
    insert_count += 1; insert_count_total += 1
    insert_count = testCommit(insert_count, kbdb)


def getTitlepar(wptitle, titlepar_pattern):
    m = titlepar_pattern.search(wptitle)
    if m is None: return ""
    else: return m.group(1)

def insertMain(aledaid, artid, wptitle, titlepar):
    global kbdb; global kbdbc
    global insert_count; global insert_count_total; global insert_main
    # print >> sys.stderr, "kbdb.tables['main'].insertIntoTable(kbdbc, {'aledaid':%s, 'artid':%s, 'wptitle':%s, 'titlepar':%s})"%(aledaid, artid, wptitle, titlepar)
    try:
        kbdb.tables['main'].insertIntoTable(kbdbc, {'aledaid':int(aledaid), 'artid':int(artid), 'wptitle':wptitle, 'titlepar':titlepar})
        insert_count += 1; insert_count_total += 1; insert_main += 1
        insert_count = testCommit(insert_count, kbdb)
        return True
    except sqlite3.IntegrityError:
        print >> sys.stdout, wptitle+"\n"
        return False
    

def insertBow(aledaid, artid):
    global wordsdb; global wordsdbc
    global kbdb; global kbdbc
    wordsdb.tables['bow'].selectFromTable(wordsdbc, ['bow'], [('artid', '=', artid)])
    res = wordsdbc.fetchone()
    if res != None and res != '':
        bow = res[0]
        # print >> sys.stderr, "kbdb.tables['bow'].insertIntoTable(kbdbc, {'aledaid':%s, 'bow':%s})"%(aledaid, bow)
        kbdb.tables['bow'].insertIntoTable(kbdbc, {'aledaid':int(aledaid), 'bow':bow})
        global insert_count; global insert_count_total
        insert_count += 1; insert_count_total += 1
        insert_count = testCommit(insert_count, kbdb)

def insertSwords(aledaid, artid):
    global wordsdb; global wordsdbc
    global kbdb; global kbdbc
    wordsdb.tables['swords'].selectFromTable(wordsdbc, ['swords'], [('artid', '=', artid)])
    res = wordsdbc.fetchone()
    if res != None and res != '':
        swords = res[0]
        # print >> sys.stderr, "kbdb.tables['swords'].insertIntoTable(kbdbc, {'aledaid':%s, 'swords':%s})"%(aledaid, swords)
        kbdb.tables['swords'].insertIntoTable(kbdbc, {'aledaid':int(aledaid), 'swords':swords})
        global insert_count; global insert_count_total
        insert_count += 1; insert_count_total += 1
        insert_count = testCommit(insert_count, kbdb)

def insertCatsSlugs(aledaid, cats, slugs):
    if cats != {}:
        insertCats(aledaid, cats)
    if slugs != {}:
        insertSlugs(aledaid, slugs)

def insertCats(aledaid, cats):
    global kbdb; global kbdbc
    # print >> sys.stderr, "kbdb.tables['categories'].insertIntoTable(kbdbc, {'aledaid':%s, 'categories':%s})"%(aledaid, '#'.join(cats.keys()))
    kbdb.tables['categories'].insertIntoTable(kbdbc, {'aledaid':int(aledaid), 'categories':'#'.join(cats.keys())})
    global insert_count; global insert_count_total
    insert_count += 1; insert_count_total += 1
    insert_count = testCommit(insert_count, kbdb)

def insertSlugs(aledaid, slugs):
    global kbdb; global kbdbc
    # print >> sys.stderr, "kbdb.tables['slugs'].insertIntoTable(kbdbc, {'aledaid':%s, 'slugs':%s})"%(aledaid, '#'.join(slugs.keys()))
    kbdb.tables['slugs'].insertIntoTable(kbdbc, {'aledaid':int(aledaid), 'slugs':'#'.join(slugs.keys())})
    global insert_count; global insert_count_total
    insert_count += 1; insert_count_total += 1
    insert_count = testCommit(insert_count, kbdb)

def insertPeers(aledaid, peers):
    global kbdb; global kbdbc
    if peers != {}:
        # print >> sys.stderr, "kbdb.tables['peers'].insertIntoTable(kbdbc, {'aledaid':%s, 'peers':%s})"%(aledaid, '#'.join(peers.keys()))
        kbdb.tables['peers'].insertIntoTable(kbdbc, {'aledaid':int(aledaid), 'peers':'#'.join(peers.keys())})
        global insert_count; global insert_count_total
        insert_count += 1; insert_count_total += 1
        insert_count = testCommit(insert_count, kbdb)

def getOptions():
    usage = '\n\n\t%prog [options] <articles>\n'
    parser = OptionParser(usage)
    parser.add_option("--wptitles2aledaids", action="store", dest="wptitles2aledaids", default=None, help="mapping wptitles to aleda ids")
    parser.add_option("--wptitles2related", action="store", dest="wptitles2related", default=None, help="mapping wptitles to related wptitles")
    parser.add_option("--wordsdb", action="store", dest="wordsdb", default=None, help="DB words (to get bow and sword)")
    parser.add_option("--kbdb", action="store", dest="kbdb", default=None, help="DB KB to populate")
    try: (options, args) = parser.parse_args()
    except OptionError: parser.print_help(); sys.exit('OptionError')
    except: parser.print_help(); sys.exit('An error occured during options parsing\n')
    if options.wordsdb == None: parser.print_help(); sys.exit()
    if options.kbdb == None: parser.print_help(); sys.exit()
    if options.wptitles2aledaids == None: parser.print_help(); sys.exit()
    # if options.wptitles2related == None: parser.print_help(); sys.exit()
    return options, args

if __name__ == "__main__":
    main()    
