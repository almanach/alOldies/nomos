#!/usr/bin/python
# -*- coding: utf-8 -*-
#import os
import sys
#import re
#import sqlite3
from optparse import *
import codecs
from collections import defaultdict
sys.path.append('@common_src@')
sys.path.append('@kb_src@')
from kbwp_utils import *
from utils import *


def main():
    options, args = getOptions()
    print >> sys.stderr, "Loading mapping from wptitles to aleda ids..."
    wptitles2aledaids = {}
    F = codecs.open(options.wptitles2aledaids, "r", "utf-8")
    for line in F:
        line = re.sub('', '', line.rstrip())
        (aledaid, wptitle) = line.split('|')
        wptitles2aledaids[wptitle] = aledaid
    F.close()
    print >> sys.stderr, " Done."

    global wordsdb; global wordsdbc
    wordsdb = DBConnection(options.wordsdb, getWordsDBtables())
    wordsdbc = wordsdb.cursor


    # <article id="3" title="Antoine Meillet" wikipedia_title="Antoine Meillet">
    art_pattern_in1 = re.compile('<article id="\d+"')
    art_pattern_in2 = re.compile('<article id="(\d+)"')
    art_pattern_out = re.compile('</article>')
    # <category title="Algorithmique" normalized_title="algorithmique" afp_slug="null" lang="fr"/>
    cat_pattern1 = re.compile('<category title="[^"]+" normalized_title="[^"]+" afp_slug="[^"]+"')
    cat_pattern2 = re.compile('<category title="[^"]+" normalized_title="([^"]+)" afp_slug="([^"]+)"')
    wl_pattern1 = re.compile('<nicewl')
    wl_pattern2 = re.compile('<nicewl title="([^"]+)"[^<>]*>((?: *<token[^<>]+>[^<>]+</token> *)+)</nicewl>')
    

    # REGEX FOR EXTRACTING MENTIONS OUT OF AN WIKILINK WITH RIGHT SPACING
    wltokens_pattern = re.compile('<token[^<>]+>([^<>]+)</token>( *)')
    # >>> s = '<token id="1">Jean</token><token id="2">-</token><token id="3">Charles</token> <token id="4">Dupont</token>'
    # >>> re.findall(p, s)
    # [('Jean', ''), ('-', ''), ('Charles', ' '), ('Dupont', '')]
    # >>> list(re.findall(p, s))
    # [('Jean', ''), ('-', ''), ('Charles', ' '), ('Dupont', '')]
    # >>> map(lambda i: i[0]+i[1], re.findall(p, s))
    # ['Jean', '-', 'Charles ', 'Dupont']
    # >>> ''.join(map(lambda i: i[0]+i[1], re.findall(p, s)))
    # 'Jean-Charles Dupont'
    
    # REGEX FOR EXTRACTING SURROUNDING WORDS OF AN ENTITY WIKILINK
    

    # INIT DATA FOR ENTITY-WL
    global entities_wl2wl; global entities_wl2mentions
    entities_wl2wl = defaultdict(dict); entities_wl2mentions = defaultdict(dict)
    global entities_wl2cats; global entities_wl2slugs; global entities_wl2swords
    entities_wl2cats = defaultdict(dict); entities_wl2slugs = defaultdict(dict); entities_wl2swords = defaultdict(dict)

    articles_all = 0
    inart = False

    sys.stdin = codecs.getreader("utf-8")(sys.stdin)
    sys.stdout = codecs.getwriter('utf8')(sys.stdout)
    for line in sys.stdin:
        line = line.rstrip()
        if art_pattern_in1.search(line):
            articles_all += 1; message = "Articles total: %d"%(articles_all)
            # sys.stderr.write(message+'\r'*(len(message)+1))
            artid = art_pattern_in2.search(line).group(1)
            inart = True
            # INIT ARTICLE CATS AND SLUGS
            local_cats = []; local_slugs = []
            # INIT ARTICLE ENTITIES WL
            local_entities_wl2mentions = defaultdict(dict)
            # GET ARTICLE SWORDS
            wordsdb.tables['swords'].selectFromTable(wordsdbc, ['swords'], [('artid', '=', artid)])
            res = wordsdbc.fetchone()
            if res != None and res != "": swords = res[0]
            else: swords = ""
            # print >> sys.stderr, "swords: %s"%(swords)
            # #GET ARTICLE IPTC
        elif inart and cat_pattern1.search(line):
            # INSERT CATEGORIES AND SLUGS
            cat, slug = cat_pattern2.search(line).groups()
            if cat != "null": local_cats.append(cat)
            if slug != "null": local_slugs.append(slug)
            # print >> sys.stderr, "cat: %s, slug: %s"%(cat, slug)
        elif inart and wl_pattern1.search(line):
            wl2tokens = re.findall(wl_pattern2, line) # LIST OF TUPLES (WL-TITLE, TOKENS)
            # SELECT ENTITY WIKILINKS FROM LIST OF (WL-TITLE, TOKENS)
            current_entities_wl2mentions = map(lambda i: tuple([wptitles2aledaids[normalizeTitle(i[0])], i[1]]), filter(lambda i: normalizeTitle(i[0]) in wptitles2aledaids, wl2tokens))
            for i in current_entities_wl2mentions:
                # ADD ENTITY WL-TITLE IN ARTICLE ENTITY WL-TITLES
                # ADD MENTION FOR EACH OCC
                mention = ''.join(map(lambda j: j[0]+j[1], re.findall(wltokens_pattern, i[1])))
                try: local_entities_wl2mentions[i[0]][mention] += 1
                except KeyError: local_entities_wl2mentions[i[0]][mention] = 1
        elif inart and art_pattern_out.search(line):
            addData(swords, local_cats, local_slugs, local_entities_wl2mentions)
            inart = False

    message = "Articles %d"%(articles_all)
    sys.stderr.write(message+"\n")
    wordsdb.close()

    print >> sys.stderr, "Writing data for entities..."
    global colon; colon = re.compile(':')
    global diese; diese = re.compile('#')
    outputData()

def outputData():
    global entities_wl2wl
    for wl in entities_wl2wl:
        writePeers(wl)
        writeSecCats(wl)
        writeSecSlugs(wl)
        writeSecSwords(wl)
        writeMentions(wl)

def writePeers(wl):
    global entities_wl2wl; global entities_wl2mentions
    global entities_wl2cats; global entities_wl2slugs; global entities_wl2swords
    global colon; global diese
    # for wl in entities_wl2wl:
    if entities_wl2wl[wl] == {}:
        return
    line = []
    line.append(wl)
    line.append("coocs")            
    for peer in entities_wl2wl[wl]:
        # aledaid#coocs#peer:count#...
        # peer2 = colon.sub('', peer)
        line.append(peer+":"+str(entities_wl2wl[wl][peer]))
    sys.stdout.write("#".join(line)+"\n")
        
def writeSecCats(wl):
    global entities_wl2wl; global entities_wl2mentions
    global entities_wl2cats; global entities_wl2slugs; global entities_wl2swords
    global colon; global diese
    # for wl in entities_wl2wl:
    if entities_wl2cats[wl] == {}:
        return
    line = []
    line.append(wl)
    line.append("seccats")
    for cat in entities_wl2cats[wl]:
        # aledaid#seccats#cat:count#...
        cat2 = colon.sub('', diese.sub('', cat))
        line.append(cat2+":"+str(entities_wl2cats[wl][cat]))
    sys.stdout.write("#".join(line)+"\n")
        
def writeSecSlugs(wl):
    global entities_wl2wl; global entities_wl2mentions
    global entities_wl2cats; global entities_wl2slugs; global entities_wl2swords
    # for wl in entities_wl2wl:
    if entities_wl2slugs[wl] == {}:
        return
    line = []
    line.append(wl)
    line.append("secslugs")
    for slug in entities_wl2slugs[wl]:
        # aledaid#secslugs#slug:count#...
        line.append(slug+":"+str(entities_wl2slugs[wl][slug]))
    sys.stdout.write("#".join(line)+"\n")

def writeSecSwords(wl):
    global entities_wl2wl; global entities_wl2mentions
    global entities_wl2cats; global entities_wl2slugs; global entities_wl2swords
    global colon; global diese
    line = []
    # for wl in entities_wl2wl:
    if entities_wl2swords[wl] == {}:
        return
    line = []
    line.append(wl)
    line.append("secswords")
    for sword in entities_wl2swords[wl]:
        # aledaid#secswords#sword:count#...
        sword2 = colon.sub('', diese.sub('', sword))
        line.append(sword2+":"+str(entities_wl2swords[wl][sword]))
    sys.stdout.write("#".join(line)+"\n")

def writeMentions(wl):
    global entities_wl2wl; global entities_wl2mentions
    global entities_wl2cats; global entities_wl2slugs; global entities_wl2swords
    global colon; global diese
    # for wl in entities_wl2wl:
    if entities_wl2mentions[wl] == {}:
        return
    line = []
    line.append(wl)
    line.append("mentions")
    # FILTER OUT MENTIONS NOT CAPITALIZED AND MENTIONS WITH #
    for mention in entities_wl2mentions[wl]:
        # aledaid#mentions#mention:count#...
        mention2 = colon.sub('', mention)
        if mention2.istitle() and diese.search(mention2) is None:
            line.append(mention2+":"+str(entities_wl2mentions[wl][mention]))
    sys.stdout.write("#".join(line)+"\n")

def addData(swords, local_cats, local_slugs, local_entities_wl2mentions):
    global entities_wl2wl; global entities_wl2mentions
    global entities_wl2cats; global entities_wl2slugs; global entities_wl2swords
    if local_entities_wl2mentions != {}:
        for wl in local_entities_wl2mentions:
            for peer in filter(lambda p: p != wl, local_entities_wl2mentions):
                try: entities_wl2wl[wl][peer] += 1
                except KeyError: entities_wl2wl[wl][peer] = 1
            for mention in local_entities_wl2mentions[wl]:
                try: entities_wl2mentions[wl][mention] += local_entities_wl2mentions[wl][mention]
                except KeyError: entities_wl2mentions[wl][mention] = local_entities_wl2mentions[wl][mention]
        if swords != "":
            swords_list = map(lambda i:i.split(':')[0], swords.split('#'))
            for sword in swords_list:
                try: entities_wl2swords[wl][sword] += 1
                except KeyError: entities_wl2swords[wl][sword] = 1
        if local_cats != []:
            for cat in local_cats:
                try: entities_wl2cats[wl][cat] += 1
                except KeyError: entities_wl2cats[wl][cat] = 1
        if local_slugs != []:
            for slug in local_slugs:
                try: entities_wl2slugs[wl][slug] += 1
                except KeyError: entities_wl2slugs[wl][slug] = 1

    

def getOptions():
    usage = '\n\n\t%prog [options] <articles>\n'
    parser = OptionParser(usage)
    parser.add_option("--wptitles2aledaids", action="store", dest="wptitles2aledaids", default=None, help="mapping wptitles to aleda ids")
    parser.add_option("--wordsdb", action="store", dest="wordsdb", default=None, help="DB words (to get bow and sword)")
    try: (options, args) = parser.parse_args()
    except OptionError: parser.print_help(); sys.exit('OptionError')
    except: parser.print_help(); sys.exit('An error occured during options parsing\n')
    if options.wordsdb == None: parser.print_help(); sys.exit()
    if options.wptitles2aledaids == None: parser.print_help(); sys.exit()
    return options, args

if __name__ == "__main__":
    main()    
