#!/usr/bin/python
# -*- coding: utf-8 -*-
#import os
import sys
import re
import codecs
#import sqlite3
from optparse import *
from collections import defaultdict
sys.path.append('@common_src@')
sys.path.append('@kb_src@')
from kbwp_utils import *
from utils import *


def main():
    options, args = getOptions()
    # print >> sys.stderr, "Loading mapping from wptitles to aleda ids..."
    # wptitles2aledaids = {}
    # F = open(options.wptitles2aledaids)
    # for line in F:
    #     line = line.rstrip()
    #     (aledaid, wptitle) = line.split('|')
    #     wptitles2aledaids[wptitle] = aledaid
    # F.close()
    # print >> sys.stderr, " Done."

    # global kbdb; global kbdbc
    # kbdb = DBConnection(options.kbdb, getKBDBtables())
    # kbdbc = kbdb.cursor

    global datatype; datatype = options.datatype

    global entities2secpeers
    global entities2seccats
    global entities2secslugs
    global entities2secswords
    global entities2mentions

    entities2secpeers = defaultdict(dict)
    entities2seccats = defaultdict(dict)
    entities2secslugs = defaultdict(dict)
    entities2secswords = defaultdict(dict)
    entities2mentions = defaultdict(dict)

    sys.stdin = codecs.getreader("utf-8")(sys.stdin)
    sys.stdout = codecs.getwriter('utf8')(sys.stdout)
    print >> sys.stderr, "Filling %s..."%(datatype)
    for line in sys.stdin:
        line = line.rstrip()
        elements = line.split("#")
        if len(elements) > 2:
            aledaid = elements[0]
            thedatatype = elements[1]
            if thedatatype == datatype:
                data2counts = map(lambda i:tuple(i.split(':')), elements[2:len(elements)])
                fillData(aledaid, data2counts, datatype)
    print >> sys.stderr, "Done"

    global insert; insert = 0
    # for datatype in ['secpeers', 'seccats', 'secslugs', 'secswords', 'mentions']:
    #     print >> sys.stderr, "Inserting data to %s..."%(options.kbdb)
    #     insertData(datatype)
    #     print >> sys.stderr, "Done"
        
    # kbdb.commit()
    # kbdb.close()
    
    print >> sys.stderr, "Writing merged %s..."%(datatype)
    output()
    print >> sys.stderr, "Wrote %d queries for %s"%(insert, datatype)
    
def output():
    if datatype == 'coocs':
        insertSecPeers()
    elif datatype == 'seccats':
        insertSecCats()
    elif datatype == 'secslugs':
        insertSecSlugs()
    elif datatype == 'secswords':
        insertSecSwords()
    elif datatype == 'mentions':
        insertMentions()


def insertData(datatype):
    if datatype == 'coocs':
        insertSecPeers()
    elif datatype == 'seccats':
        insertSecCats()
    elif datatype == 'secslugs':
        insertSecSlugs()
    elif datatype == 'secswords':
        insertSecSwords()
    elif datatype == 'mentions':
        insertMentions()

def insertSecPeers():
    # MINIMUM COOC FREQUENCY: 2
    # SORT
    global entities2secpeers; global insert
    for aledaid in entities2secpeers:
        secpeers2counts = "#".join(map(lambda i: i[0]+":"+str(i[1]), filter(lambda i:i[1] > 1, sorted(entities2secpeers[aledaid].items(), key=lambda i:i[1], reverse=True))))
        if secpeers2counts != "":
            # print >> sys.stderr, "kbdb.tables['secpeers'].insertIntoTable(kbdbc, {'aledaid':"+aledaid+", 'secpeers':"+secpeers2counts+"})"
            # kbdb.tables['secpeers'].insertIntoTable(kbdbc, {'aledaid':aledaid, 'secpeers':secpeers2counts})
            sys.stdout.write("INSERT INTO secpeers (aledaid, secpeers) VALUES (%s, '%s');\n"%(aledaid, secpeers2counts))
            # sys.stdout.write(aledaid+"#coocs#"+secpeers2counts+"\n")
            insert += 1
            # insert = testCommit(insert, kbdb)

def insertSecCats():
    # NO MINIMUM CATS
    # SORT
    global entities2seccats; global insert
    for aledaid in entities2seccats:
        cats2counts = "#".join(map(lambda i: i[0]+":"+str(i[1]), sorted(entities2seccats[aledaid].items(), key=lambda i:i[1], reverse=True)))
        # print >> sys.stderr, "kbdb.tables['seccats'].insertIntoTable(kbdbc, {'aledaid':"+aledaid+", 'seccats':"+cats2counts+"})"
        # kbdb.tables['seccategories'].insertIntoTable(kbdbc, {'aledaid':aledaid, 'seccategories':cats2counts})
        sys.stdout.write("INSERT INTO seccategories (aledaid, seccategories) VALUES (%s, '%s');\n"%(aledaid, cats2counts))
        insert += 1
        # insert = testCommit(insert, kbdb)

def insertSecSlugs():
    # NO MINIMUM SLUGS
    # SORT
    global entities2secslugs; global insert
    for aledaid in entities2secslugs:
        slugs2counts = "#".join(map(lambda i: i[0]+":"+str(i[1]), sorted(entities2secslugs[aledaid].items(), key=lambda i:i[1], reverse=True)))
        # print >> sys.stderr, "kbdb.tables['secslugs'].insertIntoTable(kbdbc, {'aledaid':"+aledaid+", 'secslugs':"+slugs2counts+"})"
        # kbdb.tables['secslugs'].insertIntoTable(kbdbc, {'aledaid':aledaid, 'secslugs':slugs2counts})
        sys.stdout.write("INSERT INTO secslugs (aledaid, secslugs) VALUES (%s, '%s');\n"%(aledaid, slugs2counts))
        insert += 1
        # insert = testCommit(insert, kbdb)

def insertSecSwords():
    # MINIMUM SWORDS: 30 MOST FREQUENT
    # SORT
    global entities2secswords; global insert
    for aledaid in entities2secswords:
        swords2counts = "#".join(map(lambda i: i[0]+":"+str(i[1]), sorted(entities2secswords[aledaid].items(), key=lambda i:i[1], reverse=True)[0:len(entities2secswords[aledaid])]))
        # print >> sys.stderr, "kbdb.tables['secswords'].insertIntoTable(kbdbc, {'aledaid':"+aledaid+", 'secswords':"+swords2counts+"})"
        # kbdb.tables['secswords'].insertIntoTable(kbdbc, {'aledaid':aledaid, 'secswords':swords2counts})
        sys.stdout.write("INSERT INTO secswords (aledaid, secswords) VALUES (%s, '%s');\n"%(aledaid, swords2counts))
        insert += 1
        # insert = testCommit(insert, kbdb)

def insertMentions():
    global entities2mentions; global insert
    for aledaid in entities2mentions:
        mentions2counts = "#".join(map(lambda i: i[0]+":"+str(i[1]), sorted(entities2mentions[aledaid].items(), key=lambda i:i[1], reverse=True)))
        # print >> sys.stderr, "kbdb.tables['mentions'].insertIntoTable(kbdbc, {'aledaid':"+aledaid+", 'mentions':"+mentions2counts+"})"
        # kbdb.tables['mentions'].insertIntoTable(kbdbc, {'aledaid':aledaid, 'mentions':mentions2counts})
        sys.stdout.write('INSERT INTO mentions (aledaid, mentions) VALUES (%s, "%s");\n'%(aledaid, mentions2counts))
        insert += 1
        # insert = testCommit(insert, kbdb)

def fillData(aledaid, data, datatype):
    if datatype == 'coocs':
        fillSecPeers(aledaid, data, datatype)
    elif datatype == 'seccats':
        fillSecCats(aledaid, data, datatype)
    elif datatype == 'secslugs':
        fillSecSlugs(aledaid, data, datatype)
    elif datatype == 'secswords':
        fillSecSwords(aledaid, data, datatype)
    elif datatype == 'mentions':
        fillMentions(aledaid, data, datatype)
        
def fillSecPeers(aledaid, data, datatype):
    global entities2secpeers
    for peer, count in data:
        try: entities2secpeers[aledaid][peer] += int(count)
        except KeyError: entities2secpeers[aledaid][peer] = int(count)

def fillSecCats(aledaid, data, datatype):
    global entities2seccats
    for cat, count in data:
        try: entities2seccats[aledaid][cat] += int(count)
        except KeyError: entities2seccats[aledaid][cat] = int(count)

def fillSecSlugs(aledaid, data, datatype):
    global entities2secslugs
    for slug, count in data:
        try: entities2secslugs[aledaid][slug] += int(count)
        except KeyError: entities2secslugs[aledaid][slug] = int(count)

def fillSecSwords(aledaid, data, datatype):
    global entities2secswords
    for sword, count in data:
        try: entities2secswords[aledaid][sword] += int(count)
        except KeyError: entities2secswords[aledaid][sword] = int(count)

def fillMentions(aledaid, data, datatype):
    global entities2mentions
    d = entities2mentions[aledaid]
    for mention, count in data:
        try: d[mention] += int(count)
        except KeyError: d[mention] = int(count)


def getOptions():
    usage = '\n\n\t%prog [options] <articles>\n'
    datachoices = ["coocs", "seccats", "secslugs", "secswords", "mentions"]
    parser = OptionParser(usage)
    # parser.add_option("--kbdb", action="store", dest="kbdb", default=None, help="DB KB to populate")
    parser.add_option("--data", action="store", dest="datatype", type="choice", choices=datachoices, default=None, help="data type to merge [%s]"%(", ".join(datachoices)))
    try: (options, args) = parser.parse_args()
    except OptionError: parser.print_help(); sys.exit('OptionError')
    except: parser.print_help(); sys.exit('An error occured during options parsing\n')
    # if options.kbdb == None: parser.print_help(); sys.exit()
    if options.datatype not in datachoices: parser.print_help(); sys.exit()
    return options, args

if __name__ == "__main__":
    main()    

            
