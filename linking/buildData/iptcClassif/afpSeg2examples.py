#!/usr/bin/python
# -*- coding: utf-8 -*-
#import codecs
import re
import os
import sys
sys.path.append('@common_src@')
sys.path.append('@kb_src@')
from utils import *
from kbwp_utils import *
from collections import defaultdict
#from subprocess import *
from optparse import *
import codecs

def main():
    usage = 'cat <input> | %s --db <words DB>'%sys.argv[0]
    parser = OptionParser(usage)
    # parser.add_option("--stop", action="store", dest="stop", default=None, help="stoplist file")
    parser.add_option("--wordsdb", action="store", dest="db", default=None, help="words DB")
    try: (options, args) = parser.parse_args()
    except OptionError: sys.stderr.write('OptionError'); parser.print_help(); sys.exit()
    except: sys.stderr.write('An error occured during options parsing\n'); parser.print_help(); sys.exit()
    if options.db == None: parser.print_help(); sys.exit()

    global stop; stop = getStopWords()

    global db; global dbc
    db = DBConnection(options.db, getWordsDBtables())
    dbc = db.cursor

    in_news = False
    news_pattern_in = re.compile('<news_item ')
    news_pattern_out = re.compile('</news_item>')
    iptc_pattern = re.compile('<iptc_code value="([A-Z]{3})"/>')
    has_iptc = False # only take first iptc code if more than one => NO
    token_pattern1 = re.compile('<token')
    token_pattern2 = re.compile('<token[^<>]+>([^<>]+)</token>')
    # is_xml_pattern = re.compile('_XML\s*')
    p1 = re.compile('^\{')
    p2 = re.compile('\}$')
    p3 = re.compile(r"\\")
    dirty_words = re.compile('\s+|:')


    iptc = []
    sys.stdout = codecs.getwriter('utf8')(sys.stdout)
    sys.stdin = codecs.getreader("utf-8")(sys.stdin)
    for line in sys.stdin:
        line = line.rstrip()
        if news_pattern_in.search(line):
            in_news = True
            tokens2counts = defaultdict(int)
        elif news_pattern_out.search(line):
            for i in iptc:
                sys.stdout.write('0 '+i+' '+' '.join(map((lambda (w,c): '%s:%d'%(w,c)), tokens2counts.items()))+'\n')
            in_news = False
            has_iptc = False
            iptc = []
        # elif in_news and not has_iptc and iptc_pattern.search(line):
        elif in_news and iptc_pattern.search(line):
            iptc.append(iptc_pattern.search(line).group(1))
            has_iptc = True
        elif in_news and has_iptc and token_pattern1.search(line):
            # words = map((lambda w: normalize(w)), filter((lambda w: w not in stop and dirty_words.search(w) is None), line.split(' ')))
            tokens = selectTokens(re.findall(token_pattern2, line), stop)
            for token in tokens:
                # if not word.startswith('_') and word not in stop:
                db.tables['words2frequency'].selectFromTable(dbc, ['frequency'], [('word', '=', token)])
                res = dbc.fetchone()
                if res != None and res != '':
                    wpfreq = res[0]
                    if wpfreq > 2:
                        tokens2counts[token] += 1

    db.close()

# def normalize(w):
#     w = w.lower()
#     w = p1.sub('', p2.sub('', p3.sub('', w)))
#     return w

if __name__ == "__main__":
    main()    
