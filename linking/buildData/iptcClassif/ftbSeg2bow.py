#!/usr/bin/python
# -*- coding: utf-8 -*-
#import codecs
import re
import os
import sys
sys.path.append('@common_src@')
sys.path.append('@kb_src@')
from utils import *
from kbwp_utils import *
from collections import defaultdict
#from subprocess import *
from optparse import *
import codecs

def main():
    usage = 'cat <input> | %s --db <words DB>'%sys.argv[0]
    parser = OptionParser(usage)
    # parser.add_option("--stop", action="store", dest="stop", default=None, help="stoplist file")
    parser.add_option("--wordsdb", action="store", dest="db", default=None, help="words DB")
    try: (options, args) = parser.parse_args()
    except OptionError: sys.stderr.write('OptionError'); parser.print_help(); sys.exit()
    except: sys.stderr.write('An error occured during options parsing\n'); parser.print_help(); sys.exit()
    if options.db == None: parser.print_help(); sys.exit()

    global stop; stop = getStopWords()

    global db; global dbc
    db = DBConnection(options.db, getWordsDBtables())
    dbc = db.cursor

    in_doc = False
    docid = ""
    doc_pattern_in = re.compile('<document ')
    doc_pattern_out = re.compile('</document>')
    docid_pattern = re.compile('<document id="([^"]+)"')
    token_pattern1 = re.compile('<token')
    token_pattern2 = re.compile('<token[^<>]+>([^<>]+)</token>')

    sys.stdout = codecs.getwriter('utf8')(sys.stdout)
    sys.stdin = codecs.getreader("utf-8")(sys.stdin)
    for line in sys.stdin:
        line = line.rstrip()
        if doc_pattern_in.search(line):
            in_doc = True
            docid = docid_pattern.search(line).group(1)
            tokens2counts = defaultdict(int)
        elif doc_pattern_out.search(line):
            sys.stdout.write(docid+' C '+' '.join(map((lambda (w,c): '%s:%d'%(w,c)), tokens2counts.items()))+'\n')
            in_doc = False
        elif in_doc and token_pattern1.search(line):
            tokens = selectTokens(re.findall(token_pattern2, line), stop)
            for token in tokens:
                db.tables['words2frequency'].selectFromTable(dbc, ['frequency'], [('word', '=', token)])
                res = dbc.fetchone()
                if res != None and res != '':
                    wpfreq = res[0]
                    if wpfreq > 2:
                        tokens2counts[token] += 1

    db.close()

# def normalize(w):
#     w = w.lower()
#     w = p1.sub('', p2.sub('', p3.sub('', w)))
#     return w

if __name__ == "__main__":
    main()    
