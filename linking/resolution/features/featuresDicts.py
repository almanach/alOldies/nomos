    atomic_features = {
        1:{'values':range(0,5), 'name':'cand_doc_cats_sim'},#5-
        2:{'values':range(0,5), 'name':'cand_doc_slugs_sim'},#5-
        3:{'values':range(0,5), 'name':'cand_doc_swords_sim',},#5-
        4:{'values':range(0,3), 'name':'cand_title_mention_sim'},#3- (totally different, exact same, partially different)
        5:{'values':range(0,6), 'name':'mention_cand_amb_rate'},#6- (number of candidates for this mention)
        6:{'values':range(0,6), 'name':'cand_relative_frequency'},#6- (cand freq / sum all cand freq)
        7:{'values':range(0,2), 'name':'mention_in_aleda'},#2-
        8:{'values':range(0,3), 'name':'cand_types'},#3- (all in aleda = 1, none in aleda = 0, both in and not in aleda = 2)
        9:{'values':range(0,6), 'name':'cand_frequency'},#6-
        10:{'values':range(0,2), 'name':'cand_in_aleda'},#2-
        11:{'values':range(0,2), 'name':'cand_has_mention_in_aleda'},#2-
        12:{'values':range(0,5), 'name':'cand_aleda_type'},#4- (0:null, 1:pers, 2:loc, 3:org, 4:com)
        13:{'values':range(0,5), 'name':'cand_aleda_geonames_weight'},#5-
        14:{'values':range(0,5), 'name':'cand_aleda_wikipedia_weight'},#5-
        15:{'values':range(0,2), 'name':'mention_longer_present'},#2-
        16:{'values':range(0,5), 'name':'cand_aleda_variant_number'},#5-
        17:{'values':range(0,2), 'name':'cand_aleda_type_in_mention_types'},#2-
        18:{'values':range(0,2), 'name':'mention_is_longer'},#2-
        19:{'values':range(0,2), 'name':'cand_is_nil'},#2-
        20:{'values':range(0,2), 'name':'cand_is_nae'}}#2-
    #101: mean of (1,2,3)
    complex_features_1 = {101:{'f_to_use':(1,2,3), 'values':range(0,5), 'name':'cand_doc_sim'}}
    complex_features_2 = {
        102:{'f_to_use':(10,5), 'size':12, 'not_0':False},
        103:{'f_to_use':(10,6), 'size':12, 'not_0':False},
        104:{'f_to_use':(10,7), 'size':4, 'not_0':False},
        105:{'f_to_use':(10,8), 'size':6, 'not_0':False},
        106:{'f_to_use':(10,9), 'size':12, 'not_0':False},
        107:{'f_to_use':(11,5), 'size':6, 'not_0':True},
        108:{'f_to_use':(11,6), 'size':6, 'not_0':True},
        109:{'f_to_use':(11,7), 'size':2, 'not_0':True},
        110:{'f_to_use':(11,8), 'size':3, 'not_0':True},
        111:{'f_to_use':(11,9), 'size':6, 'not_0':True},
        112:{'f_to_use':(4,5), 'size':18, 'not_0':False},
        113:{'f_to_use':(4,6), 'size':18, 'not_0':False},
        114:{'f_to_use':(4,8), 'size':9, 'not_0':False},
        115:{'f_to_use':(4,9), 'size':18, 'not_0':False},
        116:{'f_to_use':(4,10), 'size':6, 'not_0':False},
        117:{'f_to_use':(4,15), 'size':6, 'not_0':False}}
    complex_features_3 = {
        118:{'f_to_use':(4,101), 'size':15, 'not_0':False},
        119:{'f_to_use':(12,101), 'size':20, 'not_0':True},
        120:{'f_to_use':(12,102), 'size':48, 'not_0':True},
        121:{'f_to_use':(12,103), 'size':48, 'not_0':True},
        122:{'f_to_use':(12,104), 'size':16, 'not_0':True},
        123:{'f_to_use':(12,105), 'size':24, 'not_0':True},
        124:{'f_to_use':(12,106), 'size':48, 'not_0':True},
        125:{'f_to_use':(12,107), 'size':24, 'not_0':True},
        126:{'f_to_use':(12,108), 'size':24, 'not_0':True},
        127:{'f_to_use':(12,109), 'size':8, 'not_0':True},
        128:{'f_to_use':(12,110), 'size':12, 'not_0':True},
        129:{'f_to_use':(12,111), 'size':24, 'not_0':True},
        130:{'f_to_use':(12,112), 'size':72, 'not_0':True},
        131:{'f_to_use':(12,113), 'size':72, 'not_0':True},
        132:{'f_to_use':(12,114), 'size':36, 'not_0':True},
        133:{'f_to_use':(12,115), 'size':72, 'not_0':True},
        134:{'f_to_use':(12,116), 'size':24, 'not_0':True},
        135:{'f_to_use':(12,117), 'size':60, 'not_0':True}}
    complex_features_4 = {136:{'f_to_use':(12,118), 'size':16, 'not_0':True}}
