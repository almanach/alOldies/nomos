#!/bin/bash
#==========================================================================================
# ACTIONS AND ARGS
## TO BUILD GOLD NEWS DIR FROM NEWS XML CORPUS
buildgold=0
gold_corpus="null"
conf="/Users/triantafillo/dev/alpc/sxpipe/sxpipe-afp-nomos.conf"
## IF GOLD NEWS DIR ALREADY BUILT
golddir="null"
# TO BUILD NOMOS FILES FROM RAW NEWS
buildnomos="null"
# IF NOMOS FILES ALREADY BUILT
nomosdir="null"
# TO TRAIN
train=0
learn="megam"
bernoulli_implicit=1
non_bernoulli_implicit=0
fclasses="fclasses"
features="/Users/triantafillo/dev/alpc/nomos/linking/resolution/features"
# DATA BASES
aledadir="/usr/local/lib/aleda/"
#entities="/usr/local/lib/nomos/entities.dat"
#words="/usr/local/lib/nomos/words.dat"
entities="/Users/triantafillo/dev/alpc/nomos/linking/data/wikipedia/data4training/kbwp.dat"
words="/Users/triantafillo/dev/alpc/nomos/linking/data/wikipedia/data4training/wpwords.dat"
# SCRIPTS
scripts="/usr/local/share/nomos/build/linking/resolution"
# NER AND NOMOS BEHAVIOUR OPTIONS
goldner="null"
goldneroption=""
nonae=""


echousage(){
echo "Options and arguments: 
    [[--buildgold <gold_corpus> --conf <sxpipe conf file>] | --gold <golddir>]
    [--buildnomos <input_newsdir> | --nomosdir <nomosdir>]
    [--goldner <gold NER dir>]
    --data <datadir>
    --features <features file>
    [--train [[--bernoulli-implicit --fclasses <fclasses-file-basename>] | --non-bernoulli-implicit]
    --learn [megam | admirable] [default megam] ]

    --goldneroption 
    --nonae

    --entities <entities_db> (/Users/triantafillo/dev/alpc/nomos/linking/data/wikipedia/data4training/kbwp.dat)
    --words <words_db> (/Users/triantafillo/dev/alpc/nomos/linking/data/wikipedia/data4training/wpwords.dat)
    --aledadir <aledadir (/usr/local/lib/aleda/)>
    --scripts <scripts path (/usr/local/share/nomos/build/linking/resolution)>
"
}

echoargs(){
echo "Options and arguments: 
    [[--buildgold $gold_corpus --conf $conf] | --gold $golddir]
    [--buildnomos $buildnomos | --nomosdir $nomosdir]
    [--goldner $goldner]
    --data >$datadir<
    --features $features

    [--train $train
        [ [--bernoulli-implicit $bernoulli_implicit --fclasses $fclasses] 
        |
        --non-bernoulli-implicit $non_bernoulli_implicit] 
    --learn $learn
    ]

    --goldneroption $goldneroption
    --nonae $nonae

    --entities $entities
    --words $words
    --aledadir $aledadir
    --scripts $scripts
"
}


if [ "$1" = "" ] || [ "$1" = "--help" ] || [ "$1" = "-help" ] || [ "$1" = "-h" ]; then 
    echousage
    exit
fi
# GET OPTIONS
while [ "$1" != "" ]
do
    case $1 in
	--buildgold )
	    buildgold=1
	    shift
	    gold_corpus=$1
	    ;;
	--conf )
	    shift
	    conf=$1
	    ;;
	--gold )
	    shift
	    golddir=$1
	    ;;
        --buildnomos )           
	    shift
	    buildnomos=$1
	    ;;
	--nomosdir )
	    shift
	    nomosdir=$1
	    ;;
        --train )           
	    train=1
	    ;;
        --learn )
            shift
	    learn=$1
	    ;;
        --non-bernoulli-implicit )           
	    non_bernoulli_implicit=1
	    ;;
        --bernoulli-implicit )           
	    bernoulli_implicit=1
	    ;;
        --fclasses )  
            shift
	    fclasses=$1
	    ;;
        --features )  
            shift
	    features=$1
	    ;;
	--data )
	    shift
	    datadir=$1
	    ;;
	--goldner )
	    shift
	    goldner=$1
	    goldneroption="--goldner"
	    ;;
	--goldneroption )
	    goldneroption="--goldner"
	    ;;
	--nonae )
	    nonae="--nonae"
	    ;;
	--aledadir )
	    shift
	    aledadir=$1
	    ;;
	--entities )
	    shift
	    entities=$1
	    ;;
	--words )
	    shift
	    words=$1
	    ;;
	--scripts )
	    shift
	    scripts=$1
	    ;;
    esac
    shift
done

# ECHO USAGE AND INSTANCIATED ARGS
echoargs

#==========================================================================================
# CREATE DATA DIR
mkdir -v $datadir

#==========================================================================================
# BUILD GOLD
if [ $buildgold == "1" ]; then
    mkdir -v $datadir/gold
    echo "========================================================================="
    echo "Building gold news from $gold_corpus to $datadir/gold..."
    echo "========================================================================="
    python $scripts/gold2news.py --dir $datadir/gold $gold_corpus --conf $conf
    if [ $? != "0" ]; then
	exit
    fi
    # SET GOLD DIR PATH
    golddir=$datadir/gold
fi

#==========================================================================================
# IF DON'T BUILD NOMOS: SET NOMOS DIR PATH
if [ $nomosdir == "null" ]; then
    nomosdir=$datadir/nomos
    mkdir -v $nomosdir
fi
# IF BUILD NOMOS
if [ $buildnomos != "null" ]; then
    echo "========================================================================="
    echo "Building nomos files from $buildnomos to $nomosdir (goldner: $goldner)..."
    echo "========================================================================="
    for file in $buildnomos/*.xml
    do 
	input=$(basename $file)
	nomos_filename=${input/.xml/.nomos.xml}
	nomos=$nomosdir/$nomos_filename
	echo "Building $nomos..."
	if [ $goldner == "null" ]; then
	    echo "Build Nomos: $file > $nomos"
	    python $scripts/buildNomos.py --words $words --aledadir $aledadir -o $nomos $file
	    if [ $? != "0" ]; then
		exit
	    fi
	else
	    goldnerfile="$goldner/$nomos_filename"
	    goldnerfile=${goldnerfile/.nomos.xml/.gold.xml}
	    echo "Build Nomos: $file + $goldner > $nomos"
	    python $scripts/buildNomos.py --words $words --aledadir $aledadir --goldner $goldnerfile -o $nomos $file
	    if [ $? != "0" ]; then
		exit
	    fi
	fi
    done
fi

#==========================================================================================
# TRAIN
# IF BERNOULLI IMPLICIT: MEGAM INPUT HAS TO BE BUILT ONCE IN NON-BERNOULLI-IMPLICIT, THEN GET FCLASSES WITH CLASSIFYFEATURES, THEN DO MEGAM INPUT WITH FCLASSES
if [ $train == "1" ]; then
    echo "========================================================================="
    echo "Features acquisition and Training..."
    echo "========================================================================="
    mkdir -v $datadir/$learn.data
    for i in {1..10}
    do
	mkdir -v $datadir/$learn.data/${i}
	ith_golddir=${golddir}/${i}
	echo "Building $learn input from $ith_golddir and $nomosdir..."
	# NAME MEGAM / ADMIRABLE INPUT TEMP FILE AND FILE
	temp_data_file=${datadir}/$learn.data/${i}/$learn.input.temp
	if [ "$learn" == "megam" ]; then
	    data_file=${datadir}/megam.data/${i}/megam.input
	elif [ "$learn" == "admirable" ]; then
	    data_file_train=${datadir}/admirable.data/${i}/admirable.train.input
	    data_file_test=${datadir}/admirable.data/${i}/admirable.test.input
	    data_file_dev=${datadir}/admirable.data/${i}/admirable.dev.input
	fi
	# CASE MEGAM / ADMIRABLE INPUT IS NON BERNOULLI IMPLICIT: WRITE "FEATURE VALUE"
	echo "Writing $learn input..." # ONLY FOR MEGAM - NOT FOR ADMIRABLE
	if [ $non_bernoulli_implicit == "1" ]; then
	    echo "Features from $golddir + $nomosdir > $temp_data_file (NBI)"
	    python $scripts/buildLinkInstances.py $goldneroption \
		--features $features \
		--golddir $ith_golddir \
		--nomosdir $nomosdir \
		--entities $entities \
		--aledadir $aledadir > $temp_data_file
    	    if [ $? != "0" ]; then
    		exit
    	    fi
	    echo "Converting $temp_data_file to $data_file..."
	    if [ "$nonae" == "--nonae" ]; then
		# DON'T WRITE NAE EXAMPLES
		grep '#train' $temp_data_file | grep -v "_NAE#" | cut -f4,5 -d '#' | perl -pe 's/#/\t/' > $data_file
		echo "DEV" >> $data_file
		grep '#dev' $temp_data_file | grep -v "_NAE#" | cut -f4,5 -d '#' | perl -pe 's/#/\t/' >> $data_file
		echo "TEST" >> $data_file
		grep '#test' $temp_data_file | grep -v "_NAE#" | cut -f4,5 -d '#' | perl -pe 's/#/\t/' >> $data_file
	    else
		grep '#train' $temp_data_file | cut -f4,5 -d '#' | perl -pe 's/#/\t/' > $data_file
		echo "DEV" >> $data_file
		grep '#dev' $temp_data_file | cut -f4,5 -d '#' | perl -pe 's/#/\t/' >> $data_file
		echo "TEST" >> $data_file
		grep '#test' $temp_data_file | cut -f4,5 -d '#' | perl -pe 's/#/\t/' >> $data_file
	    fi
    	    echo "Running Megam training (non-bernoulli-implicit)..."
    	    megam.opt -fvals binary ${data_file} > ${datadir}/megam.data/megam.weights 2> ${datadir}/megam.data/megam.train.log
    	    if [ $? != "0" ]; then
		echo "Error in Megam training"
    		exit
    	    fi
    	    echo "Running Megam evaluation..."
    	    megam.opt -fvals -predict ${datadir}/megam.data/megam.weights binary ${data_file} > ${datadir}/megam.data/megam.eval.log 2>&1
    	    if [ $? != "0" ]; then
    		exit
    	    fi
	elif [ $bernoulli_implicit == "1" ];then
	    # GET NAME OF FCLASSES TO TEST EXISTENCE
	    fclasses_file=$datadir/$learn.data/${i}/$fclasses
	    # IF EXISTS, CAN DIRECTLY LAUNCH BERNOULLI IMPLICIT FEATURE BUILDING
	    if [ -e $fclasses_file ]; then
		echo "Features from $golddir + $nomosdir > $temp_data_file (BI)"
	    	time python $scripts/buildLinkInstances.py $goldneroption \
		    --features $features \
		    --golddir $ith_golddir \
		    --nomosdir $nomosdir \
		    --entities $entities \
		    --aledadir $aledadir \
		    --learn $learn \
		    --fclasses $fclasses_file > $temp_data_file
    	    	if [ $? != "0" ]; then
    	    	    exit
    	    	fi
	    else 
		# IF FCLASSES DOESN'T EXIST, MUST CREATE IF FROM NON BERNOULLI IMPLICIT RUN, WHICH WILL WRITE FVALUES, READ BY CLASSIFY SCRIPT TO GET FCLASSES
	    	echo "Building Fvalues / Fclasses from $ith_golddir and $nomosdir..."
	    	feature_values=${datadir}/$learn.data/${i}/feature_values
		echo "Features from $golddir + $nomosdir > $temp_data_file (NBI)"
		# CALL BUILLINKINSTANCES IN NBI AND MEGAM LEARN ANYWAY
	    	time python $scripts/buildLinkInstances.py $goldneroption \
		    --features $features \
		    --golddir $ith_golddir \
		    --nomosdir $nomosdir \
		    --entities $entities \
		    --aledadir $aledadir > $feature_values
    	    	if [ $? != "0" ]; then
    	    	    exit
    	    	fi
	    	echo "Making bernoulli-implicit features from fclasses to $temp_data_file..."
	    	$scripts/classifyFeatures.py --fclasses $fclasses_file $feature_values --learn $learn > $temp_data_file
    	    	if [ $? != "0" ]; then
    	    	    exit
    	    	fi
	    fi
	    if [ "$learn" == "megam" ]; then
		echo "Converting $temp_data_file to $data_file..."
		if [ "$nonae" == "--nonae" ]; then
                    # DON'T WRITE NAE EXAMPLES
		    grep '#train' $temp_data_file | grep -v "#newline" | grep -v '_NAE#' | cut -f4,5 -d '#' | perl -pe 's/#/\t/' > $data_file
		    echo "DEV" >> $data_file
		    grep '#dev' $temp_data_file | grep -v "#newline" | grep -v '_NAE#' | cut -f4,5 -d '#' | perl -pe 's/#/\t/' >> $data_file
		    echo "TEST" >> $data_file
		    grep '#test' $temp_data_file | grep -v "#newline" | grep -v '_NAE#' | cut -f4,5 -d '#' | perl -pe 's/#/\t/' >> $data_file
		else
		    grep '#train' $temp_data_file | grep -v "#newline" | cut -f4,5 -d '#' | perl -pe 's/#/\t/' > $data_file
		    echo "DEV" >> $data_file
		    grep '#dev' $temp_data_file | grep -v "#newline" | cut -f4,5 -d '#' | perl -pe 's/#/\t/' >> $data_file
		    echo "TEST" >> $data_file
		    grep '#test' $temp_data_file | grep -v "#newline" | cut -f4,5 -d '#' | perl -pe 's/#/\t/' >> $data_file
		fi

    		echo "Running Megam training (bernoulli-implicit)..."
    		megam.opt binary ${data_file} > ${datadir}/megam.data/${i}/megam.weights 2> ${datadir}/megam.data/${i}/megam.train.log
    		if [ $? != "0" ]; then
    		    exit
    		fi
    		echo "Running Megam evaluation..."
    		megam.opt -predict ${datadir}/megam.data/${i}/megam.weights binary ${data_file} > ${datadir}/megam.data/${i}/megam.eval.log 2>&1
    		if [ $? != "0" ]; then
    		    exit
    		fi
		if [ "$learn" == "megam" ]; then
		    tail -n 1 ${datadir}/megam.data/${i}/megam.eval.log
		fi
	    elif [ "$learn" == "admirable" ]; then
		echo "Converting $temp_data_file to $data_file_train and $data_file_test..."
		if [ "$nonae" == "--nonae" ]; then
                    # DON'T WRITE NAE EXAMPLES
		    grep '#train' $temp_data_file | grep -v '_NAE#' | cut -f4,5 -d '#' | perl -pe 's/^1#/11#/; s/^0#/1#/; s/^11#/0#/;s/#/\t/;s/\t+/\t/g' > $data_file_train
		    grep '#dev' $temp_data_file | grep -v '_NAE#' | cut -f4,5 -d '#' | perl -pe 's/^1#/11#/; s/^0#/1#/; s/^11#/0#/;s/#/\t/;s/\t+/\t/g' > $data_file_dev
		    grep '#test' $temp_data_file | grep -v '_NAE#' | cut -f4,5 -d '#' | perl -pe 's/^1#/11#/; s/^0#/1#/; s/^11#/0#/;s/#/\t/;s/\t+/\t/g' > $data_file_test
		else
		    grep '#train' $temp_data_file | cut -f4,5 -d '#' | perl -pe 's/^1#/11#/; s/^0#/1#/; s/^11#/0#/;s/#/\t/;s/\t+/\t/g' > $data_file_train
		    grep '#dev' $temp_data_file | cut -f4,5 -d '#' | perl -pe 's/^1#/11#/; s/^0#/1#/; s/^11#/0#/;s/#/\t/;s/\t+/\t/g' > $data_file_dev
		    grep '#test' $temp_data_file | cut -f4,5 -d '#' | perl -pe 's/^1#/11#/; s/^0#/1#/; s/^11#/0#/;s/#/\t/;s/\t+/\t/g' > $data_file_test
		fi
		echo "Converting data for Admirable"
		count_by_instance < $data_file_train > ${datadir}/admirable.data/${i}/features2counts
		data_file_train_num=${data_file_train/\.input/.input.num}
		data_file_dev_num=${data_file_dev/\.input/.input.num}
		data_file_test_num=${data_file_test/\.input/.input.num}
		filter_and_map ${datadir}/admirable.data/${i}/features2counts 0 < $data_file_train > $data_file_train_num
		filter_and_map ${datadir}/admirable.data/${i}/features2counts 0 < $data_file_dev > $data_file_dev_num
		filter_and_map ${datadir}/admirable.data/${i}/features2counts 0 < $data_file_test > $data_file_test_num
		echo "Running Admirable training (bernoulli-implicit)"
		ranker-learn --train $data_file_train_num --test $data_file_test_num --dev $data_file_dev_num > ${datadir}/admirable.data/${i}/features2weights 2> ${datadir}/admirable.data/${i}/admirable.train.log
		# ranker-learn --train $data_file_train_num --test $data_file_test_num --iter 5 --clip 0.001 > ${datadir}/admirable.data/${i}/features2weights 2> ${datadir}/admirable.data/${i}/admirable.train.log
		# echo "Running Admirable prediction"
		echo "Converting Admirable data to model"
		$scripts/getAdmirableModel.pl ${datadir}/admirable.data/${i}/features2counts ${datadir}/admirable.data/${i}/features2weights > ${datadir}/admirable.data/${i}/admirable.weights
	    fi
	fi
    done
fi


mkdir -v $datadir/nomos_results/
for i in {1..10}
do
    mkdir -v $datadir/nomos_results/${i}
    mkdir -v $datadir/nomos_results/${i}/dev
    mkdir -v $datadir/nomos_results/${i}/test
    echo "" > $datadir/nomos_results/${i}/test_resolution.log
    echo "" > $datadir/nomos_results/${i}/dev_resolution.log
done
mkdir -v $datadir/evalResults
echo "" > $datadir/evalResults/eval_dev_all.log
echo "" > $datadir/evalResults/eval_dev_all.results.log
echo "" > $datadir/evalResults/eval_test_all.log
echo "" > $datadir/evalResults/eval_test_all.results.log

#==========================================================================================
# RESOLVE
for x in "dev" "test"
do
    echo "========================================================================="
    echo "Resolving $x..."
    echo "========================================================================="
    for i in {1..10}
    do
	for gold_file in `ls $golddir/${i}/$x/*.xml`
	do
	    fclasses_file=$datadir/$learn.data/${i}/fclasses
	    gold_file_name=`basename $gold_file`
	    nomos_file=${gold_file_name/.gold.xml/.nomos.xml}
	    res=$datadir/nomos_results/${i}/$x/$nomos_file
	    nomos_file=$nomosdir/$nomos_file
	    res=${res/.nomos.xml/.nomos.resolved.xml}
	    filename=`basename "$file"`
	    echo "Nomos file: $nomos_file"
	    echo "Resolved nomos file: $res"
	    if [ $non_bernoulli_implicit == "1" ]; then 
		python $scripts/resolve.py $nonae \
		    --features $features \
		    --non-bernoulli-implicit \
		    --weights ${datadir}/$leanr.data/${i}/$learn.weights \
		    --entities $entities \
		    --aledadir $aledadir \
		    --log $datadir/nomos_results/${i}/${x}_resolution.log \
		    $nomos_file \
		    | perl -pe 's/(<candidate[^<>]+\/>)/          $1\n/g' > $res
	    elif [ $bernoulli_implicit == "1" ]; then 
		python $scripts/resolve.py $nonae \
		    --features $features \
	    	    --weights ${datadir}/$learn.data/${i}/$learn.weights \
	    	    --fclasses $fclasses_file \
	    	    --entities $entities \
	    	    --aledadir $aledadir \
	    	    --log $datadir/nomos_results/${i}/${x}_resolution.log \
	    	    $nomos_file \
	    	    | perl -pe 's/(<candidate[^<>]+\/>)/          $1\n/g' > $res
	    fi
	    if [ $? != "0" ]; then
		exit
	    fi
	done
    done
done
    
mkdir -v $datadir/nomos_results/all/
for x in "dev" "test"
do
    echo "========================================================================="
    echo "Evaluation $x..."
    echo "========================================================================="
    mkdir -v $datadir/nomos_results/all/$x
    for i in {1..10}
    do
	cp $datadir/nomos_results/${i}/$x/*.xml $datadir/nomos_results/all/$x
	echo "Evaluation $x from $golddir/all + $datadir/nomos_results/$i > $datadir/evalResults/eval_${x}_${i}.results.log"
	python $scripts/evaluate.py $nonae $goldneroption \
	    --gold $golddir/all/ \
	    --nomos $datadir/nomos_results/${i}/$x \
	    --log $datadir/evalResults/eval_${x}_${i}.log > $datadir/evalResults/eval_${x}_${i}.results.log
    done
    echo "========================================================================="
    echo "Evaluation $x - All from $golddir/all + $datadir/nomos_results/all/$x ... > ${datadir}/evalResults/eval_${x}_all.results.log"
    python $scripts/evaluate.py $nonae $goldneroption \
	--gold $golddir/all/ \
	--nomos $datadir/nomos_results/all/$x \
	--log $datadir/evalResults/eval_${x}_all.log > $datadir/evalResults/eval_${x}_all.results.log
    if [ $? != "0" ]; then
	exit
    fi
    echo "Results all $x:"
    cat ${datadir}/evalResults/eval_${x}_all.results.log
done
