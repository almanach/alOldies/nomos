#!/bin/bash
#==========================================================================================
# ACTIONS AND ARGS
## TO BUILD GOLD NEWS DIR FROM NEWS XML CORPUS
buildgold=0
gold_corpus="null"
conf="/Users/triantafillo/dev/alpc/sxpipe/sxpipe-afp-nomos.conf"
## IF GOLD NEWS DIR ALREADY BUILT
golddir="null"
# TO BUILD NOMOS FILES FROM RAW NEWS
buildnomos="null"
# IF NOMOS FILES ALREADY BUILT
nomosdir="null"
# TO TRAIN
train=0
resolve="1"
learn="megam"
iter=10
bernoulli_implicit=0
non_bernoulli_implicit=1
features="/usr/local/share/nomos/features"
fclasses="/usr/local/share/nomos/fclassesGafp"
fvals=""
# DATA BASES
#aledadir="/usr/local/lib/aleda/"
aledadir="/usr/local/share/aleda/"
kb="/usr/local/share/nomos/kbwp.dat"
words="/usr/local/share/nomos/frwikiwords.dat"
# SCRIPTS
scripts="/usr/local/share/nomos/build/linking/resolution/src"
# NER AND NOMOS BEHAVIOUR OPTIONS
goldner="null"
goldneroption=""
nonae=""
fold=10


echousage(){
echo "Options and arguments: 
    [
        [--buildgold <gold_corpus> --conf <sxpipe conf file>]
        |
        --golddir <golddir>
    ]
    [
    --buildnomos <input_newsdir>
    |
    --nomosdir <nomosdir>
    ]
    [--goldner <gold NER dir>]
    --data <datadir>
    --fold <fold> [default 10]
    --features <features file> [default $features]
    --fvals <fvals file> [default $fvals]
    [--train
        [
            [--bi --fclasses <fclasses-file> [default $fclasses]]
            | --nbi
        ]
        --learn [megam | admirable] [default megam]
        [--iter [default 10]
    ]
    --resolve [default 1]
    --goldneroption 
    --nonae
    --kb <kb_db> [default $kb]
    --words <words_db> [default $words]
    --aledadir <aledadir> [default $aledadir]
    --scripts <scripts path> [default $scripts]
"
}

echoargs(){
echo "Options and arguments: 
    [
        [--buildgold $gold_corpus --conf $conf]
        |
        --golddir $golddir
    ]
    [
    --buildnomos $buildnomos
    |
    --nomosdir $nomosdir
    ]
    [--goldner $goldner]
    --data $datadir
    --fold $fold
    --features $features
    --fvals $fvals
    [--train $train
        [
            [--bi $bernoulli_implicit --fclasses $fclasses]
            | --nbi $non_bernoulli_implicit
        ]
        --learn $learn
        --iter $iter
    ]
    --resolve $resolve
    --goldneroption $goldneroption
    --nonae: >$nonae<
    --kb $kb
    --words $words
    --aledadir $aledadir
    --scripts $scripts
"
}


if [ "$1" = "" ] || [ "$1" = "--help" ] || [ "$1" = "-help" ] || [ "$1" = "-h" ]; then 
    echousage
    exit
fi
# GET OPTIONS
while [ "$1" != "" ]
do
    case $1 in
	--buildgold )
	    buildgold=1
	    shift
	    gold_corpus=$1
	    ;;
	--conf )
	    shift
	    conf=$1
	    ;;
	--golddir )
	    shift
	    golddir=$1
	    ;;
        --buildnomos )           
	    shift
	    buildnomos=$1
	    ;;
	--nomosdir )
	    shift
	    nomosdir=$1
	    ;;
        --train )           
	    train=1
	    ;;
        --resolve )
            shift
	    resolve=$1
	    ;;
        --learn )
            shift
	    learn=$1
	    ;;
        --iter )
            shift
	    iter=$1
	    ;;
        --nbi )           
	    non_bernoulli_implicit=1
	    bernoulli_implicit=0
	    ;;
        --bi )           
	    bernoulli_implicit=1
	    non_bernoulli_implicit=0
	    ;;
        --fclasses )  
            shift
	    fclasses=$1
	    ;;
        --features )  
            shift
	    features=$1
	    ;;
        --fvals )  
            shift
	    fvals=$1
	    ;;
	--data )
	    shift
	    datadir=$1
	    ;;
	--fold )
	    shift
	    fold=$1
	    ;;
	--goldner )
	    shift
	    goldner=$1
	    goldneroption="--goldner"
	    ;;
	--goldneroption )
	    goldneroption="--goldner"
	    ;;
	--nonae )
	    nonae="--nonae"
	    ;;
	--aledadir )
	    shift
	    aledadir=$1
	    ;;
	--kb )
	    shift
	    kb=$1
	    ;;
	--words )
	    shift
	    words=$1
	    ;;
	--scripts )
	    shift
	    scripts=$1
	    ;;
    esac
    shift
done

# ECHO USAGE AND INSTANCIATED ARGS
echoargs


#==========================================================================================
# CREATE DATA DIR
mkdir -v $datadir

#==========================================================================================
# BUILD GOLD
if [ $buildgold == "1" ]; then
    mkdir -v $datadir/gold
    echo "========================================================================="
    echo "Building gold news from $gold_corpus to $datadir/gold..."
    echo "========================================================================="
    # python $scripts/gold2news.py --dir $datadir/gold $gold_corpus --conf $conf
    # CHANGE PATH!!
    if [ $? != "0" ]; then
	exit
    fi
    # SET GOLD DIR PATH
    golddir=$datadir/gold
fi

#==========================================================================================
# IF DON'T BUILD NOMOS: SET NOMOS DIR PATH
if [ $nomosdir == "null" ]; then
    nomosdir=$datadir/nomos
    mkdir -v $nomosdir
fi
# IF BUILD NOMOS
if [ $buildnomos != "null" ]; then
    echo "========================================================================="
    echo "Building nomos files from $buildnomos to $nomosdir (goldner: $goldner)..."
    echo "========================================================================="
    for file in $buildnomos/*.xml
    do 
	input=$(basename $file)
	nomos_filename=${input/.xml/.nomos.xml}
	nomos=$nomosdir/$nomos_filename
	echo "Building $nomos..."
	if [ $goldner == "null" ]; then
	    echo "Build Nomos: $file > $nomos"
	    python $scripts/buildNomos.py --words $words --aledadir $aledadir -o $nomos $file
	    if [ $? != "0" ]; then
		exit
	    fi
	else
	    goldnerfile="$goldner/$nomos_filename"
	    goldnerfile=${goldnerfile/.nomos.xml/.gold.xml}
	    echo "Build Nomos: $file + $goldner > $nomos"
	    python $scripts/buildNomos.py --words $words --aledadir $aledadir --goldner $goldnerfile -o $nomos $file
	    if [ $? != "0" ]; then
		exit
	    fi
	fi
    done
fi

#==========================================================================================
# TRAIN
if [ $train == "1" ]; then
    echo "========================================================================="
    echo "Features acquisition and Training..."
    echo "========================================================================="
    mkdir -v $datadir/$learn.data
    for i in $(eval echo "{1..$fold}")
    do
	mkdir -v $datadir/$learn.data/$i
    done
    if [ ! -e $fvals ]; then
	temp_data_file=${datadir}/$learn.data/$learn.input.temp
	feature_values=${datadir}/$learn.data/feature_values
        # GET ALL FEATURE-VALUES, UNLESS EXISTS, OR UNLESS FCLASSES EXISTS AND BERNOULILI-IMPLICIT
        # CASE NBI: TEMP DATA FILE = FEATURE-VALUES FILE
	echo "Writing $learn input..." # FOR FEATURE-VALUES, NO LEARN OPTION SPECIFIED (DEFAULT TO MEGAM)
	if [ $non_bernoulli_implicit == "1" ]; then
	    echo "Features from $golddir + $nomosdir > $temp_data_file (NBI)"
	    python $scripts/buildLinkInstances.py $goldneroption \
		--features $features \
		--golddir $golddir/all \
		--nomosdir $nomosdir \
		--kb $kb \
		--aledadir $aledadir > $temp_data_file
    	    if [ $? != "0" ]; then
    		exit
    	    fi
	elif [ $bernoulli_implicit == "1" ];then
            # fclasses_file=$datadir/$learn.data/$fclasses
	    # IF EXISTS, CAN DIRECTLY LAUNCH BERNOULLI IMPLICIT FEATURE BUILDING
	    if [ -e $fclasses ]; then
		echo "Features from $golddir + $nomosdir > $temp_data_file (BI)"
		time python $scripts/buildLinkInstances.py $goldneroption \
		    --features $features \
		    --golddir $golddir/all \
		    --nomosdir $nomosdir \
		    --kb $kb \
		    --aledadir $aledadir \
		    --learn $learn \
		    --fclasses $fclasses > $temp_data_file
    		if [ $? != "0" ]; then
    	    	    exit
    		fi
	    elif [ ! -e $fclasses ]; then
	        # IF NOT, BUILD FEATURE-VALUES UNLESS EXISTS
		if [ ! -f $feature_values ]; then
		    echo "Building Fvalues / Fclasses from $ith_golddir and $nomosdir..."
		    echo "Features from $golddir + $nomosdir > $temp_data_file (NBI FOR BI)"
		    python $scripts/buildLinkInstances.py $goldneroption \
			--features $features \
			--golddir $golddir/all \
			--nomosdir $nomosdir \
			--kb $kb \
			--aledadir $aledadir > $feature_values
    		    if [ $? != "0" ]; then
    			exit
    		    fi
		fi
	        # MAKE BI FEATURES FROM FEATURE-VALUES 
	        echo "Making fclasses..."
	        echo "Making $temp_data_file..."
	        $scripts/classifyFeatures.py --features $features --fclasses $fclasses $feature_values --learn $learn > $temp_data_file
    		if [ $? != "0" ]; then
    	    	    exit
    		fi
	    fi
	fi
    elif [ -e $fvals ]  && [ $bernoulli_implicit == "1" ] && [ ! -e $fclasses ]; then
        # MAKE BI FEATURES FROM FEATURE-VALUES 
	echo "Making fclasses..."
	echo "Making $temp_data_file..."
	$scripts/classifyFeatures.py --features $features --fclasses $fclasses $feature_values --learn $learn > $temp_data_file
    	if [ $? != "0" ]; then
    	    exit
    	fi
    elif [ -e $fvals ]; then
	temp_data_file=$fvals
    fi
    # FOR EACH FOLD, BUILD DATA FILE FROM GENERAL TEMP DATA FILE WITH TRAIN/DEV/TEST SPLIT
    for i in $(eval echo "{1..$fold}")
    do
	echo "============"
	echo "Training $i"
	echo "============"
	ith_golddir=$golddir/$i
	# BUILD DATA FILE FOR ITH FOLD WITH GOLD TRAIN/DEV/TEST SPLIT
	echo "Making examples to data sets $ith_golddir $temp_data_file  => $datadir/$learn.data/${i}"
	python $scripts/examples2datasets.py $ith_golddir $temp_data_file $datadir/$learn.data/${i}
	# OUTPUT: train/dev/test.input.temp IN EACH FOLD
	# IF MEGAM:
	# MOVE TEMPS TO MEGAM ITH FOLD DATA FILE WITH DEV AND TEST LINES
	if [ "$learn" == "megam" ]; then
	    data_file=$datadir/$learn.data/$i/megam.input
	    echo "Moving data to $data_file"
	    if [ "$nonae" == "--nonae" ]; then
		grep -v '_NAE#' $datadir/$learn.data/${i}/train.input.temp | cut -f4,5 -d '#' | perl -pe 's/#\s$/\n/;s/#next\s*$/\n/;s/#/\t/g' > $data_file
		# echo "DEV" >> $data_file
		# grep -v '_NAE#' $datadir/$learn.data/${i}/dev.input.temp | cut -f4,5 -d '#' | perl -pe 's/#\s$/\n/;s/#next\s*$/\n/;s/#/\t/g' >> $data_file
		echo "TEST" >> $data_file
		grep -v '_NAE#' $datadir/$learn.data/${i}/test.input.temp | cut -f4,5 -d '#' | perl -pe 's/#\s$/\n/;s/#next\s*$/\n/;s/#/\t/g' >> $data_file
	    else
		cut -f4,5 -d '#'  $datadir/$learn.data/${i}/train.input.temp | perl -pe 's/#\s$/\n/;s/#next\s*$/\n/;s/#/\t/g' > $data_file
		# echo "DEV" >> $data_file
		# cut -f4,5 -d '#'  $datadir/$learn.data/${i}/dev.input.temp | perl -pe 's/#\s$/\n/;s/#next\s*$/\n/;s/#/\t/g' >> $data_file
		echo "TEST" >> $data_file
		cut -f4,5 -d '#' $datadir/$learn.data/${i}/test.input.temp | perl -pe 's/#\s$/\n/;s/#next\s*$/\n/;s/#/\t/g' >> $data_file
	    fi
	    # RUN MEGAM TRAINNING AND EVALUATION, BI OR NBI
	    if [ $bernoulli_implicit == "1" ]; then
    		echo "Running Megam training (bernoulli-implicit)..."
    		megam.opt binary ${data_file} > ${datadir}/megam.data/${i}/megam.weights 2> ${datadir}/megam.data/${i}/megam.train.log
    		if [ $? != "0" ]; then
		    echo "Error in Megam training"
    		    exit
    		fi
    		echo "Running Megam evaluation..."
    		megam.opt -predict ${datadir}/megam.data/${i}/megam.weights binary ${data_file} > ${datadir}/megam.data/${i}/megam.eval.log 2>&1
    		if [ $? != "0" ]; then
    		    exit
    		fi
		tail -n 1 ${datadir}/megam.data/${i}/megam.eval.log
	    elif [ $non_bernoulli_implicit == "1" ]; then
    		echo "Running Megam training (non-bernoulli-implicit)..."
    		megam.opt -fvals binary ${data_file} > ${datadir}/megam.data/$i/megam.weights 2> ${datadir}/megam.data/$i/megam.train.log
    		if [ $? != "0" ]; then
		    echo "Error in Megam training"
    		    exit
    		fi
    		echo "Running Megam evaluation..."
    		megam.opt -fvals -predict ${datadir}/megam.data/$i/megam.weights binary ${data_file} > ${datadir}/megam.data/$i/megam.eval.log 2>&1
    		if [ $? != "0" ]; then
    		    exit
    		fi
		tail -n 1 ${datadir}/megam.data/${i}/megam.eval.log
	    fi
	# IF ADMIRABLE:
     	# MOVE TEMPS TO ADMIRABLE ITH FOLD TRAIN/DEV/TEST DATA FILES
	# SAME FOR BI AND NBI
	elif [ "$learn" == "admirable" ]; then
	    data_file_train=${datadir}/admirable.data/${i}/admirable.train.input
	    data_file_test=${datadir}/admirable.data/${i}/admirable.test.input
	    # data_file_dev=${datadir}/admirable.data/${i}/admirable.dev.input
	    # echo "Moving data to $data_file_train, $data_file_test, $data_file_dev"
	    echo "Moving data to $data_file_train, $data_file_test"
	    if [ "$nonae" == "--nonae" ]; then
		grep -v '_NAE#' $datadir/$learn.data/${i}/train.input.temp | cut -f4,5,6 -d '#' | perl -pe 's/#\s*$/\n/;s/#next\s*$/\n\n/;s/^1#/11#/;s/^0#/1#/;s/^11#/0#/;s/#/\t/;s/\t+/\t/g' > $data_file_train
		echo "
" >> $data_file_train
		# grep -v '_NAE#' $datadir/$learn.data/${i}/dev.input.temp | cut -f4,5,6 -d '#' | perl -pe 's/#\s*$/\n/;s/#next\s*$/\n\n/;s/^1#/11#/;s/^0#/1#/;s/^11#/0#/;s/#/\t/;s/\t+/\t/g' > $data_file_dev
		grep -v '_NAE#' $datadir/$learn.data/${i}/test.input.temp | cut -f4,5,6 -d '#' | perl -pe 's/#\s*$/\n/;s/#next\s*$/\n\n/;s/^1#/11#/;s/^0#/1#/;s/^11#/0#/;s/#/\t/;s/\t+/\t/g' > $data_file_test
		echo "
" >> $data_file_test
	    else
		cut -f4,5,6 -d '#' $datadir/$learn.data/${i}/train.input.temp | perl -pe 's/#\s*$/\n/;s/#next\s*$/\n\n/;s/^1#/11#/;s/^0#/1#/;s/^11#/0#/;s/#/\t/;s/\t+/\t/g' > $data_file_train
		echo "
" >> $data_file_train
		# cut -f4,5,6 -d '#' $datadir/$learn.data/${i}/dev.input.temp | perl -pe 's/#\s*$/\n/;s/#next\s*$/\n\n/;s/^1#/11#/;s/^0#/1#/;s/^11#/0#/;s/#/\t/;s/\t+/\t/g' > $data_file_dev
		cut -f4,5,6 -d '#' $datadir/$learn.data/${i}/test.input.temp | perl -pe 's/#\s*$/\n/;s/#next\s*$/\n\n/;s/^1#/11#/;s/^0#/1#/;s/^11#/0#/;s/#/\t/;s/\t+/\t/g' > $data_file_test
		echo "
" >> $data_file_test
	    fi
	    echo "Converting data for Admirable..."
	    count_by_instance < $data_file_train > ${datadir}/admirable.data/${i}/features2counts
	    data_file_train_num=${data_file_train/\.input/.input.num}
	    # data_file_dev_num=${data_file_dev/\.input/.input.num}
	    data_file_test_num=${data_file_test/\.input/.input.num}
	    filter_and_map ${datadir}/admirable.data/${i}/features2counts 0 < $data_file_train > $data_file_train_num
	    # filter_and_map ${datadir}/admirable.data/${i}/features2counts 0 < $data_file_dev > $data_file_dev_num
	    filter_and_map ${datadir}/admirable.data/${i}/features2counts 0 < $data_file_test > $data_file_test_num
	    echo "Running Admirable training"
	    # ranker-learn --train $data_file_train_num --test $data_file_test_num --dev $data_file_dev_num > ${datadir}/admirable.data/${i}/features2weights 2> ${datadir}/admirable.data/${i}/admirable.train.log
	    ranker-learn -i $iter --train $data_file_train_num --test $data_file_test_num > ${datadir}/admirable.data/${i}/features2weights 2> ${datadir}/admirable.data/${i}/admirable.train.log
	    echo "Converting Admirable data to model..."
	    $scripts/getAdmirableModel.pl ${datadir}/admirable.data/${i}/features2counts ${datadir}/admirable.data/${i}/features2weights > ${datadir}/admirable.data/${i}/admirable.weights
	fi
    done
fi


mkdir -v $datadir/nomos_results/
mkdir -v $datadir/nomos_results/all
# mkdir -v $datadir/nomos_results/all/dev
mkdir -v $datadir/nomos_results/all/test
for i in $(eval echo "{1..$fold}")
do
    mkdir -v $datadir/nomos_results/${i}
    # mkdir -v $datadir/nomos_results/${i}/dev
    mkdir -v $datadir/nomos_results/${i}/test
    echo "" > $datadir/nomos_results/${i}/test_resolution.log
    # echo "" > $datadir/nomos_results/${i}/dev_resolution.log
done

#==========================================================================================
# RESOLVE

# for x in "test"
if [ $resolve == "1" ]; then
#do
    echo "========================================================================="
    echo "Resolving $datadir/$x..."
    echo "========================================================================="
    for i in $(eval echo "{1..$fold}")
    do
	echo "Resolving $datadir/$i/test..."
	for gold_file in `ls $golddir/${i}/test/*.xml`
	do
	    gold_file_name=`basename $gold_file`
	    nomos_file=${gold_file_name/.gold.xml/.nomos.xml}
	    res=$datadir/nomos_results/${i}/test/$nomos_file
	    nomos_file=$nomosdir/$nomos_file
	    if [ ! -e $nomos_file ]; then
		echo "$nomos_file does not exist - continue"
		continue
	    fi
	    res=${res/.nomos.xml/.nomos.resolved.xml}
	    filename=`basename "$file"`
	    # echo "Nomos file: $nomos_file"
	    # echo "Resolved nomos file: $res"
	    if [ $non_bernoulli_implicit == "1" ]; then 
		python $scripts/resolve.py $nonae \
		    --features $features \
		    --non-bernoulli-implicit \
		    --weights ${datadir}/$learn.data/${i}/$learn.weights \
		    --kb $kb \
		    --aledadir $aledadir \
		    --log $datadir/nomos_results/${i}/test_resolution.log \
		    $nomos_file \
		    | perl -pe 's/(<candidate[^<>]+\/>)/          $1\n/g' > $res
	    elif [ $bernoulli_implicit == "1" ]; then 
		python $scripts/resolve.py $nonae \
		    --features $features \
	    	    --weights ${datadir}/$learn.data/${i}/$learn.weights \
	    	    --fclasses $fclasses \
	    	    --kb $kb \
	    	    --aledadir $aledadir \
	    	    --log $datadir/nomos_results/${i}/test_resolution.log \
	    	    $nomos_file \
	    	    | perl -pe 's/(<candidate[^<>]+\/>)/          $1\n/g' > $res
	    fi
	    if [ $? != "0" ]; then
		exit
	    fi
	done
    done
fi
#done
    
mkdir -v $datadir/evalResults
echo "" > $datadir/evalResults/eval_test_all.results.log
echo "" > $datadir/evalResults/eval_test_all.missed.log

# for x in "dev" "test"
for x in "test"
do
    echo "========================================================================="
    echo "Evaluation $datadir/$x..."
    echo "========================================================================="
    for i in $(eval echo "{1..$fold}")
    do
	cp $datadir/nomos_results/${i}/$x/*.xml $datadir/nomos_results/all/$x
	echo "Evaluation $datadir/$x from $golddir/all + $datadir/nomos_results/$i/$x > $datadir/evalResults/eval_${x}_${i}.results.log"
	python $scripts/evaluate.py $nonae $goldneroption \
	    --gold $golddir/all/ \
	    --nomos $datadir/nomos_results/${i}/$x > $datadir/evalResults/eval_${x}_${i}.results.log 
	grep -A1 'NER-P' ${datadir}/evalResults/eval_${x}_${i}.results.log
	# grep -A1 'ORACLE ALL' ${datadir}/evalResults/eval_${x}_${i}.results.log
    done
    echo "========================================================================="
    echo "Evaluation $x - All from $golddir/all + $datadir/nomos_results/all/$x ... > ${datadir}/evalResults/eval_${x}_all.results.log"
    python $scripts/evaluate.py $nonae $goldneroption \
	--gold $golddir/all/ \
	--nomos $datadir/nomos_results/all/$x > $datadir/evalResults/eval_${x}_all.results.log 2> $datadir/evalResults/eval_${x}_all.missed.log 
    if [ $? != "0" ]; then
	exit
    fi
    echo "Results all $x:"
    grep -A1 'NER-P' ${datadir}/evalResults/eval_${x}_all.results.log
    # grep -A1 'ORACLE ALL' ${datadir}/evalResults/eval_${x}_all.results.log
    grep 'Person' ${datadir}/evalResults/eval_${x}_${i}.results.log
    grep 'Organization' ${datadir}/evalResults/eval_${x}_${i}.results.log
    grep 'Location' ${datadir}/evalResults/eval_${x}_${i}.results.log
done



