#!/bin/bash

root=""
golddir=""
thisner=""
naeparam=""
features="/Users/triantafillo/dev/alpc/nomos/linking/resolution/features"
train=""
featureconf=0

# GET OPTIONS
while [ "$1" != "" ]
do
    case $1 in
	--root )
	    shift
	    root=$1
	    ;;
	--golddir )
	    shift
	    golddir=$1
	    ;;
        --ner )           
	    shift
	    thisner=$1
	    ;;
	--nonae )
	    naeparam="--nonae"
	    ;;
	--features )
	    shift
	    features=$1
	    ;;
        --train )           
	    train="--train"
	    ;;
	--featureconf )
	    featureconf=1
	    ;;
    esac
    shift
done

if [ ! -e $root ]; then
    echo "Argument ::$root:: doesn't exist. Exit."
fi
if [ ! -e $golddir ]; then
    echo "Argument ::$golddir:: doesn't exist. Exit."
fi
nomosdir="$root/nomosDocuments/${thisner}Nomos/"
if [ ! -e $nomosdir ]; then
    echo "No Nomos documents found for NER ::$thisner::. Exit"
    exit
fi

echoargs(){
echo "Options and arguments: resolutionAllNer.sh
        --root $root
        --golddir $golddir
        --ner $thisner
        --train $train
        naeparam $naeparam
        --features $features
        --featureconf $featureconf
"
}

echoargs
# for ner in goldNer sxpipeAmbNer sxpipeDsbNer lianeNbestNer liane1bestNer lianeNbestSxpipeAmbNer lianeNbestSxpipeAmbInterNer liane1bestSxpipeDsbNer liane1bestSxpipeDsbInterNer
# do

if [ "$naeparam" == "" ]; then
    naedir="nae"
elif [ "$naeparam" == "--nonae" ]; then
    naedir="nonae"
fi
mkdir -v $root/$thisner/
mkdir -v $root/$thisner/$naedir
if [ "$featureconf" == "1" ]; then
    name=`basename $features`
    datadir=$root/$thisner/$naedir/$name
    mkdir -v $datadir
else
    datadir=$root/$thisner/$naedir
fi

if [ "$thisner" == "goldNer" ]; then
    goldoption="--goldneroption"
else
    goldoption=""
fi

# for param in nae nonae
# do
echo "==================================="
echo "===== $thisner / $naeparam ====="
echo "==================================="

/Users/triantafillo/dev/alpc/nomos/linking/resolution/resolution.sh --gold $golddir --nomosdir $nomosdir --data $datadir $train $naeparam $goldoption --features $features

#done
#done


