#!/usr/bin/python
# -*- coding: utf-8 -*-
import codecs
import re
import os
import sys
from glob import glob
from optparse import *
from xml.dom.minidom import *
sys.path.append('@common_src@')
import NomosUtils
import NomosDocument
from utils import *



def main():
    global options
    options, args = getOptions()
    # global LOG
    # if options.log != None: LOG = codecs.open(options.log, 'a', 'utf-8')
    # else: LOG = None

    # RECORDS OF TOTALS FOR EVALUATION
    # PREFIX d_ FOR DETECTION TOTALS, r_ FOR RESOLUTION TOTALS
    global aledarr; aledarr = 0.0
    global mcandrecall; mcandrecall = 0.0
    global mcandrecall3; mcandrecall3 = 0.0
    global mcandrecall2; mcandrecall2 = 0.0
    global mcandrecall1; mcandrecall1 = 0.0
    global d_rt; global d_rl; global d_rr

    global acc_rt; global acc_rt_k; global acc_rt_u
    global acc_rl; global acc_rl_k; global acc_rl_u
    global acc_rr; global acc_rr_k; global acc_rr_u
    global acc_rr_k2; global acc_rl_k2
    
    global r_rt; global r_rt_k; global r_rt_u
    global r_rl; global r_rl_k; global r_rl_u
    global r_rr; global r_rr_k; global r_rr_u

    global false; global true_nae; global nae
    global false_and_nae; global false_not_nae; global correct_and_nae; global exact_matches

    (d_rt, d_rl, d_rr) = 0.0, 0.0, 0.0
    (acc_rt, acc_rl, acc_rr, acc_rt_k, acc_rt_u, acc_rl_k, acc_rl_k2, acc_rl_u, acc_rr_k, acc_rr_k2, acc_rr_u) = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
    (r_rt, r_rt_k, r_rt_u, r_rl, r_rl_k, r_rl_u, r_rr, r_rr_k, r_rr_u) = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
    (false, true_nae, nae, false_and_nae, false_not_nae, correct_and_nae, exact_matches) = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0

    # GET FILES AND RUN EVALUATION ON EACH PAIR
    readFiles()

    if d_rt == 0.0: sys.exit('No mentions detected\n')
    if d_rl == 0.0: sys.exit('No gold mentions\n')

    d_p = d_rr / d_rt
    d_r = d_rr / d_rl
    try:
        d_f = (2*(d_p * d_r)) / (d_p + d_r)
    except ZeroDivisionError:
        sys.exit('ZeroDivisionError in NER-Fscore:\n>>> d_f = (2*(d_p * d_r)) / (d_p + d_r) = (2*(%.4f * %.4f)) / (%.4f + %.4f)\n'%(d_p, d_r, d_p, d_r))

    acc = r_rr / d_rr
    if acc_rl_k != 0.0: acc_k = '%.4f'%(acc_rr_k / acc_rl_k)
    else: acc_k = '-'
    if acc_rl_k2 != 0.0: acc_k2 = '%.4f'%(acc_rr_k2 / acc_rl_k2)
    else: acc_k2 = '-'
    if acc_rl_u != 0.0: acc_u = '%.4f'%(acc_rr_u / acc_rl_u)
    else: acc_u = '-'

    el_p = d_p * acc
    el_r = d_r * acc
    try:
        el_f = (2*(el_p * el_r) / (el_p + el_r))
    except ZeroDivisionError:
        sys.exit('ZeroDivisionError in EL-Fscore:\n>>> el_f = (2*(el_p * el_r) / (el_p + el_r)) el_f = (2*(%.4f * %.4f) / (%.4f + %.4f))\n'%(el_p, el_r, el_p, el_r))

    if not options.nonae:
        try: nae_discards_correct = false_and_nae / false
        except ZeroDivisionError: nae_discards_correct = 0.0
        try: nae_discards_false = correct_and_nae / exact_matches
        except ZeroDivisionError: nae_discards_false = 0.0
  
    # A = false; C = nae; E = true_nae
    # P = E/C; R = E/A
    if not options.nonae:
        try: nae_p = true_nae / nae
        except ZeroDivisionError: nae_p = '-'
        try: nae_r = true_nae / false
        except ZeroDivisionError: nae_r = '-'
        if isinstance(nae_p, float) and isinstance(nae_r, float):
            try: nae_f = "%.4f"%(2*(nae_p * nae_r) / (nae_p + nae_r))
            except ZeroDivisionError: nae_f = "0.0"
            nae_p = "%.4f"%(nae_p)
            nae_r = "%.4f"%(nae_r)
        else:
            nae_f = '-'

    sys.stdout.write('|  NER-P |  NER-R |  NER-F | AccAll |  AccK  | AccK2  | AccNil  |  EL-P  |  EL-R   |  EL-F   |  NAE + |  NAE - |  NAE P |  NAE R |  NAE F |\n')
    if not options.nonae:
        sys.stdout.write('| %.4f | %.4f | %.4f | %.4f | %s | %s | %s | %.4f | %.4f | %.4f | %.4f | %.4f | %s | %s | %s |\n'%(d_p, d_r, d_f, acc, acc_k, acc_k2, acc_u, el_p, el_r, el_f, nae_discards_correct, nae_discards_false, nae_p, nae_r, nae_f))
    else:
        sys.stdout.write('| %.4f | %.4f | %.4f | %.4f | %s | %s | %s | %.4f | %.4f | %.4f | - | - | - | - | - |\n'%(d_p, d_r, d_f, acc, acc_k, acc_k2, acc_u, el_p, el_r, el_f))

    candrecall = mcandrecall / aledarr
    candrecall3 = mcandrecall3 / aledarr
    candrecall2 = mcandrecall2 / aledarr
    candrecall1 = mcandrecall1 / aledarr
    sys.stdout.write('|ORACLE ALL|ORACLE 3|ORACLE 2|ORACLE 1|\n')
    sys.stdout.write('|%.4f|%.4f|%.4f|%.4f|\n'%(candrecall, candrecall3, candrecall2, candrecall1))

def evaluate(nomos_document, gold_document):
    global aledarr
    global mcandrecall; global mcandrecall3; global mcandrecall2; global mcandrecall1
    global d_rt; global d_rl; global d_rr
    
    global acc_rt; global acc_rt_k; global acc_rt_u
    global acc_rl; global acc_rl_k; global acc_rl_u
    global acc_rr; global acc_rr_k; global acc_rr_u
    global acc_rr_k2; global acc_rl_k2

    global r_rt; global r_rt_k; global r_rt_u
    global r_rl; global r_rl_k; global r_rl_u
    global r_rr; global r_rr_k; global r_rr_u

    global false; global true_nae; global nae
    global false_and_nae; global false_not_nae; global correct_and_nae; global exact_matches

    nomos_document.getXmlScoredPaths()
    NomosUtils.getDetection(gold_document, nomos_document, True, options.goldner)
    NomosUtils.getResolution(nomos_document)

    # LIST OF GOLD MENTIONS
    gold_mentions = gold_document.mentions.values()
    # LIST OF NOMOS MENTIONS = NOT DISCARDED (NOT NAE) AND IN PATH RANKED FIRST
    nomos_mentions = filter(lambda m:m.is_in_path_part.is_in_path.rank < 2, nomos_document.mentions.values())
    d_rr_mentions = filter(lambda m: m.discarded==False and m.detection == 'exact_match', nomos_mentions) # exact match and in first path
    exact_matches += float(len(d_rr_mentions))
    nae_nomos_mentions = filter(lambda m:m.discarded, nomos_mentions)
    false_nomos_mentions = filter(lambda m: m.detection == 'false_match', nomos_mentions)
    true_nae_nomos_mentions = filter(lambda m: m.resolution, false_nomos_mentions)

    nomos_mentions_aledacand = filter(lambda m: m.gold_mention.entity_type == 'ALEDA', d_rr_mentions)
    mcandrecall += len(filter(lambda m:m.candrecall == True, nomos_mentions_aledacand))
    mcandrecall3 += len(filter(lambda m:m.candrecall3 == True, nomos_mentions_aledacand))
    mcandrecall2 += len(filter(lambda m:m.candrecall2 == True, nomos_mentions_aledacand))
    mcandrecall1 += len(filter(lambda m:m.candrecall1 == True, nomos_mentions_aledacand))
    aledarr += len(nomos_mentions_aledacand)

    nae += float(len(nae_nomos_mentions))
    false += float(len(false_nomos_mentions))
    true_nae += float(len(true_nae_nomos_mentions))
    false_and_nae += float(len(filter(lambda m: m.cand_type == 'NAE', false_nomos_mentions)))
    false_not_nae += float(len(filter(lambda m: m.cand_type != 'NAE', false_nomos_mentions)))
    correct_and_nae = float(len(filter(lambda m: m.discarded and m.detection == 'exact_match', nomos_mentions)))
    
    # DETECTION SETS CARDINALITIES:
    # RETRIEVED = ALL NOMOS MENTIONS NOT DISC. AND IN FIRST PATH
    # RELEVANT = ALL GOLD MENTIONS
    # RETRIEVED-RELEVANT = ALL NOMOS MENTIONS NOT DISC. AND IN FIRST PATH AND WITH EXACT DETECTION
    d_rt += float(len(nomos_mentions))
    d_rl += float(len(gold_mentions))
    d_rr += float(len(d_rr_mentions))
    # print >> sys.stderr, 'd_rt: %.4f | d_rl: %.4f | d_rr: %.4f'%(d_rt, d_rl, d_rr)

    # RESOLUTION SETS CARDINALITIES
    r_rr_mentions = filter((lambda m: m.resolution == True), d_rr_mentions)

    # FOR ACCURACY:
    # RETRIEVED = NOMOS MENTIONS WHERE NOMOS DETECTION IS CORRECT = NOMOS MENTIONS IN D_RR
    acc_rt = d_rr
    # RELEVANT = GOLD MENTIONS WHERE NOMOS DETECTION IS CORRECT (SAME SIZE AS RETRIEVED) = GOLD MENTIONS IN D_RR
    acc_rl = d_rr
    # RETRIEVED-RELEVANT = MENTIONS WHERE DETECTION AND RESOLUTION IS CORRECT
    acc_rr += float(len(r_rr_mentions))
    # BREAKDOWN K/NIL:
    # K:
    # RETRIEVED = NOMOS MENTIONS WHERE DETECTION IS CORRECT AND CAND TYPE IS ALEDA
    acc_rt_k += float(len(filter((lambda m: m.cand_type == 'ALEDA'), d_rr_mentions)))
    # RELEVANT = GOLD MENTIONS WHERE NOMOS DETECTION IS CORRECT AND ENTITY TYPE IS ALEDA (SAME SIZE AS RETRIEVED)
    acc_rl_k += float(len(filter((lambda m: m.gold_mention.entity_type == 'ALEDA'), d_rr_mentions)))
    acc_rl_k2 += float(len(filter((lambda m: m.gold_mention.entity_type == 'ALEDA' and m.candrecall==True), d_rr_mentions)))
    # RETRIEVED-RELEVANT = MENTIONS WHERE DETECTION AND RESOLUTION IS CORRECT AND CAND TYPE IS ALEDA
    acc_rr_k += float(len(filter((lambda m: m.cand_type == 'ALEDA'), r_rr_mentions)))
    acc_rr_k2 += float(len(filter((lambda m: m.cand_type == 'ALEDA' and m.candrecall==True), r_rr_mentions)))
    # U:
    # RETRIEVED = NOMOS MENTIONS WHERE DETECTION IS CORRECT AND CAND TYPE IS NIL
    acc_rt_u += float(len(filter((lambda m: m.cand_type == 'NIL'), d_rr_mentions)))
    # RELEVANT = GOLD MENTIONS WHERE NOMOS DETECTION IS CORRECT AND ENTITY TYPE IS NIL (SAME SIZE AS RETRIEVED)

    acc_rl_u += float(len(filter((lambda m: m.gold_mention.entity_type == 'NIL'), d_rr_mentions)))

    # RETRIEVED-RELEVANT = MENTIONS WHERE DETECTION AND RESOLUTION IS CORRECT AND CAND TYPE IS NIL
    acc_rr_u += float(len(filter((lambda m: m.cand_type == 'NIL'), r_rr_mentions)))

    # FOR EL-JOINT MEASURE
    # RETRIEVED = DETECTION RETRIEVED = NOMOS MENTIONS NOT DISC. AND IN FIRST PATH
    r_rt += d_rt
    # RELEVANT = DETECTION RELEVANT = GOLD MENTIONS
    r_rl += d_rl
    # RETRIEVED-RELEVANT = RETRIEVED NOMOS MENTIONS WHERE DETECTION IS CORRECT AND RESOLUTION IS CORRECT
    r_rr += float(len(r_rr_mentions))
    # print >> sys.stderr, 'r_rr: %.4f'%(r_rr)
    # BREAKDOWN K/NIL:
    # K:
    # RETRIEVED = RETRIEVED NOMOS MENTIONS WHERE CAND TYPE IS ALEDA
    r_rt_k += float(len(filter((lambda m: m.cand_type == 'ALEDA'), nomos_mentions)))
    # RELEVANT = GOLD MENTIONS WHERE ENTITY TYPE = ALEDA
    r_rl_k += float(len(filter((lambda m: m.entity_type == 'ALEDA'), gold_mentions)))
    # RETRIEVED-RELEVANT = RETRIEVED NOMOS MENTIONS WHERE DETECTION IS CORRECT AND RESOLUTION IS CORRECT AND CAND TYPE IS ALEDA
    r_rr_k += float(len(filter((lambda m: m.cand_type == 'ALEDA'), r_rr_mentions)))
    # U:
    # RETRIEVED = RETRIEVED NOMOS MENTIONS WHERE CAND TYPE IS NIL
    r_rt_u += float(len(filter((lambda m: m.cand_type == 'NIL'), nomos_mentions)))
    # RELEVANT = GOLD MENTIONS WHERE ENTITY TYPE = NIL
    r_rl_u += float(len(filter((lambda m: m.entity_type == 'NIL'), gold_mentions)))
    # RETRIEVED-RELEVANT = RETRIEVED NOMOS MENTIONS WHERE DETECTION IS CORRECT AND RESOLUTION IS CORRECT AND CAND TYPE IS NIL
    r_rr_u += float(len(filter((lambda m: m.cand_type == 'NIL'), r_rr_mentions)))
    



def readFiles():
    global options
    if os.path.isfile(options.gold) and os.path.isfile(options.nomos) and options.file_type == 'news':
        # print >> sys.stderr, "Nomos file: %s\nGold file: %s"%(options.nomos, options.gold)
        nomos_document = NomosDocument.Document(parse(options.nomos), 'auto', True)
        gold_document = NomosDocument.Document(parse(options.gold), 'gold')
        last_gold_token = gold_document.dom.getElementsByTagName('token')[-1].getAttribute('id')
        last_nomos_token = nomos_document.dom.getElementsByTagName('token')[-1].getAttribute('id')
        if last_gold_token != last_nomos_token:
            sys.exit('Tokens badly aligned in %s (last: %s) and %s (last: %s)'%(gold_file, last_gold_token, nomos_file, last_nomos_token))
        evaluate(nomos_document, gold_document)
        return
    if os.path.isdir(options.gold) and os.path.isdir(options.nomos):
        # print >> sys.stderr, "Nomos dir: %s\nGold dir: %s"%(options.nomos, options.gold)
        matches = {}
        gold_dir = options.gold
        nomos_dir = options.nomos
        nomos_files = filter((lambda f: not re.match('^\.', f) and re.search('.xml$', f)), os.listdir(nomos_dir))
        for nomos_file in nomos_files:
            gold_file = re.sub('nomos\.res(olved)?\.xml', 'gold.xml', nomos_file)
            gold_file = gold_dir+"/"+gold_file
            nomos_file = nomos_dir+"/"+nomos_file
            # print >> sys.stderr, "Nomos file : %s\nGold file: %s"%(nomos_file, gold_file)
            if os.path.isfile(nomos_file) and os.path.isfile(gold_file):
                matches[nomos_file] = gold_file
            else:
                # sys.exit("""
                # !!!! EVALUATION FILES ERROR (not os.path.isfile)
                # Nomos dir: %s
                # Gold dir: %s
                # Nomos file: %s
                # Gold file: %s
                # !!!!
                # """%(options.nomos, options.gold, nomos_file, gold_file))
                print >> sys.stderr, "No match for files\nGold=%s (%s)\nNomos=%s (%s)"%(gold_file, str(os.path.isfile(gold_file)), nomos_file, str(os.path.isfile(nomos_file)))
                continue
        cpt = 0
        for nomos_file, gold_file in matches.items():
            nomos_document = NomosDocument.Document(parse(nomos_file), 'auto', True)
            gold_document = NomosDocument.Document(parse(gold_file), 'gold')
            last_gold_token = gold_document.dom.getElementsByTagName('token')[-1].getAttribute('id')
            last_nomos_token = nomos_document.dom.getElementsByTagName('token')[-1].getAttribute('id')
            if last_gold_token != last_nomos_token:
                # sys.exit('Tokens badly aligned in %s (last: %s) and %s (last: %s)'%(gold_file, last_gold_token, nomos_file, last_nomos_token))
                print >> sys.stderr, 'Tokens badly aligned in %s (last: %s) and %s (last: %s)'%(gold_file, last_gold_token, nomos_file, last_nomos_token)
                continue
            evaluate(nomos_document, gold_document); cpt +=1
            message = "%d documents evaluated"%(cpt)
            # sys.stderr.write(message + '\b'*(len(message)))
    elif options.file_type == 'newscorpus':
        cpt = 0
        if options.nomos != None: F = codecs.open(options.nomos, 'r',  'utf-8')
        else: F = sys.stdin
        in_doc = False; string_to_parse = ""
        id_pattern = re.compile('urn:newsml:afp.com:\d+T\d+Z:([^:]+)')
        for line in F:
            if re.match('^\s*<nomos>', line):
                in_doc = True; string_to_parse += line
            elif in_doc and re.search('</nomos>', line):
                in_doc = False; string_to_parse += line
                nomos_document = NomosDocument.Document(parseString(string_to_parse.encode('utf-8')), 'auto', True)
                string_to_parse = ""
                try: gold_file = glob(options.gold+'/'+'afp.com*'+id_pattern.match(nomos_document.id).group(1)+'*')[0]
                except IndexError: sys.exit(nomos_document.id)
                gold_document = NomosDocument.Document(parse(gold_file), 'gold')
                last_gold_token = gold_document.dom.getElementsByTagName('token')[-1].getAttribute('id')
                last_nomos_token = nomos_document.dom.getElementsByTagName('token')[-1].getAttribute('id')
                if last_gold_token != last_nomos_token: sys.exit('Tokens badly aligned in %s (last: %s) and %s (last: %s)'%(gold_file, last_gold_token, nomos_file, last_nomos_token))
                evaluate(nomos_document, gold_document)
                cpt += 1
                message = "%d documents evaluated"%(cpt)
                # sys.stderr.write(message + '\b'*(len(message)))
            elif in_doc:
                string_to_parse += line

def getOptions():
    parser = OptionParser()
    file_types = ['news', 'newscorpus']
    parser.add_option("--gold", action="store", dest="gold", default=None, help="Gold Dir")
    parser.add_option("--nomos", action="store", dest="nomos", default=None, help="Nomos Dir")
    parser.add_option('--file', '-f', action='store', dest='file_type', type="choice", choices=file_types, default='news', help='Input type: One news item in Nomos format or news corpus in Nomos format (default: one news item)')
    parser.add_option("--goldner", action="store_true", dest="goldner", default=False, help="True: NER is gold")
    parser.add_option("--nonae", action="store_true", dest="nonae", default=False, help="True: Don't compute NAE scores")
    # parser.add_option("--log", action="store", dest="log", default=None, help="<log file>")
    try: (options, args) = parser.parse_args()
    except OptionError: sys.stderr.write('OptionError'); parser.print_help(); sys.exit()
    except: sys.stderr.write('An error occured during options parsing\n'); parser.print_help(); sys.exit()
    if options.gold == None: parser.print_help(); sys.exit()
    # if len(args) != 1: parser.print_help(); sys.exit()
    return options, args


if __name__ == "__main__":
    main()
