#!/usr/bin/python
# -*- coding: utf-8 -*-
import codecs
import re
import os
import sys
from glob import glob
from optparse import *
from xml.dom.minidom import *
sys.path.append('@common_src@')
from utils import *
import NomosDocument
import NomosUtils
import LinkingInstance

# def doResolution(nomos_dom, gold_dom):
def doResolution(nomos_dom):
    # print >> sys.stderr, "In resolution"
    nomos_document = NomosDocument.Document(nomos_dom, 'auto', False, lex_connection, kb_connection)
    nomos_document.populateCandidates(aleda_vars_connection, aleda_refs_connection, kb_connection)
    for m in nomos_document.mentions.values():
        m.setOtherAttributes(kb_connection)

    # BUILD LINK INSTANCES => FEATURES VECTORS
    # COMPUTE INSTANCES SCORES WITH WEIGHTS => SCORE IS CANDIDATE'S SCORE (IN CLASS LINKING INSTANCE)
    for mention_id, mention in nomos_document.mentions.items():
        if options.nonae:
            mention.deleteNAE()
        # for candidate in mention.candidates:
        # linking_instance = LinkingInstance.LinkingInstance(nomos_document, mention, candidate, features2type, fclasses)
        linking_instance = LinkingInstance.LinkingInstance(nomos_document, mention, features_list, features2type, fclasses)
        linking_instance.computeCandidatesScore(features2weights, bias)
    nomos_document.scorePaths(bias)
    nomos_document.buildResolutionModule()#gold_document)
    if options.file_type == 'newscorpus': sys.stdout.write('<document>\n')
    sys.stdout.write(nomos_document.dom.documentElement.toxml('utf-8')+"\n")
    if options.file_type == 'newscorpus': sys.stdout.write('</document>\n')
    if LOG != None:
        # TODO: BETTER LOG FOR RESOLUTION
        writeLog('Mentions in Nomos Document %s'%(nomos_document.id), LOG)
        writeLog('===========================================================', LOG)
        for mention in nomos_document.mentions.values():
            mention.display(LOG)
    return

def main():
    global options
    (options, args) = getOptions()
    # print >> sys.stderr, options

    # print >> sys.stderr, "Open DBs"
    # CONNECT TO SXPIPE-aleda DB
    global aleda_refs_connection; global aleda_vars_connection; global kb_connection; global lex_connection
    aleda_refs_connection = DBConnection(options.aledadir+'ne_refs.fr.dat', ['data'])
    aleda_vars_connection = DBConnection(options.aledadir+'ne_vars.fr.dat', ['data'])
    lex_connection = DBConnection(options.aledadir+'lefff.dat', ['data'])
    # CONNECT TO NOMOS-DB
    # kb_connection = DBConnection(options.entities, ['categories', 'coocs', 'frequency', 'main', 'mentions', 'slugs', 'surrwords', 'swords', 'wordclusters'])
    # kb_connection.addTablesInstances(initTables(initDB.getTables('entities')))
    kb_connection = DBConnection(options.kb, getKBDBtables())

    # print >> sys.stderr, "Get feature weights and feature classes"
    global features2weights; global bias; global fclasses; global features_list; global features2type
    features_list, features2type = NomosUtils.getFeatures(options.features_file)
    (features2weights, bias) = NomosUtils.getFeaturesWeights(options.weights_file)
    if options.non_bernoulli_implicit:
        fclasses = None
        bernoulli_implicit = False
    else:
        fclasses = NomosUtils.getFeaturesClasses(options.fclasses_file)

    global LOG
    if options.log != None: LOG = codecs.open(options.log, 'a', 'utf-8')
    else: LOG = None

    # gold_dom = None
                    
    if options.file_type == 'news':
        try:
            nomos_dom = parse(args[0])
        except IndexError:
            file_data = sys.stdin.readlines()
            nomos_dom = parseString("".join(file_data))
        # if options.gold != None: gold_dom = parse(options.gold)
        doResolution(nomos_dom)#, gold_dom)
    elif options.file_type == 'newscorpus':
        try:
            # print >> sys.stderr, "Open arg file"
            F = codecs.open(args[0], 'r', 'utf-8')
        except IndexError:
            # print >> sys.stderr, "Reading from STDIN"
            F = sys.stdin
        sys.stdout.write('<documents>\n')
        in_doc = False; string_to_parse = ""
        cpt = 0
        id_pattern = re.compile('urn:newsml:afp.com:\d+T\d+Z:([^:]+)')
        for line in F:
            if re.match('^\s*<nomos', line):
                # print >> sys.stderr, "Start of document..."
                in_doc = True; string_to_parse += line
            elif in_doc and re.match('^\s*</nomos', line):
                in_doc = False; string_to_parse += line
                # print >> sys.stderr, "End of document..."
                # nomos_dom = parseString(string_to_parse.encode('utf-8'))
                nomos_dom = parseString(string_to_parse)
                string_to_parse = ""
                nomos_news_id = id_pattern.search(nomos_dom.getElementsByTagName('meta')[0].getAttribute('doc_id')).group(1)
                # print >> sys.stderr, "Parsing document %s"%(nomos_news_id)
                # if options.gold != None:
                #     # print >> sys.stderr, "Getting gold dom"
                #     gold_file = glob(options.gold+'/'+'afp.com*'+nomos_news_id+'*')[0]
                #     if os.path.isfile(gold_file): gold_dom = parse(gold_file)
                doResolution(nomos_dom)#, gold_dom)
                cpt += 1
                message = "%d documents resolved"%(cpt)
                sys.stderr.write(message + '\b'*(len(message)))
            elif in_doc:
                string_to_parse += line
        sys.stdout.write('</documents>\n')

    aleda_refs_connection.close(); aleda_vars_connection.close(); kb_connection.close(); lex_connection.close()
    if options.log != None: LOG.close()
    
def getOptions():
    usage = '%s [options] <xml output of buildNomos.py>'%sys.argv[0]
    parser = OptionParser(usage)
    file_types = ['news', 'newscorpus']
    parser.add_option("--non-bernoulli-implicit", action="store_true", dest="non_bernoulli_implicit", default=True, help="True if Megam is using non-bernoulli-implicit format; Else False (default True)")
    parser.add_option("--bernoulli-implicit", action="store_true", dest="bernoulli_implicit", default=False, help="True if Megam is using bernoulli-implicit format; Else False (default False)")
    parser.add_option("--nonae", action="store_true", dest="nonae", default=False, help="True: No NAE candidates nor resolution (default False)")
    parser.add_option("--features", action="store", dest="features_file", default="__prefix__/share/nomos/features", help="<file for features reading (f|type<bin/multi>|size)>")
    parser.add_option("--weights", action="store", dest="weights_file", default='__prefix__/share/nomos/feature_weights', help="Features weights file (default __prefix__/share/nomos/feature_weights)")
    parser.add_option("--fclasses", action="store", dest="fclasses_file", default=None, help="Features classes file (if bernoulli-implicit training) (default __prefix__/share/nomos/fclasses")        
    parser.add_option("--kb", action="store", dest="kb", default='__prefix__/share/nomos/kbwp.dat', help="<path to KB data base> (default __prefix__/share/nomos/kbwp.dat)")
    parser.add_option("--aledadir", action="store", dest="aledadir", default='__prefix__/share/aleda/', help="<path to dir with aleda data bases (default __prefix__/share/aleda/)")
    parser.add_option('--file', '-f', action='store', dest='file_type', type="choice", choices=file_types, default='news', help='Input type: One news item in Nomos format or news corpus in Nomos format (default: one news item)')
    parser.add_option("--log", action="store", dest="log", default=None, help="<log file>")
    try: (options, args) = parser.parse_args()
    except OptionError: sys.stderr.write('OptionError\n'); parser.print_help(); sys.exit()
    except: sys.stderr.write('An error occured during options parsing\n'); parser.print_help(); sys.exit()
    if options.non_bernoulli_implicit and options.fclasses_file != None: parser.print_help(); sys.exit('Incompatible options features & non-bernoulli-implicit')
    if options.weights_file == None or options.features_file == None: sys.stderr.write("Mandatory option argument missing.\n"); parser.print_help(); sys.exit()

    # print >> sys.stderr, "WEIGHTS: %s"%(options.weights_file)
    # if options.dev_gold_file != None and options.test_gold_file != None: sys.stderr.write("Cannot get both dev and test gold file\n"); parser.print_help(); sys.exit()
    if options.non_bernoulli_implicit: options.bernoulli_implicit = False
    # if len(args) != 1: sys.stderr.write('Args error (%d)\n'%(len(args))); parser.print_help(); sys.exit()
    # if options.file_type == 'newscorpus' and options.gold != None and not os.path.isdir(options.gold): sys.exit('Matching gold files must be specified in a directory\n')
    return options, args

if __name__ == '__main__':
    main()
