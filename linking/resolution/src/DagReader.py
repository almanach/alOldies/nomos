#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
from xml.dom.minidom import *
sys.path.append('@common_src@')
import Mention
from utils import *

class Zone(): # ONLY AN ENAMEX NODE, NOT IN ALT (UNDER PARA/SENT NODE)
    def __init__(self, node, document):
        self.node = node
        self.document = document
        self.populatePaths()
        self.type = 'simple'

    def populatePaths(self):
        # PATH ID IS LOCAL EID
        # self.paths = [Path(self.node.getAttribute('local_eid'), [self.node], self.document, self)]
        local_eid = self.node.getAttribute('local_eid')
        self.paths = {local_eid: Path(local_eid, [self.node], self)}
        if self.document.in_eval:
            # ONLY ENAMEX ZONE WITH VALID ATTRIBUTE
            self.paths[local_eid].valid = getAttributeTest(self.node, 'valid', 'true')
            # OTHERWISE, VALID IS SET WHILE BUILDING PATH PARTS: IF PATH PART MENTIONS HAS BEEN DELETED, THE PATH IS NOT VALID
        self.valid_paths = filter((lambda p: p.valid), self.paths.values())

    def rankPaths(self, bias):
        for path in self.valid_paths:
            path.setScore(bias)
        if len(self.valid_paths) == 1:
            self.valid_paths[0].rank = 0
        else:
            self.sorted_paths = sorted([path for path in self.valid_paths], key=lambda path: path.score, reverse=True)
            x = 1
            for path in self.sorted_paths:
                path.rank = x; x += 1
            
    def display(self, FH=None):
        writeLog('Zone ----------------------------------', FH)
        for path in self.sorted_paths:
            path.display(FH)

class AmbiguousZone(Zone):
    def __init__(self, node, document):
        self.node = node
        self.choices = node.getElementsByTagName('CHOICE')
        self.document = document
        self.populatePaths()
        self.type = 'ambiguous'

    def populatePaths(self):
        self.paths = {}
        for choice in self.choices:
            # ID IS JUST AN INTEGER, INCREMENTED IN ALT (SEE buildNomos)
            # PATH NODES  = ALL ENAMEX OR FORM CHILD NODES OF CHOICE NODE
            choice_id = choice.getAttribute('id')
            self.paths[choice_id] = Path(choice_id, selectNodes(choice.childNodes, 'node_type', ('element', None)), self)
            if self.document.in_eval:
                self.paths[choice_id].valid = getAttributeTest(choice, 'valid', 'true')
            # OTHERWISE, VALID IS SET WHILE BUILDING PATH PARTS: IF PATH PART MENTIONS HAS BEEN DELETED, THE PATH IS NOT VALID
        self.valid_paths = filter((lambda p: p.valid), self.paths.values())
                       
class Path():
    def __init__(self, path_id, nodes, zone):
        self.id = path_id
        self.valid = True
        self.zone = zone
        self.path_parts = map((lambda node: PathPart(node, self)), nodes)

    def setScore(self, bias):
        # if self.has_entity:
        # PATH SCORE FUNCTION: MIN(ENTITY/PARTS SCORES)
        # BEURK: ONLY SET SCORE IF VALID!! IF ONE PATH PART IS INVALID, SO IS THE WHOLE PATH
        # self.score = min(map((lambda path_part: path_part.getScore(bias)), filter((lambda path_part: path_part.type == 'ENAMEX' and path_part.valid), self.path_parts)))
        for pp in self.path_parts:
            pp.setScore()#bias)
        try: self.score = min(map((lambda path_part: path_part.score), filter((lambda path_part: path_part.type == 'ENAMEX'), self.path_parts)))
        # TODO: SET SCORE FOR PATHS WITH NO ENAMEX
        except ValueError: self.score = bias
        # TODO: SET SCORE ACCORDING TO OTHER PATH PARTS SCORES (+ IF OTHER WITH ENTITY VERY LOW, - OTHERWISE)

    def getXmlScore(self, node):
        self.score = float(node.getAttribute('path_score'))

    def getXmlRank(self, node):
        self.rank = int(node.getAttribute('rank'))

    def display(self, FH=None):
        writeLog('Path %s | %s | Rank %d | Score %f'%(str(self.valid), self.id, self.rank, self.score), FH)
        for path_part in self.path_parts:
            path_part.display(FH)

class PathPart():
    def __init__(self, node, path):
        self.is_in_path = path
        self.type = node.tagName
        self.mention = None
        # ASSOCIATE PATH PART WITH MENTION IF ANY
        # IF THE MENTION HAS BEEN DISCARDED AS A DOUBLE, SET PATH PART AS INVALID, AS WELL AS WHOLE PATH
        if self.type == 'ENAMEX':# and not self.is_in_path.zone.document.goldner:
            try:
                self.mention = self.is_in_path.zone.document.mentions[node.getAttribute('local_eid')]
                self.mention.is_in_path_part = self
            except KeyError:
                self.is_in_path.valid = False
            self.tokens = tuple(map((lambda token: token.getAttribute('id')), node.getElementsByTagName('token')))

    def setScore(self):#, bias):
        # ??? if self.mention.best_candidate.id == 'null': self.score = bias
        # else: self.score = self.document.candidates[self.mention.best_candidate.id].score
        # self.score = self.mention.best_candidate.scores[self.mention.id]
        # print >> sys.stderr, '>>'+self.is_in_path.zone.node.getAttribute('id')+'<<'
        # print >> sys.stderr, '>>'+self.is_in_path.zone.node.getAttribute('local_eid')+'<<'
        # print >> sys.stderr, '>>'+self.is_in_path.id+'<<'
        # print >> sys.stderr, '>>'+str(self.is_in_path.valid)+'<<'
        # print >> sys.stderr, '>>'+self.type+'<<'
        if self.type == 'ENAMEX':
            # if self.mention is None: sys.exit('None mention')
            self.score = self.mention.best_candidate.scores[self.mention.id]
        else:
            self.score = 0

    def display(self, FH=None):
        if self.type == 'ENAMEX':
            if self.mention != None:
                writeLog('>>PathPart %s | Mention %s'%(', '.join(self.tokens), mention.id), FH)
                self.mention.display(FH)
            else:
                writeLog('>>PathPart %s | Mention deleted'%(', '.join(self.tokens), mention.id), FH)
        else:
            writeLog('>>PathPart %s | No Mention'%(', '.join(self.tokens)), FH)
            
