#!/usr/bin/python
# -*- coding: utf-8 -*-
import math
import sys
import re
import string
from xml.dom.minidom import *
sys.path.append('@common_src@')
from utils import *
from NomosUtils import *
from editDistance import *

class Variant():
    def __init__(self, string, document):
        self.string = string
        self.document = document
        self.document.variants[string] = self
        self.mentions = set()

    def getCandidates(self, aleda_vars_connection, aleda_refs_connection, kb_connection):
        aleda_vars_connection.tables['data'].selectFromTable(aleda_vars_connection.cursor, ['key'], [('variant', '=', self.string)])
        # CANDIDATES_IDS ARE ALL KEYS FROM ALEDA FOR THE VARIANT
        self.candidates_ids = map((lambda t: int(t[0])), aleda_vars_connection.cursor.fetchall())
        # TODO: LOOK FOR OTHER VARIANTS (+LONG...)
        # self.findAltVariants()
        # for alt_variant in self.alt_variants:
        # aleda_vars_connection.tables['data'].selectFromTable(aleda_vars_connection.cursor, ['key'], [('variant', '=', replaceDiacr(self.string))])
        # self.candidates_ids.extend(map((lambda t: int(t[0])), aleda_vars_connection.cursor.fetchall()))
        # FOR EACH CANDIDATE, CHECK IF IT HAS ALREADY BEEN INSTANCIATED IN THIS DOCUMENT
        for candidate_id in set(self.candidates_ids): # MAY BE EMPTY
            # IF NOT, INSTANCIATE CANDIDATE
            if not self.document.candidates.has_key(candidate_id):
                # IF CAND ID IS INTEGER, THEN IT'S AN ALEDA CANDIDATE
                if isinstance(candidate_id, int):
                    # ADD TO IT ALL ITS VARIANTS FROM ALEDA
                    aleda_vars_connection.tables['data'].selectFromTable(aleda_vars_connection.cursor, ['variant'], [('key', '=', candidate_id)])
                    candidate_variants = map((lambda t: t[0]), aleda_vars_connection.cursor.fetchall())
                    candidate_variants2counts = {}
                    for v in candidate_variants:
                        aleda_vars_connection.tables['data'].selectFromTable(aleda_vars_connection.cursor, ['count(*)'], [('variant', '=', v)])
                        candidate_variants2counts[v] = aleda_vars_connection.cursor.fetchone()[0]
                    # ADD CANDIDATES TO DOCUMENT WHEN INSTANCIATED
                    self.document.candidates[candidate_id] = AledaCandidate(candidate_id, candidate_variants2counts, aleda_refs_connection, aleda_vars_connection, kb_connection, self.document)
        # ADD NIL AND NAE CANDIDATE IN ALL CASES - IN MENTION INSTANTIATION

        # CREATE TABLE data(key INTEGER,variant TEXT, firstname TEXT, middlename TEXT, lastname TEXT, othername TEXT);
        # aleda_vars_connection.tables['data'].selectFromTable(aleda_vars_connection.cursor, ['firstname', 'middlename', 'lastname'], [('variant', '=', self.string)])
        # res = aleda_vars_connection.cursor.fetchall()
        # (fname, mname, lname) = 

    def findAltVariants(self):
        self.alt_variants = []
        self.alt_variants.append(replaceDiacr(self.string))

class Candidate(object):
    def __init__(self, document):
        self.document = document
        self.kb_data = {'slugs':None, 'iptc':None, 'swords':None, 'mentions':None, 'peers':None, 'peersvars':None, 'frequency':None, 'title':None, 'titlepar':None}
        self.aleda_data = {'in_aleda':False, 'aleda_type':None, 'aleda_geonames_weight':None, 'aleda_wikipedia_weight':None, 'syn_rate':None, 'amb_rate':None}
        self.gender = None
        
    def displayId(self):
        return unicode(self.id)

    def display(self, mention, FH=None):
        writeLog('%s | %s | %s | %f'%(self.candidate_type, self.displayId(), self.name, self.scores[mention.id]), FH)

    # VERSION WITH WP KB; CHECK IF FETCHALL IS NOT EMPTY (SOME CORRESPONDANCES BUGS B/ ALEDA AND KBWP...)
    # IF EMPTY: LEAVE VALUE AT NONE INSTEAD OF DICT
    def addKBWPData(self, kb_connection):
        # WPOCCS*: TOTAL FREQ IN FRWIKI
        kb_connection.tables['frequencies'].selectFromTable(kb_connection.cursor, ['frequency'], [('aledaid', '=', self.id)])
        res = kb_connection.cursor.fetchone()
        if res != None and res != '': self.kb_data['frequency'] = res[0]
        # CATEGORIES (NORMALIZED)
        # kb_connection.tables['categories'].selectFromTable(kb_connection.cursor, ['categories'], [('aledaid', '=', self.id)])
        # res = kb_connection.cursor.fetchone()
        # if res != []: self.kb_data['cats'] = res[0].split('#')
        # SALIENT WORDS (ONLY 30 FIRST, NOT SECSWORDS)
        kb_connection.tables['swords'].selectFromTable(kb_connection.cursor, ['swords'], [('aledaid', '=', self.id)])
        res = kb_connection.cursor.fetchone()
        if res != None and res != '': self.kb_data['swords'] = res[0].split('#')[0:31]
        # SLUGS
        kb_connection.tables['slugs'].selectFromTable(kb_connection.cursor, ['slugs'], [('aledaid', '=', self.id)])
        res = kb_connection.cursor.fetchone()
        if res != None and res != '': self.kb_data['slugs'] = res[0].split('#')
        # IPTC
        kb_connection.tables['iptc'].selectFromTable(kb_connection.cursor, ['iptc'], [('aledaid', '=', self.id)])
        res = kb_connection.cursor.fetchone()
        if res != None and res != '': self.kb_data['iptc'] = res[0].split('#')
        # PEERS (ONLY 20 FIRST, NOT SECPEERS)
        kb_connection.tables['peers'].selectFromTable(kb_connection.cursor, ['peers'], [('aledaid', '=', self.id)])
        res = kb_connection.cursor.fetchone()
        if res != None and res != '': self.kb_data['peers'] = res[0].split('#')[0:11]
        # RELATED: ADD TO PEERS
        kb_connection.tables['related'].selectFromTable(kb_connection.cursor, ['related'], [('aledaid', '=', self.id)])
        res = kb_connection.cursor.fetchone()
        if res != None and res != '':
            if res != '':
                if self.kb_data['peers'] != None:
                    rel = filter(lambda r:r not in self.kb_data['peers'], res[0].split('#'))
                    self.kb_data['peers'].extend(rel)
                else:
                    rel = res[0].split('#')
                    self.kb_data['peers'] = rel

        # MENTIONS TO FREQ
        kb_connection.tables['mentions'].selectFromTable(kb_connection.cursor, ['mentions'], [('aledaid', '=', self.id)])
        res = kb_connection.cursor.fetchone()
        if res != None and res != '':
            m2f = res[0].split('#')
            self.kb_data['mentions'] = dict(zip(map(lambda m:m.split(':')[0], m2f), map(lambda m:int(m.split(':')[1]), m2f)))
        # TITLE AND TITLEPAR
        kb_connection.tables['main'].selectFromTable(kb_connection.cursor, ['wptitle', 'titlepar'], [('aledaid', '=', self.id)])
        res = kb_connection.cursor.fetchone()
        if res != None and res != '': 
            self.kb_data['title'] = res[0]
            self.kb_data['titlepar'] = res[1]
        else:
            # print >> sys.stderr, "%d: not in main table info"%(self.id)
            self.kb_data['title'] = ""
            self.kb_data['titlepar'] = ""

    def addPeersData(self, aleda_vars_connection):
        if self.kb_data['peers'] != None:
            self.kb_data['peersvars'] = set([])
            for peerid in self.kb_data['peers']:
                aleda_vars_connection.tables['data'].selectFromTable(aleda_vars_connection.cursor, ['variant'], [('key', '=', peerid)])
                peersvars = map(lambda x:x[0], aleda_vars_connection.cursor.fetchall())
                for peervar in peersvars:
                    self.kb_data['peersvars'].add(peervar)
            

    def addMention(self, mention):
        self.mentions[mention.id] = mention

    def displayInitTime(self, start):
        print >> sys.stderr, '[Doc %s] Candidate %s init: %.2fs'%(self.document.id, self.displayId(), getTime() - start)
  
class AledaCandidate(Candidate):
    def __init__(self, aleda_id, variants2counts, aleda_refs_connection, aleda_vars_connection, kb_connection, document):
        super(AledaCandidate, self).__init__(document)
        start = getTime()
        self.id = aleda_id
        self.uid = aleda_id
        self.candidate_type = 'ALEDA'
        self.candidate_type2 = None
        self.mentions = {}
        self.scores = {}
        # self.known = True
        self.name_sims = {}
        self.wpoccs = {}

        self.variants = variants2counts.keys()
        # FILL ALEDA DATA
        aleda_refs_connection.tables['data'].selectFromTable(aleda_refs_connection.cursor,  ['name', 'type', 'weight', 'link'], [('key', '=', self.id)])
        (self.name, temp_type, aleda_weight, href) = aleda_refs_connection.cursor.fetchone()
        self.href = href
        self.type = aledaType2nomosType(temp_type)
        self.aleda_data['amb_rate'] = sum(map(lambda v:variants2counts[v]-1, variants2counts))
        self.aleda_data['syn_rate'] = float(len(self.variants))
        self.aleda_data['in_aleda'] = True
        self.aleda_data['aleda_type'] = self.type
        if self.type == 'Location':
            self.candidate_type2 = 'B'
            self.aleda_data['aleda_wikipedia_weight'] = None
            if aleda_weight == 10 or aleda_weight == "NULL": self.aleda_data['aleda_geonames_weight'] = None
            else: self.aleda_data['aleda_geonames_weight'] = float(aleda_weight)
            aleda_refs_connection.tables['data'].selectFromTable(aleda_refs_connection.cursor,  ['subtype', 'geonames_type', 'country_code'], [('key', '=', self.id)])
            (self.subtype, self.geonamestype, self.country_code) = aleda_refs_connection.cursor.fetchone()
        else:
            self.candidate_type2 = 'A'
            self.aleda_data['aleda_geonames_weight'] = None
            if aleda_weight == "NULL":
                self.aleda_data['aleda_wikipedia_weight'] = None
            else:
                self.aleda_data['aleda_wikipedia_weight'] = float(aleda_weight)
            self.subtype = None; self.geonamestype = None
            try: self.gender = re.match('^_PERSON_(m|f)$', temp_type).group(1)
            except AttributeError: pass
            self.relSwords = {}

        # CHECK IF ENTITY IS IN KB
        if self.candidate_type2 == 'A':
            self.addKBWPData(kb_connection)
            self.addPeersData(aleda_vars_connection)

    def computeNameSim(self, mention):
        # try: self.name_sims[mention.id] = 1.0/edit_distance(mention.variant.string, self.name)
        self.name_sims[mention.id] = edit_distance(mention.variant.string, self.name)
        # except ZeroDivisionError: self.name_sims[mention.id] = 1.0
        if self.name in self.document.variants:
            self.name_in_doc = True
        else:
            self.name_in_doc = False
            

class NilCandidate(Candidate):
    def __init__(self, name, document):
        super(NilCandidate, self).__init__(document)
        self.id = name
        self.uid = "NIL#%s"%(name)
        self.candidate_type = 'NIL'
        self.candidate_type2 = None
        self.mentions = {}
        self.scores = {}
        # FILL ALEDA DATA (NONE)
        self.type = 'Null'
        self.name = name
        self.href = 'http://nomos/linking/anomymousAFPentities/'+self.name+'.not'


class NAE(Candidate):
    def __init__(self, string, document):
        super(NAE, self).__init__(document)
        self.id = string
        self.uid = "NAE#%s"%(string)
        self.name = string
        self.mentions = {}
        self.scores = {}
        self.variants = []
        # FILL ALEDA DATA (NONE)
        self.type = 'Null'
        self.string = string
        self.href = 'http://nomos/linking/discardedEntities/'+self.name+'.not'
        self.candidate_type = 'NAE'
        self.candidate_type2 = None

        
