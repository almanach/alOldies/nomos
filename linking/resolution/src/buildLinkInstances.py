#!/usr/bin/python
# -*- coding: utf-8 -*-
import math
import sqlite3
import codecs
import re
import os
import sys
from optparse import *
from xml.dom.minidom import *
sys.path.append('@common_src@')
import NomosUtils
import NomosDocument
import LinkingInstance
from utils import *

# def build(nomos_file, gold_file, datatype, aleda_vars_connection, aleda_refs_connection, lex_connection, kb_connection):
def build(nomos_file, gold_file, aleda_vars_connection, aleda_refs_connection, lex_connection, kb_connection):
    try: nomos_dom = parse(nomos_file)
    except: print >> sys.stderr, "Can't parse %s"%(nomos_file); return
    # print >> sys.stderr, 'Creating Document for nomos file'
    nomos_document = NomosDocument.Document(nomos_dom, 'auto', False, lex_connection, kb_connection)
    try: gold_dom = parse(gold_file)
    except: print >> sys.stderr, "Can't parse %s"%(gold_file); return
    gold_document = NomosDocument.Document(gold_dom, 'gold')
    # print >> sys.stderr, 'Creating Document for gold file'

    # print >> sys.stderr, 'Populating candidates for nomos mentions'
    nomos_document.populateCandidates(aleda_vars_connection, aleda_refs_connection, kb_connection)
    for m in nomos_document.mentions.values():
        m.setOtherAttributes(kb_connection)

    # print >> sys.stderr, 'Getting Detection for nomos mentions in gold document'
    # NomosUtils.getDetectionAndResolution(gold_document, nomos_document, False)
    # SET DETECTION BY COMPARING NOMOS AND GOLD MENTIONS
    # IF NER IS GOLD, WILL JUST SET ALL TO EXACT MATCHES
    NomosUtils.getDetection(gold_document, nomos_document, False, options.goldner)
    
    # writeExamples(nomos_document, datatype, options.learn)
    writeExamples(nomos_document, options.learn)

    nomos_document.unlinkDom()
    gold_document.unlinkDom()

# def writeExamples(nomos_document, datatype, learn):
def writeExamples(nomos_document, learn):
    if learn == 'megam':
        # writeMegamExamples(nomos_document, datatype)
        writeMegamExamples(nomos_document)
    elif learn == 'admirable':
        # writeAdmirableExamples(nomos_document, datatype)
        writeAdmirableExamples(nomos_document)

# def writeAdmirableExamples(nomos_document, datatype):
def writeAdmirableExamples(nomos_document):
    for mention in filter((lambda mention: mention.detection == 'exact_match'), nomos_document.mentions.values()):
        # i = 0
        # for candidate in mention.candidates:
        # DONT WRITE EXAMPLE OF NAE IF NER IS GOLD
        # if candidate.candidate_type == 'NAE' and options.goldner: continue
        # ==> DO WRITE IT!! TO EVALUATE NAE DETECTION
        # WRITE NAE ANYWAY: EXPERIENCES WITH AND WITHOUT
        # linking_instance = LinkingInstance.LinkingInstance(nomos_document, mention, candidate, features, fclasses)
        linking_instance = LinkingInstance.LinkingInstance(nomos_document, mention, features_list, features2type, fclasses)
        # linking_instance.writeAdmirableExample(datatype)
        linking_instance.writeAdmirableExample()
        # i += 1
        # if i == len(mention.candidates):
        #     sys.stdout.write('#next\n')
        # else:
        #     sys.stdout.write('#\n')
        # sys.stdout.write('#%s#newline\n'%(datatype))
        # sys.stdout.write('#newline\n')
    # NO FALSE MATCHES IF GOLD NER
    for mention in filter((lambda mention: mention.detection == 'false_match'), nomos_document.mentions.values()):
        # i = 0
        # for candidate in mention.candidates:
        # DONT WRITE EXAMPLE OF NAE IF NER IS GOLD  - BUT SHOULD NEVER HAPPEN WITH FALSE MATCH
        # if candidate.candidate_type == 'NAE' and options.goldner: continue
        # WRITE NAE ANYWAY: EXPERIENCES WITH AND WITHOUT
        # linking_instance = LinkingInstance.LinkingInstance(nomos_document, mention, candidate, features, fclasses)
        linking_instance = LinkingInstance.LinkingInstance(nomos_document, mention, features_list, features2type, fclasses)
        # linking_instance.writeAdmirableExample(datatype)
        linking_instance.writeAdmirableExample()
        # i += 1
        # if i == len(mention.candidates):
        #     sys.stdout.write('#next\n')
        # else:
        #     sys.stdout.write('#\n')
        # sys.stdout.write('#%s#newline\n'%(datatype))
        # sys.stdout.write('#newline\n')


# def writeMegamExamples(nomos_document, datatype):
def writeMegamExamples(nomos_document):        
    for mention in filter((lambda mention: mention.detection == 'exact_match'), nomos_document.mentions.values()):
        i = 0
        # for candidate in mention.candidates:
        # linking_instance = LinkingInstance.LinkingInstance(nomos_document, mention, candidate, features, fclasses)
        linking_instance = LinkingInstance.LinkingInstance(nomos_document, mention, features_list, features2type, fclasses)
        # linking_instance.writeMegamExample(datatype, options.mode)
        linking_instance.writeMegamExample(options.mode)
        # i += 1
        # if i == len(mention.candidates):
        #     sys.stdout.write('#next\n')
        # else:
        #     sys.stdout.write('#\n')
        # sys.stdout.write('#%s#newline\n'%(datatype))
        # sys.stdout.write('#newline\n')
    # NO FALSE MATCHES IF GOLD NER
    for mention in filter((lambda mention: mention.detection == 'false_match'), nomos_document.mentions.values()):
        # i = 0
        # for candidate in mention.candidates:
        # DONT WRITE EXAMPLE OF NAE IF NER IS GOLD  - BUT SHOULD NEVER HAPPEN WITH FALSE MATCH
        # if candidate.candidate_type == 'NAE' and options.goldner: continue
        # WRITE NAE ANYWAY: EXPERIENCES WITH AND WITHOUT
        # linking_instance = LinkingInstance.LinkingInstance(nomos_document, mention, candidate, features, fclasses)
        linking_instance = LinkingInstance.LinkingInstance(nomos_document, mention, features_list, features2type, fclasses)
        # linking_instance.writeMegamExample(datatype, options.mode)
        linking_instance.writeMegamExample(options.mode)
        # i += 1
        # if i == len(mention.candidates):
        #     sys.stdout.write('#next\n')
        # else:
        #     sys.stdout.write('#\n')
        # sys.stdout.write('#%s#newline\n'%(datatype))
        # sys.stdout.write('#newline\n')
        
def main():
    global options
    (options, args) = getOptions()
    # CONNECT TO SXPIPE-aleda DB + LEFFF
    aleda_refs_connection = DBConnection(options.aledadir+'ne_refs.fr.dat', ['data'])
    aleda_vars_connection = DBConnection(options.aledadir+'ne_vars.fr.dat', ['data'])
    lex_connection = DBConnection(options.aledadir+'lefff.dat', ['data'])
    # CONNECT TO NOMOS-DB
    # kb_connection = DBConnection(options.entities, ['categories', 'coocs', 'frequency', 'main', 'mentions', 'slugs', 'surrwords', 'swords', 'wordclusters'])
    kb_connection = DBConnection(options.kb, getKBDBtables())
    # kb_connection.addTablesInstances(initTables(initDB.getTables('entities')))
    # PROCESS NEWS
    # arg = args[0]
    global features_list; global features2type
    features_list, features2type = NomosUtils.getFeatures(options.features_file)
    global fclasses
    if options.fclasses_file != None: fclasses = NomosUtils.getFeaturesClasses(options.fclasses_file)
    else: fclasses = None
    cpt = 0
    for root, dirs, files in os.walk(options.nomosdir):
        for file in files:
            if file.endswith('.xml'):
                cpt += 1
                nomos_file = os.path.join(root, file)
                message = 'Build from %d docs'%(cpt)
                sys.stderr.write(message+'\b'*(len(message)))
                gold_file = options.golddir+"/"+re.sub("nomos.xml", "gold.xml", os.path.basename(nomos_file))
                if not os.path.exists(gold_file):                
                    sys.exit("%s is not a matching gold file for %s\n"%(gold_file, nomos_file))
                # print >> sys.stderr, "Processing %s and %s"%(nomos_file, gold_file)
                build(nomos_file, gold_file, aleda_vars_connection, aleda_refs_connection, lex_connection, kb_connection)
    printerr(str(cpt)+' files processed.')

    aleda_refs_connection.close(); aleda_vars_connection.close(); kb_connection.close(); lex_connection.close()
    

def getOptions():
    usage = '%s [options]'%sys.argv[0]
    parser = OptionParser(usage)
    parser.add_option("--goldner", action="store_true", dest="goldner", default=False, help="NER is gold")
    parser.add_option("--golddir", action="store", dest="golddir", default=None, help="Gold directory")
    parser.add_option("--nomosdir", action="store", dest="nomosdir", default=None, help="Nomos directory")
    parser.add_option("--kb", action="store", dest="kb", default='__prefix__/share/nomos/kbwp.dat', help="<path to KB data base (__prefix__/share/nomos/kbwp.dat)>")
    parser.add_option("--aledadir", action="store", dest="aledadir", default='__prefix__/share/aleda/', help="<path to dir with aleda data bases (__prefix__/share/aleda/)>")
    parser.add_option("--fclasses", action="store", dest="fclasses_file", default=None, help="<classes for multi-value features>")
    parser.add_option("--features", action="store", dest="features_file", default=None, help="<file for features reading (f|type<bin/multi>|size)>")
    parser.add_option("--learn", action="store", dest="learn", default="megam", type="choice", choices=['megam', 'admirable'], help="learn with [megam | admirable] (default megam)")
    parser.add_option("--mode", action="store", dest="mode", default=None, type="int", help="<mode for output writing - if mode=1: print mention string in output>")
    # parser.add_option("--swords", action="store", dest="swords_limit", type="int", default=10, help="max number of first saillant words to look up for a candidate entity (default 10)")            
    try: (options, args) = parser.parse_args()
    except OptionError: sys.stderr.write('OptionError'); parser.print_help(); sys.exit()
    except: sys.stderr.write('An error occured during options parsing\n'); parser.print_help(); sys.exit()
    # if len(args) != 1: parser.print_help(); sys.exit()
    # if options.entities == None or options.aledadir == None or
    if options.golddir == None or options.nomosdir == None: parser.print_help(); sys.exit()
    if options.fclasses_file == None and options.learn == "admirable": print >> sys.stderr, "Can't build Admirable input in BI without fclasses file"; sys.exit()
    # or not os.path.isdir(options.gold)
    return options, args

if __name__ == "__main__":
    main()
