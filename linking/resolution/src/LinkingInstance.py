#!/usr/bin/python
# -*- coding: utf-8 -*-
import re
import sys
import math
from decimal import Decimal as dec
from collections import defaultdict
sys.path.append('@common_src@')
from utils import *
from Entity import Variant
from NomosUtils import *
from editDistance import *

class LinkingInstance():
    # type2num = {'Person':1, 'Location':2, 'Organization':3, 'Company':4}
    # def __init__(self, doc, mention, candidate, features2type, fclasses):
    def __init__(self, doc, mention, features_list, features2type, fclasses):
        self.doc = doc
        self.mention = mention
        # self.candidate = candidate
        self.cands = self.mention.candidates
        self.Acands = filter(lambda c:c.candidate_type2=='A', self.cands)
        self.Bcands = filter(lambda c:c.candidate_type2=='B', self.cands)
        self.ABcands = self.Acands+self.Bcands
        self.nil = filter(lambda c:c.candidate_type=='NIL', self.cands)[0]
        try: self.z = filter(lambda c:c.candidate_type=='NAE', self.cands)[0]
        # IF NONAE: MENTION HAS NO CAND NAE
        except IndexError: self.z = None
        # self.ctype = self.candidate.candidate_type
        # self.ctype2 = self.candidate.candidate_type2
        # self.features = {}
        self.cands2features = {}
        for c in self.cands:
            self.cands2features[c.uid] = {}
        self.featureRecord = defaultdict(dict)
        if fclasses == None: self.bernoulli_implicit = False
        else: self.bernoulli_implicit = True
        self.setFeatureVectorKBWP(features_list)
        self.normalizeFeatures(features2type, fclasses)

    def computeCandidatesScore(self, features2weights, bias):
        for cand in self.cands:
            score = dec(str(bias)) # bias is float converted to decimal
            for item in self.cands2normalized_features[cand.uid].items():
                if self.bernoulli_implicit:
                    feature = '__'.join(item) # GLUE FEATURE NAME AND VALUE (features2weights.keys())
                    try: weight = dec(str(features2weights[feature])) # weight is float converted to decimal
                    except KeyError: continue
                    score += weight # operation on decimals
                else:
                    # if cand.candidate_type == 'NIL':
                    #     print >> sys.stderr, score
                    (feature, value) = item
                    try: weight = dec(str(features2weights[feature])) # weight is float converted to decimal
                    except KeyError: print >> sys.stderr, "WARNING: feature %s not in weights file"%(feature); continue
                    if value != None:
                        value = dec(value) # value is str converted to decimal
                        score += weight * value # operations on decimals

            # if cand.candidate_type == 'NIL':
            #     print >> sys.stderr, score
            #     print >> sys.stderr, "=============="
            cand.scores[self.mention.id] = score # score is decimal


    def setFeatureVectorKBWP(self, features):
        for f in features:
            if f != '':
                # featureFunction = feature2featureFunction(f)
                # self.featureFunction()
                # exec "self.%s()"%('get_'+f)
                exec feature2featureFunction(f)

    def normalizeFeatures(self, features2type, fclasses=None):
        self.cands2normalized_features = {}
        self.cands2additional_normalized_features = {}
        for cand in self.cands:
            self.cands2normalized_features[cand.uid] = {}
            self.cands2additional_normalized_features[cand.uid] = defaultdict(list)
            for feature in self.cands2features[cand.uid]:
                value = self.cands2features[cand.uid][feature]
                if value == None:
                    continue
                if isinstance(value, str):
                    self.cands2normalized_features[cand.uid][feature] = value
                    continue
                # IF FEATURE ALREADY HAS CLASSES
                # if feature not in binary_features and feature not in multi_features:
                if features2type[feature]['type'] != 'bin' and features2type[feature]['type'] != 'multi':
                    self.cands2normalized_features[cand.uid][feature] = str(self.cands2features[cand.uid][feature])
                    continue
                # ELSE
                if fclasses == None:
                    self.cands2normalized_features[cand.uid][feature] = getFeatureValue(value)
                else:
                    if features2type[feature]['type'] == 'bin':
                        self.cands2normalized_features[cand.uid][feature] = getFeatureClass(value, 'binary')
                    elif features2type[feature]['type'] == 'multi':
                        try:
                            value = getFeatureClass(value, 'multi', fclasses[feature])
                            self.cands2normalized_features[cand.uid][feature] = value#; print >> sys.stderr, self.normalized_features[feature],; print >> sys.stderr, type(value)
                            # IF FEATURE HAS A CLASS GREATER THAN 1, ADD FEATURE WITH LESSER CLASSES
                            # = IF CAND DOC SIM == 5, IT IS ALSO 4, 3, 2 AND 1
                            if int(value) > 1:
                                for below_value in range(1, int(value)):
                                    self.cands2additional_normalized_features[cand.uid][feature].append(str(below_value))
                                    
                        except KeyError:
                            continue
                            # self.normalized_features[feature] = getFeatureClass(value, 'binary', fclasses[feature])
                            # print >> sys.stderr, "No discrete values for feature %s"%(feature)
                            # No discrete values for feature cand_mention_surrwords_sim# No discrete values for feature cand_titlepar_mention_surrwords_dist
                            ## TAKE BINARY VALUE INSTEAD
                

    # def writeAdmirableExample(self, datatype):
    def writeAdmirableExample(self):
        # 0 1:43 2:34 5:21     \
        #  0.3 1:2 3:0.32        -- one instance (3 candidates)
        # 2 2:3 3:4 19:-0.63   /
        i = 0
        for cand in self.cands:
            i += 1
            line = self.doc.id+'#'+self.mention.id+'#'+cand.displayId()+'_'+cand.candidate_type+'#'
            # ADD RES 0 FOR FALSE MENTIONS (NO GOLD MENTION)
            # RESULT = RANK SETTING (INVERSE OF CLASSES)
            # WARNING! PARTIAL MATCHES ARE NOT TREATED AS FALSE WRT NAE EXAMPLES, ONLY TOTAL FALSE MATCHES
            if self.mention.gold_mention is None:
                if cand.candidate_type == 'NAE': rank = 0 
                else: rank = 1
            # IF GOLD MENTION AND CAND IS NAE: 0; OTHERWISE CHECK CAND/GOLD ID/NAME
            else:
                if cand.candidate_type == 'NAE':
                    rank = 1
                # TREAT PARTIAL MATCHES AS CORRECT MATCHES IF CAND IS RIGHT = ONLY WRT LINKING => COMMENT FOLLOWING ELSE
                # OR NEGATIVE CLASS FOR *ALL* CANDIDATES
                else:
                    if self.mention.detection == 'false_match':
                        rank = 1
                    else:
                        if cand.candidate_type == 'ALEDA':
                            if str(cand.id) == self.mention.gold_mention.entity_id: rank = 0
                            else: rank = 1
                        elif cand.candidate_type == 'NIL':
                            if self.mention.gold_mention.entity_type == 'NIL': rank = 0
                            else: rank = 1
            line += str(rank)
            for feature, value in self.cands2normalized_features[cand.uid].items():
                line += '\t'
                # feature_code = str(feature2code(feature))
                value = getAdmirableValue(value)
                line += feature+'__'+value+':1'
            for add_feature, values in self.cands2additional_normalized_features[cand.uid].items():
                # feature_code = str(feature2code(add_feature))
                for value in values:
                    line += '\t'
                    value = getAdmirableValue(value)
                    line += add_feature+'__'+value+':1'
            # line += '#'+datatype+'\n'
            # line += '\n'
            sys.stdout.write(line.encode('utf-8'))
            if i == len(self.cands):
                sys.stdout.write('#next\n')
            else:
                sys.stdout.write('#\n')


    # def writeMegamExample(self, datatype, mode=None):
    def writeMegamExample(self, mode=None):
        i = 0
        for cand in self.cands:
            i += 1
            line = self.doc.id+'#'+self.mention.id+'#'+cand.displayId()+'_'+cand.candidate_type+'#'
            # ADD RES 0 FOR FALSE MENTIONS (NO GOLD MENTION)
            # WARNING! PARTIAL MATCHES ARE NOT TREATED AS FALSE WRT NAE EXAMPLES, ONLY TOTAL FALSE MATCHES
            if self.mention.gold_mention is None:
                if cand.candidate_type == 'NAE': result = 1 
                else: result = 0
            # IF GOLD MENTION AND CAND IS NAE: 0; OTHERWISE CHECK CAND/GOLD ID/NAME
            else:
                if cand.candidate_type == 'NAE':
                    result = 0
                # TREAT PARTIAL MATCHES AS CORRECT MATCHES IF CAND IS RIGHT = ONLY WRT LINKING => COMMENT FOLLOWING ELSE
                # OR NEGATIVE CLASS FOR *ALL* CANDIDATES
                else:
                    if self.mention.detection == 'false_match':
                        result = 0
                    else:
                        if cand.candidate_type == 'ALEDA':
                            if str(cand.id) == self.mention.gold_mention.entity_id: result = 1
                            else: result = 0
                        elif cand.candidate_type == 'NIL':
                            if self.mention.gold_mention.entity_type == 'NIL': result = 1
                            else: result = 0
            line += str(result)+'#'
            # line += '\t'.join(map((lambda t: ' '.join((t[0], str(t[1])))), filter((lambda t: t[1] != None), self.normalized_features.items())))
            if self.bernoulli_implicit:
                line += '\t'.join(map((lambda t: '__'.join((t[0], t[1]))), self.cands2normalized_features[cand.uid].items()))
                if self.cands2additional_normalized_features[cand.uid] != {}: line += '\t'
                for add_feature, values in self.cands2additional_normalized_features[cand.uid].items():
                    line += '\t'
                    line += '\t'.join(map(lambda value: add_feature+'__'+value, values))
            else:
                try:
                    line += '\t'.join(map((lambda t: ' '.join((t[0], t[1]))), self.cands2normalized_features[cand.uid].items()))
                except TypeError:
                    pass
                    # print >> sys.stderr, cand.uid
                    # print >> sys.stderr, self.cands2normalized_features[cand.uid].items()
                    # sys.exit()
            # line += '#'+datatype+'\n'
            # line += '\n'
            # TO WRITE ONLY POSITIVE EXAMPLES, UNCOMMENT THIS TEST
            # TO WRITE BOTH POSITIVE AND NEGATIVE EXAMPLES, COMMENT THIS TEST
            # if result == 1:
            if mode == 1: # FOR VARIABLE STUDY
                #urn:newsml:afp.com:20090520T164214Z:TX-PAR-FVU98:1
                line = ''+re.sub('^[^-]+-[A-Z]+-([^:]+):', r'\1', self.doc.id)+'\t'+self.mention.variant.string+'\t'+cand.displayId()+'_'+cand.candidate_type+'\t'+str(result)+'\t'
                line += '\t'.join(map((lambda t: '__'.join((t[0], t[1]))), self.cands2normalized_features[cand.uid].items()))
                # line += "\n"
                sys.stdout.write(line.encode('utf-8'))
                return
            sys.stdout.write(line.encode('utf-8'))
            if i == len(self.cands):
                sys.stdout.write('#next\n')
            else:
                sys.stdout.write('#\n')

    ##################################################################################################################################
    ##################################################################################################################################
    # METHODS FOR FEATURE COMPUTATION
    ##################################################################################################################################
    ##################################################################################################################################
    
    def setNoneFeature(self, f, cand):
        self.cands2features[cand.uid][f] = None
    def setFeature(self, cand, f, val):
        # IF NONAE: MENTION HAS NO CAND NAE => self.z == NONE
        if cand != None:
            if val == None:
                # self.cands2features[cand.uid][f] = None
                return
            self.cands2features[cand.uid][f] = val
            
    ##################################################################################################################################
    # ABNZ
    ##################################################################################################################################
    def get_m_amb_rate(self, f):
        if self.ABcands != []:
            self.setFeature(self.z, f, len(self.ABcands))
        
    ##################################################################################################################################
    # AB
    ##################################################################################################################################
    def get_c_weightnull(self, f):
        for cand in self.Acands:
            if cand.aleda_data['aleda_wikipedia_weight'] == None:
                self.setFeature(cand, f, True)
        for cand in self.Bcands:
            if cand.aleda_data['aleda_geonames_weight'] == None:
                self.setFeature(cand, f, True)
    def get_c_name_in_doc(self, f):
        for cand in [c for c in self.Acands if c.name_in_doc]:
            self.setFeature(cand, f, True)            
    def get_c_noname_in_doc(self, f):
        for cand in [c for c in self.Acands if not c.name_in_doc]:
            self.setFeature(cand, f, True)            

    def get_c_multi_m(self, f):
        for cand in filter(lambda c:len(c.mentions)>1, self.ABcands):
            self.setFeature(cand, f, True)
    def get_c_aleda_type_in_m_types(self, f):
        for cand in self.ABcands:
            val = cand.type in self.mention.types
            if val:
                self.setFeature(cand, f, True)
    def get_c_name_m_sim(self, f):
        for cand in self.ABcands:
            if cand.name_sims[self.mention.id] != 0.0:
                self.setFeature(cand, f, cand.name_sims[self.mention.id])
    def get_c_name_m_sim_rank_1(self, f):
        name_sims = map(lambda c:c.name_sims[self.mention.id], self.ABcands)
        if len(name_sims)>1:
            for cand in self.ABcands:
                if cand.name_sims[self.mention.id] == max(name_sims) and cand.name_sims[self.mention.id] != 0:
                    self.setFeature(cand, f, True)
    def get_c_name_m_sim_rank_mid(self, f):
        name_sims = map(lambda c:c.name_sims[self.mention.id], self.ABcands)
        if len(name_sims)>2:
            for cand in self.ABcands:
                if cand.name_sims[self.mention.id] != max(name_sims) and cand.name_sims[self.mention.id] != min(name_sims) and cand.name_sims[self.mention.id] != 0:
                    self.setFeature(cand, f, True)
    def get_c_name_m_sim_rank_bot(self, f):
        name_sims = map(lambda c:c.name_sims[self.mention.id], self.ABcands)
        if len(name_sims)>1:
            for cand in self.ABcands:
                if cand.name_sims[self.mention.id] == min(name_sims) and cand.name_sims[self.mention.id] != 0:
                    self.setFeature(cand, f, True)
    def get_c_syn_rate(self, f):
        for cand in self.ABcands:
            self.setFeature(cand, f, cand.aleda_data['syn_rate'])
    def get_c_amb_rate(self, f):
        for cand in self.ABcands:
            self.setFeature(cand, f, cand.aleda_data['amb_rate'])
    ##################################################################################################################################
    # AB COMBINED
    ##################################################################################################################################
    def get_c_name_m_sim_rank_1_and_c_aleda_type_in_m_types(self, f):
        for cand in self.ABcands:
            try: val = self.cands2features[cand.uid]['c_name_m_sim_rank_1'] and self.cands2features[cand.uid]['c_aleda_type_in_m_types']
            except KeyError: val = None
            if val != None:
                self.setFeature(cand, f, True)
    def get_c_name_m_sim_rank_1_and_c_aleda_type_in_m_types_and_c_gender_equals_m_gender(self, f):
        for cand in self.ABcands:
            try: val = self.cands2features[cand.uid]['c_name_m_sim_rank_1'] and self.cands2features[cand.uid]['c_aleda_type_in_m_types'] and self.cands2features[cand.uid]['c_gender_equals_m_gender']
            except KeyError: val = None
            if val != None:
                self.setFeature(cand, f, True)
    # def get_c_name_m_sim_rank_mid_and_c_aleda_type_in_m_types(self, f):
    # def get_c_name_m_sim_rank_mid_and_c_aleda_type_in_m_types_and_c_gender_equals_m_gender(self, f):
    # def get_c_name_m_sim_rank_bot_and_c_aleda_type_in_m_types(self, f):
    # def get_c_name_m_sim_rank_bot_and_c_aleda_type_in_m_types_and_c_gender_equals_m_gender(self, f):
    ##################################################################################################################################
    # A
    ##################################################################################################################################
    def get_c_name_m_longer_sim(self, f):
        if self.mention.longer != None and self.mention.longer and self.Acands != []:
            sims = []
            for cand in self.Acands:
                for longer in self.mention.longer:
                    sims.append(edit_distance(longer, cand.name))
            sim = min(sims)
            self.setFeature(cand, f, sim)
    def get_c_wp_occs(self, f):
        for cand in self.Acands:
            if cand.kb_data['frequency'] == 0.0 or cand.kb_data['frequency'] == None:
                val = -10.0
            else:
                val = math.log(cand.kb_data['frequency'])
            self.setFeature(cand, f, val)
    def get_c_wp_occs_rank_1(self, f):
        freqs = map(lambda c:c.kb_data['frequency'], self.Acands)
        if len(freqs)>1:
            for cand in self.Acands:
                if cand.kb_data['frequency'] == max(freqs):
                    self.setFeature(cand, f, True)
    def get_c_wp_occs_rank_mid(self, f):
        freqs = map(lambda c:c.kb_data['frequency'], self.Acands)
        if len(freqs)>2:
            for cand in self.Acands:
                if cand.kb_data['frequency'] != max(freqs) and cand.kb_data['frequency'] != min(freqs):
                    self.setFeature(cand, f, True)
    def get_c_wp_occs_rank_bot(self, f):
        freqs = map(lambda c:c.kb_data['frequency'], self.Acands)
        if len(freqs)>1:
            for cand in self.Acands:
                if cand.kb_data['frequency'] == min(freqs):
                    self.setFeature(cand, f, True)
    def get_c_wp_occs_m(self, f):
        for cand in self.Acands:
            try: val = cand.kb_data['mentions'][self.mention.variant.string]
            except KeyError: val = None
            except TypeError: val = None
            if val != 0.0 and val != None:
                self.setFeature(cand, f, math.log(val))
    def get_c_wp_occs_m_rank_1(self, f):
        for cand in self.Acands:
            try:
                mention_counts = cand.kb_data['mentions'].values()
                val = cand.kb_data['mentions'][self.mention.variant.string] == max(mention_counts)
            except KeyError:
                val = False
            except TypeError:
                val = False
            except AttributeError:
                val = False
            if val:
                self.setFeature(cand, f, True)
    def get_c_wp_occs_m_rank_mid(self, f):
        for cand in self.Acands:
            try:
                mention_counts = cand.kb_data['mentions'].values()
                if len(mention_counts) > 1:
                    val = cand.kb_data['mentions'][self.mention.variant.string] != max(mention_counts) and cand.kb_data['mentions'][self.mention.variant.string] != min(mention_counts)
            except KeyError:
                val = False
            except TypeError:
                val = False
            except AttributeError:
                val = False
                if val:
                    self.setFeature(cand, f, True)
    def get_c_wp_occs_m_rank_bot(self, f):
        for cand in self.Acands:
            try:
                mention_counts = cand.kb_data['mentions'].values()
                val = cand.kb_data['mentions'][self.mention.variant.string] == min(mention_counts)
            except KeyError:
                val = False
            except TypeError:
                val = False
            except AttributeError:
                val = False
            if val:
                self.setFeature(cand, f, True)
    def get_c_wp_occs_rel(self, f):
        wpoccs = filter(lambda f:f!=None, map(lambda c:c.kb_data['frequency'], self.Acands))
        if len(wpoccs) > 1:
            for cand in self.Acands:
                try: cand_rel_wpoccs = float(cand.kb_data['frequency']) / float(sum(wpoccs))
                except TypeError: continue
                self.setFeature(cand, f, cand_rel_wpoccs)
                self.featureRecord[f][cand.uid] = cand_rel_wpoccs

    def get_c_wp_occs_rel_rank_1(self, f):
        if 'c_wp_occs_rel' in self.featureRecord and len(self.featureRecord['c_wp_occs_rel'])>1:
            rel_wp_occs = map(lambda x:self.featureRecord['c_wp_occs_rel'][x], self.featureRecord['c_wp_occs_rel'])
            for cand in self.Acands:
                try: cand_rel_wpoccs = self.featureRecord['c_wp_occs_rel'][cand.uid]
                except KeyError: continue
                if cand_rel_wpoccs == max(rel_wp_occs):
                    self.setFeature(cand, f, True)
    def get_c_wp_occs_rel_rank_mid(self, f):
        if 'c_wp_occs_rel' in self.featureRecord and len(self.featureRecord['c_wp_occs_rel'])>2:
            rel_wp_occs = map(lambda x:self.featureRecord['c_wp_occs_rel'][x], self.featureRecord['c_wp_occs_rel'])
            for cand in self.Acands:
                try: cand_rel_wpoccs = self.featureRecord['c_wp_occs_rel'][cand.uid]
                except KeyError: continue
                if cand_rel_wpoccs != min(rel_wp_occs) and cand_rel_wpoccs != max(rel_wp_occs):
                    self.setFeature(cand, f, True)
    def get_c_wp_occs_rel_rank_bot(self, f):
        if 'c_wp_occs_rel' in self.featureRecord and len(self.featureRecord['c_wp_occs_rel'])>1:
            rel_wp_occs = map(lambda x:self.featureRecord['c_wp_occs_rel'][x], self.featureRecord['c_wp_occs_rel'])
            for cand in self.Acands:
                try: cand_rel_wpoccs = self.featureRecord['c_wp_occs_rel'][cand.uid]
                except KeyError: continue
                if cand_rel_wpoccs == max(rel_wp_occs):
                    self.setFeature(cand, f, True)
    def get_c_titlepar_in_m_suw(self, f):
        for cand in self.Acands:
            if cand.kb_data['titlepar'] in self.mention.surrwords:
                self.setFeature(cand, f, True)
    def get_c_sw_d_sw_sim(self, f):
        for cand in filter(lambda c:c.relSwords[self.mention.id] != None, self.Acands):
            sim = computeCosSim(cand.relSwords[self.mention.id], self.doc.analytics['swords'])
            if sim != 0.0:
                self.setFeature(cand, f, sim)
                self.featureRecord[f][cand.uid] = sim
    def get_c_sw_m_suw_sim(self, f):
        for cand in filter(lambda c:c.relSwords[self.mention.id] != None, self.Acands):
            sim = computeCosSim(cand.relSwords[self.mention.id], self.mention.surrwords)
            if sim != 0.0:
                self.setFeature(cand, f, sim)
                self.featureRecord[f][cand.uid] = sim
    def get_c_sw_m_suw_sim_rank_1(self, f):
        if 'c_sw_m_suw_sim' in self.featureRecord and len(self.featureRecord['c_sw_m_suw_sim'])>1:
            sims = map(lambda x:self.featureRecord['c_sw_m_suw_sim'][x], self.featureRecord['c_sw_m_suw_sim'])
            for cand in filter(lambda c:c.relSwords[self.mention.id] != None, self.Acands):
                try: cand_sim = self.featureRecord['c_sw_m_suw_sim'][cand.uid]
                except KeyError: continue
                if cand_sim == max(sims) and cand_sim != 0.0:
                    self.setFeature(cand, f, True)
    def get_c_sw_m_suw_sim_rank_mid(self, f):
        if 'c_sw_m_suw_sim' in self.featureRecord and len(self.featureRecord['c_sw_m_suw_sim'])>2:
            sims = map(lambda x:self.featureRecord['c_sw_m_suw_sim'][x], self.featureRecord['c_sw_m_suw_sim'])
            for cand in filter(lambda c:c.relSwords[self.mention.id] != None, self.Acands):
                try: cand_sim = self.featureRecord['c_sw_m_suw_sim'][cand.uid]
                except KeyError: continue
                if cand_sim != min(sims) and cand_sim != max(sims) and cand_sim != 0.0:
                    self.setFeature(cand, f, True)
    def get_c_sw_m_suw_sim_rank_bot(self, f):
        if 'c_sw_m_suw_sim' in self.featureRecord and len(self.featureRecord['c_sw_m_suw_sim'])>1:
            sims = map(lambda x:self.featureRecord['c_sw_m_suw_sim'][x], self.featureRecord['c_sw_m_suw_sim'])
            for cand in filter(lambda c:c.relSwords[self.mention.id] != None, self.Acands):
                try: cand_sim = self.featureRecord['c_sw_m_suw_sim'][cand.uid]
                except KeyError: continue
                if cand_sim == min(sims) and cand_sim != 0.0:
                    self.setFeature(cand, f, True)
    def get_c_slugs_d_slugs_sim(self, f):
        for cand in filter(lambda c:c.kb_data['slugs'] != None, self.Acands):
            sim = computeCosSim(cand.kb_data['slugs'], self.doc.analytics['slugs'])
            if sim != 0.0:
                self.setFeature(cand, f, sim)
                self.featureRecord[f][cand.uid] = sim
    def get_c_slugs_d_slugs_sim_rank_1(self, f):
        if 'c_slugs_d_slugs_sim' in self.featureRecord and len(self.featureRecord['c_slugs_d_slugs_sim'])>1:
            sims = map(lambda x:self.featureRecord['c_slugs_d_slugs_sim'][x], self.featureRecord['c_slugs_d_slugs_sim'])
            for cand in filter(lambda c:c.kb_data['slugs'] != None, self.Acands):
                try: cand_sim = self.featureRecord['c_slugs_d_slugs_sim'][cand.uid]
                except KeyError: continue
                if cand_sim == max(sims) and cand_sim != 0.0:
                    self.setFeature(cand, f, True)
    def get_c_slugs_d_slugs_sim_rank_mid(self, f):
        if 'c_slugs_d_slugs_sim' in self.featureRecord and len(self.featureRecord['c_slugs_d_slugs_sim'])>2:
            sims = map(lambda x:self.featureRecord['c_slugs_d_slugs_sim'][x], self.featureRecord['c_slugs_d_slugs_sim'])
            for cand in filter(lambda c:c.kb_data['slugs'] != None, self.Acands):
                try: cand_sim = self.featureRecord['c_slugs_d_slugs_sim'][cand.uid]
                except KeyError: continue
                if cand_sim != min(sims) and cand_sim != max(sims) and cand_sim != 0.0:
                    self.setFeature(cand, f, True)
    def get_c_slugs_d_slugs_sim_rank_bot(self, f):
        if 'c_slugs_d_slugs_sim' in self.featureRecord and len(self.featureRecord['c_slugs_d_slugs_sim'])>1:
            sims = map(lambda x:self.featureRecord['c_slugs_d_slugs_sim'][x], self.featureRecord['c_slugs_d_slugs_sim'])
            for cand in filter(lambda c:c.kb_data['slugs'] != None, self.Acands):
                try: cand_sim = self.featureRecord['c_slugs_d_slugs_sim'][cand.uid]
                except KeyError: continue
                if cand_sim == min(sims) and cand_sim != 0.0:
                    self.setFeature(cand, f, True)
    def get_c_iptc_d_iptc_sim(self, f):
        for cand in filter(lambda c:c.kb_data['iptc'] != None, self.Acands):
            sim = computeCosSim(cand.kb_data['iptc'], self.doc.analytics['iptc'])
            if sim != 0.0:
                self.setFeature(cand, f, sim)
                self.featureRecord[f][cand.uid] = sim
    def get_c_iptc_d_iptc_sim_rank_1(self, f):
        if 'c_iptc_d_iptc_sim' in self.featureRecord and len(self.featureRecord['c_iptc_d_iptc_sim'])>1:
            sims = map(lambda x:self.featureRecord['c_iptc_d_iptc_sim'][x], self.featureRecord['c_iptc_d_iptc_sim'])
            for cand in filter(lambda c:c.kb_data['iptc'] != None, self.Acands):
                try: cand_sim = self.featureRecord['c_iptc_d_iptc_sim'][cand.uid]
                except KeyError: continue
                if cand_sim == max(sims) and cand_sim != 0.0:
                    self.setFeature(cand, f, True)
    def get_c_iptc_d_iptc_sim_rank_mid(self, f):
        if 'c_iptc_d_iptc_sim' in self.featureRecord and len(self.featureRecord['c_iptc_d_iptc_sim'])>2:
            sims = map(lambda x:self.featureRecord['c_iptc_d_iptc_sim'][x], self.featureRecord['c_iptc_d_iptc_sim'])
            for cand in filter(lambda c:c.kb_data['iptc'] != None, self.Acands):
                try: cand_sim = self.featureRecord['c_iptc_d_iptc_sim'][cand.uid]
                except KeyError: continue
                if cand_sim != min(sims) and cand_sim != max(sims) and cand_sim != 0.0:
                    self.setFeature(cand, f, True)
    def get_c_iptc_d_iptc_sim_rank_bot(self, f):
        if 'c_iptc_d_iptc_sim' in self.featureRecord and len(self.featureRecord['c_iptc_d_iptc_sim'])>1:
            sims = map(lambda x:self.featureRecord['c_iptc_d_iptc_sim'][x], self.featureRecord['c_iptc_d_iptc_sim'])
            for cand in filter(lambda c:c.kb_data['iptc'] != None, self.Acands):
                try: cand_sim = self.featureRecord['c_iptc_d_iptc_sim'][cand.uid]
                except KeyError: continue
                if cand_sim == min(sims) and cand_sim != 0.0:
                    self.setFeature(cand, f, True)
    def get_c_peers_vars_d_mentions_sim(self, f):
        for cand in filter(lambda c:c.kb_data['peersvars'] != None, self.Acands):
            sim = computeCosSim(cand.kb_data['peersvars'], map(lambda m:m.variant.string, self.doc.mentions.values()))
            if sim != 0.0:
                self.setFeature(cand, f, sim)
                self.featureRecord[f][cand.uid] = sim
    def get_c_peers_vars_d_mentions_sim_rank_1(self, f):
        if 'c_peers_vars_d_mentions_sim' in self.featureRecord and len(self.featureRecord['c_peers_vars_d_mentions_sim'])>1:
            sims = map(lambda x:self.featureRecord['c_peers_vars_d_mentions_sim'][x], self.featureRecord['c_peers_vars_d_mentions_sim'])
            for cand in filter(lambda c:c.kb_data['peersvars'] != None, self.Acands):
                try: cand_sim = self.featureRecord['c_peers_vars_d_mentions_sim'][cand.uid]
                except KeyError: continue
                if cand_sim == max(sims) and cand_sim != 0.0:
                    self.setFeature(cand, f, True)
    def get_c_peers_vars_d_mentions_sim_rank_mid(self, f):
        if 'c_peers_vars_d_mentions_sim' in self.featureRecord and len(self.featureRecord['c_peers_vars_d_mentions_sim'])>2:
            sims = map(lambda x:self.featureRecord['c_peers_vars_d_mentions_sim'][x], self.featureRecord['c_peers_vars_d_mentions_sim'])
            for cand in filter(lambda c:c.kb_data['peersvars'] != None, self.Acands):
                try: cand_sim = self.featureRecord['c_peers_vars_d_mentions_sim'][cand.uid]
                except KeyError: continue
                if cand_sim != min(sims) and cand_sim != max(sims) and cand_sim != 0.0:
                    self.setFeature(cand, f, True)
    def get_c_peers_vars_d_mentions_sim_rank_bot(self, f):
        if 'c_peers_vars_d_mentions_sim' in self.featureRecord and len(self.featureRecord['c_peers_vars_d_mentions_sim'])>1:
            sims = map(lambda x:self.featureRecord['c_peers_vars_d_mentions_sim'][x], self.featureRecord['c_peers_vars_d_mentions_sim'])
            for cand in filter(lambda c:c.kb_data['peersvars'] != None, self.Acands):
                try: cand_sim = self.featureRecord['c_peers_vars_d_mentions_sim'][cand.uid]
                except KeyError: continue
                if cand_sim == min(sims) and cand_sim != 0.0:
                    self.setFeature(cand, f, True)
    def get_c_peers_vars_d_sw_sim(self, f):
        for cand in filter(lambda c:c.kb_data['peersvars'] != None, self.Acands):
            sim = computeCosSim(cand.kb_data['peersvars'], self.doc.analytics['swords'])
            if sim != 0.0:
                self.setFeature(cand, f, sim)
                self.featureRecord[f][cand.uid] = sim
    def get_c_peers_vars_d_sw_sim_rank_1(self, f):
        if 'c_peers_vars_d_sw_sim' in self.featureRecord and len(self.featureRecord['c_peers_vars_d_sw_sim'])>1:
            sims = map(lambda x:self.featureRecord['c_peers_vars_d_sw_sim'][x], self.featureRecord['c_peers_vars_d_sw_sim'])
            for cand in filter(lambda c:c.kb_data['peersvars'] != None, self.Acands):
                try: cand_sim = self.featureRecord['c_peers_vars_d_sw_sim'][cand.uid]
                except KeyError: continue
                if cand_sim == max(sims) and cand_sim != 0.0:
                    self.setFeature(cand, f, True)
    def get_c_peers_vars_d_sw_sim_rank_mid(self, f):
        if 'c_peers_vars_d_sw_sim' in self.featureRecord and len(self.featureRecord['c_peers_vars_d_sw_sim'])>2:
            sims = map(lambda x:self.featureRecord['c_peers_vars_d_sw_sim'][x], self.featureRecord['c_peers_vars_d_sw_sim'])
            for cand in filter(lambda c:c.kb_data['peersvars'] != None, self.Acands):
                try: cand_sim = self.featureRecord['c_peers_vars_d_sw_sim'][cand.uid]
                except KeyError: continue
                if cand_sim != min(sims) and cand_sim != max(sims) and cand_sim != 0.0:
                    self.setFeature(cand, f, True)
    def get_c_peers_vars_d_sw_sim_rank_bot(self, f):
        if 'c_peers_vars_d_sw_sim' in self.featureRecord and len(self.featureRecord['c_peers_vars_d_sw_sim'])>1:
            sims = map(lambda x:self.featureRecord['c_peers_vars_d_sw_sim'][x], self.featureRecord['c_peers_vars_d_sw_sim'])
            for cand in filter(lambda c:c.kb_data['peersvars'] != None, self.Acands):
                try: cand_sim = self.featureRecord['c_peers_vars_d_sw_sim'][cand.uid]
                except KeyError: continue
                if cand_sim == min(sims) and cand_sim != 0.0:
                    self.setFeature(cand, f, True)
    def get_c_sw_d_locvars_sim(self, f):
        for cand in filter(lambda c:c.relSwords[self.mention.id] != None, self.Acands):
            sim = computeCosSim(cand.relSwords[self.mention.id], self.doc.analytics['locvars'])
            if sim != 0.0:
                self.setFeature(cand, f, sim)
                self.featureRecord[f][cand.uid] = sim
    def get_c_sw_d_locvars_sim_rank_1(self, f):
        if 'c_sw_d_locvars_sim' in self.featureRecord and len(self.featureRecord['c_sw_d_locvars_sim'])>1:
            sims = map(lambda x:self.featureRecord['c_sw_d_locvars_sim'][x], self.featureRecord['c_sw_d_locvars_sim'])
            for cand in filter(lambda c:c.relSwords[self.mention.id] != None, self.Acands):
                try: cand_sim = self.featureRecord['c_sw_d_locvars_sim'][cand.uid]
                except KeyError: continue
                if cand_sim == max(sims) and cand_sim != 0.0:
                    self.setFeature(cand, f, True)
    def get_c_sw_d_locvars_sim_rank_mid(self, f):
        if 'c_sw_d_locvars_sim' in self.featureRecord and len(self.featureRecord['c_sw_d_locvars_sim'])>2:
            sims = map(lambda x:self.featureRecord['c_sw_d_locvars_sim'][x], self.featureRecord['c_sw_d_locvars_sim'])
            for cand in filter(lambda c:c.relSwords[self.mention.id] != None, self.Acands):
                try: cand_sim = self.featureRecord['c_sw_d_locvars_sim'][cand.uid]
                except KeyError: continue
                if cand_sim != min(sims) and cand_sim != max(sims) and cand_sim != 0.0:
                    self.setFeature(cand, f, True)
    def get_c_sw_d_locvars_sim_rank_bot(self, f):
        if 'c_sw_d_locvars_sim' in self.featureRecord and len(self.featureRecord['c_sw_d_locvars_sim'])>1:
            sims = map(lambda x:self.featureRecord['c_sw_d_locvars_sim'][x], self.featureRecord['c_sw_d_locvars_sim'])
            for cand in filter(lambda c:c.relSwords[self.mention.id] != None, self.Acands):
                try: cand_sim = self.featureRecord['c_sw_d_locvars_sim'][cand.uid]
                except KeyError: continue
                if cand_sim == min(sims) and cand_sim != 0.0:
                    self.setFeature(cand, f, True)
    def get_c_wpw(self, f):
        for cand in self.Acands:
            # val = cand.aleda_data['aleda_wikipedia_weight']
            if cand.aleda_data['aleda_wikipedia_weight'] != None and cand.aleda_data['aleda_wikipedia_weight'] != 0:
                val = math.log(cand.aleda_data['aleda_wikipedia_weight'])
                if val != 0.0 or val != None:
                    self.setFeature(cand, f, val)
                    self.featureRecord[f][cand.uid] = val
    def get_c_wpw_rank_1(self, f):
        if 'c_wpw' in self.featureRecord and len(self.featureRecord['c_wpw'])>1:
            wpweights = map(lambda x:self.featureRecord['c_wpw'][x], self.featureRecord['c_wpw'])
            for cand in self.Acands:
                try: wpw = self.featureRecord['c_wpw'][cand.uid]
                except KeyError: continue
                if wpw == max(wpweights):
                    self.setFeature(cand, f, True)
    def get_c_wpw_rank_mid(self, f):
        if 'c_wpw' in self.featureRecord and len(self.featureRecord['c_wpw'])>2:
            wpweights = map(lambda x:self.featureRecord['c_wpw'][x], self.featureRecord['c_wpw'])
            for cand in self.Acands:
                try: wpw = self.featureRecord['c_wpw'][cand.uid]
                except KeyError: continue
                if wpw != min(wpweights) and wpw != max(wpweights):
                    self.setFeature(cand, f, True)
    def get_c_wpw_rank_bot(self, f):
        if 'c_wpw' in self.featureRecord and len(self.featureRecord['c_wpw'])>1:
            wpweights = map(lambda x:self.featureRecord['c_wpw'][x], self.featureRecord['c_wpw'])
            for cand in self.Acands:
                try: wpw = self.featureRecord['c_wpw'][cand.uid]
                except KeyError: continue
                if wpw == min(wpweights):
                    self.setFeature(cand, f, True)
    def get_c_gender_equals_m_gender(self, f):
        for cand in filter(lambda c:c.gender!=None, self.Acands):
            if cand.gender in self.mention.genders:
                self.setFeature(cand, f, True)
    ##################################################################################################################################
    # A COMBINED
    ##################################################################################################################################
    def get_c_d_sim(self, f):
        for cand in self.Acands:
            sims = []
            for feature in ['c_sw_m_suw_sim', 'c_sw_d_sw_sim', 'c_slugs_d_slugs_sim', 'c_iptc_d_iptc_sim', 'c_peers_vars_d_mentions_sim', 'c_peers_vars_d_sw_sim', 'c_sw_d_locvars_sim']:
                try: sims.append(self.cands2features[cand.uid][feature])
                except KeyError: continue
            # try:
            #     arithm = sum(sims)/float(len(sims))
            #     self.setFeature(cand, f, arithm)
            #     if arithm != 0.0:
            #         self.featureRecord[f][cand.uid] = arithm
            # except ZeroDivisionError:
            #     self.featureRecord[f][cand.uid] = 0.0
            # !!! CHANGE: TAKE MAX VALUE OF SIMS
            val = max(sims)
            if val != 0.0:
                self.setFeature(cand, f, val)
    def get_c_d_sim_rank_1(self, f):
        if 'c_d_sim' in self.featureRecord and len(self.featureRecord['c_d_sim']) > 1:
            sims = filter(lambda s:s!=0.0, map(lambda x:self.featureRecord['c_d_sim'][x], self.featureRecord['c_d_sim']))
            for cand in self.Acands:
                try: sim = self.featureRecord['c_d_sim'][cand.uid]
                except KeyError: continue
                try: m = max(sims)
                except ValueError: continue
                if sim == max(sims) and sim != 0.0:
                    self.setFeature(cand, f, True)
    def get_c_d_sim_rank_mid(self, f):
        if 'c_d_sim' in self.featureRecord and len(self.featureRecord['c_d_sim']) > 2:
            sims = filter(lambda s:s!=0.0, map(lambda x:self.featureRecord['c_d_sim'][x], self.featureRecord['c_d_sim']))
            for cand in self.Acands:
                try: sim = self.featureRecord['c_d_sim'][cand.uid]
                except KeyError: continue
                try: m = max(sims)
                except ValueError: continue
                if sim != min(sims) and sim != max(sims) and sim != 0.0:
                    self.setFeature(cand, f, True)
    def get_c_d_sim_rank_bot(self, f):
        if 'c_d_sim' in self.featureRecord and len(self.featureRecord['c_d_sim']) > 1:
            sims = filter(lambda s:s!=0.0, map(lambda x:self.featureRecord['c_d_sim'][x], self.featureRecord['c_d_sim']))
            for cand in self.Acands:
                try: sim = self.featureRecord['c_d_sim'][cand.uid]
                except KeyError: continue
                try: m = max(sims)
                except ValueError: continue
                if sim == min(sims) and sim != 0.0:
                    self.setFeature(cand, f, True)
    def get_c_d_sim_rank_1_margin(self, f):
        for cand in self.Acands:
            if 'c_d_sim_rank_1' in self.cands2features[cand.uid]:
                sims = sorted(filter(lambda s:s!=0.0, map(lambda x:self.featureRecord['c_d_sim'][x], self.featureRecord['c_d_sim'])), reverse=True)
                try:
                    margin = sims[0] - sims[1]
                    self.setFeature(cand, f, margin)
                except IndexError:
                    pass
    # def get_c_popularity(self, f):
    # def get_c_popularity_rank_1(self, f):
    # def get_c_popularity_rank_mid(self, f):
    # def get_c_popularity_rank_bot(self, f):

    ##################################################################################################################################
    # B
    ##################################################################################################################################
    def get_c_admloc(self, f):
        for cand in self.Bcands:
            if cand.subtype == "CountryDivision":
                self.setFeature(cand, f, True)
    def get_c_countrycode_equals_d_countrycode(self, f):
        for cand in self.Bcands:
            if cand.country_code == self.doc.analytics['country_code']:
                self.setFeature(cand, f, True)
    def get_c_country_equals_d_country(self, f):
        for cand in self.Bcands:
            if str(cand.id) == str(self.doc.analytics['country']):
                self.setFeature(cand, f, True)
    def get_c_geonw(self, f):
        for cand in filter(lambda c:c.aleda_data['aleda_geonames_weight']!=None, self.Bcands):
            # val = cand.aleda_data['aleda_geonames_weight']
            if cand.aleda_data['aleda_geonames_weight'] != None and cand.aleda_data['aleda_geonames_weight'] != 0:
                val = math.log(cand.aleda_data['aleda_geonames_weight'])
                if val != 0:
                    self.setFeature(cand, f, val)
                    self.featureRecord[f][cand.uid] = val
    def get_c_geonw_rank_1(self, f):
        if 'c_geonw' in self.featureRecord and len(self.featureRecord['c_geonw'])>1:
            gweights = map(lambda x:self.featureRecord['c_geonw'][x], self.featureRecord['c_geonw'])
            for cand in filter(lambda c:c.aleda_data['aleda_geonames_weight']!=None, self.Bcands):
                gw = self.featureRecord['c_geonw'][cand.uid]
                if gw == max(gweights):
                    self.setFeature(cand, f, True)
    def get_c_geonw_rank_mid(self, f):
        if 'c_geonw' in self.featureRecord and len(self.featureRecord['c_geonw'])>2:
            gweights = map(lambda x:self.featureRecord['c_geonw'][x], self.featureRecord['c_geonw'])
            for cand in filter(lambda c:c.aleda_data['aleda_geonames_weight']!=None, self.Bcands):
                gw = self.featureRecord['c_geonw'][cand.uid]
                if gw != min(gweights) and gw != max(gweights):
                    self.setFeature(cand, f, True)          
    def get_c_geonw_rank_bot(self, f):
        if 'c_geonw' in self.featureRecord and len(self.featureRecord['c_geonw'])>1:
            gweights = map(lambda x:self.featureRecord['c_geonw'][x], self.featureRecord['c_geonw'])
            for cand in filter(lambda c:c.aleda_data['aleda_geonames_weight']!=None, self.Bcands):
                gw = self.featureRecord['c_geonw'][cand.uid]
                if gw == min(gweights):
                    self.setFeature(cand, f, True)
    ##################################################################################################################################
    # NZ
    ##################################################################################################################################
    def get_m_wpoccs(self, f):
        # self.setFeature(self.nil, f, self.mention.wpoccs)
        if self.z != None and self.mention.wpoccs != None:
            if self.mention.wpoccs == 0.0:
                val = -10.0
            else:
                val = math.log(self.mention.wpoccs)
            self.setFeature(self.z, f, val)
    def get_m_doccs(self, f):
        # self.setFeature(self.nil, f, self.mention.occs)
        if self.z != None:
            self.setFeature(self.z, f, self.mention.occs)
    def get_c_nil_sole(self, f):
        if self.ABcands == []:
            # self.setFeature(self.nil, f, True)
            if self.z != None:
                self.setFeature(self.z, f, True)
    def get_c_size(self, f):
        if self.ABcands != []:
            # self.setFeature(self.nil, f, len(self.ABcands))
            if self.z != None:
                self.setFeature(self.z, f, len(self.ABcands))
    def get_m_nolongerexists(self, f):
        if self.mention.longer != None and self.mention.longer is False and self.z != None:
            self.setFeature(self.z, f, True)
            # self.setFeature(self.nil, f, True)

    ##################################################################################################################################
    # N
    ##################################################################################################################################
    # def get_c_sim_level(self, f):
    #     if self.Acands != []:
    #         if 'c_d_sim' in self.featureRecord:
    #             try: max_sim = max(filter(lambda s:s!=0.0, self.featureRecord['c_d_sim'].values()))
    #             except ValueError: return
    #             self.setFeature(self.nil, f, max_sim)
    def get_c_sim_level(self, f):
        if self.Acands != []:
            for cand in filter(lambda c:c.relSwords[self.mention.id] != None, self.Acands):
                sims = []
                if cand.relSwords[self.mention.id] != None:
                    sims.append(computeCosSim(cand.relSwords[self.mention.id], self.mention.surrwords))
                    sims.append(computeCosSim(cand.relSwords[self.mention.id], self.doc.analytics['locvars']))
                    sims.append(computeCosSim(cand.relSwords[self.mention.id], self.doc.analytics['swords']))
                if cand.kb_data['slugs'] != None:
                    sims.append(computeCosSim(cand.kb_data['slugs'], self.doc.analytics['slugs']))
                if cand.kb_data['iptc'] != None:
                    sims.append(computeCosSim(cand.kb_data['iptc'], self.doc.analytics['iptc']))
                if cand.kb_data['peersvars'] != None:
                    sims.append(computeCosSim(cand.kb_data['peersvars'], map(lambda m:m.variant.string, self.doc.mentions.values())))
                    sims.append(computeCosSim(cand.kb_data['peersvars'], self.doc.analytics['swords']))
                simsnonzero = filter(lambda x:x != 0.0, sims)
                if simsnonzero != []:
                    val = max(simsnonzero)
                    for cand in self.ABcands:
                        self.setFeature(cand, f, val)
                    self.setFeature(self.nil, f, val)
                    if self.z != None:
                        self.setFeature(self.z, f, val)
    # def get_c_pop_level_wp(self, f):
    #     if self.Acands != [] and self.z != None:
    #         avg = 0.0
    #         pops = filter(lambda p:p!=None, map(lambda c:c.aleda_data['aleda_wikipedia_weight'], self.Acands))
    #         try: avg = sum(pops) / float(len(pops))
    #         except ZeroDivisionError: avg = 0.0
    #         if avg != 0.0:
    #             val = math.log(avg)
    #             if val != 0.0:
    #                 self.setFeature(self.z, f, val)
    # def get_c_pop_level_geon(self, f):
    #     avg = 0.0
    #     if self.Bcands != []:
    #         pops = filter(lambda p:p!=None, map(lambda c:c.aleda_data['aleda_geonames_weight'], self.Bcands))
    #         try: avg = sum(pops) / float(len(pops))
    #         except ZeroDivisionError: avg = 0.0
    #         if avg != 0.0:
    #             val = math.log(avg)
    #             if val != 0.0:
    #                 self.setFeature(self.nil, f, val)
    def get_c_pop_level(self, f):
        if self.Acands != [] and self.z != None:
            avg = 0.0
            pops = filter(lambda p:p!=None, map(lambda c:c.aleda_data['aleda_wikipedia_weight'], self.Acands))
            try: avg = sum(pops) / float(len(pops))
            except ZeroDivisionError: avg = 0.0
            if avg != 0.0:
                val = math.log(avg)
                if val != 0.0:
                    self.setFeature(self.z, f, val)
        if self.Bcands != []:
            avg = 0.0
            pops = filter(lambda p:p!=None, map(lambda c:c.aleda_data['aleda_geonames_weight'], self.Bcands))
            try: avg = sum(pops) / float(len(pops))
            except ZeroDivisionError: avg = 0.0
            if avg != 0.0:
                val = math.log(avg)
                if val != 0.0:
                    self.setFeature(self.nil, f, val)
    def get_c_noname(self, f):
        if self.Acands != []:
            if [c for c in self.Acands if c.name_in_doc] == []:
                self.setFeature(self.nil, f, True)
    ##################################################################################################################################
    # Z
    ##################################################################################################################################
    def get_m_inlefff(self, f):
        if self.mention.is_in_lefff and self.z != None:
            self.setFeature(self.z, f, True)
    def get_m_inlefff_nocap(self, f):
        if self.mention.is_in_lefff_nocap and self.z != None:
            self.setFeature(self.z, f, True)
    def get_m_spos1(self, f):
        if self.mention.position==1 and self.z != None:
            self.setFeature(self.z, f, True)
    def get_m_dagu(self, f):
        if self.mention.dagu and self.z != None:
            self.setFeature(self.z, f, True)
    def get_m_short(self, f):
        if self.mention.short and self.z != None:
            self.setFeature(self.z, f, True)
    def get_m_long(self, f):
        if self.mention.long and self.z != None:
            self.setFeature(self.z, f, True)
    def get_m_conf(self, f):
        if self.z != None and self.mention.conf != 0.0:
            self.setFeature(self.z, f, self.mention.conf)
    # def get_m_conf_rank_1(self, f):
    def get_m_multiner(self, f):
        if len(self.mention.origins)>1 and self.z != None:
            self.setFeature(self.z, f, True)
    ##################################################################################################################################
    # Z COMBINED
    ##################################################################################################################################
    def get_m_spos1_and_m_inlefff(self, f):
        if self.mention.is_in_lefff and self.mention.position==1 and self.z != None:
            self.setFeature(self.z, f, True)
    def get_m_spos1_and_m_inlefff_nocap(self, f):
        if self.mention.is_in_lefff_nocap and self.mention.position==1 and self.z != None:
            self.setFeature(self.z, f, True)
    # def get_m_conf_rank_1_and_multiner(self, f):


    

def feature2featureFunction(feature):
    # return locals()['get_'+feature]
    return "self.%s('%s')"%('get_'+feature, feature)
