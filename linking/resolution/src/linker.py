#!/usr/bin/python
# -*- coding: utf-8 -*-
import sqlite3
import codecs
import re
import os
import sys
from collections import defaultdict
from optparse import *
from xml.dom.minidom import *
from subprocess import *
sys.path.append('@common_src@')
sys.path.append('@kb_src@')
from utils import *
from kbwp_utils import *
import NomosDocument
import NomosUtils
import LinkingInstance


# INPUT: NEWSML WHERE ELEMENT <BODY.CONTENT> HAS <P> ELEMENTS WITH SXPIPE DAGS CONVERTED TO XML, WITH <ENAMEX>, <ALT> AND <CHOICES>
# OUTPUT: ANNOTATED NEWSML *OR* FOR SXPIPE-EDYLEX LIST OF TOKENS/TYPE/ID OF LINKED ENTITY MENTIONS

def getOptions():
    usage = '%s [options] <news_item>\n'%sys.argv[0]
    parser = OptionParser(usage)
    linkers = ['nomos', 'npn']
    modes = ['list', 'doc']
    parser.add_option('--linker', action='store', dest='linker', default="nomos", type="choice", choices=linkers, help='Linker (nomos or npn, default nomos)')
    parser.add_option('--mode', action='store', dest='mode', default="list", type="choice", choices=modes, help='Output mode (list or document (doc), default list)')
    parser.add_option('--words', action='store', dest='words', default="__prefix__/share/nomos/frwikiwords.dat", help='<path to words data base> (default __prefix__/share/nomos/frwikiwords.dat)')
    parser.add_option('--totalwords', action='store', dest='total_words_population', type='int', default='175401109', help='<total words population - default 175,401,109>')
    parser.add_option('--aledadir', action='store', dest='aledadir', default='__prefix__/share/aleda', help='<path to dir with aleda data bases (default __prefix__/share/aleda)>')
    parser.add_option('--output', '-o', action='store', dest='outputfile', default=None, help='<outputfile name>')
    parser.add_option("--non-bernoulli-implicit", action="store_true", dest="non_bernoulli_implicit", default=True, help="True if Megam is using non-bernoulli-implicit format; else False (default False)")
    parser.add_option("--bernoulli-implicit", action="store_true", dest="bernoulli_implicit", default=False, help="True if Megam is using bernoulli-implicit format; else False (default True)")
    parser.add_option("--nonae", action="store_true", dest="nonae", default=False, help="True: No NAE candidates nor resolution (default False)")
    parser.add_option("--features", action="store", dest="features_file", default='__prefix__/share/nomos/features', help="<File for features reading (default __prefix__/share/nomos/features)>")
    parser.add_option("--weights", action="store", dest="weights_file", default='__prefix__/share/nomos/feature_weights', help="Features weights file (default __prefix__/share/nomos/feature_weights)")
    parser.add_option("--fclasses", action="store", dest="fclasses_file", default=None, help="Features classes file (if bernoulli-implicit) (default __prefix__/share/nomos/fclasses")        
    parser.add_option("--kb", action="store", dest="kb", default='__prefix__/share/nomos/kbwp.dat', help="<path to KB data base> (default __prefix__/share/nomos/kbwp.dat)")

    try: (options, args) = parser.parse_args()
    except OptionError: sys.stderr.write('OptionError'); parser.print_help(); sys.exit()
    except: sys.stderr.write('An error occured during options parsing\n'); parser.print_help(); sys.exit()
    if options.non_bernoulli_implicit and options.fclasses_file != None: parser.print_help(); sys.exit('Incompatible options features & non-bernoulli-implicit')
    if options.weights_file == None or options.features_file == None: sys.stderr.write("Mandatory option argument missing.\n"); parser.print_help(); sys.exit()
    # print >> sys.stderr, "WEIGHTS: %s"%(options.weights_file)
    # if options.dev_gold_file != None and options.test_gold_file != None: sys.stderr.write("Cannot get both dev and test gold file\n"); parser.print_help(); sys.exit()
    if options.non_bernoulli_implicit: options.bernoulli_implicit = False

    return options, args

def main():
    # SET ENCODINGS
    # sys.stdin = codecs.getreader("utf-8")(sys.stdin)
    sys.stdout = codecs.getwriter('utf8')(sys.stdout)
    # GET OPTIONS
    global options
    options, args = getOptions()
    global words_connection; global refs_connection; global vars_connection
    global lex_connection; global kb_connection
    global total_words_population; total_words_population = options.total_words_population
    # OPEN USEFUL DATA BASES AND LOAD TABLES
    words_connection = DBConnection(options.words, getWordsDBtables())
    refs_connection = DBConnection(options.aledadir+'/ne_refs.fr.dat', ['data'])
    vars_connection = DBConnection(options.aledadir+'/ne_vars.fr.dat', ['data'])
    lex_connection = DBConnection(options.aledadir+'/lefff.dat', ['data'])
    kb_connection = DBConnection(options.kb, getKBDBtables())
    # READ USEFUL FILES (FEATURES AND CO.)
    global features2weights; global bias; global fclasses; global features_list; global features2type
    features_list, features2type = NomosUtils.getFeatures(options.features_file)
    (features2weights, bias) = NomosUtils.getFeaturesWeights(options.weights_file)
    if options.non_bernoulli_implicit:
        fclasses = None
        bernoulli_implicit = False
    else:
        fclasses = NomosUtils.getFeaturesClasses(options.fclasses_file)

    if options.outputfile == None: G = sys.stdout
    else: G = codecs.open(options.outputfile, 'w', 'utf-8')
    # READ STDIN FROM PIPE
    data = "".join(sys.stdin.readlines())
    # CONVERT NEWSML DATA TO DOM
    try:
        newsmldom = parseString(data)
    except Exception, error:
        print >> sys.stderr, Exception
        print >> sys.stderr, error
        sys.exit("Can't parse data to XML. End.\n")
    # FIX ALTS IN DOM
    newsxml_dom = buildAlts(newsml2newsxml(newsmldom))
    # GET TOKENS TO FREQ FOR SWORDS
    tokens2frequency_dict = getTokens(newsxml_dom)
    # CREATE XML FOR NER MODULE
    ner_xml = buildNerModule(newsxml_dom)
    # CREATE XML FOR ANALYTICS MODULE
    analytics_xml = getNewsAnalytics(newsxml_dom, tokens2frequency_dict)
    # CREATE NOMOS DOC WITH MODULES
    nomos_xml_file = '<nomos>\n' + analytics_xml + ner_xml + '</nomos>\n'
    # VALIDATE NOMOS DOC XML AND CONVERT NOMOS DOC TO DOM
    xmllint = Popen(['xmllint', '--format', '--encode', 'UTF-8', '-'], stdin=PIPE, stdout=PIPE)
    nomos_input_dom = parseString(xmllint.communicate(nomos_xml_file.encode('utf-8'))[0])
    # CALL LINKING
    nomos_output_dom = link(nomos_input_dom, options.mode)
    if options.mode == "list":
        # EXTRACT LINKING INFO FROM OUTPUT
        # OUTPUT LINKING INFO
        for e in filter(lambda e:e.getAttribute('valid')=="true", nomos_output_dom.getElementsByTagName('module')[2].getElementsByTagName('ENAMEX')):
            if (e.parentNode.tagName != 'CHOICE' or (e.parentNode.tagName == 'CHOICE' and int(e.parentNode.getAttribute('rank')) < 2)) and e.getAttribute('cand_type') != 'NAE':
                tokens = map(lambda t:t.getAttribute('id'), e.getElementsByTagName('token'))
                if e.getAttribute('cand_type') == 'ALEDA': aleda_id = e.getAttribute('resolution_id'); t = e.getAttribute('candidate_type')
                else: aleda_id = 'NIL'; t = e.getAttribute('types')
                G.write(";".join(tokens)+"|"+t+"|"+aleda_id+"\n")
    # OR OUTPUT NOMOS DOC WITH LINKING
    elif options.mode == "doc":
        G.write(nomos_output_dom.toxml())

    refs_connection.close(); vars_connection.close(); kb_connection.close(); lex_connection.close()

def link(nomos_dom, mode):
    print >> sys.stderr, "Linking..."
    print >> sys.stderr, "\tCreate Nomos document..."
    nomos_document = NomosDocument.Document(nomos_dom, 'auto', False, lex_connection, kb_connection)
    print >> sys.stderr, "\tPopulate candidates"
    nomos_document.populateCandidates(vars_connection, refs_connection, kb_connection)
    for m in nomos_document.mentions.values():
        m.setOtherAttributes(kb_connection)

    # BUILD LINK INSTANCES => FEATURES VECTORS
    # COMPUTE INSTANCES SCORES WITH WEIGHTS => SCORE IS CANDIDATE'S SCORE (IN CLASS LINKING INSTANCE)
    print >> sys.stderr, "\tMake linking instances..."
    for mention_id, mention in nomos_document.mentions.items():
        if options.nonae:
            mention.deleteNAE()
        # for candidate in mention.candidates:
        # linking_instance = LinkingInstance.LinkingInstance(nomos_document, mention, candidate, features2type, fclasses)
        linking_instance = LinkingInstance.LinkingInstance(nomos_document, mention, features_list, features2type, fclasses)
        linking_instance.computeCandidatesScore(features2weights, bias)
    print >> sys.stderr, "\tScore paths..."
    nomos_document.scorePaths(bias)
    print >> sys.stderr, "\tBuild linking module in Nomos doc..."
    nomos_document.buildResolutionModule()
    print >> sys.stderr, "Done."
    return nomos_document.dom



def newsml2newsxml(newsmldom):
    # EXTRACT IPTC CODES FOR FILTER
    iptc_codes = set([])
    for node in mergeLists([newsmldom.getElementsByTagName('SubjectMatter'), newsmldom.getElementsByTagName('SubjectDetail'), newsmldom.getElementsByTagName('SubjectQualifier'), newsmldom.getElementsByTagName('Subject')]):
        iptc_codes.add(convertIptcCodes(node.getAttribute('FormalName')))
    # BUILD: NEWS ID
    news_id = getNodeData(newsmldom.getElementsByTagName('PublicIdentifier')[0])
    try: genre = newsmldom.getElementsByTagName('Genre')[0].getAttribute('FormalName')
    except IndexError: genre = "null"
    news = '<news_item ref="%s" genre="%s">\n'%(news_id, genre)
    # BUILD: LANGUAGE
    news += '<meta lang="%s"'%(newsmldom.getElementsByTagName('Language')[0].getAttribute('FormalName'))
    # BUILD: DATE & TIME
    datetimepattern = re.compile('^(\d+)T(\d+)Z$')
    date = datetimepattern.match(getNodeData(newsmldom.getElementsByTagName('DateAndTime')[0])).group(1)
    time = datetimepattern.match(getNodeData(newsmldom.getElementsByTagName('DateAndTime')[0])).group(2)
    news += ' date="%s" time="%s"'%(date, time)
    # BUILD SPATIAL CONTEXT (TODO)
    # <Location><Property FormalName="Country" Value="FRA"/>
    # <Property FormalName="City" Value="paris"/></Location>
    try:
        country = selectNodes(newsmldom.getElementsByTagName('Location')[0].getElementsByTagName('Property'), 'attr_value', ('FormalName', 'Country'))[0].getAttribute('Value')
        news += ' country="%s"'%(country)
    except IndexError:
        pass
    try:
        city = selectNodes(newsmldom.getElementsByTagName('Location')[0].getElementsByTagName('Property'), 'attr_value', ('FormalName', 'City'))[0].getAttribute('Value')
        news += ' city="%s"'%(city)
    except IndexError:
        pass
    news += '/>\n'
    # BUILD: HEADLINE + DATELINE
    try:
        dateline = getNodeData(newsmldom.getElementsByTagName('DateLine')[0])
    except IndexError:
        # print >> sys.stderr, "News %s has no DateLine"%(news_id); return None
        if robust: dateline = "NULL"
        else: return None
    try:
        headline = getNodeData(newsmldom.getElementsByTagName('HeadLine')[0])
    except IndexError:
        # print >> sys.stderr, "News %s has no HeadLine"%(news_id); return None
        if robust: headline = "NULL"
        else: return None
    news += '<news_head dateline="%s" headline="%s"/>\n'%(convertXmlEntities(dateline), convertXmlEntities(headline))
    # BUILD: SLUGS
    news += '<news_slugs text="%s"/>\n'%(getNodeData(newsmldom.getElementsByTagName('NameLabel')[0]))
    # BUILD: IPTC
    for iptc in iptc_codes:
        news += '<iptc_code value="%s"/>\n'%(iptc)
    # BUILD: PARAS
    news += '<news_text/>\n</news_item>\n'
    newsxml_dom = parseString(news.encode('utf-8'))
    para_parent = newsxml_dom.getElementsByTagName('news_text')[0]
    for para in newsmldom.getElementsByTagName('p'):
        newpara = para.cloneNode(True)
        para_parent.appendChild(newpara)
    return newsxml_dom
    
def buildAlts(dom):
    # BUILD ALTS FROM PDAGS
    for alt in dom.getElementsByTagName('ALT'):
        choice_id = 0
        for choice in alt.getElementsByTagName('CHOICE'):
            if choice.getAttribute('type') == 'ENAMEX':
                # REPLACE PERSON MENTIONS WHERE TOKEN "M." INSIDE ENAMEX WITH FORM "M." + ENAMEX (will be removed as a double mention)
                # <CHOICE id="70" type="ENAMEX"><ENAMEX local_eid="70" type="Person"><form value="M._Valls"><token id="T244">M.</token><token id="T245">Valls</token></form></ENAMEX></CHOICE>
                # <CHOICE id="71" type="ENAMEX"><form value="M."><token id="T244">M.</token></form><ENAMEX gender="f" local_eid="71" type="Person"><form value="Valls"><token id="T245">Valls</token></form></ENAMEX></CHOICE>
                for enamex in choice.getElementsByTagName('ENAMEX'):
                        tokens = enamex.getElementsByTagName('token')
                        tokens_content = map((lambda token: getNodeData(token)), tokens)
                        if re.match('^M\._?', tokens_content[0]):
                            # enamex.removeChild(form_node)
                            enamex.removeChild(tokens[0])
                            outside_token_node = createElementNode(dom, 'token', {'id':tokens[0].getAttribute('id')})
                            choice.insertBefore(outside_token_node, enamex)
                choice_id += 1
                addAttribute2Element(dom, choice, 'id', str(choice_id))
    return dom

def buildNerModule(dom):
    # printerr('Building NER module")
    xml = '<module type="ner" id="sxpipener">\n<news_content>\n'
    xml += dom.getElementsByTagName('news_head')[0].toxml()
    xml += dom.getElementsByTagName('news_text')[0].toxml() + '\n</news_content>\n'    
    xml += '</module>\n'
    return xml

def getTokens(dom):
    # HANDLE AMBIGUOUS DAGS => DONT COUNT SEVERAL TIMES SAME TOKEN
    unique_id_tokens = dict(map(lambda t:tuple([t.getAttribute('id'), getNodeData(t)]), dom.getElementsByTagName('token'))).values()
    tokens = selectTokens(unique_id_tokens)
    tokens2frequency_dict = defaultdict(int)
    for t in tokens:
        tokens2frequency_dict[t] += 1
    return tokens2frequency_dict

def getNewsAnalytics(newsxml_dom, tokens2frequency_dict):
    metadata = {}
    for x in ['news_id', 'date', 'time', 'dateline', 'headline', 'lang', 'genre', 'subject_codes', 'location', 'keywords']:
        metadata[x] = None
    # WARNING: TYPE OF DICT VALUE IS DIFFERENT FOR EACH KEY
    # DON'T ITERATE AS HASH OF HASH...
    metadata['subjects'] = {}
    metadata['location'] = {}
    metadata['keywords'] = []
    ##########################################################################
    # elif options.file_type == 'newscorpus':
    metadata['news_id'] = newsxml_dom.getElementsByTagName('news_item')[0].getAttribute('ref')
    metadata['date'] = newsxml_dom.getElementsByTagName('meta')[0].getAttribute('date')
    metadata['time'] = newsxml_dom.getElementsByTagName('meta')[0].getAttribute('time')
    metadata['lang'] = newsxml_dom.getElementsByTagName('meta')[0].getAttribute('lang')
    # TODO: USE DATELINE LOCATION?
    # TODO: CONVERT AFP LOCS TO ALEDA IDS
    metadata['country'] = newsxml_dom.getElementsByTagName('meta')[0].getAttribute('country')
    metadata['city'] = newsxml_dom.getElementsByTagName('meta')[0].getAttribute('city')
    metadata['dateline'] = newsxml_dom.getElementsByTagName('news_head')[0].getAttribute('dateline')
    metadata['headline'] = newsxml_dom.getElementsByTagName('news_head')[0].getAttribute('headline')
    metadata['genre'] = newsxml_dom.getElementsByTagName('news_item')[0].getAttribute('genre')
    metadata['keywords'] = newsxml_dom.getElementsByTagName('news_slugs')[0].getAttribute('text').split('-') #<news_slugs text="Santé-maladies-prévention"/>
    for subject in map((lambda node: node.getAttribute('value')), newsxml_dom.getElementsByTagName('iptc_code')):
        metadata['subjects'][subject] = True
    # metadata['swords'] = getSwords(tokens2frequency_dict)
    metadata['swords'] = computeSalience(tokens2frequency_dict, total_words_population, words_connection)
    analytics_xml = analyticsToXml(metadata)
    return analytics_xml

def analyticsToXml(metadata):
    # printerr('Building Analytics module')
    xml = '<module type="news_analytics">\n'
    xml += '<meta doc_id="'+metadata['news_id']+'" lang="'+metadata['lang']+'" country="'+metadata['country']+'" city="'+metadata['city']+'"/>\n'
    xml += '<category>\n'
    ######################################################
    for subject in metadata['subjects']:
        xml += '<iptc value="'+subject+'"/>\n'
    # TODO
    xml += '<domain/>\n'
    ######################################################
    for keyword in metadata['keywords']:
        xml += '<keyword value="'+keyword+'"/>\n'
    xml += '</category>\n'
    xml += '<context>\n'
    try: xml += '<spatial_context country="'+metadata['location']['country']+'" city="'+metadata['location']['city']+'"'
    except KeyError: xml += '<spatial_context'# country="'+metadata['location']['country']+'" city="'+metadata['location']['city']+'"'
    if metadata['location'].has_key('area'): xml += ' area="'+metadata['location']['area']+'"'
    xml += '/>\n'
    xml += '<temporal_context date="'+metadata['date']+'" time="'+metadata['time']+'"/>\n'
    xml += '</context>\n'
    xml += '<salient_words>\n'
    i = 0
    # threshold = 1.2
    for (word, z) in metadata['swords'][0:21]:
        # if z >= threshold:
        i += 1
        xml += '<salient_word value="'+word+'" rank="'+str(i)+'" z="'+str(z)+'"/>\n'
    xml += '</salient_words>\n'
    xml += '</module>\n'
    return xml


if __name__ == '__main__':
    main()
