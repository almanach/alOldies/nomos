#!/usr/bin/python
# -*- coding: utf-8 -*-
import copy
import re
import sys
from collections import defaultdict
import decimal
from decimal import Decimal as dec
sys.path.append('@common_src@')
from utils import *

# def getFeatures(features_file):
#     features = []
#     F = open(features_file)
#     features = filter(lambda line: not line.startswith('#'), map(lambda line: line.rstrip(), F.readlines()))
#     return features

def getFeatures(features_file):
    features_list = []
    features2type = defaultdict(dict)
    F = open(features_file)
    for line in filter(lambda l:not l.startswith('#'), F.readlines()):
        line = line.rstrip()
        (f, t, s) = line.split('|')
        features_list.append(f)
        features2type[f]['type'] = t
        features2type[f]['size'] = int(s)
    F.close()
    return features_list, features2type

def getFeaturesWeights(weights_file):
    features2weights = {}
    F = open(weights_file)
    bias = 0.0
    # **BIAS** -3.33925700187683105469
    # **BIAS** 0.44433975219726562500
    # NO BIAS WITH ADMIRABLE MODEL
    # iptc_is_level_7 3.15544605255126953125
    feature_pattern = re.compile("^(\S+)\s+([-\d\.]+)\s*$")
    bias_pattern = re.compile("^\*\*BIAS\*\*\s+([-\d\.]+)$")
    for line in F:
        feature_match = feature_pattern.match(line)
        bias_match = bias_pattern.match(line)
        if bias_match != None:
            bias = bias_match.group(1)
        elif feature_match != None:
            feature = feature_match.group(1)
            weight = feature_match.group(2)
            features2weights[feature] = float(weight)
    F.close()
    return features2weights, float(bias)

def getFeaturesClasses(fclasses_file):
    # iptc#0.0927320758559|0.195708181543|0.337997742791|0.408248290464|0.611225216862|0.682486778796|0.799874157123|0.861869634261|0.920214198508|0.955692316603|0.974616752112|1.0
    # frequency#6.0|22.0|46.0|103.0|350.0|755.0|2006.0|4052.0|6134.0|11821.0|19281.0|57764.0|134241.0
    # swords#0.0191601242641|0.0324541548269|0.0635380840575|0.0788110406239|0.136755586706|0.2321833568
    # slugs#0.0283980917124|0.051987524491|0.107813910498|0.160128153805|0.258198889747
    features2classes = {}
    F = open(fclasses_file)
    for line in F:
        try:
            (feature, values) = line.split('#')
            try: features2classes[feature] = map( (lambda value: float(value)), values.split('|'))
            except ValueError: features2classes[feature] = values.split('|')
        except Exception, error:
            sys.exit("In getFeatureClasses: error = %s"%("\n".join(error.args)))
    F.close()
    return features2classes

def getDetection(gold_document, nomos_document, in_eval=False, goldner=False):
    if goldner:
        for nomos_mention in nomos_document.mentions.values():
            gold_mention = gold_document.tokens2mentions[nomos_mention.tokens]
            nomos_mention.setDetection('exact_span', 'exact_type', 'exact_match', gold_mention)
        for gold_mention in gold_document.mentions.values():
            gold_mention.setDetection('matched_span', 'exact_type', 'matched')
        return
    seen_gold_tokens = set()
    # FIND MENTION TOKENS IN NOMOS DOC ALSO TO MENTION TOKENS IN GOLD DOC: EXACT MATCHES (TRUE POSITIVES) IF IN PATH RANKED 1 IF EVAL!!!!
    for tokens in [tokens for tokens in nomos_document.tokens2mentions if tokens in gold_document.tokens2mentions]:
        # FLAG GOLD TOKENS ALREADY MATCHED
        seen_gold_tokens.add(tokens)
        for nomos_mention in nomos_document.tokens2mentions[tokens]:
            # GET MATCHING GOLD MENTION FOR NOMOS MENTION
            gold_mention = gold_document.tokens2mentions[tokens]
            if gold_mention.types[0] in nomos_mention.types:
                nomos_mention.setDetection('exact_span', 'exact_type', 'exact_match', gold_mention)
                if in_eval:
                    if nomos_mention.is_in_path_part.is_in_path.rank < 2:
                        gold_mention.setDetection('matched_span', 'exact_type', 'matched')
                        # IF FURTHER GET-RESOLUTION SHOWS THAT MENTION IS RESOLVED TO NAE, GOLD MENTION DETECTION WILL BE SET TO MISSED
            else:
                # IF WRONG TYPE DOESN'T PREVENT EXACT MATCH:
                nomos_mention.setDetection('exact_span', 'wrong_type', 'exact_match', gold_mention)
                # ELSE:
                # nomos_mention.setDetection('exact_span', 'wrong_type', 'false_match', gold_mention)
                if in_eval:
                    if nomos_mention.is_in_path_part.is_in_path.rank < 2:
                        gold_mention.setDetection('matched_span', 'wrong_type', 'matched')
                        # IF FURTHER GET-RESOLUTION SHOWS THAT MENTION IS RESOLVED TO NAE, GOLD MENTION DETECTION WILL BE SET TO MISSED
                # ELSE:
                # nomos_mention.setDetection('exact_span', 'wrong_type', 'false_match', gold_mention)
    # FIND MENTION TOKENS IN NOMOS DOC PARTIALLY IN GOLD DOC: PARTIAL SPAN, ELSE FALSE SPANS; BOTH ARE FALSE SPANS (FALSE POSITIVES)
    for tokens in [tokens for tokens in nomos_document.tokens2mentions if tokens not in gold_document.tokens2mentions]:
        for nomos_mention in nomos_document.tokens2mentions[tokens]:
            # PARTIAL SPAN
            a_token_was_found = False
            for auto_token in tokens:
                if a_token_was_found:
                    break
                for gold_tokens in gold_document.tokens2mentions:
                    if auto_token in gold_tokens:
                        gold_mention = gold_document.tokens2mentions[gold_tokens]
                        if gold_mention.types[0] in nomos_mention.types:
                            # nomos_mention.setDetection('partial_span', 'exact_type', 'false_match')
                            # ADD GOLD MENTION TO PARTIAL MATCH SO THAT MENTION IS NOT CONSIDERED A FALSE MATCH FOR NAE TRAINING EXAMPLE
                            nomos_mention.setDetection('partial_span', 'exact_type', 'false_match', gold_mention)
                            if in_eval:
                                if nomos_mention.is_in_path_part.is_in_path.rank < 2:
                                    gold_mention.setDetection('partial_span', 'exact_type', 'partial_match')
                                    # IF FURTHER GET-RESOLUTION SHOWS THAT MENTION IS RESOLVED TO NAE, GOLD MENTION DETECTION WILL BE SET TO MISSED
                        else:
                            nomos_mention.setDetection('partial_span', 'wrong_type', 'false_match')
                            if in_eval:
                                if nomos_mention.is_in_path_part.is_in_path.rank < 2:
                                    gold_mention.setDetection('partial_span', 'wrong_type', 'partial_match')
                                    # IF FURTHER GET-RESOLUTION SHOWS THAT MENTION IS RESOLVED TO NAE, GOLD MENTION DETECTION WILL BE SET TO MISSED
                        seen_gold_tokens.add(tokens)
                        a_token_was_found = True
                        break
            # FALSE MATCH
            if not a_token_was_found:
                nomos_mention.setDetection('false_span', 'null', 'false_match')
    # FLAG GOLD TOKENS WITHOUT MATCHES AS MISSED MATCHES (FALSE NEGATIVES)
    for tokens in gold_document.tokens2mentions:
        gold_mention = gold_document.tokens2mentions[tokens]
        if tokens not in seen_gold_tokens:
            gold_mention.setDetection('missed_span', 'null', 'missed')

def getResolution(nomos_document): # USED BY EVALUATION ONLY: READS XML OUTPUT BY NOMOS RESOLUTION
    # DO THE SAME FOR GOLD MENTIONS!!!
    for mention_id, mention in nomos_document.mentions.items():
        mention.setResolution()
        mention.candRecall()

def getFeatureClass(value, feature_type, list=None):
    if value == None: return None    
    if feature_type == 'multi':    
        if value == 'null': return 'is_null'
        if isinstance(value, int):
            value = dec(str(float(value)))
        elif isinstance(value, float):
            value = dec(str(value))
        if value == dec('0.0'):     
            return '0'
        else:
            ceils = copy.copy(list)
            ceils.sort(reverse=True)
            for i in range(0, len(ceils)):
                if value >= dec(str(ceils[i])):
                    # return 'is_level_'+str(len(ceils)+1-i)
                    return str(len(ceils)+1-i)
                if value < dec(str(ceils[-1])):
                    # return 'is_level_1'
                    return '1'
    elif feature_type == 'binary':
        if value == True: return 'true'
        elif value == False: return 'false'
        if value == 'true': return 'true'
        elif value == 'false': return 'false'
        if isinstance(value, float) or isinstance(value, int):
            if value == 0.0: return 'true'
            elif value > 0.0: return 'true'
            # IF VALUE NEGATIVE?
        else:
            return 'false'
    # except Exception, error:
    #     print >> sys.stderr, sys.exc_info()[0]
    #     print >> sys.stderr, 'Error in getFeatureClass: Value: %s (%s)'%(value, feature_type)
    #     print >> sys.stderr, error.args
    #     sys.exit('Exit\n')

def getFeatureValue(value):
    if isinstance(value, int):
        return str(float(value))
    if isinstance(value, float):
        return str(value)
    if isinstance(value, dec):
        return str(value)
    if value == True:
        return str(1.0)
    if value == False:
        return str(0.0)


def getAdmirableValue(value):
    if value == 'true': return '1'
    elif value == 'false': return '0'
    elif re.match('^\d+$', value): return value
    else: print >> sys.stderr, 'Unexpected value for Admirable input: %s'%(value)
   
# def listFeatures(mode):
#     # not binary nor multi => actual value: 'cand_aleda_type2':[1,2,3,4,5], 'cand_type':[ALEDA(1), NIL(2), NAE(3)]
#     if mode == 'multi':
#         # 'cand_doc_cats_sim':float::multi - not yet - cats not normalized
#         #'cand_doc_slugs_sim':2, #4,
#         #'cand_doc_swords_sim':4,
#         #'cand_doc_sim':2, #4,
#         #'cand_mention_surrwords_sim':4,
#         # 'cand_aleda_gender_in_mention_genders':2,
#         return {'cand_name_mention_sim':3,
#                 'cand_mention_assocs':2,
#                 'cand_relative_frequency':3,
#                 'mention_amb_rate_and_cand_nae':3,
#                 'cand_frequency':3,
#                 'cand_aleda_geonames_weight':3,
#                 'cand_aleda_wikipedia_weight':2,
#                 'mention_conf_level':3,
#                 'mention_conf_level_and_cand_nae':3
#                 }
#     if mode == 'binary':
#         return {'cand_aleda_type_in_mention_types':2,
#                 'mention_longer_present_and_cand_nae':2,
#                 'mention_is_longer_and_cand_nae':2,
#                 'cand_aleda_and_mention_in_lefff':2,
#                 'cand_nae_and_mention_in_lefff':2,
#                 'cand_nae_and_mention_in_lefff_nocap':2,
#                 'cand_nae_and_mention_in_lefff_pos1':2,
#                 'cand_nae_and_mention_in_lefff_nocap_pos1':2,
#                 'cand_aleda_and_other_popular_aleda_cands':2,
#                 'cand_nil_and_other_popular_aleda_cands':2,
#                 'cand_nae_and_other_popular_aleda_cands':2,
#                 'cand_aleda_geonames_popular':2,
#                 'cand_aleda_wikipedia_popular':2,
#                 'mention_in_multiple_ner':2,
#                 'mention_in_multiple_ner_and_cand_nae':2,
#                 'mention_conf_with_cand_type_is_max':2,
#                 'mention_cand_share_gender':2,
#                 'cand_mention_surrwords_sim':2,
#                 'cand_doc_sim':2,
#                 'cand_doc_slugs_sim':2,
#                 'cand_doc_swords_sim':2,
#                 'cand_titlepar_mention_surrwords_sim':2,
#                 'mention_is_shortest_path_and_cand_nae':2
#                 }


def aledaType2nomosType(type):
    table = {'_PERSON':'Person', '_PERSON_m':'Person', '_PERSON_f':'Person', '_LOCATION':'Location', '_ORGANIZATION':'Organization', '_COMPANY':'Company', '_POI':'POI', '_PRODUCT':'Product', '_WORK':'Work', '_FICTIONCHAR':'FictionChar'}
    if type in table:
        return table[type]
    else:
        return type

def fixVar(string):
    replacements = {"l' ":"l'", "d' ":"d'", "s' ":"s'", "m' ":"m'", " - ":"-"}
    for s, t in replacements.items():
        string = re.sub(s, t, string)
    return string

def getSurrWords(tokens, dom):
    sentid = re.sub('E(\d+)', '\\1', tokens[0])
    try: frank = int(re.sub('T(\d+)', '\\1', tokens[0]))
    except ValueError: frank = int(re.sub('E\d+F(\d+)', '\\1', tokens[0]))
    try: lrank = int(re.sub('T(\d+)', '\\1', tokens[-1]))
    except ValueError: lrank = int(re.sub('E\d+F(\d+)', '\\1', tokens[-1]))
    (m1, m2, m3, p1, p2, p3) = (None, None, None, None, None, None)
    try:
        # m1 = selectNodes(dom.getElementsByTagName('token'), 'attr_value', ('id', 'T'+str(frank-1)))[0]
        m1 = selectNodes(dom.getElementsByTagName('token'), 'attr_value', ('id', 'E'+sentid+'F'+str(frank-1)))[0]
    except IndexError:
        pass
    if m1 is not None:
        try:
            # m2 = selectNodes(dom.getElementsByTagName('token'), 'attr_value', ('id', 'T'+str(frank-2)))[0]
            m2 = selectNodes(dom.getElementsByTagName('token'), 'attr_value', ('id', 'E'+sentid+'F'+str(frank-2)))[0]
        except IndexError:
            pass
    if m2 is not None:
        try:
            # m3 = selectNodes(dom.getElementsByTagName('token'), 'attr_value', ('id', 'T'+str(frank-3)))[0]
            m3 = selectNodes(dom.getElementsByTagName('token'), 'attr_value', ('id', 'E'+sentid+'F'+str(frank-3)))[0]
        except IndexError:
            pass 
    try:
        # p1 = selectNodes(dom.getElementsByTagName('token'), 'attr_value', ('id', 'T'+str(lrank+1)))[0]
        p1 = selectNodes(dom.getElementsByTagName('token'), 'attr_value', ('id', 'E'+sentid+'F'+str(lrank+1)))[0]
    except IndexError:
        pass
    if p1 is not None:
        try:
            # p2 = selectNodes(dom.getElementsByTagName('token'), 'attr_value', ('id', 'T'+str(lrank+2)))[0]
            p2 = selectNodes(dom.getElementsByTagName('token'), 'attr_value', ('id', 'E'+sentid+'F'+str(lrank+2)))[0]
        except IndexError:
            pass
    if p2 is not None:
        try:
            # p3 = selectNodes(dom.getElementsByTagName('token'), 'attr_value', ('id', 'T'+str(lrank+3)))[0]
            p3 = selectNodes(dom.getElementsByTagName('token'), 'attr_value', ('id', 'E'+sentid+'F'+str(lrank+3)))[0]
        except IndexError:
            pass
    # NO STOPLIST => SURROUNDING WORDS IN STOPLIST JUST WONT BE MATCHED WITH MENTION CANDIDATE SURROUNING WORDS
    return map((lambda w: re.sub('^ +', '', re.sub(' +$', '', w))), map((lambda x: getNodeData(x)), filter((lambda x: x is not None), [m1, m2, m3, p1, p2, p3])))
