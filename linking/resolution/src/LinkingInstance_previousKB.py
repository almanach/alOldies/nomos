#!/usr/bin/python
# -*- coding: utf-8 -*-
import re
import sys
import math
from decimal import Decimal as dec
from collections import defaultdict
sys.path.append('@common_src@')
from utils import *
from editDistance import *
from Entity import Variant
from NomosUtils import *

class LinkingInstance():
    type2num = {'Person':1, 'Location':2, 'Organization':3, 'Company':4}
    def __init__(self, doc, mention, candidate, features, fclasses):
        self.doc = doc
        self.mention = mention
        self.candidate = candidate
        self.features = {}
        if fclasses == None: self.bernoulli_implicit = False
        else: self.bernoulli_implicit = True
        # CHOOSE KB: COMMENT ONE OF THESE
        # IF NOMOS KB
        # self.setFeatureVector()
        # IF KBWP
        self.setFeatureVectorKBWP(features)
        self.normalizeFeatures(fclasses)

    def computeCandidateScore(self, features2weights, bias):
        score = dec(str(bias)) # bias is float converted to decimal
        for item in self.normalized_features.items():
            if self.bernoulli_implicit:
                feature = '__'.join(item) # GLUE FEATURE NAME AND VALUE (features2weights.keys())
                try: weight = dec(str(features2weights[feature])) # weight is float converted to decimal
                except KeyError: continue
                score += weight # operation on decimals
            else:
                (feature, value) = item
                weight = dec(str(features2weights[feature])) # weight is float converted to decimal
                value = dec(value) # value is str converted to decimal
                score += weight * value # operations on decimals
        self.candidate.scores[self.mention.id] = score # score is decimal

    # VERSION WITH KBWP
    def setFeatureVectorKBWP(self, features):
        for f in features:
            if f != '':
                # featureFunction = feature2featureFunction(f)
                # self.featureFunction()
                # exec "self.%s()"%('get_'+f)
                exec feature2featureFunction(f)

    def normalizeFeatures(self, fclasses=None):
        binary_features = listFeatures('binary').keys()
        multi_features = listFeatures('multi').keys()
        self.normalized_features = {}
        self.additional_normalized_features = defaultdict(list)
        for feature, value in self.features.items():
            if value == None:
                continue
            if isinstance(value, str):
                self.normalized_features[feature] = value
                # print >> sys.stderr, self.normalized_features[feature]
                continue
            # IF FEATURE ALREADY HAS CLASSES
            if feature not in binary_features and feature not in multi_features:
                self.normalized_features[feature] = str(self.features[feature])
                # print >> sys.stderr, self.normalized_features[feature]
                continue
            # ELSE
            if fclasses == None:
                self.normalized_features[feature] = getFeatureValue(value)
                # print >> sys.stderr, self.normalized_features[feature]
            else:
                if feature in binary_features:
                    self.normalized_features[feature] = getFeatureClass(value, 'binary')#; print >> sys.stderr, self.normalized_features[feature],; print >> sys.stderr, type(value)
                elif feature in multi_features:
                    try:
                        value = getFeatureClass(value, 'multi', fclasses[feature])
                        self.normalized_features[feature] = value#; print >> sys.stderr, self.normalized_features[feature],; print >> sys.stderr, type(value)
                        # IF FEATURE HAS A CLASS GREATER THAN 1, ADD FEATURE WITH LESSER CLASSES
                        # = IF CAND DOC SIM == 5, IT IS ALSO 4, 3, 2 AND 1
                        if int(value) > 1:
                            for below_value in range(1, int(value)):
                                self.additional_normalized_features[feature].append(str(below_value))
                                # print >> sys.stderr, 'self.additional_normalized_features['+feature+'].append('+str(below_value)+')'
                    except KeyError:
                        continue
                        # self.normalized_features[feature] = getFeatureClass(value, 'binary', fclasses[feature])
                        # print >> sys.stderr, "No discrete values for feature %s"%(feature)
                        # No discrete values for feature cand_mention_surrwords_sim# No discrete values for feature cand_titlepar_mention_surrwords_dist
                        ## TAKE BINARY VALUE INSTEAD
                    
    def computeSimilarity(self, doc_data, cand_data):
        temp_vector = {}; vector = {}; temp_vector['doc'] = {}; temp_vector['cand'] = {}; vector['doc'] = {}; vector['cand'] = {}
        for value in doc_data:
            temp_vector['doc'][value] = 1.0
            if value in cand_data: temp_vector['cand'][value] = float(cand_data[value])
            else: temp_vector['cand'][value] = 0.0
        for value in cand_data:            
            temp_vector['cand'][value] = float(cand_data[value])
            if value not in temp_vector['doc']: temp_vector['doc'][value] = 0.0
        doc_rootsquaresum = math.sqrt( sum( map( (lambda value: pow(value, 2)), temp_vector['doc'].values() ) ) )
        if doc_rootsquaresum != 0:
            for value in temp_vector['doc']:
                vector['doc'][value] = temp_vector['doc'][value] / doc_rootsquaresum
        else:
            vector['doc'] = temp_vector['doc']
        cand_rootsquaresum = math.sqrt( sum( map( (lambda value: pow(value, 2)), temp_vector['cand'].values() ) ) )
        if cand_rootsquaresum != 0:        
            for value in temp_vector['cand']:
                vector['cand'][value] = temp_vector['cand'][value] / cand_rootsquaresum
        else:
            vector['cand'] = temp_vector['cand']
        # SCALAR PRODUCT OF VECTORS
        cos_similarity = sum( map( (lambda value: vector['doc'][value] * vector['cand'][value] ), vector['doc'].keys() ) )
        # similarity_level = assignFeatureClass(cos_similarity)
        return cos_similarity

    # def writeAdmirableExample(self, datatype):
    def writeAdmirableExample(self):
        # 0 1:43 2:34 5:21     \
        #  0.3 1:2 3:0.32        -- one instance (3 candidates)
        # 2 2:3 3:4 19:-0.63   /
        line = self.doc.id+'#'+self.mention.id+'#'+self.candidate.displayId()+'_'+self.candidate.candidate_type+'#'
        # RESULT = RANK SETTING (INVERSE OF CLASSES)
        if self.mention.gold_mention is None:
            if self.candidate.candidate_type == 'NAE': rank = 0 
            else: rank = 1
        # IF GOLD MENTION AND CAND IS NAE: 0; OTHERWISE CHECK CAND/GOLD ID/NAME
        else:
            if self.candidate.candidate_type == 'NAE': rank = 1
            elif self.candidate.candidate_type == 'ALEDA':
                if str(self.candidate.id) == self.mention.gold_mention.entity_id: rank = 0
                else: rank = 1
            elif self.candidate.candidate_type == 'NIL':
                if self.mention.gold_mention.entity_type == 'NIL': rank = 1
                else: rank = 1
        line += str(rank)
        for feature, value in self.normalized_features.items():
            line += '\t'
            # feature_code = str(feature2code(feature))
            value = getAdmirableValue(value)
            line += feature+'__'+value+':1'
        for add_feature, values in self.additional_normalized_features.items():
            # feature_code = str(feature2code(add_feature))
            for value in values:
                line += '\t'
                value = getAdmirableValue(value)
                line += feature+'__'+value+':1'
        # line += '#'+datatype+'\n'
        # line += '\n'
        sys.stdout.write(line)
        
    # def writeMegamExample(self, datatype, mode=None):
    def writeMegamExample(self, mode=None):
        line = self.doc.id+'#'+self.mention.id+'#'+self.candidate.displayId()+'_'+self.candidate.candidate_type+'#'
        # ADD RES 0 FOR FALSE MENTIONS (NO GOLD MENTION)
        if self.mention.gold_mention is None:
            if self.candidate.candidate_type == 'NAE': result = 1 
            else: result = 0
        # IF GOLD MENTION AND CAND IS NAE: 0; OTHERWISE CHECK CAND/GOLD ID/NAME
        else:
            if self.candidate.candidate_type == 'NAE':
                result = 0
            elif self.candidate.candidate_type == 'ALEDA':
                if str(self.candidate.id) == self.mention.gold_mention.entity_id: result = 1
                else: result = 0
            elif self.candidate.candidate_type == 'NIL':
                if self.mention.gold_mention.entity_type == 'NIL': result = 1
                else: result = 0
        line += str(result)+'#'
        # line += '\t'.join(map((lambda t: ' '.join((t[0], str(t[1])))), filter((lambda t: t[1] != None), self.normalized_features.items())))
        if self.bernoulli_implicit:
            line += '\t'.join(map((lambda t: '__'.join((t[0], t[1]))), self.normalized_features.items()))
            if self.additional_normalized_features != {}: line += '\t'
            for add_feature, values in self.additional_normalized_features.items():
                line += '\t'
                line += '\t'.join(map(lambda value: add_feature+'__'+value, values))
        else:
            line += '\t'.join(map((lambda t: ' '.join((t[0], t[1]))), self.normalized_features.items()))
        # line += '#'+datatype+'\n'
        # line += '\n'
        # TO WRITE ONLY POSITIVE EXAMPLES, UNCOMMENT THIS TEST
        # TO WRITE BOTH POSITIVE AND NEGATIVE EXAMPLES, COMMENT THIS TEST
        # if result == 1:
        if mode == 1: # FOR VARIABLE STUDY
            #urn:newsml:afp.com:20090520T164214Z:TX-PAR-FVU98:1
            line = ''+re.sub('^[^-]+-[A-Z]+-([^:]+):', r'\1', self.doc.id)+'\t'+self.mention.variant.string+'\t'+self.candidate.displayId()+'_'+self.candidate.candidate_type+'\t'+str(result)+'\t'
            line += '\t'.join(map((lambda t: '__'.join((t[0], t[1]))), self.normalized_features.items()))
            # line += "\n"
            sys.stdout.write(line.encode('utf-8'))
            return
        sys.stdout.write(line.encode('utf-8'))

    # METHODS FOR FEATURE COMPUTATION
    def get_cand_name_mention_sim(self):
        if self.candidate.candidate_type == 'ALEDA':
            try: self.features['cand_name_mention_sim'] = 1.0/edit_distance(self.mention.variant.string, self.candidate.name)
            except ZeroDivisionError: self.features['cand_name_mention_sim'] = 1.0

    def get_cand_mention_assocs(self):
        try:
            if self.candidate.kb_data['mentions'] != None:
                self.features['cand_mention_assocs'] = self.candidate.kb_data['mentions'][self.mention.variant.string]
        except KeyError:
            self.features['cand_mention_assocs'] = None
            
    def get_cand_titlepar_mention_surrwords_sim(self):
        if self.candidate.kb_data['surrwords'] != None:
            if self.candidate.kb_data['titlepar'] != None and self.candidate.kb_data['titlepar'] != '':#set([]):
                #clusters+
                # dist = 0.0
                # for surrword in self.mention.surrwords:
                #     dist += edit_distance(self.candidate.kb_data['titlepar'], surrword)
                # try: self.features['cand_titlepar_mention_surrwords_sim'] = 1.0/dist
                # except ZeroDivisionError: self.features['cand_titlepar_mention_surrwords_sim'] = 1.0
                inter = self.mention.cluster_surrwords.intersection(self.candidate.kb_data['titlepar'])
                if len(inter) != 0:
                    self.features['cand_titlepar_mention_surrwords_sim'] = len(inter)
                #clusters-
            else:
                self.features['cand_titlepar_mention_surrwords_sim'] = None
        else:
            self.features['cand_titlepar_mention_surrwords_sim'] = None

    def get_cand_mention_surrwords_sim(self):
        if self.candidate.kb_data['surrwords'] != None:
            cand_surrwords = set(self.candidate.kb_data['surrwords'].keys())
            #clusters+
            # inter = cand_surrwords.intersection(self.mention.surrwords)
            inter = cand_surrwords.intersection(self.mention.cluster_surrwords)
            #clusters-
            if len(inter) != 0:
                self.features['cand_mention_surrwords_sim'] = len(inter)# TODO: BETTER SIMILARITY COMPUTATION

    def get_cand_relative_frequency(self):
        if self.candidate.kb_data['frequency'] == None:
            self.features['cand_relative_frequency'] = None
        else:
            self.features['cand_relative_frequency'] = float(self.candidate.kb_data['frequency']) / float(sum(map((lambda candidate: candidate.kb_data['frequency']), filter((lambda candidate: candidate.kb_data['frequency'] != None), self.mention.candidates))))

    def get_mention_amb_rate(self):
        self.features['mention_amb_rate'] = len(self.mention.candidates)
    def get_mention_amb_rate_and_cand_nil(self):
        self.features['mention_amb_rate_and_cand_nil'] = len(self.mention.candidates)
    def get_mention_amb_rate_and_cand_nae(self):
        self.features['mention_amb_rate_and_cand_nae'] = len(self.mention.candidates)
    def get_mention_amb_rate_and_cand_aleda(self):
        self.features['mention_amb_rate_and_cand_aleda'] = len(self.mention.candidates)
        
    def get_cand_frequency(self):
        self.features['cand_frequency'] = self.candidate.kb_data['frequency'] # NONE OR INT
        
    def get_cand_aleda_variant_number(self):
        if self.candidate.candidate_type == 'ALEDA':
            self.features['cand_aleda_variant_number'] = self.candidate.aleda_data['aleda_variants_number']

    def get_cand_aleda_geonames_weight(self):
        if self.candidate.candidate_type == 'ALEDA':
            self.features['cand_aleda_geonames_weight'] = self.candidate.aleda_data['aleda_geonames_weight']
    def get_cand_aleda_wikipedia_weight(self):
        if self.candidate.candidate_type == 'ALEDA':        
            self.features['cand_aleda_wikipedia_weight'] = self.candidate.aleda_data['aleda_wikipedia_weight']
            
    def get_mention_conf_level(self):
        if self.mention.conf != dec('0.0'):
            self.features['mention_conf_level'] = self.mention.conf

    def get_mention_conf_level_and_cand_nae(self):
        if self.candidate.candidate_type == 'NAE':
            if self.mention.conf != dec('0.0'):
                self.features['mention_conf_level_and_cand_nae'] = self.mention.conf

            
    def get_cand_aleda_type_in_mention_types(self):
        if self.candidate.candidate_type == 'ALEDA':
            self.features['cand_aleda_type_in_mention_types'] = self.candidate.type in self.mention.types or None
        
    # def get_cand_aleda_gender_in_mention_genders(self):
    #     if self.candidate.gender != None and self.candidate.candidate_type == 'ALEDA':
    #         self.features['cand_aleda_gender_in_mention_genders'] = self.candidate.gender in self.mention.genders

    def get_mention_longer_present(self):
        for v in self.doc.variants:
            if v != self.mention.variant.string:
                if v.startswith(self.mention.variant.string):
                    self.features['mention_longer_present'] = True
                elif v.endswith(self.mention.variant.string):
                    self.features['mention_longer_present'] = True
                elif self.mention.variant.string in v:
                    self.features['mention_longer_present'] = True

    def get_mention_is_longer(self):
        for v in self.doc.variants:
            if v != self.mention.variant.string:
                if self.mention.variant.string.endswith(v):
                    self.features['mention_is_longer'] = True
                if v in self.mention.variant.string:
                    self.features['mention_is_longer'] = True

    def get_mention_longer_present_and_cand_nae(self):
        if self.candidate.candidate_type == 'NAE':
            for v in self.doc.variants:
                if v != self.mention.variant.string:
                    if v.startswith(self.mention.variant.string):
                        self.features['mention_longer_present_and_cand_nae'] = True
                    elif v.endswith(self.mention.variant.string):
                        self.features['mention_longer_present_and_cand_nae'] = True
                    elif self.mention.variant.string in v:
                        self.features['mention_longer_present_and_cand_nae'] = True

    def get_mention_is_longer_and_cand_nae(self):
        if self.candidate.candidate_type == 'NAE':
            for v in self.doc.variants:
                if v != self.mention.variant.string:
                    if self.mention.variant.string.endswith(v):
                        self.features['mention_is_longer_and_cand_nae'] = True
                    if v in self.mention.variant.string:
                        self.features['mention_is_longer_and_cand_nae'] = True

    def get_mention_is_shortest_path(self):
        try: zone = self.mention.is_in_path_part.is_in_path.zone
        except: print >> sys.stderr, self.mention.__dict__
        is_shortest_path = False
        if zone.type == 'ambiguous':
            min_length = 10
            for path in zone.valid_paths:
                length = len(path.path_parts)
                if length <= min_length:
                    min_length = length
                    if path == self.mention.is_in_path_part.is_in_path: is_shortest_path = True
                    else: is_shortest_path = None
            if is_shortest_path:                    
                self.features['mention_is_shortest_path'] = True
                
    def get_mention_is_shortest_path_and_cand_nae(self):
        if self.candidate.candidate_type == 'NAE':
            try: zone = self.mention.is_in_path_part.is_in_path.zone
            except: print >> sys.stderr, self.mention.__dict__
            is_shortest_path = False
            if zone.type == 'ambiguous':
                min_length = 10
                for path in zone.valid_paths:
                    length = len(path.path_parts)
                    if length <= min_length:
                        min_length = length
                        if path == self.mention.is_in_path_part.is_in_path: is_shortest_path = True
                        else: is_shortest_path = None
                if is_shortest_path:
                    self.features['mention_is_shortest_path_and_cand_nae'] = True


    def get_mention_in_lefff(self):
        self.features['mention_in_lefff'] = self.mention.is_in_lefff or None
    def get_mention_in_lefff_nocap(self):
        self.features['mention_in_lefff_nocap'] = self.mention.is_in_lefff_nocap or None
    def get_mention_in_lefff_pos1(self):
        self.features['mention_in_lefff_pos1'] = self.mention.is_in_lefff and self.mention.position == 1 or None
    def get_mention_in_lefff_nocap_pos1(self):
        self.features['mention_in_lefff_nocap_pos1'] = self.mention.is_in_lefff_nocap and self.mention.position == 1 or None
        
    def get_cand_aleda_and_mention_in_lefff(self):
        if self.candidate.candidate_type == 'ALEDA':
            self.features['cand_aleda_and_mention_in_lefff'] = self.mention.is_in_lefff or None
    def get_cand_nil_and_mention_in_lefff(self):
        if self.candidate.candidate_type == 'NIL':
            self.features['cand_nil_and_mention_in_lefff'] = self.mention.is_in_lefff or None
    def get_cand_nae_and_mention_in_lefff(self):
        if self.candidate.candidate_type == 'NAE':
            self.features['cand_nae_and_mention_in_lefff'] = self.mention.is_in_lefff or None
            
    def get_cand_aleda_and_mention_in_lefff_nocap(self):
        if self.candidate.candidate_type == 'ALEDA':
            self.features['cand_aleda_and_mention_in_lefff_nocap'] = self.mention.is_in_lefff_nocap or None
    def get_cand_nil_and_mention_in_lefff_nocap(self):
        if self.candidate.candidate_type == 'NIL':
            self.features['cand_nil_and_mention_in_lefff_nocap'] = self.mention.is_in_lefff_nocap or None
    def get_cand_nae_and_mention_in_lefff_nocap(self):
        if self.candidate.candidate_type == 'NAE':
            self.features['cand_nae_and_mention_in_lefff_nocap'] = self.mention.is_in_lefff_nocap or None
    def get_cand_aleda_and_mention_in_lefff_pos1(self):
        if self.candidate.candidate_type == 'ALEDA':
            self.features['cand_aleda_and_mention_in_lefff_pos1'] = (self.mention.is_in_lefff and self.mention.position == 1) or None
    def get_cand_nil_and_mention_in_lefff_pos1(self):
        if self.candidate.candidate_type == 'NIL':
            self.features['cand_nil_and_mention_in_lefff_pos1'] = (self.mention.is_in_lefff and self.mention.position == 1) or None
    def get_cand_nae_and_mention_in_lefff_pos1(self):
        if self.candidate.candidate_type == 'NAE':
            self.features['cand_nae_and_mention_in_lefff_pos1'] = (self.mention.is_in_lefff and self.mention.position == 1) or None
            
    def get_cand_aleda_and_mention_in_lefff_nocap_pos1(self):
        if self.candidate.candidate_type == 'ALEDA':
            self.features['cand_aleda_and_mention_in_lefff_nocap_pos1'] = (self.mention.is_in_lefff_nocap and self.mention.position == 1) or None
    def get_cand_nil_and_mention_in_lefff_nocap_pos1(self):
        if self.candidate.candidate_type == 'NIL':
            self.features['cand_nil_and_mention_in_lefff_nocap_pos1'] = (self.mention.is_in_lefff_nocap and self.mention.position == 1) or None
    def get_cand_nae_and_mention_in_lefff_nocap_pos1(self):
        if self.candidate.candidate_type == 'NAE':
            self.features['cand_nae_and_mention_in_lefff_nocap_pos1'] = (self.mention.is_in_lefff_nocap and self.mention.position == 1) or None
            
    def get_cand_aleda_and_other_popular_aleda_cands(self):
        if self.candidate.candidate_type == 'ALEDA':
            mention_other_aleda_candidates = filter(lambda c: c.candidate_type == 'ALEDA' and c != self.candidate, self.mention.candidates)
            if mention_other_aleda_candidates != []:
                gn = filter(lambda c: c.candidate_type == 'ALEDA' and c.type == 'Location' and c.aleda_data['aleda_geonames_weight'] != None, mention_other_aleda_candidates)
                wp = filter(lambda c: c.candidate_type == 'ALEDA' and c.type != 'Location', mention_other_aleda_candidates)
                max_gn = 0.0; max_wp = 0.0
                if gn != []: max_gn = max(gn, key=lambda c: c.aleda_data['aleda_geonames_weight']).aleda_data['aleda_geonames_weight']
                if wp != []: max_wp = max(wp, key=lambda c: c.aleda_data['aleda_wikipedia_weight']).aleda_data['aleda_wikipedia_weight']
                if max_gn >= 50000.0 or max_wp >= 80.0:
                    self.features['cand_aleda_and_other_popular_aleda_cands'] = True
                else:
                    self.features['cand_aleda_and_other_popular_aleda_cands'] = None

    def get_cand_nil_and_other_popular_aleda_cands(self):
        if self.candidate.candidate_type == 'NIL':
            mention_other_aleda_candidates = filter((lambda c: c.candidate_type == 'ALEDA'), self.mention.candidates)
            if mention_other_aleda_candidates != []:
                gn = filter(lambda c: c.candidate_type == 'ALEDA' and c.type == 'Location' and c.aleda_data['aleda_geonames_weight'] != None, mention_other_aleda_candidates)
                wp = filter(lambda c: c.candidate_type == 'ALEDA' and c.type != 'Location', mention_other_aleda_candidates)
                max_gn = 0.0; max_wp = 0.0
                if gn != []: max_gn = max(gn, key=lambda c: c.aleda_data['aleda_geonames_weight']).aleda_data['aleda_geonames_weight']
                if wp != []: max_wp = max(wp, key=lambda c: c.aleda_data['aleda_wikipedia_weight']).aleda_data['aleda_wikipedia_weight']
                if max_gn >= 50000.0 or max_wp >= 80.0:
                    self.features['cand_nil_and_other_popular_aleda_cands'] = True
                else:
                    self.features['cand_nil_and_other_popular_aleda_cands'] = None
                    
    def get_cand_nae_and_other_popular_aleda_cands(self):
        if self.candidate.candidate_type == 'NAE':
            mention_other_aleda_candidates = filter((lambda c: c.candidate_type == 'ALEDA'), self.mention.candidates)
            if mention_other_aleda_candidates != []:
                gn = filter(lambda c: c.candidate_type == 'ALEDA' and c.type == 'Location' and c.aleda_data['aleda_geonames_weight'] != None, mention_other_aleda_candidates)
                wp = filter(lambda c: c.candidate_type == 'ALEDA' and c.type != 'Location', mention_other_aleda_candidates)
                max_gn = 0.0; max_wp = 0.0
                if gn != []: max_gn = max(gn, key=lambda c: c.aleda_data['aleda_geonames_weight']).aleda_data['aleda_geonames_weight']
                if wp != []: max_wp = max(wp, key=lambda c: c.aleda_data['aleda_wikipedia_weight']).aleda_data['aleda_wikipedia_weight']
                if max_gn >= 50000.0 or max_wp >= 80.0:
                    self.features['cand_nae_and_other_popular_aleda_cands'] = True
                else:
                    self.features['cand_nae_and_other_popular_aleda_cands'] = None

    def get_cand_doc_slugs_sim(self):
        if self.candidate.kb_data['slugs'] == None:
            self.features['cand_doc_slugs_sim'] = None
        else:
            self.features['cand_doc_slugs_sim'] = self.computeSimilarity(self.doc.analytics['slugs'], self.candidate.kb_data['slugs'])
            if self.features['cand_doc_slugs_sim'] == 0.0:
                self.features['cand_doc_slugs_sim'] = None

    def get_cand_doc_swords_sim(self):
        if self.candidate.kb_data['swords'] == None:
            self.features['cand_doc_swords_sim'] = None
        else:
            self.features['cand_doc_swords_sim'] = self.computeSimilarity(self.doc.analytics['swords'], self.candidate.kb_data['swords'])
            if self.features['cand_doc_swords_sim'] == 0.0:
                self.features['cand_doc_swords_sim'] = None

    def get_cand_doc_sim(self):
        doc_sim = 0.0; i = 0
        for data in ['slugs', 'swords', 'cats']:
            try:
                if self.features['cand_doc_'+data+'_sim'] != None and self.features['cand_doc_'+data+'_sim'] != 0.0:
                    i += 1
                    doc_sim += self.features['cand_doc_'+data+'_sim']
            except KeyError:
                continue
        if doc_sim != 0.0 and i != 0:
           self.features['cand_doc_sim'] = doc_sim / float(i)

    def get_cand_aleda_geonames_popular(self):
        if self.candidate.candidate_type == 'ALEDA':
            self.features['cand_aleda_geonames_popular'] = self.candidate.aleda_data['aleda_geonames_weight'] >= 50000.0 or None
    def get_cand_aleda_wikipedia_popular(self):
        if self.candidate.candidate_type == 'ALEDA':
            self.features['cand_aleda_wikipedia_popular'] = self.candidate.aleda_data['aleda_wikipedia_weight'] >= 80.0 or None

    def get_cand_aleda_type(self):
        if self.candidate.candidate_type == 'ALEDA':
            try: self.features['cand_aleda_type'] = LinkingInstance.type2num[self.candidate.type]
            except KeyError: self.features['cand_aleda_type'] = 5

    def get_mention_in_multiple_ner(self):
        if len(self.mention.origins) > 1:
            self.features['mention_in_multiple_ner'] = True

    def get_mention_in_multiple_ner_and_cand_nae(self):
        if self.candidate.candidate_type == 'NAE':
            if len(self.mention.origins) > 1:
                self.features['mention_in_multiple_ner_and_cand_nae'] = True

    def get_mention_in_multiple_ner_and_cand_nae(self):
        if self.candidate.candidate_type == 'NAE':
            if len(self.mention.origins) > 1:
                self.features['mention_in_multiple_ner_and_cand_nae'] = True

            
    def get_mention_conf_with_cand_type_is_max(self):
        if self.mention.types2conf != {}:
            max_conf_type = sorted(self.mention.types2conf.keys(), key=lambda t: dec(self.mention.types2conf[t]), reverse=True)[0]
            if self.candidate.type == max_conf_type:
                self.features['mention_conf_with_cand_type_is_max'] = True
                
    def get_mention_cand_share_gender(self):
        if self.mention.genders != [None] and self.candidate.gender != None and self.candidate.gender in self.mention.genders:
            self.features['mention_cand_share_gender'] = True

    def get_cand_type(self):
        candtype2num = {'ALEDA':1, 'NIL':2, 'NAE':3}
        self.features['cand_type'] = candtype2num[self.candidate.candidate_type]

def feature2featureFunction(feature):
    # return locals()['get_'+feature]
    return "self.%s()"%('get_'+feature)
