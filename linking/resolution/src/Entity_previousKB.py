#!/usr/bin/python
# -*- coding: utf-8 -*-
import math
import sys
import re
import string
from xml.dom.minidom import *
sys.path.append('@common_src@')
from utils import *
from NomosUtils import *

class Variant():
    def __init__(self, string, document):
        self.string = string
        self.document = document
        self.document.variants[string] = self
        self.mentions = set()

    def getCandidates(self, aleda_vars_connection, aleda_refs_connection, kb_connection):
        aleda_vars_connection.tables['data'].selectFromTable(aleda_vars_connection.cursor, ['key'], [('variant', '=', self.string)])
        # CANDIDATES_IDS ARE ALL KEYS FROM ALEDA FOR THE VARIANT
        self.candidates_ids = map((lambda t: int(t[0])), aleda_vars_connection.cursor.fetchall())
        # TODO: LOOK FOR OTHER VARIANTS (+LONG...)
        # self.findAltVariants()
        # for alt_variant in self.alt_variants:
        # aleda_vars_connection.tables['data'].selectFromTable(aleda_vars_connection.cursor, ['key'], [('variant', '=', replaceDiacr(self.string))])
        # self.candidates_ids.extend(map((lambda t: int(t[0])), aleda_vars_connection.cursor.fetchall()))
        # FOR EACH CANDIDATE, CHECK IF IT HAS ALREADY BEEN INSTANCIATED IN THIS DOCUMENT
        for candidate_id in set(self.candidates_ids): # MAY BE EMPTY
            # IF NOT, INSTANCIATE CANDIDATE
            if not self.document.candidates.has_key(candidate_id):
                # IF CAND ID IS INTEGER, THEN IT'S AN ALEDA CANDIDATE
                if isinstance(candidate_id, int):
                    # ADD TO IT ALL ITS VARIANTS FROM ALEDA
                    aleda_vars_connection.tables['data'].selectFromTable(aleda_vars_connection.cursor, ['variant'], [('key', '=', candidate_id)])
                    candidate_variants = map((lambda t: t[0]), aleda_vars_connection.cursor.fetchall())
                    # ADD CANDIDATES TO DOCUMENT WHEN INSTANCIATED
                    self.document.candidates[candidate_id] = AledaCandidate(candidate_id, candidate_variants, aleda_refs_connection, kb_connection, self.document)
        # ADD NIL AND NAE CANDIDATE IN ALL CASES - IN MENTION INSTANTIATION

    def findAltVariants(self):
        self.alt_variants = []
        self.alt_variants.append(replaceDiacr(self.string))

class Candidate(object):
    def __init__(self, document):
        self.document = document
        # VERSION WITH NOMOS KB (WITH NPNORMALIZER)
        # self.kb_data = {'cats':None, 'swords':None, 'slugs':None, 'frequency':None}
        # VERSION WITH WIKIPEDIA KB
        self.kb_data = {'cats':None, 'swords':None, 'slugs':None, 'surrwords':None, 'mentions':None, 'frequency':None, 'title':None, 'titlepar':None}
        self.aleda_data = {'in_aleda':False, 'aleda_type':None, 'aleda_geonames_weight':None, 'aleda_wikipedia_weight':None, 'aleda_variants_number':None}
        self.gender = None
        
    def displayId(self):
        return unicode(self.id)

    def display(self, mention, FH=None):
        writeLog('%s | %s | %s | %f'%(self.candidate_type, self.displayId(), self.name, self.scores[mention.id]), FH)

    # VERSION WITH NOMOS KB (WITH NPNORMALIZER)
    def addKBData(self, kb_connection):
        kb_connection.tables['entities2iptc'].selectFromTable(kb_connection.cursor, ['iptc', 'count'], [('id', '=', self.id)])
        self.kb_data['cats'] = dict(kb_connection.cursor.fetchall())
        kb_connection.tables['entities2swords'].selectFromTable(kb_connection.cursor, ['sword', 'count'], [('id', '=', self.id)], 10)
        self.kb_data['swords'] = dict(kb_connection.cursor.fetchall())
        kb_connection.tables['entities2slugs'].selectFromTable(kb_connection.cursor, ['slug', 'count'], [('id', '=', self.id)])
        self.kb_data['slugs'] = dict(kb_connection.cursor.fetchall())

    # VERSION WITH WP KB; CHECK IF FETCHALL IS NOT EMPTY (SOME CORRESPONDANCES BUGS B/ ALEDA AND KBWP...)
    # IF EMPTY: LEAVE VALUE AT NONE INSTEAD OF DICT
    def addKBWPData(self, kb_connection):
        # TODO: CLEAN TEXT DATA BEFORE RECORD => WHITE SPACES HEADING AND TRAILING
        kb_connection.tables['frequency'].selectFromTable(kb_connection.cursor, ['frequency'], [('aledaid', '=', self.id)])
        res = kb_connection.cursor.fetchone()
        if res != None and res != '': self.kb_data['frequency'] = res[0]
        # else: print >> sys.stderr, 'no frequenccy'
        # TODO: NORMALIZE WP CATEGORIES TO IPTC, OTHERWISE USELESS
        # kb_connection.tables['categories'].selectFromTable(kb_connection.cursor, ['category', 'count'], [('aledaid', '=', self.id)])
        # res = kb_connection.cursor.fetchall()
        # if res != []: self.kb_data['cats'] = dict(res)
        kb_connection.tables['swords'].selectFromTable(kb_connection.cursor, ['sword', 'count'], [('aledaid', '=', self.id)], 10)
        res = kb_connection.cursor.fetchall()
        if res != []:
            # self.kb_data['swords'] = dict(zip(map((lambda r: re.sub('^ +', '', re.sub(' +$', '', r[0]))), res), map((lambda r: r[1]), res)))
            # else: print >> sys.stderr, 'no swords'
            #clusters+
            self.kb_data['swords'] = {}
            swords = dict(zip(map((lambda r: re.sub('^ +', '', re.sub(' +$', '', r[0]))), res), map((lambda r: r[1]), res)))
            seen_words = {}
            for sword in swords:
                for w in [sword, sword.lower(), sword.upper(), string.capwords(sword), replaceDiacr(sword), replaceDiacr(sword.upper()), replaceDiacr(string.capwords(sword))]:
                    kb_connection.tables['wordclusters'].selectFromTable(kb_connection.cursor, ['cluster'], [('word', '=', w)])
                    res = kb_connection.cursor.fetchall()
                    if res != []:
                        for cluster in res:
                            self.kb_data['swords'][cluster[0]] = swords[sword]
                        seen_words[sword] = True
                if not seen_words.has_key(sword): seen_words[sword] = False
            for sword in filter(lambda w: seen_words[w] == False, seen_words.keys()):
                self.kb_data['swords'][sword] = swords[sword]
            #clusters-
        kb_connection.tables['slugs'].selectFromTable(kb_connection.cursor, ['slug', 'count'], [('aledaid', '=', self.id)])
        res = kb_connection.cursor.fetchall()
        if res != []: self.kb_data['slugs'] = dict(res)
        # else: print >> sys.stderr, 'no slugs'
        kb_connection.tables['surrwords'].selectFromTable(kb_connection.cursor, ['surrword', 'count'], [('aledaid', '=', self.id)])
        res = kb_connection.cursor.fetchall()
        if res != []:
            # self.kb_data['surrwords'] = dict(zip(map((lambda r: re.sub('^ +', '', re.sub(' +$', '', r[0]))), res), map((lambda r: r[1]), res)))
            # else: print >> sys.stderr, 'no surrwords'
            #clusters+
            self.kb_data['surrwords'] = {}
            surrwords = dict(zip(map((lambda r: re.sub('^ +', '', re.sub(' +$', '', r[0]))), res), map((lambda r: r[1]), res)))
            seen_words = {}
            for surrword in surrwords:
                for w in [surrword, surrword.lower(), surrword.upper(), string.capwords(surrword), replaceDiacr(surrword), replaceDiacr(surrword.upper()), replaceDiacr(string.capwords(surrword))]:
                    kb_connection.tables['wordclusters'].selectFromTable(kb_connection.cursor, ['cluster'], [('word', '=', w)])
                    res = kb_connection.cursor.fetchall()
                    if res != []:
                        for cluster in res:
                            self.kb_data['surrwords'][cluster[0]] = surrwords[surrword]
                        seen_words[surrword] = True
                if not seen_words.has_key(surrword): seen_words[surrword] = False
            for surrword in filter(lambda w: seen_words[w] == False, seen_words.keys()):
                self.kb_data['surrwords'][surrword] = surrwords[surrword]
            #clusters-
        kb_connection.tables['mentions'].selectFromTable(kb_connection.cursor, ['mention', 'count'], [('aledaid', '=', self.id)])
        res = kb_connection.cursor.fetchall()
        if res != []: self.kb_data['mentions'] = dict(zip(map((lambda r: re.sub('^ +', '', re.sub(' +$', '', r[0]))), res), map((lambda r: r[1]), res)))
        # else: print >> sys.stderr, 'no mentions'
        kb_connection.tables['main'].selectFromTable(kb_connection.cursor, ['title', 'titlepar'], [('aledaid', '=', self.id)])
        res = kb_connection.cursor.fetchone()
        if res != None and len(res) == 2:
            self.kb_data['title'] = res[0]
            #clusters+
            # self.kb_data['titlepar'] = re.sub('^ +', '', re.sub(' +$', '', res[1]))
            titlepar = re.sub('^ +', '', re.sub(' +$', '', res[1]))
            if titlepar == '':
                self.kb_data['titlepar'] = set([])
            else:
                clusters = []
                for w in [titlepar, titlepar.lower(), titlepar.upper(), string.capwords(titlepar), replaceDiacr(titlepar), replaceDiacr(titlepar.upper()), replaceDiacr(string.capwords(titlepar))]:
                    kb_connection.tables['wordclusters'].selectFromTable(kb_connection.cursor, ['cluster'], [('word', '=', w)])
                    res = kb_connection.cursor.fetchall()
                    if res != []:
                        for cluster in res:
                            clusters.append(cluster[0])
                if clusters == []: self.kb_data['titlepar'] = set([titlepar])
                else: self.kb_data['titlepar'] = set(clusters)
            #clusters-
        # print >> sys.stderr, '(title, titlepar) =',
        # print >> sys.stderr, res
            

    # NOT NEEDED, DICT INIT WITH ALL KEYS TO NONE VALUES
    # def addEmptyKBData(self):
    #     self.kb_data['cats'] = None
    #     self.kb_data['swords'] = None
    #     self.kb_data['slugs'] = None

    def addMention(self, mention):
        self.mentions[mention.id] = mention

    def displayInitTime(self, start):
        print >> sys.stderr, '[Doc %s] Candidate %s init: %.2fs'%(self.document.id, self.displayId(), getTime() - start)
  
class AledaCandidate(Candidate):
    def __init__(self, aleda_id, variants, aleda_refs_connection, kb_connection, document):
        super(AledaCandidate, self).__init__(document)
        start = getTime()
        self.id = aleda_id
        self.candidate_type = 'ALEDA'
        self.mentions = {}
        self.scores = {}
        # self.known = True

        self.variants = variants
        # FILL ALEDA DATA
        aleda_refs_connection.tables['data'].selectFromTable(aleda_refs_connection.cursor,  ['name', 'type', 'weight', 'link'], [('key', '=', self.id)])
        (self.name, temp_type, aleda_weight, href) = aleda_refs_connection.cursor.fetchone()
        self.href = href
        self.type = aledaType2nomosType(temp_type)
        if self.type == 'Location':
            self.aleda_data = {'in_aleda':True, 'aleda_type':self.type, 'aleda_geonames_weight':float(aleda_weight), 'aleda_wikipedia_weight':None, 'aleda_variants_number':float(len(self.variants))}
            aleda_refs_connection.tables['data'].selectFromTable(aleda_refs_connection.cursor,  ['subtype', 'geonames_type'], [('key', '=', self.id)])
            (self.subtype, self.geonamestype) = aleda_refs_connection.cursor.fetchone()
            if self.aleda_data['aleda_geonames_weight'] == 10: self.aleda_data['aleda_geonames_weight'] = None
        else:
            self.aleda_data = {'in_aleda':True, 'aleda_type':self.type, 'aleda_geonames_weight':None, 'aleda_wikipedia_weight':float(aleda_weight), 'aleda_variants_number':float(len(self.variants))}
            self.subtype = None; self.geonamestype = None
        try: self.gender = re.match('^_PERSON_(m|f)$', temp_type).group(1)
        except AttributeError: pass

        # CHECK IF ENTITY IS IN KB
        # COMMENT ON OF THESE 2 BLOCKS TO USE NOMOS KB OR KBWP

        # IF KB == NOMOS, NOT NECESSARILY IN KB; CHECK FOR FREQUENCY != 0
        # kb_connection.tables['entities2frequency'].selectFromTable(kb_connection.cursor, ['frequency'], [('id', '=', self.id)])
        # frequency = kb_connection.cursor.fetchone()
        # if frequency != None and frequency != '':
        #     self.kb_data['frequency'] = frequency[0]
        #     self.addKBData(kb_connection)
            # self.frequency = frequency[0]
        # else:
            # self.kb_data['frequency'] = None
            # self.addEmptyKBData()
            # self.frequency = None

        # IF KB == WP, SHOULD BE IN IT, BUT CORRESPONDANCE NOT EXACT... STILL HAVE TO CHECK IN EACH DATA QUERIES (IN addKBWPData())
        # FREQUENCY SET IN addKBWPData()
        self.addKBWPData(kb_connection)

        # print >> sys.stderr, 'Candidate %d'%(self.id)
        # print >> sys.stderr, self.__dict__
        # stdin()

        # self.displayInitTime(start)



class NilCandidate(Candidate):
    def __init__(self, name, document):
        super(NilCandidate, self).__init__(document)
        self.id = name
        self.candidate_type = 'NIL'
        self.mentions = {}
        self.scores = {}
        # FILL ALEDA DATA (NONE)
        self.type = 'Null'
        self.name = name
        self.href = 'http://nomos/linking/anomymousAFPentities/'+self.name+'.not'
        # self.addEmptyKBData()
        # self.frequency = None
        # FILL KB DATA
        # kb_connection.tables['unk_entities2frequency'].selectFromTable(kb_connection.cursor, ['frequency'], [('id', '=', self.id)])
        # frequency = kb_connection.cursor.fetchone()
        # if frequency != None and frequency != '':
        #     self.kb_data['frequency'] = frequency[0]
        #     self.addKBData(kb_connection)
        #     self.frequency = frequency[0]
        # else:
        #     self.kb_data['frequency'] = None
        #     self.addEmptyKBData()
        #     self.frequency = None


class NAE(Candidate):
    def __init__(self, string, document):
        super(NAE, self).__init__(document)
        self.id = string
        self.name = string
        self.mentions = {}
        self.scores = {}
        self.variants = []
        # FILL ALEDA DATA (NONE)
        self.type = 'Null'
        self.string = string
        self.href = 'http://nomos/linking/discardedEntities/'+self.name+'.not'
        # FILL KB DATA (NONE)
        # self.kb_data['frequency'] = None
        # self.addEmptyKBData()
        # self.frequency = None
        self.candidate_type = 'NAE'
        
