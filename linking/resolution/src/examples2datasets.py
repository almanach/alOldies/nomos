#!/usr/bin/python
# -*- coding: utf-8 -*-
import re
import os
import sys
from glob import glob
from optparse import *
from xml.dom.minidom import *
sys.path.append('@common_src@')
from utils import *

def main(golddir, examples, location):
    docids = {'dev':[], 'test':[], 'train':[]}
    for root, dirs, files in os.walk(golddir):
        for d in dirs:
            for f in os.listdir(os.path.join(root, d)):
                # afp.com-20090520T164214ZTX-PAR-FVU98.gold
                if re.search('afp\.com', f):
                    docid = re.sub('afp\.com-[^-]+-[A-Z]+-([A-Z\d]+)\.gold\.xml', '\\1', f)
                else:
                    docid = re.sub('\.gold\.xml', '', f)
                docids[d].append(docid)
    TRAIN = open(location+'/train.input.temp', 'w')
    DEV = open(location+'/dev.input.temp', 'w')
    TEST = open(location+'/test.input.temp', 'w')
    F = open(examples)
    for line in F:
        #urn:newsml:afp.com:20090520T164214Z:TX-PAR-FVU98:1#11#2000000002147714_ALEDA#1# mention_in_lefff_pos1__0:1      cand_aleda_and_mention_in_lefff_nocap__0:1      mention_is_shortest_path__0:1   cand_aleda_and_other_popular_aleda_cands__1:1   cand_aleda_type__2:1    mention_in_lefff__1:1   cand_aleda_wikipedia_popular__0:1       mention_in_lefff_nocap__0:1     mention_in_multiple_ner__1:1    cand_aleda_and_mention_in_lefff_nocap_pos1__0:1 cand_aleda_variant_number__1:1  cand_aleda_and_mention_in_lefff__1:1    cand_aleda_type_in_mention_types__0:1   cand_aleda_geonames_weight__3:1 cand_name_mention_sim__3:1      mention_in_lefff_nocap_pos1__0:1        cand_aleda_and_mention_in_lefff_pos1__0:1       mention_amb_rate_and_cand_aleda__1:1    cand_aleda_geonames_popular__1:1        cand_relative_frequency__0:1    cand_relative_frequency__1:1    cand_relative_frequency__2:1    cand_relative_frequency__1:1    cand_relative_frequency__2:1
        docid = line.split('#')[0]
        if re.search('afp\.com', f):
            docid = re.sub('urn:newsml:afp\.com:[^:]+:[A-Z]+-[A-Z]+-([A-Z\d]+):\d', '\\1', docid)
        if docid in docids['dev']:
            DEV.write(line)
        elif docid in docids['test']:
            TEST.write(line)
        elif docid in docids['train']:
            TRAIN.write(line)
    F.close()
    DEV.close(); TEST.close(); TRAIN.close()

if __name__ == '__main__':
    try:
        golddir = sys.argv[1]
        examples = sys.argv[2]
        location = sys.argv[3]
        main(golddir, examples, location)
    except IndexError:
        sys.exit('Usage: %s <ith gold dir with dev/test/train split> <examples> <directory for output files>'%(sys.argv[0]))
        
            
            
            
