#!/usr/bin/perl -w

use strict;

my %features2counts = ();
my %features2weights = ();

my $counts = shift or die "Usage: $0 <admirable features 2 counts> <admirable features 2 weights>\n";
my $weights = shift or die "Usage: $0 <admirable features 2 counts> <admirable features 2 weights>\n";

open(COUNTS, $counts) or die "Can't open $counts: $!\n";
open(WEIGHTS, $weights) or die "Can't open $weights: $!\n";

while(<COUNTS>)
  {
    # if line of features 2 counts
    # cand_nil_and_mention_in_lefff_nocap_pos1__0 1634
    if(/^(\S+) \d+$/)
      {
	$features2counts{$.} = $1;
      }
  }

while(<WEIGHTS>)
  {
    # if line of features index to weights
    # 1 6.345810954575505058468642407762e-08
    if(/^(\d+) ([-e\.\d]+)$/)
      {
	if(exists($features2counts{$1}))
	  {
	    my $feature = $features2counts{$1};
	    $features2weights{$feature} = $2;
	  }
      }
  }

foreach my $f(sort(keys(%features2weights)))
  {
    my $w = $features2weights{$f};
    print "$f\t$w\n";
  }

close(COUNTS);
close(WEIGHTS);
