#!/usr/bin/python
# -*- coding: utf-8 -*-
import re
import sys
from optparse import *
from collections import defaultdict
sys.path.append('@common_src@')
from utils import *
from NomosUtils import *
    
def main():
    options, args = getOptions()
    arg = args[0]
    global data
    data = {}
    # multi_values_features2size = listFeatures('multi')
    features_list, features2type = getFeatures(options.features_file)
    for feature in features_list:
        if features2type[feature]['type'] == 'multi':
            data[feature] = {'values':[], 'ceils':[], 'size':features2type[feature]['size']}
    F = open(arg)
    lines = F.readlines()
    F.close()

    # READ LINES OF FVALS, RECORD ALL VALS FOR FEATURES WHERE IT IS APPROPRIATE
    for line in lines:
        # features2values = read(line)
        if len(line.split('#')) == 6:
            # (doc_id, mention_id, cand_id, result, temp_features2values, datatype) = line.split('#')
            (doc_id, mention_id, cand_id, result, temp_features2values, nextex) = line.split('#')
            try:
                features2values = dict(zip(map((lambda f2v: f2v.split(' ')[0]), temp_features2values.split('\t')), map((lambda f2v: float(f2v.split(' ')[1])), temp_features2values.split('\t'))))
                for feature, value in features2values.items():
                    if value != 'null' and value != 0 and features2type[feature]['type'] == 'multi':
                        data[feature]['values'].append(float(value))
            except IndexError:
                pass

    # DISCRETIZE FVALS
    # print 'Opening file for features2classes: %s'%(options.features2classes)
    G = open(options.fclasses, 'w')
    data = classifyFeatures(data, G)
    G.close()

    # RE-READ LINES, APPLY DISCRETIZATION OF FVALS, PRINT
    for line in lines:
        # features2values = read(line)
        if re.match('^#(train|test)#newline', line): sys.stdout.write(line); continue        
        if len(line.split('#')) == 6:
            # (doc_id, mention_id, cand_id, result, temp_features2values, datatype) = line.split('#')
            (doc_id, mention_id, cand_id, result, temp_features2values, nextex) = line.split('#')
            try:
                features2values = dict(zip(map((lambda f2v: f2v.split(' ')[0]), temp_features2values.split('\t')), map((lambda f2v: float(f2v.split(' ')[1])), temp_features2values.split('\t'))))
            except IndexError:
                doc_id+"#"+mention_id+"#"+cand_id+"#"+result+"##"+nextex
            features2classes = {}; additional_features = defaultdict(list)
            for feature, value in features2values.items():
                if features2type[feature]['type'] == 'multi':
                    feature_class = getFeatureClass(value, 'multi', data[feature]['ceils'])
                    if str(feature_class) > 1:
                        for below_value in range(1, int(feature_class)):
                            additional_features[feature].append(str(below_value))
                elif features2type[feature]['type'] == 'binary':
                    feature_class = getFeatureClass(value, 'binary')
                else: # predefined values
                    if int(value) == value: feature_class = str(int(value))
                    else: feature_class = str(value)
                features2classes[feature] = feature_class
            # datatype = datatype.rstrip()
            newline = doc_id+"#"+mention_id+"#"+cand_id+"#"+result+"#"
            if options.learn == "megam":
                newline += '\t'.join( map( (lambda tuple: '__'.join(tuple)), features2classes.items() ) )
                for feature, values in additional_features.items():
                    newline += '\t'
                    newline += '\t'.join(map(lambda value: feature+'__'+value, values))
            elif options.learn == "admirable":
                for feature, value in features2classes.items():
                    newline += '\t'
                    value = getAdmirableValue(value)
                    newline += feature+'__'+value+':1'
                for add_feature, values in additional_features.items():
                    for value in values:
                        newline += '\t'
                        value = getAdmirableValue(value)
                        newline += add_feature+'__'+value+':1'
            # newline += "#"+datatype+'\n'
            nextex = nextex.rstrip()
            newline += '#'+nextex+'\n'
            sys.stdout.write(newline)

def classifyFeatures(data, FH, param=5):
    for feature in data:
        values = data[feature]['values']
        ceils = data[feature]['ceils']
        size = data[feature]['size']
        if values != []:
            # SORTED LIST OF FEATURE VALUES
            values.sort()
            # LIST LENGTH = TOTAL POPULATION 
            l = len(values)
            # INTEGER DIVISION BY A PARAM => NUMBER OF CLASSES FOR THIS FEATURE
            n = l // size
            for i in range(1, size):
                data[feature]['ceils'].append(values[(n*i)-1])
            # print 'Writing in file for features2classes: %s'%(FH.name)
            FH.write('%s#%s\n'%(feature, '|'.join(map((lambda x: str(x)), data[feature]['ceils']))))
    return data

def getOptions():
    usage = '%s [options] <NBI megam temp input> > <BI megam input>'%sys.argv[0]
    parser = OptionParser(usage)
    #parser.add_option("--bernoulli-implicit", action="store_true", dest="bernoulli_implicit", default=True, help="")
    #parser.add_option("--non-bernoulli-implicit", action="store_true", dest="non_bernoulli_implicit", default=False, help="")
    # parser.add_option("--param", action="store", dest="param", default=None, help="Param for feature classification")
    # parser.add_option("--megam", action="store", dest="megam", default=None, help="Output file: Megam input")
    parser.add_option("--fclasses", action="store", dest="fclasses", default=None, help="Output file: Features => Classes")
    parser.add_option("--features", action="store", dest="features_file", default=None, help="features files (f|type<bin/multi>|size)")
    parser.add_option("--learn", action="store", dest="learn", default="megam", type="choice", choices=["megam", "admirable"], help="learn: [megam | admirable] (default megam)")
    try: (options, args) = parser.parse_args()
    except OptionError: sys.stderr.write('OptionError'); parser.print_help(); sys.exit()
    except: sys.stderr.write('An error occured during options parsing\n'); parser.print_help(); sys.exit()
    if len(args) != 1: print >> sys.stderr, 'Error: Arguments == %s (%d)'%(', '.join(args), len(args)); parser.print_help(); sys.exit()
    # if options.megam == None or options.features2classes == None: print >> sys.stderr, 'Outputs not specified'; parser.print_help(); sys.exit()
    #if options.non_bernoulli_implicit: options.bernoulli_implicit = False
    return options, args


if __name__ == '__main__':
    # print >> sys.stderr, sys.argv
    main()

