#!/usr/bin/python
# -*- coding: utf-8 -*-
import re
# import sys
# from collections import defaultdict
# sys.path.append('@common_src@')
# from utils import *
# from editDistance import *
# from Entity import Variant
# from NomosUtils import *

def clustering(mentions):
    record = {}
    cluster = None
    for m in mentions:
        # print "Start comparison of %s"%(m)
        if m not in record:
            cluster = set([m])
            record[m] = cluster
        else:
            cluster = record[m]
        for mm in mentions - set([m]):
            # print "Comparing %s and %s"%(m, mm)
            if sim(m, mm):
                if mm in record:
                    cluster = cluster | record[mm]
                    record[m] = cluster
                    record[mm] = cluster
                else:
                    cluster.add(mm)
                    record[mm] = cluster
        # print "End comparison of %s"%(m)
    return makeUniqueSet(record.values())
    # return record


def makeUniqueSet(listofsets):
    tempuniquesetslist = []
    uniquesetslist = []
    intersections = {}
    for s in listofsets:
        if tuple(s) not in intersections:
            intersections[tuple(s)] = []
            for s2 in listofsets:
                if len(s.intersection(s2)):
                    intersections[tuple(s)].append(s2)
    for s in intersections:
        newset = set([])
        for inter in intersections[s]:
            newset = newset | inter
        tempuniquesetslist.append(newset)
    for s in tempuniquesetslist:
        if set(s) not in uniquesetslist:
            uniquesetslist.append(set(s))
    return uniquesetslist

def sim(m, mm):
    if m.endswith(mm):
        # print "%s endswith %s"%(m, mm)
        return True
    if mm.endswith(m):
        # print "%s endswith %s"%(mm, m)
        return True
    # if isCapOf(m, mm): return True
    # if isCapOf(mm, m): return True
    if isAcroOf(m, mm):
        # print "%s isAcroOf %s"%(m, mm)
        return True
    if isAcroOf(mm, m):
        # print "%s isAcroOf %s"%(mm, m)
        return True
    if isAcroVarOf(m, mm):
        # print "%s isAcroVarOf %s"%(m, mm)
        return True
    if isAcroVarOf(mm, m):
        # print "%s isAcroVarOf %s"%(mm, m)
        return True
    if isExpansionOf(m, mm):
        # print "%s isExpansionOf %s"%(m, mm)
        return True
    if isExpansionOf(mm, m):
        # print "%s isExpansionOf %s"%(mm, m)
        return True
    return False

# def isCapOf(m, mm):
#     if re.match('\w\. *', m)

def isAcroOf(m, mm):
    if re.match('^[A-Z]+$', m) or re.match('^([A-Z]\.?)+$', m):
        letters = map(lambda l: l.lower(), list(re.sub('\.', '', m)))
        letters2 = map(lambda w:list(w)[0].lower(), filter(lambda w:not (w.islower() and len(w)<4), mm.split(' ')))
        # print "letters of %s:"%(m)
        # print letters
        # print "letters2 of %s:"%(mm)
        # print letters2
        if letters == letters2:
            return True
        else:
            return False
    else:
        return False

def isAcroVarOf(m, mm):
    if re.match('^[A-Z]+$', m) and re.match('^([A-Z]\.?)+$', mm):
        letters = map(lambda l: l.lower(), list(m))
        letters2 = map(lambda l: l.lower(), list(re.sub('\.', '', mm)))
        if letters == letters2:
            return True
        else:
            return False
    else:
        return False

def isAcroVarOf(m, mm):
    if re.match('^[A-Z]+$', mm) and re.match('^([A-Z]\.?)+$', m):
        letters = map(lambda l: l.lower(), list(mm))
        letters2 = map(lambda l: l.lower(), list(re.sub('\.', '', m)))
        if letters == letters2:
            return True
        else:
            return False
    else:
        return False

def isExpansionOf(m, mm):
    if re.match('^[A-Z]+$', m) or re.match('^([A-Z]\.)+\.?$', m):
        letters = map(lambda l: l.lower(), list(re.sub('\.', '', m)))
        letters2 = map(lambda w:list(w)[0].lower(), filter(lambda w:not (w.islower() and len(w)<4), mm.split(' ')))
        if letters == letters2:
            return True
        else:
            return False
    else:
        return False
