#!/usr/bin/python
# -*- coding: utf-8 -*-
import codecs
import re
import os
import sys
from glob import glob
from optparse import *
from xml.dom.minidom import *
from collections import defaultdict
#sys.path.append('@common_src@')
#import NomosUtils
#import NomosDocument
#from utils import *

class GMention():
    def __init__(self, node):
        self.tokens = tuple(map(lambda t:t.getAttribute('id'), node.getElementsByTagName('token')))
        self.content = "+".join(map(lambda t:t.firstChild.nodeValue, node.getElementsByTagName('token')))
        self.id = node.getAttribute('local_eid')
        self.eid = node.getAttribute('eid')
        self.type = node.getAttribute('type')
        if self.eid == "null":
            self.entitytype = "NIL"
        else:
            self.entitytype = "ALEDA"

class NMention():
    def __init__(self, node):
        self.tokens = tuple(map(lambda t:t.getAttribute('id'), node.getElementsByTagName('token')))
        self.content = "+".join(map(lambda t:t.firstChild.nodeValue, node.getElementsByTagName('token')))
        self.id = node.getAttribute('local_eid')
        self.eid = node.getAttribute('resolution_id')
        self.entitytype = node.getAttribute('cand_type')
        if self.entitytype == 'NIL' or self.entitytype == 'NAE':
            self.types = node.getAttribute('types').split(';')
        elif self.entitytype == "ALEDA":
            self.types = [node.getAttribute('candidate_type')]
        if node.parentNode.tagName == 'CHOICE':
            if node.parentNode.getAttribute('valid') == "true":
                if node.getAttribute('valid') == "true" or node.getAttribute('valid') == '':
                    self.rank = int(node.parentNode.getAttribute('rank'))
                else:
                    self.rank = -1
            else:
                self.rank = -1
        elif node.getAttribute('valid') == "true" or node.getAttribute('valid') == '':
            self.rank = int(node.getAttribute('rank'))
        else:
            self.rank = -1
        # if self.entitytype == '':
        #     if re.match('^\d+$', self.eid): self.entitytype = 'ALEDA'
        #     else: self.entitytype = 'NIL'


class Eval():
    def __init__(self):
        self.ret, self.rel, self.retrel = (0.0, 0.0, 0.0)
        self.true_nae, self.retrieved_nae, self.retrel_nae = (0.0, 0.0, 0.0)
        self.rellink, self.corlink, self.rellinkk, self.corlinkk, self.rellinku, self.corlinku = (0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
        self.rellink_type = defaultdict(float)
        self.corlink_type = defaultdict(float)
        self.acc_type = {}
        self.retk, self.retu, self.retrelk, self.retrelu = (0.0, 0.0, 0.0, 0.0)
        
    def count(self, gdoc, ndoc):
        temp_relevant = map(lambda x:GMention(x), gdoc.getElementsByTagName('ENAMEX'))
        relevant = filter(lambda t:re.match('[a-z]', t.content) is None, temp_relevant)
        relevant_tokens = map(lambda x:x.tokens, relevant)
        temp_retrieved_all = filter(lambda x:x.rank<2, map(lambda x:NMention(x), ndoc.getElementsByTagName('module')[2].getElementsByTagName('ENAMEX')))
        retrieved_all = filter(lambda t:re.match('[a-z]', t.content) is None, temp_retrieved_all)
        retrieved = filter(lambda x:x.entitytype != 'NAE', retrieved_all)
        retrieved_nae = filter(lambda x:x.entitytype == 'NAE', retrieved_all)
        # sys.stderr.write("\n".join(map(lambda x:x.content, retrieved_nae)).encode('utf-8'))
        retrel = filter(lambda x:x.tokens in relevant_tokens, retrieved)
        true_nae = filter(lambda x:x.tokens not in relevant_tokens, retrieved_all)
        true_nae_tokens = map(lambda x:x.tokens, true_nae)
        retrel_nae = filter(lambda x:x.tokens in true_nae_tokens, retrieved_nae)
        
        self.rel += float(len(relevant))
        self.ret += float(len(retrieved))
        self.retrel += float(len(retrel))
        self.true_nae += float(len(true_nae))
        self.retrieved_nae += float(len(retrieved_nae))
        self.retrel_nae += float(len(retrel_nae))

        seengm = {}

        for nm in retrel:
            gm = filter(lambda x:x.tokens == nm.tokens, relevant)[0]
            seengm[gm.content] = True
            self.rellink += 1
            if gm.entitytype == 'ALEDA':
                self.rellinkk += 1
            else:
                self.rellinku += 1
            if nm.eid == gm.eid:
                self.corlink += 1
                if gm.entitytype == 'ALEDA':
                    self.corlinkk += 1
                else:
                    self.corlinku += 1
        for nm in retrel:
            gm = filter(lambda x:x.tokens == nm.tokens, relevant)[0]
            if gm.entitytype == 'ALEDA':
                self.rellink_type[gm.type] += 1
                if gm.eid == nm.eid:
                    self.corlink_type[gm.type] += 1

        for nm in retrel:
            gm = filter(lambda x:x.tokens == nm.tokens, relevant)[0]
            if nm.entitytype == 'ALEDA':
                self.retk += 1
                if gm.entitytype == 'ALEDA':
                    self.retrelk += 1
            else:
                self.retu += 1
                if gm.entitytype == 'NIL':
                    self.retrelu += 1

        # for gm in relevant:
        #     if gm.content not in seengm:
        #         sys.stderr.write(gm.content.encode('utf-8')+"\n")


    def evaluate(self):
        # SCORES NER
        self.p = self.retrel / self.ret
        self.r = self.retrel / self.rel
        self.f = 2 * self.p * self.r / (self.p + self.r)
        for t in ['Person', 'Location', 'Organization']: 
            try: self.acc_type[t] = self.corlink_type[t] / self.rellink_type[t]
            except ZeroDivisionError: self.acc_type[t] = float('nan')

        # SCORES NAE
        if not options.nonae:
            try: self.naep = self.retrel_nae / self.retrieved_nae
            except ZeroDivisionError: self.naep = float('nan')
            try: self.naer = self.retrel_nae / self.true_nae
            except ZeroDivisionError: self.naer = float('nan')
            try: self.naef = 2 * self.naep * self.naer / (self.naep + self.naer)
            except ZeroDivisionError: self.naef = float('nan')
        else:
            self.naep, self.naer, self.naef = 0.0, 0.0, 0.0

        # SCORES REFERENCE TYPE DETECTION
        # KNOWN
        try: self.pk = self.retrelk / self.retk
        except ZeroDivisionError: self.pk = float('nan')
        try: self.rk = self.retrelk / self.rellinkk
        except ZeroDivisionError: self.rk = float('nan')
        try: self.fk = 2 * self.pk * self.rk / (self.pk + self.rk)
        except ZeroDivisionError: self.fk = float('nan')
        # UNKNOWN
        try: self.pu = self.retrelu / self.retu
        except ZeroDivisionError: self.pu = float('nan')
        try: self.ru = self.retrelu / self.rellinku
        except ZeroDivisionError: self.ru = float('nan')
        try: self.fu = 2 * self.pu * self.ru / (self.pu + self.ru)
        except ZeroDivisionError: self.fu = float('nan')

        # SCORES ACCURACY LINKING
        self.acc = self.corlink / self.rellink
        try: self.acc_k = self.corlinkk / self.rellinkk
        except ZeroDivisionError: self.acc_k = float('nan')
        try: self.acc_u = self.corlinku / self.rellinku
        except ZeroDivisionError: self.acc_u = float('nan')
        
        self.pl = self.p * self.acc
        self.rl = self.r * self.acc
        self.fl = 2 * self.pl * self.rl / (self.pl + self.rl)

    def write(self):
        sys.stdout.write('|  NER-P |  NER-R |  NER-F | AccAll |  AccK  | AccK2  | AccNil  |  EL-P  |  EL-R   |  EL-F   |  NAE + |  NAE - |  NAE P |  NAE R |  NAE F |\n')
        sys.stdout.write('| %.4f | %.4f | %.4f | %.4f | %.4f |    -   | %.4f  | %.4f | %.4f  | %.4f  |   -    |    -   | %.4f | %.4f | %.4f  |\n'%(self.p, self.r, self.f, self.acc, self.acc_k, self.acc_u, self.pl, self.rl, self.fl, self.naep, self.naer, self.naef))

        sys.stdout.write("|        |        |        |        |        |        |         |        |         |         |        |        |        |        |        |\n")
        for t in ['Person', 'Location', 'Organization']:
            sys.stdout.write("|        |        |        |        |  %.4f|        |         |        |         |         |        |        |        |        |        |\n"%(self.acc_type[t]))

        sys.stdout.write("\n|  P-k  |  R-k  |  F-k  |  P-u  |  R-u  |  F-u  |\n")
        sys.stdout.write("| %.4f | %.4f | %.4f | %.4f | %.4f | %.4f |\n"%(self.pk, self.rk, self.fk, self.pu, self.ru, self.fu))


    
def main():
    global options
    options, args = getOptions()
    evaluation = Eval()
    if os.path.isfile(options.gold) and os.path.isfile(options.nomos) and options.file_type == 'news':
        evaluation.count(parse(options.gold), parse(options.nomos))
    elif os.path.isdir(options.gold) and os.path.isdir(options.nomos):
        matches = {}
        gold_dir = options.gold
        nomos_dir = options.nomos
        nomos_files = filter((lambda f: not re.match('^\.', f) and re.search('.xml$', f)), os.listdir(nomos_dir))
        for nomos_file in nomos_files:
            gold_file = re.sub('nomos\.res(olved)?\.xml', 'gold.xml', nomos_file)
            gold_file = gold_dir+"/"+gold_file
            nomos_file = nomos_dir+"/"+nomos_file
            if os.path.isfile(nomos_file) and os.path.isfile(gold_file):
                matches[nomos_file] = gold_file
            else:
                print >> sys.stderr, "No match for files\nGold=%s (%s)\nNomos=%s (%s)"%(gold_file, str(os.path.isfile(gold_file)), nomos_file, str(os.path.isfile(nomos_file)))
                continue
        cpt = 0
        for nomos_file, gold_file in matches.items():
            evaluation.count(parse(gold_file), parse(nomos_file))
    elif options.file_type == 'newscorpus':
        cpt = 0
        if options.nomos != None: F = codecs.open(options.nomos, 'r',  'utf-8')
        else: F = sys.stdin
        in_doc = False; string_to_parse = ""
        id_pattern = re.compile('urn:newsml:afp.com:\d+T\d+Z:([^:]+)')
        for line in F:
            if re.match('^\s*<nomos>', line):
                in_doc = True; string_to_parse += line
            elif in_doc and re.search('</nomos>', line):
                in_doc = False; string_to_parse += line
                ndoc = parseString(string_to_parse.encode('utf-8'))
                string_to_parse = ""
                try: gold_file = glob(options.gold+'/'+'afp.com*'+id_pattern.match(nomos_document.id).group(1)+'*')[0]
                except IndexError: sys.exit(nomos_document.id)
                evaluation.count(parse(gold_file), ndoc)
                cpt += 1
                # message = "%d documents evaluated"%(cpt)
                # sys.stderr.write(message + '\b'*(len(message)))
            elif in_doc:
                string_to_parse += line

    evaluation.evaluate()
    evaluation.write()

def getOptions():
    parser = OptionParser()
    file_types = ['news', 'newscorpus']
    parser.add_option("--gold", action="store", dest="gold", default=None, help="Gold Dir")
    parser.add_option("--nomos", action="store", dest="nomos", default=None, help="Nomos Dir")
    parser.add_option('--file', '-f', action='store', dest='file_type', type="choice", choices=file_types, default='news', help='Input type: One news item in Nomos format or news corpus in Nomos format (default: one news item)')
    parser.add_option("--goldner", action="store_true", dest="goldner", default=False, help="True: NER is gold")
    parser.add_option("--nonae", action="store_true", dest="nonae", default=False, help="True: Don't compute NAE scores")
    # parser.add_option("--log", action="store", dest="log", default=None, help="<log file>")
    try: (options, args) = parser.parse_args()
    except OptionError: sys.stderr.write('OptionError'); parser.print_help(); sys.exit()
    except: sys.stderr.write('An error occured during options parsing\n'); parser.print_help(); sys.exit()
    if options.gold == None: parser.print_help(); sys.exit()
    # if len(args) != 1: parser.print_help(); sys.exit()
    return options, args


if __name__ == "__main__":
    main()



    # for t in ['Person', 'Location', 'Organization']:
    #     trelevant = filter(lambda x:x.type == t, relevant)
    #     tretrieved = filter(lambda x:t in x.types, retrieved)
    #     trelevant_tokens = map(lambda x:x.tokens, trelevant)
    #     tretrel = filter(lambda x:x.tokens in trelevant_tokens, tretrieved)
    #     try: tp = float(len(tretrel)) / float(len(tretrieved))
    #     except ZeroDivisionError: tp = '-'
    #     try: tr = float(len(tretrel)) / float(len(trelevant))
    #     except ZeroDivisionError: tr= '-'
    #     if tp == '-' or tr == '-':
    #         tf = '-'
    #     else:
    #         try: tf = 2*tp*tr / (tp+tr)
    #         except ZeroDivisionError: tf = '-'
    #     print >> sys.stderr, t
    #     sys.stdout.write('|  NER-P |  NER-R |  NER-F | AccAll |  AccK  | AccK2  | AccNil  |  EL-P  |  EL-R   |  EL-F   |  NAE + |  NAE - |  NAE P |  NAE R |  NAE F |\n')
    #     try: sys.stdout.write('|%.4f|%.4f|%.4f|\n'%(tp, tr, tf))
    #     except TypeError: sys.stdout.write('|%s|%s|%s|\n'%(str(tp), str(tr), str(tf)))
