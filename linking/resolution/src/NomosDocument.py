#!/usr/bin/python
# -*- coding: utf-8 -*-
import copy
import sys
import string
from collections import defaultdict
import codecs
from xml.dom.minidom import *
sys.path.append('@common_src@')
from utils import *
import DagReader
import Mention
import NomosUtils

class Document():
    def __init__(self, dom, mode, in_eval=False, lex_connection=None, kb_connection=None):
        self.in_eval = in_eval
        self.dom = dom
        if mode != 'gold': self.nermodule = selectNodes(dom.getElementsByTagName('module'), 'attr_value', ('type', 'ner'))[0]
        else: self.nermodule = dom.getElementsByTagName('paras')[0]
        self.mode = mode
        if self.mode == 'auto':
            self.id = self.dom.getElementsByTagName('meta')[0].getAttribute('doc_id')
            self.tokens2mentions = defaultdict(list)
        else: # gold doc mode
            self.id = None
            self.tokens2mentions = {}
        tokens = self.dom.getElementsByTagName('token')
        self.tokens2value = dict(zip(map((lambda token: token.getAttribute('id')), tokens), map((lambda token: getNodeData(token)), tokens)))
        self.candidates = {}
        self.analytics = {}
        self.variants = {}
        self.tokens2surrwords = {}
        if mode == 'auto' and not self.in_eval:
            analytics_node = selectNodes(dom.getElementsByTagName('module'), 'attr_value', ('type', 'news_analytics'))[0]
            self.analytics['iptc'] = map((lambda x: x.getAttribute('value')), analytics_node.getElementsByTagName('iptc')) # LIST OF IPTC CODES
            self.analytics['slugs'] = filter(lambda kw:kw!="null", map((lambda x: x.getAttribute('value')), analytics_node.getElementsByTagName('keyword'))) # LIST OF KEYWORDS
            # CREATE TABLE countries (afpcc text unique, geonamescc text unique, aledaid integer unique, vars text);
            country_code_afp = analytics_node.getElementsByTagName('meta')[0].getAttribute('country')
            city_afp = analytics_node.getElementsByTagName('meta')[0].getAttribute('city')
            area_afp = analytics_node.getElementsByTagName('meta')[0].getAttribute('area')
            kb_connection.tables['countries'].selectFromTable(kb_connection.cursor, ['aledaid', 'geonamescc', 'vars'], [('afpcc', '=', country_code_afp)])
            res = kb_connection.cursor.fetchone()
            if res != None and res != []:
                (aleda_country, country_code, aleda_country_vars) = res
                self.analytics['country'] =  aleda_country# COUNTRY EID
                self.analytics['country_code'] = country_code# GEONAMES CC ISO-2
                self.analytics['locvars'] = aleda_country_vars.split('#')
                if city_afp != "": self.analytics['locvars'].append(city_afp)
                if area_afp != "": self.analytics['locvars'].append(area_afp)
            else:
                self.analytics['country'] =  ""# COUNTRY EID
                self.analytics['country_code'] = ""# GEONAMES CC ISO-2
                self.analytics['locvars'] = []

            self.analytics['swords'] = map((lambda x: x.getAttribute('value')), analytics_node.getElementsByTagName('salient_word')) # LIST OF SWORDS
            # self.date = date
            # TODO: DOMAIN ANALYTICS
        if self.in_eval == True and self.mode == 'auto':
            self.resolution_module = selectNodes(self.dom.getElementsByTagName('module'), 'attr_value', ('type', 'resolution'))[0]
        if self.mode == 'auto' and re.search('goldner', self.nermodule.getAttribute('id')):
            self.goldner = True
        else:
            self.goldner = False

        # TODO: ADAPT FOR MULTIPLE NER
        self.populateMentions(lex_connection, kb_connection)
        self.populateZones()


    def addTokens2mention(self, mention):
        # GROUPS OF MENTIONS FOR A GIVEN TOKEN SEQUENCE
        if self.mode == 'gold':
            self.tokens2mentions[mention.tokens] = mention
        elif self.mode == 'auto':# and not self.goldner:
            self.tokens2mentions[mention.tokens].append(mention)
        
    def populateMentions(self, lex_connection, kb_connection):
        # TODO: WHAT IF THERE IS MORE THAN 1 NER MODULE?
        if self.in_eval == True:
            # local_eids = map((lambda node: node.getAttribute('local_eid')), filter((lambda enamex: enamex.parentNode.getAttribute('valid') == 'true'), self.resolution_module.getElementsByTagName('ENAMEX')))
            local_eids = {}
            for enamex in self.resolution_module.getElementsByTagName('ENAMEX'):
                if enamex.parentNode.tagName == 'CHOICE':
                    if enamex.parentNode.getAttribute('valid') == 'true': local_eids[enamex.getAttribute('local_eid')] = enamex
                else:
                    if enamex.getAttribute('valid') == 'true': local_eids[enamex.getAttribute('local_eid')] = enamex
            # ONLY MENTIONS WITH VALID ATTRIBUTE
            self.mentions = dict(map((lambda item: (item[0], Mention.Mention(item[1], self))), local_eids.items()))
        else:
            local_eids = map((lambda node: node.getAttribute('local_eid')), self.dom.getElementsByTagName('ENAMEX'))
            self.mentions = dict(zip(local_eids, map((lambda node: Mention.Mention(node, self, lex_connection)), self.nermodule.getElementsByTagName('ENAMEX'))))
            # DELETE DOUBLE MENTIONS = SAME TOKENS, DIFFERENT TYPE; CONCAT ATTR VALUES (TYPES, GENDERS) TO FIRST MENTION AND DELETE OTHERS
            if self.mode == 'auto' and not self.goldner:
                # MENTIONS ARE GROUPPED BY TOKENS IN doc.tokens2mentions{}
                for group in filter((lambda g: len(g) > 1), self.tokens2mentions.values()):
                    group[0].types = list(set(map((lambda m: m.types[0]), group)))
                    origins = list(set(map((lambda m: m.origins[0]), group)))
                    if 'liane' in origins and 'sxpipe' in origins:
                        group[0].origins.append('liane'); group[0].origins.append('sxpipe')
                    if 'Person' in group[0].types:
                        group[0].genders = filter((lambda g: g is not None), map((lambda m: m.genders[0]), group))
                    for m in group[1:len(group)]:
                        m.deleteSelf()
            ################################
            # MAKE CLUSTERS
            ################################
                    
    def populateZones(self):
        if self.nermodule.getElementsByTagName('SENT') != []:
            parent = 'SENT'
        else:
            if self.nermodule.getElementsByTagName('para') != []:
                parent = 'para'
            elif self.nermodule.getElementsByTagName('p') != []:
                parent = "p"
            else:
                sys.exit('No parent node available to fetch ENAMEX\n')
        if self.in_eval: module = self.resolution_module
        else: module = self.nermodule
        # SIMPLE ZONES: ENAMEX CHILDREN OF PARA/SENT
        simple_zones = mergeLists(map((lambda para: selectNodes(para.childNodes, 'node_type', ('element', ['ENAMEX']))), module.getElementsByTagName(parent)))
        # AMBIGUOUS ZONES: ALT CHILDREN OF PARA/SENT
        ambiguous_zones = mergeLists(map((lambda para: selectNodes(para.childNodes, 'node_type', ('element', ['ALT']))), module.getElementsByTagName(parent)))
        self.simple_zones = dict(zip(map((lambda node: 'SZ'+node.getAttribute('local_eid')), simple_zones), map((lambda node: DagReader.Zone(node, self)), simple_zones)))
        self.ambiguous_zones = dict(zip(map((lambda node: 'AZ'+node.getAttribute('id')), ambiguous_zones), map((lambda node: DagReader.AmbiguousZone(node, self)), ambiguous_zones)))
        self.zones = copy.copy(self.simple_zones)
        self.zones.update(self.ambiguous_zones)

    def populateCandidates(self, aleda_vars_connection, aleda_refs_connection, kb_connection):
        for variant_id, variant in self.variants.items():
            variant.getCandidates(aleda_vars_connection, aleda_refs_connection, kb_connection)
        for mention_id, mention in self.mentions.items():
            mention.addCandidates()
            # mention.setVariantType(aleda_vars_connection)

    def scorePaths(self, bias):
        for mention in self.mentions.values():
            mention.setBestCandidate()
        for zone_id, zone in self.zones.items():
            zone.rankPaths(bias)

    def buildResolutionModule(self):#, gold_document):
        resolution_module = createElementNode(self.dom, "module", {"type":"resolution", "id":"nomos_resolver"})
        news_content = self.dom.documentElement.getElementsByTagName('news_content')[0].cloneNode(True)
        if self.nermodule.getElementsByTagName('SENT') != []:
            parent = 'SENT'
        else:
            if self.nermodule.getElementsByTagName('para') != []:
                parent = 'para'
            elif self.nermodule.getElementsByTagName('p') != []:
                parent = "p"
            else:
                sys.exit('No parent node available to fetch ENAMEX\n')

        # for zone in mergeLists(map((lambda para: selectNodes(para.childNodes, 'node_type', ('element', ['ENAMEX', 'ALT']))), news_content.getElementsByTagName(parent))):
        for enamex in filter((lambda node: isElementNode(node) and node.tagName == 'ENAMEX'), mergeLists(map((lambda parent: parent.childNodes), news_content.getElementsByTagName(parent)))):
            # IF ENAMEX IS NOT IN ALT, IT SHOULD NOT HAVE BEEN SUBJECTED TO DELETION AS DOUBLE
            local_eid = enamex.getAttribute('local_eid')
            z = self.zones['SZ'+local_eid]
            path = z.paths[local_eid]
            mention = self.mentions[local_eid]
            # if mention.is_in_path_part.valid:
            self.addPathResolution(path, enamex)
            self.addMentionResolution(mention, enamex)
        for alt in filter((lambda node: isElementNode(node) and node.tagName == 'ALT'), mergeLists(map((lambda parent: parent.childNodes), news_content.getElementsByTagName(parent)))):
            zone_id = 'AZ'+alt.getAttribute('id')
            for choice in alt.getElementsByTagName('CHOICE'):
                path = self.zones[zone_id].paths[choice.getAttribute('id')]
                if path.valid:
                    self.addPathResolution(path, choice)
                    for enamex in choice.getElementsByTagName('ENAMEX'):
                        try:
                            mention = self.mentions[enamex.getAttribute('local_eid')]
                            self.addMentionResolution(mention, enamex)
                        except KeyError:
                            addAttribute2Element(self.dom, enamex, 'discarded', 'true')
                else:
                    addAttribute2Element(self.dom, choice, 'valid', 'false')
        resolution_module.appendChild(news_content)
        self.dom.documentElement.appendChild(resolution_module)

    def addPathResolution(self, path, choice):
        if path.valid:
            addAttribute2Element(self.dom, choice, 'path_score', str(path.score))
            addAttribute2Element(self.dom, choice, 'rank', str(path.rank))
            addAttribute2Element(self.dom, choice, 'valid', 'true')
        else:
            addAttribute2Element(self.dom, choice, 'valid', 'false')

    def addMentionResolution(self, mention, enamex):
        # addAttribute2Element(self.dom, enamex, 'valid', 'true')
        addAttribute2Element(self.dom, enamex, 'types', mention.displayType())
        i = 0
        for candidate in mention.sorted_candidates:
            i += 1
            candidate_node = createElementNode(self.dom, 'candidate', {'rank':str(i), 'score':str(candidate.scores[mention.id]), 'cand_type':candidate.candidate_type, 'id':candidate.id, 'name':candidate.name, 'type':candidate.type, 'href':candidate.href}, None, ['rank', 'score', 'cand_type', 'id', 'name', 'type', 'href'])
            enamex.appendChild(candidate_node)
        if mention.best_candidate.candidate_type == 'NIL':
            addAttribute2Element(self.dom, enamex, 'resolution_id', 'null')
            addAttribute2Element(self.dom, enamex, 'cand_type', 'NIL')
            addAttribute2Element(self.dom, enamex, 'candidate_name', mention.best_candidate.name)
            addAttribute2Element(self.dom, enamex, 'candidate_type', 'null')
        elif mention.best_candidate.candidate_type == 'NAE':
            addAttribute2Element(self.dom, enamex, 'resolution_id', 'null')
            addAttribute2Element(self.dom, enamex, 'cand_type', 'NAE')
            addAttribute2Element(self.dom, enamex, 'candidate_type', 'null')
        elif mention.best_candidate.candidate_type == 'ALEDA':
            try: addAttribute2Element(self.dom, enamex, 'resolution_id', mention.best_candidate.id)
            except: print >> sys.stderr, mention.best_candidate.id; sys.exit()
            addAttribute2Element(self.dom, enamex, 'cand_type', 'ALEDA')
            addAttribute2Element(self.dom, enamex, 'candidate_type', mention.best_candidate.type)
            addAttribute2Element(self.dom, enamex, 'candidate_name', mention.best_candidate.name)
        addAttribute2Element(self.dom, enamex, 'candidate_score', str(mention.best_candidate.scores[mention.id]))

    def getXmlScoredPaths(self):
        for zone in self.zones.values():
            for path in zone.valid_paths:
                if zone.type == 'simple':
                    node = zone.node
                elif zone.type == 'ambiguous':
                    node = filter((lambda choice: choice.getAttribute('id') == path.id), zone.choices)[0]
                path.getXmlRank(node)
                path.getXmlScore(node)
            zone.sorted_paths = sorted([path for path in zone.valid_paths], key=lambda path: path.score, reverse=True)
            # if len(zone.sorted_paths) == 1:
            #     zone.sorted_paths[0].rank = 0
            # else:
            #     i = 0
            #     for p in zone.sorted_paths:
            #         i += 1
            #         p.rank = i
                

    def unlinkDom(self):
        self.dom.unlink()
