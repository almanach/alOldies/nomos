#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import re
import sys
import string
from decimal import Decimal as dec
from xml.dom.minidom import *
sys.path.append('@common_src@')
from utils import *
from Entity import Variant, NilCandidate, NAE
from NomosUtils import *
from collections import defaultdict
import codecs

class Mention(): # EACH ENAMEX OCCURRENCE, ID=LOCAL_EID
    # deleted_mentions = {}
    def __init__(self, node, document, lex_connection=None):
        # print >> sys.stderr, 'Init mention (%s)'%(document.mode)
        # <ENAMEX conf="0.17" local_eid="12" origin="sxpipe;liane" type="Organization_0.17;Person">
        # <ENAMEX conf="0.12" gender="m;f" local_eid="5" origin="sxpipe;liane" type="Person;Location_0.12">
        # <ENAMEX conf="0.17" local_eid="2" origin="liane" type="Organization">
        # <ENAMEX gender="m" local_eid="1" type="Person">
        self.types = []; self.types2conf = {}; self.conf = dec('0.0')
        if re.search(';', node.getAttribute('type')):
            for t in node.getAttribute('type').split(';'):
                if re.search('_\d\.\d+', t):
                    (type, conf) = t.split('_')
                    if dec(conf) > self.conf: self.conf = dec(conf)
                    self.types2conf[type] = conf
                    self.types.append(type)
                else:
                    self.types.append(t)
        else:
            self.types = [node.getAttribute('type')]
        if self.conf == 0.0 and node.getAttribute('conf') != '':
            self.conf = node.getAttribute('conf')
        self.aleda_types = map(lambda t:aledaType2nomosType(t), self.types)
        self.typesup = getAttributeTest(node, 'typesup', '2') or getAttributeTest(node, 'typesup', '3')
        # MENTION GENDERS IS [GENDER, GENDER...] OR [NONE]
        if re.search(';', node.getAttribute('gender')): self.genders = node.getAttribute('gender').split(';')
        else: self.genders = [getAttributeDefault(node, 'gender', None)] # value of attribute gender, or None
        origins = getAttributeDefault(node, 'origin', None)
        if origins != None: self.origins = origins.split(';')
        else: self.origins = [document.nermodule.getAttribute('id')]
        self.id = node.getAttribute('local_eid')
        self.node = node
        self.document = document
        self.tokens = tuple(map((lambda token: token.getAttribute('id')), node.getElementsByTagName('token'))) # tuple of tokens Ids
        self.document.addTokens2mention(self)
        tokendata = fixVar(re.sub('_', ' ', ' '.join((map((lambda token: getNodeData(token)), node.getElementsByTagName('token'))))))
        # possible_full_pernames = []
        # if 'Person' in self.types:
        #     possible_full_pernames = filter(lambda v:v.endswith(tokendata), self.document.variants)
        try: self.variant = self.document.variants[tokendata]
        except KeyError: self.variant = Variant(tokendata, self.document)
        self.variant.mentions.add(self.id)
        self.name = getAttributeDefault(node, 'name', tokendata)
        # if self.name == None:
        #     if self.type == 'Person':
        #         self.name = max(self.variant.alt_variants)
        #     else:
        self.is_in_path_part = None
        if document.mode == 'gold':
            self.entity_id = re.sub('^\s*', '', re.sub('\s*$', '', self.node.getAttribute('eid')))
            if re.match('^\d+$', self.entity_id): self.entity_type = 'ALEDA'
            else: self.entity_type = 'NIL'
            self.detection = None
        self.discarded = False
        self.wpoccs = None
        if self.tokens == ():
            print >> sys.stderr, "mention %s empty tokens"%(self.id)
            sys.exit(self.document.id)

        if not self.document.in_eval and self.document.mode != 'gold':
            # GET SURROUNDING WORDS
            if self.document.tokens2surrwords.has_key(self.tokens):
                self.surrwords = self.document.tokens2surrwords[self.tokens]
            else:
                self.surrwords = set(getSurrWords(self.tokens, self.document.dom))
                self.document.tokens2surrwords[self.tokens] = self.surrwords
            
            # GET POSITION IN SENT
            if node.parentNode.tagName == 'CHOICE': parent = node.parentNode.parentNode.parentNode # <ENAMEX> IN <CHOICE> IN <ALT> IN <SENT OR PARA>
            else: parent = node.parentNode # <ENAMEX> IN <SENT OR PARA>
            # if parent.getElementsByTagName('form')[0] == node.getElementsByTagName('form')[0]: self.position = 1
            if parent.getElementsByTagName('token')[0] == node.getElementsByTagName('token')[0]: self.position = 1
            # else: self.position = parent.getElementsByTagName('form').index(node.getElementsByTagName('form')[0]) + 1
            else: self.position = parent.getElementsByTagName('token').index(node.getElementsByTagName('token')[0]) + 1
            # GET LEFFF INFO
            lex_connection.tables['data'].selectFromTable(lex_connection.cursor, ['key'], [('key', '=', re.sub(' ', '_', self.variant.string)), ('value', '!=', 'np')])
            res = lex_connection.cursor.fetchall()
            if res == []: self.is_in_lefff = False
            else: self.is_in_lefff = True
            lex_connection.tables['data'].selectFromTable(lex_connection.cursor, ['key'], [('key', '=', re.sub(' ', '_', self.variant.string).lower()), ('value', '!=', 'np')])
            res = lex_connection.cursor.fetchall()
            if res == []: self.is_in_lefff_nocap = False
            else: self.is_in_lefff_nocap = True

    def setOtherAttributes(self, kb_connection):
        # DAG CONFIG
        # is unique in DAG
        zone = self.is_in_path_part.is_in_path.zone
        if len(zone.valid_paths) == 1:
            self.dagu = True
            self.short = False
            self.long = False
        else:
            self.dagu = False
            # is shortest path in DAG
            if len(self.is_in_path_part.is_in_path.path_parts) == min(map(lambda p:len(p.path_parts), zone.valid_paths)):
                self.short = True
            else:
                self.short = False
            # is longest path in DAG
            if len(self.is_in_path_part.is_in_path.path_parts) == max(map(lambda p:len(p.path_parts), zone.valid_paths)):
                self.long = True
            else:
                self.long = False
            # is unique path span in DAG (useful only if types not merged)

        self.occs = len(filter(lambda m:m.variant.string == self.variant.string, self.document.mentions.values()))
        self.longer = None
        if "Person" in self.types:
            s = set([m.variant.string for m in self.document.mentions.values() if "Person" in m.types and m.variant.string != self.variant.string and m.variant.string.endswith(self.variant.string)])
            if s != set(): self.longer = s
            else: self.longer = False
        if "Person" in self.types or "Organization" in self.types:
            # WP OCCS: ONLY FOR WP ENTITIES!!
            kb_connection.tables['mentions2frequency'].selectFromTable(kb_connection.cursor, ['frequency'], [('mention', '=', self.variant.string)])
            res = kb_connection.cursor.fetchone()
            if res != None and res != "":
                self.wpoccs = res[0]
        


    def deleteSelf(self):
        # REMOVE FROM DOC MENTIONS
        del self.document.mentions[self.id]
        # REMOVE FROM DOC DICT {(TOKENS): MENTION}
        self.document.tokens2mentions[self.tokens].remove(self)
        # Mention.deleted_mentions[self.id] = True

    def deleteNAE(self):
        self.candidates = filter((lambda c: c.candidate_type != 'NAE'), self.candidates)

    def displayType(self):
        return ';'.join(self.types)

    # def setVariantType(self, aleda_vars_connection):
    #     aleda_vars_connection.tables['data'].selectFromTable(aleda_vars_connection.cursor, ['lastname'], [('variant', '=', self.string)])

    def addCandidates(self):
        candidates_ids = set(self.variant.candidates_ids)
        # FOR EACH CAND ID, GET CANDIDATE INSTANCE AND ADD IT TO MENTION INSTANCE 
        self.candidates = filter(lambda c: c.geonamestype != 'A.ADM4', map((lambda candidate_id: self.document.candidates[candidate_id]), candidates_ids))
        # COMPUTE RELATIVES SWORDS FOR CAND SET (TYPE A)
        total_words_population = sum(map(lambda c:len(c.kb_data['swords']), filter(lambda c:c.candidate_type2=='A' and c.kb_data['swords']!=None, self.candidates)))
        if total_words_population != 0:
            total_words2frequency = defaultdict(int)
            for cand in filter(lambda c:c.candidate_type2=='A', self.candidates):
                words = cand.kb_data['swords']
                if words != None:
                    for w in words:
                        total_words2frequency[w] += 1
            for cand in filter(lambda c:c.candidate_type2=='A', self.candidates):
                words = cand.kb_data['swords']
                if words != None:
                    words2frequency = dict(zip(words, [1 for x in words]))
                    ranked_words2saillance = computeSalienceLocal(words2frequency, total_words_population, total_words2frequency)
                    cand.relSwords[self.id] = map(lambda x:x[0], ranked_words2saillance)
                else:
                    cand.relSwords[self.id] = None
        else:
            for cand in filter(lambda c:c.candidate_type2=='A', self.candidates):
                cand.relSwords[self.id] = None
        for candidate in self.candidates:
            # ADD MENTION INSTANCE TO EACH MENTION CANDIDATE
            candidate.addMention(self)
            candidate.computeNameSim(self)
        # ADD NIL AND NAE CANDIDATES (NOT ADDED TO DOCUMENT CANDIDATES)
        # TODO: NORMALIZATION OF MENTION VARIANT FOR NIL AND NAE CANDIDATES NAMES
        self.candidates.append(NilCandidate(self.variant.string, self.document))
        # KEEP NAE IN GOLD NER OR NOT?
        # if not self.document.goldner:
        self.candidates.append(NAE(self.variant.string, self.document))



    def setDetection(self, span_detection, type_detection, detection, gold_mention=None):
        self.span_detection = span_detection
        self.type_detection = type_detection
        self.detection = detection
        self.gold_mention = gold_mention

    def setResolution(self):
        # DETECTION CASES: EXACT, FALSE
        # FOR ALL: RECORD ATTRIBUTES FROM XML NODE RELATIVE TO RESOLUTION
        self.cand_type = self.node.getAttribute('cand_type')
        self.resolution_id = self.node.getAttribute('resolution_id') # NULL IF NAE CAND
        # IF FALSE MATCH: NO GOLD MENTION, RESOLUTION = NONE
        # IF DISCARDED: RESOLUTION = NONE
        # IF EXACT MATCH AND CORRECT RESOLUTION, RESOLUTION = TRUE
        # IF EXACT MATCH AND WRONG RESOLUTION, RESOLUTION = FALSE
        if self.detection == 'false_match':
            if self.cand_type == 'NAE':
                self.discarded = True
                self.resolution = True
            else:
                self.resolution = False
        else:
            if self.cand_type == 'NAE':
                self.discarded = True
                self.resolution = False
            elif self.cand_type == 'NIL':
                if self.gold_mention.entity_type == 'NIL':
                    if self.name != self.gold_mention.name:
                        print >> sys.stderr, "Mname: %s\tGname: %s"%(self.name, self.gold_mention.name)
                    self.resolution = True
                    # else:
                    # self.resolution = False
                else:
                    self.resolution = False
            elif self.cand_type == 'ALEDA':
                if self.gold_mention.entity_type == 'ALEDA':
                    if self.resolution_id == self.gold_mention.entity_id:
                        self.resolution = True
                    else:
                        self.resolution = False
                else:
                    self.resolution = False


    def candRecall(self):
        if not self.document.in_eval: return
        # sys.stderr = codecs.getwriter('utf-8')(sys.stderr)
        self.candrecall = None
        self.candrecall3 = None
        self.candrecall2 = None 
        self.candrecall1 = None
        if self.detection == 'exact_match' and self.gold_mention.entity_type == 'ALEDA':
            candidates = filter(lambda c:c.getAttribute('cand_type') == 'ALEDA', self.node.getElementsByTagName('candidate'))
            sorted_candidates = map(lambda c:c.getAttribute('id'), sorted(candidates, key=lambda c:float(c.getAttribute('score')), reverse=True))
            # sys.stderr.write("Sorted Aleda cands for %s#%s#"%(self.document.id, self.id))#, self.variant.string.encode('utf-8')))
            # sys.stderr.write(self.variant.string.encode('utf-8')+"\n")
            # print >> sys.stderr, sorted_candidates
            if len(sorted_candidates) > 0:
                if self.gold_mention.entity_id in sorted_candidates:
                    self.candrecall = True
                    if len(sorted_candidates) > 2 and self.gold_mention.entity_id in sorted_candidates[0:3]:
                        self.candrecall3 = True
                    if len(sorted_candidates) > 1 and self.gold_mention.entity_id in sorted_candidates[0:2]:
                        self.candrecall2 = True
                        self.candrecall3 = True
                    if len(sorted_candidates) > 0 and self.gold_mention.entity_id == sorted_candidates[0]:
                        self.candrecall1 = True
                        self.candrecall2 = True
                        self.candrecall3 = True
                    # print >> sys.stderr, "Candidate recall == 1 (gold in cands @ rank %d)"%(sorted_candidates.index(self.gold_mention.entity_id)+1)
                    # self.display()
                # else:
                    # print >> sys.stderr, "Candidate recall == 0 (gold not in cands)"
                    # self.display()
            # else:
                # print >> sys.stderr, "Candidate recall == 0 (no cands)"
                # self.display()
            
                
        #     if self.gold_mention != None:
        #         self.gold_mention.resolution = 'null'
        # elif self.gold_mention != None:
        #     if re.match('^\d+$', self.gold_mention.node.getAttribute('eid')): gold_entity_type = 'ALEDA'
        #     else: gold_entity_type = 'NIL'
        #     # print >> sys.stderr, "======================="
        #     # print >> sys.stderr, self.gold_mention.node.getAttribute('eid')
        #     # print >> sys.stderr, self.resolved_to_id
        #     # AUTO IS KNOWN
        #     if self.cand_type == 'ALEDA':
        #         # GOLD IS KNOWN
        #         if gold_entity_type == 'ALEDA':
        #             # self.resolution_set = 'kk'
        #             if self.gold_mention.node.getAttribute('eid') == self.resolved_to_id:
        #                 self.resolution = 'correct_resolution'
        #                 self.gold_mention.resolution = 'correct_resolution'
        #             else:
        #                 self.resolution = 'wrong_resolution'
        #                 self.gold_mention.resolution = 'wrong_resolution'
        #         # GOLD IS UNKNOWN
        #         elif gold_entity_type == 'NIL':
        #             # self.resolution_set = 'ku'
        #             self.resolution = 'missed_resolution'
        #             self.gold_mention.resolution = 'missed_resolution'

        #     # AUTO IS UNKNOWN
        #     elif self.cand_type == 'NIL':
        #         # GOLD IS KNOWN
        #         if gold_entity_type == 'ALEDA':
        #             # self.resolution_set = 'uk'
        #             self.resolution = 'false_resolution'
        #             self.gold_mention.resolution = 'false_resolution'
        #             # GOLD IS UNKNOWN
        #         elif gold_entity_type == 'NIL':
        #             # self.resolution_set = 'uu'
        #             if self.candidate_name == self.gold_mention.node.getAttribute('name'):
        #                 self.resolution = 'correct_resolution'
        #                 self.gold_mention.resolution = 'correct_resolution'
        #             else:
        #                 self.resolution = 'wrong_resolution'
        #                 self.gold_mention.resolution = 'wrong_resolution'

        # self.candidate_name = self.node.getAttribute('candidate_name')
        # self.candidate_type = self.node.getAttribute('candidate_type')
        # self.candidate_score = self.node.getAttribute('candidate_score')


    def setBestCandidate(self):
        # self.temp_sorted_candidates = sorted(self.candidates, key=lambda candidate: candidate.score, reverse=True)
        self.sorted_candidates = sorted(self.candidates, key=lambda candidate: candidate.scores[self.id], reverse=True)
        self.best_candidate = self.sorted_candidates[0]            
        # IF MORE THAN ONE CAND
        if len(self.sorted_candidates) > 1:
            # GET FIRST AND ALL EX AQUO
            first_cands = filter(lambda c: c.scores[self.id] == self.sorted_candidates[0].scores[self.id], self.sorted_candidates[1:])
            # IF ONLY FIRST OF SORTED CAND HAS THIS SCORE, IT'S THE FIRST / BEST CAND
            # ELSE, IF MORE THAN ONE CAND HAS THIS SCORE
            if first_cands != []:
                # print >> sys.stderr, "%d first exaequos"%(len(first_cands)+1)
                # print >> sys.stderr, self.document.id,; print >> sys.stderr, self.id
                # GET LIST OF EXAEQUO FIRST CANDS
                first_cands.extend([self.best_candidate])
                best_is_set = False
                # TAKE CAND WITH TYPE MAXIMIZING CONF 
                if self.conf != dec('0.0'):
                    try:
                        max_conf_type = sorted(self.types2conf.keys(), key=lambda t: dec(self.types2conf[t]), reverse=True)[0]
                        res = filter(lambda c: c.type == max_conf_type, first_cands)
                        if len(res) == 1:
                            self.best_candidate = res[0]
                            best_is_set = True
                    except IndexError:
                        pass
                    # IF MORE THAN ONE CAND WITH MAX CONF TYPE
                    else:
                        # GET LIST OF ALEDA EXAEQUO FIRST CANDS
                        aleda_cands = filter(lambda c: c.candidate_type == 'ALEDA', first_cands)
                        # IF ALL ALEDA CANDS ARE LOCATIONS
                        locations = filter(lambda c: c.type == 'Location', aleda_cands)
                        if len(locations) == len(aleda_cands):
                            if len(locations) == 2: # (CONSIDER CASE WHERE THERE ARE ONLY 2)
                                # IF ONE IS CITY/PPL-C-A-A2 AND THE OTHER IS COUNTRYDIV/A.ADM > 1, TAKE CITY
                                if filter(lambda l: l.subtype == 'City', locations) != [] and filter(lambda l: re.match('A\.ADM(2|3|4)', l.geonamestype), locations) != []:
                                    self.best_candidate = filter(lambda l: l.subtype == 'City', locations)[0]
                                    best_is_set = True
                                # IF ONE IS CITY/PPL-C-A-A2 AND THE OTHER IS COUNTRYDIV/A.ADM1 > 1 (QUEBEC), TAKE ADM1
                                elif filter(lambda l: l.subtype == 'City', locations) != [] and filter(lambda l: re.match('A\.ADM1', l.geonamestype), locations) != []:
                                    try:
                                        self.best_candidate = filter(lambda l: re.match('A\.ADM1', l.subtype), locations)[0]
                                        best_is_set = True
                                    except IndexError:
                                        pass
                            if not best_is_set:
                                # PICK MAX AMONG ALEDA CANDS BY LOOKING AT GEONAMES WEIGHT
                                aleda_cands_weighted = filter(lambda c: c.aleda_data['aleda_geonames_weight'] != None, aleda_cands)
                                if aleda_cands_weighted != []:
                                    max_aleda_cand = max(aleda_cands_weighted, key=lambda c: c.aleda_data['aleda_geonames_weight'])
                                    # IF ONLY ONE MAX
                                    if len(filter(lambda c: c.scores[self.id] == max_aleda_cand.scores[self.id], aleda_cands_weighted)) == 1:
                                        self.best_candidate == max_aleda_cand
                                        best_is_set = True
                                # IF GEONAMES WEIGHT = NONE FOR ALL ALEDA CANDS OR MORE THAN ONE MAX WITH GEONAMES WEIGHT
                                # pass
                        # IF NOT ALL FIRST ARE LOCATIONS
                        # COULD HAVE BEEN RESOLVED WITH MAX CONF TYPE
                        # IF MAX CONF TYPE WAS LOC... TOO COMPLICATED, TAKE FIRST THAT COMES
                        if not best_is_set:
                            self.best_candidate = self.sorted_candidates[0]
            else:
                self.best_candidate = self.sorted_candidates[0]

        # UNCOMMENT THIS TO TEST WITH ONLY CLASS ONE CANDS
        # IF BEST CANDIDATE IS NOT CLASS 1, DISCARD (BEST MIGHT BE NAE)
        # print >> sys.stderr, self.best_candidate.scores[self.id]
        # if self.best_candidate.scores[self.id] < dec('0.5'):
        #     self.best_candidate = filter(lambda c: c.candidate_type == 'NAE', self.candidates)[0]
            # print >> sys.stderr, '=> Replaced by NAE'
        
    def display(self, FH=None):
        try: rank = self.is_in_path_part.is_in_path.rank
        except AttributeError: rank = '-'
        if self.document.mode == 'gold':
            # writeLog('Mention %s | %s | %s | %s | %s | %s | %s'%(self.id, self.name, self.entity_id, '+'.join(map((lambda token_id: token_id+'#'+self.document.tokens2value[token_id]), self.tokens)), self.displayType(), self.detection, self.resolution), FH)
            writeLog('Mention %s#%s | %s | %s | %s | %s | %s'%(self.document.id, self.id, self.name, self.entity_id, '+'.join(map((lambda token_id: token_id+'#'+self.document.tokens2value[token_id]), self.tokens)), self.displayType(), self.detection), FH)
        if self.document.mode == 'auto':
            writeLog('Mention %s#%s | %s | %s | %s => #%s'%(self.document.id, self.id, self.name, '+'.join(map((lambda token_id: token_id+'#'+self.document.tokens2value[token_id]), self.tokens)), self.displayType(), str(rank)), FH)
            if self.document.in_eval:
                if self.discarded:
                    # writeLog('Discarded: %s => %s (%s, %s)'%(self.candidate_score, self.detection, self.span_detection, self.type_detection), FH)
                    writeLog('Discarded', FH)
                else:
                    writeLog('Resolved to Candidate: %s | %s => %s (%s, %s)'%(self.resolution_id, self.node.getAttribute('candidate_score'), self.detection, self.span_detection, self.type_detection), FH)
                if self.gold_mention != None:
                    writeLog('Gold Mention: %s | %s | %s | %s => %s'%(self.gold_mention.id, self.gold_mention.displayType(), self.gold_mention.name, self.gold_mention.node.getAttribute('eid'), self.resolution), FH)
            else:
                x = 1
                for candidate in self.sorted_candidates:
                    candidate.display(self, FH)
        writeLog('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n', FH)
