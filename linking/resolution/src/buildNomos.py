#!/usr/bin/python
# -*- coding: utf-8 -*-
import sqlite3
import codecs
import re
import os
import sys
from collections import defaultdict
from optparse import *
from xml.dom.minidom import *
from subprocess import *
sys.path.append('@common_src@')
sys.path.append('@kb_src@')
from utils import *
from kbwp_utils import *

def doAll(newsxml_dom, other_dom=None):
    # IF GOLD-NER: BUILD NER MODULE FROM DOM WITH DISAMB DAGS, REPLACE NORMALIZED PARAS WITH GOLD PARAS, GET XML/TEXT (<module type="ner" id="gold_ner">)
    # GOLD ITEMS WERE CONVERTED IN NEEDED FORMAT WITH XSLT
    # GOLD-NER AND OTHER NER ARE NOT COMPATIBLE
    tokens2frequency_dict = None
    if options.goldner:
        if options.file_type == 'newscorpus':
            gold_dom = other_dom
        else:
            gold_dom = parse(options.goldner)
        tokens2frequency_dict = getTokens(gold_dom)
        fixGoldNer(gold_dom)
        ner_xml = getOtherNer(newsxml_dom, gold_dom, options.nerid)
    else: # AFP
        print >> sys.stderr, "build 31"
        # IF SXPIPE-NER ONLY: BUILD NER MODULE FROM DOM WITH AMB NER, GET XML/TEXT (<module type="ner" id="sxpipe_ner">)
        if options.sxpipener and not options.lianener and not options.mergener: # AFP
            # GET DAGS (AMBIGUOUS, WITH SXPIPE NER) FROM NEWS
            sxpipe_ner_dags = getSxpipeNerDags(newsxml_dom)
            print >> sys.stderr, "build 36"
            # TRANSFORM AMB DAGS IN XML ANNOTATED PARAS, GET DOM (AMB ANNOTATIONS WITH <ALT> ELEMENTS)
            sxpipe_ner_dom = postProcessSxpipeNerDagsXml(sxpipeNerDags2xml(sxpipe_ner_dags)) # DO ANYWAY: NEEDED FOR ANALYTICS/SWORDSbHe
            print >> sys.stderr, "build 39"
            tokens2frequency_dict = getTokens(sxpipe_ner_dom)
            if options.disamb:
                sxpipe_ner_normalized_dom = getSxpipeNerNormalizationDom(sxpipe_ner_dags)
                sxpipe_ner_disamb_dom = getSxpipeDisambXml(sxpipe_ner_normalized_dom) # TODO
                ner_xml = buildNer(sxpipe_ner_disamb_dom)
            else: # AFP
                ner_xml = buildNer(sxpipe_ner_dom)
            print >> sys.stderr, "build 46"
        # IF LIANE-NER ONLY: BUILD NER MODULE FROM DOM WITH DISAMB DAGS, REPLACE NORMALIZED PARAS WITH LIANE PARAS, GET XML/TEXT (<module type="ner" id="liane_ner">)
        # LIANE ITEMS WERE CONVERTED IN NEEDED FORMAT WITH liane2lianer.py
        elif options.lianener != None and not options.sxpipener:
            # ONE BEST LIANE NER DIRECTLY GOT FROM FILE BUILT WITH lianeNerFormat2nomosFormat.py?
            if os.path.isdir(options.lianener):
                lianer_dom = other_dom
            else:
                lianer_dom = parse(options.lianener)
            ner_xml = getOtherNer(newsxml_dom, lianer_dom.documentElement, options.nerid)
            tokens2frequency_dict = getTokens(lianer_dom)
        # IF SXPIPE-NER AND LIANE-NER, READ MERGED LIANE AND SXPIPE NER FILE 
        elif options.mergener:# and options.sxpipener:
            mergener_dom = parse(options.mergener)
            ner_xml = getOtherNer(newsxml_dom, mergener_dom.documentElement, options.nerid)
            tokens2frequency_dict = getTokens(mergener_dom)
    # analytics_xml = getNewsAnalytics(newsxml_dom) # TODO: USE NEWSXML_DOM BUT NEED TOKENIZATION FOR SWORDS
    analytics_xml = getNewsAnalytics(newsxml_dom, tokens2frequency_dict) # TODO: USE NEWSXML_DOM BUT NEED TOKENIZATION FOR SWORDS
    if not options.normalize:
        # printerr('WRITE ANALYTICS + NER MODULE TO XML')
        nomos_xml_file = '<nomos>\n' + analytics_xml + ner_xml + '</nomos>\n'
    else: # AFP
        print >> sys.stderr, "build 66"
        # print >> sys.stderr, "Normalize..."
        sxpipe_resolution_xml = getSxpipeResolution(sxpipe_ner_dags)
        # printerr('WRITE ANALYTICS + NER MODULE + RESOLUTION MODULE BY np_normalizer (SxPipe) TO XML')
        # nomos_xml_file = '<nomos>\n' + analytics_ner + ner_xml + sxpipe_resolution_xml + '</nomos>\n'
        nomos_xml_file = '<nomos>\n' + analytics_xml + ner_xml + sxpipe_resolution_xml + '</nomos>\n'
    # printerr('VALIDATE AND FORMAT XML')
    xmllint = Popen(['xmllint', '--format', '--encode', 'UTF-8', '-'], stdin=PIPE, stdout=PIPE, stderr=PIPE)
    valid_output = xmllint.communicate(nomos_xml_file.encode('utf-8'))[0]
    result = parseString(valid_output).documentElement.toxml('utf-8')
    print >> sys.stderr, "build 76"
                
    
    # return valid_output
    return result

def getOtherNer(newsxml_dom, other_dom, ner_id):
    # printerr('Building NER module")
    xml = '<module type="ner" id="'+ner_id+'_ner">\n<news_content>\n'
    xml += newsxml_dom.getElementsByTagName('news_head')[0].toxml()
    xml += '<news_text>\n' + ''.join(map((lambda p: p.toxml()), other_dom.getElementsByTagName('para'))) + '</news_text>\n</news_content>\n'    
    xml += '</module>\n'
    return xml

def fixGoldNer(gold_dom):
    for e in gold_dom.getElementsByTagName('ENAMEX'):
        for attr in ['eid', 'name', 'sub_type', 'comment']:
            try: e.removeAttribute(attr)
            except xml.dom.NotFoundErr: pass

def initXml(arg, mode):
    # CONVERT NEWSML TO TEXT
    newsxml_dom, error = convertNewsML2xml(mode, arg)
    return newsxml_dom, error

def convertNewsML2xml(mode, data):
    if mode == 'file':
        newsml2text = Popen(['newsML2text', '--infile', data], stdout=PIPE, stderr=PIPE)
        output, error = newsml2text.communicate()
    elif mode == 'data':
        newsml2text = Popen(['newsML2text'], stdin=PIPE, stdout=PIPE, stderr=PIPE)
        output, error = newsml2text.communicate(data.encode('utf-8'))
    if error is not None and error != "":
        return None, error
    try:
        output_dom = parseString(output)
    except Exception as the_exception:
        return None, str(type(the_exception))+"("+" ".join(the_exception.args)+")"
    return output_dom, None

def getSxpipeNerDags(newsxml_dom):
    news_text = newsxml_dom.toxml()
    # printerr('ANNOTATE INPUT WITH SXPIPE NER TO GET sxpipe_ner_dags')
    format_text = Popen(['xml2sxpipe'], stdin=PIPE, stdout=PIPE)
    formatted_text = format_text.communicate(news_text.encode('utf-8'))[0]
    sxpipe = Popen(['sxpipe-afp-nomos'], stdin=PIPE, stdout=PIPE)    
    sxpipe_ner_dags = sxpipe.communicate(formatted_text)[0]
    return sxpipe_ner_dags

def sxpipeNerDags2xml(sxpipe_ner_dags):
    # printerr('CONVERT AMBIGUOUS sxpipe_ner_dags TO XML WITH PDAG AMBIGUITIES')
    print >> sys.stderr, "build 130"
    dag2pdag = Popen(['dag2pdag'], stdin=PIPE, stdout=PIPE)
    sxpipe_ner_pdags = dag2pdag.communicate(sxpipe_ner_dags)[0]
    print >> sys.stderr, "build 133"
    # sxpipe2xml = Popen(['sxpipe2xml', '--pdag', '--t', '--form', '--sent'], stdin=PIPE, stdout=PIPE)
    sxpipe2xml = Popen(['sxpipe2xml', '--pdag', '--t', '--sent'], stdin=PIPE, stdout=PIPE)
    output = sxpipe2xml.communicate(sxpipe_ner_pdags)[0]
    # printerr('PARSE XML output')
    sxpipe_ner_dom = parseString(output)
    return sxpipe_ner_dom

def getSxpipeNerNormalizationDom(sxpipe_ner_dags, resolution=False):
    # printerr('DISAMBIGUATE sxpipe_ner_dags FOR ANALYTICS PROCESSING') # first normalizer, then shortest_path # or to output resolution by Normalizer if sepcified in options
    # CALL NP-NORMALIZER
    np_normalizer = Popen(['@sxpipebin@/np_normalizer.pl', '-d', '@aledalibdir@', '-l', 'fr'], stdin=PIPE, stdout=PIPE)
    sxpipe_ner_dags_normalized = np_normalizer.communicate(sxpipe_ner_dags)[0]
    # CALL SHORTEST PATH
    disamb = Popen(['shortest_path'], stdin=PIPE, stdout=PIPE)
    sxpipe_ner_dags_normalized_disamb = disamb.communicate(sxpipe_ner_dags_normalized)[0]
    # CALL CLEANER
    clean = Popen(['@sxpipebin@/comments_cleaner.pl'], stdin=PIPE, stdout=PIPE)
    clean_sxpipe_ner_dags_normalized_disamb = clean.communicate(sxpipe_ner_dags_normalized_disamb)[0]
    # CONVERT TO XML 
    # sxpipe2xml = Popen(['sxpipe2xml', '--t', '--form', '--sent'], stdin=PIPE, stdout=PIPE)
    sxpipe2xml = Popen(['sxpipe2xml', '--t', '--sent'], stdin=PIPE, stdout=PIPE)
    sxpipe_ner_normalized_xml = sxpipe2xml.communicate(clean_sxpipe_ner_dags_normalized_disamb)[0]
    sxpipe_ner_normalized_dom = parseString(sxpipe_ner_normalized_xml)
    if resolution:
        local_eid = 0
        for enamex in sxpipe_ner_normalized_dom.getElementsByTagName('ENAMEX'):
            local_eid += 1
            addAttribute2Element(sxpipe_ner_normalized_dom, enamex, 'discarded', 'false')
            addAttribute2Element(sxpipe_ner_normalized_dom, enamex, 'valid', 'true')
            addAttribute2Element(sxpipe_ner_normalized_dom, enamex, 'rank', '0')
            addAttribute2Element(sxpipe_ner_normalized_dom, enamex, 'resolution_id', enamex.getAttribute('eid'))
            if enamex.getAttribute('eid') == "null":
                addAttribute2Element(sxpipe_ner_normalized_dom, enamex, 'cand_type', 'NIL')
                addAttribute2Element(sxpipe_ner_normalized_dom, enamex, 'candidate_type', 'null')
            else:
                addAttribute2Element(sxpipe_ner_normalized_dom, enamex, 'cand_type', 'ALEDA')
                addAttribute2Element(sxpipe_ner_normalized_dom, enamex, 'candidate_type', enamex.getAttribute('type'))
            addAttribute2Element(sxpipe_ner_normalized_dom, enamex, 'path_score', '1')
            addAttribute2Element(sxpipe_ner_normalized_dom, enamex, 'types', enamex.getAttribute('type'))
            # addAttribute2Element(sxpipe_ner_normalized_dom, enamex, 'candidate_type', enamex.getAttribute('type'))
            addAttribute2Element(sxpipe_ner_normalized_dom, enamex, 'local_eid', str(local_eid))
    return sxpipe_ner_normalized_dom

def getSxpipeDisambXml(sxpipe_ner_normalized_dom):
    # MODIFY DOM TO SET ATTRIBUTES OF ENAMEX AS EXPECTED IN RESOLUTION SCRIPTS
    local_eid = 0
    for e in sxpipe_ner_normalized_dom.getElementsByTagName('ENAMEX') :
        for attr in ['eid', 'known', 'name']:
            e.removeAttribute(attr)
        local_eid += 1
        addAttribute2Element(sxpipe_ner_normalized_dom, e, 'local_eid', str(local_eid))
    return orderTokens(sxpipe_ner_normalized_dom)

def getSxpipeResolution(sxpipe_ner_dags):
    # printerr('Building RESOLUTION module with np_normalizer (SxPipe)")
    sxpipe_ner_normalized_dom = orderTokens(getSxpipeNerNormalizationDom(sxpipe_ner_dags, True))
    xml = '<module type="resolution" id="np_normalizer">\n<news_content>\n'
    xml += sxpipe_ner_normalized_dom.getElementsByTagName('news_head')[0].toxml()
    xml += sxpipe_ner_normalized_dom.getElementsByTagName('news_text')[0].toxml() + '\n</news_content>\n'
    xml += '</module>\n'
    return xml

def postProcessSxpipeNerDagsXml(sxpipe_ner_dom):
    return buildAlts(orderTokens(sxpipe_ner_dom))
    
def orderTokens(sxpipe_ner_dom):
    # PUT BACK NICE AND ORDERED TOKEN IDS
    seen_tokens = {}
    for token in sxpipe_ner_dom.getElementsByTagName('token'):
        token_id = token.getAttribute('id')
        if not seen_tokens.has_key(token_id):
            token_count = len(seen_tokens.keys())+1
            seen_tokens[token_id] = token_count
        else:
            token_count = seen_tokens[token_id]
        token.setAttribute('id', 'T'+str(token_count))
    return sxpipe_ner_dom

def buildAlts(sxpipe_ner_dom):
    # BUILD ALTS FROM PDAGS
    for alt in sxpipe_ner_dom.getElementsByTagName('ALT'):
        choice_id = 0
        for choice in alt.getElementsByTagName('CHOICE'):
            if choice.getAttribute('type') == 'ENAMEX':
                # REPLACE PERSON MENTIONS WHERE TOKEN "M." INSIDE ENAMEX WITH FORM "M." + ENAMEX (will be removed as a double mention)
                # <CHOICE id="70" type="ENAMEX"><ENAMEX local_eid="70" type="Person"><form value="M._Valls"><token id="T244">M.</token><token id="T245">Valls</token></form></ENAMEX></CHOICE>
                # <CHOICE id="71" type="ENAMEX"><form value="M."><token id="T244">M.</token></form><ENAMEX gender="f" local_eid="71" type="Person"><form value="Valls"><token id="T245">Valls</token></form></ENAMEX></CHOICE>
                for enamex in choice.getElementsByTagName('ENAMEX'):
                    # for form_node in enamex.getElementsByTagName('form'):
                        # tokens = form_node.getElementsByTagName('token')
                        tokens = enamex.getElementsByTagName('token')
                        tokens_content = map((lambda token: getNodeData(token)), tokens)
                        if re.match('^M\._?', tokens_content[0]):
                            # enamex.removeChild(form_node)
                            enamex.removeChild(tokens[0])
                            # outside_form_node = createElementNode(sxpipe_ner_dom, 'form', {'value':tokens_content[0]})
                            # outside_form_node.appendChild(tokens[0])
                            # choice.insertBefore(outside_form_node, enamex)
                            # inside_form_node = createElementNode(sxpipe_ner_dom, 'form', {'value':'_'.join(tokens_content[1:len(tokens_content)])})
                            # for token_node in tokens[1:len(tokens)]:
                            #     inside_form_node.appendChild(token_node)
                            # enamex.appendChild(inside_form_node)
                            # form_node.unlink()
                            outside_token_node = createElementNode(sxpipe_ner_dom, 'token', {'id':tokens[0].getAttribute('id')})
                            choice.insertBefore(outside_token_node, enamex)

                # choice_id = '_'.join( map( (lambda enamex: enamex.getAttribute('local_eid')), choice.getElementsByTagName('ENAMEX') ) )
                choice_id += 1
                addAttribute2Element(sxpipe_ner_dom, choice, 'id', str(choice_id))
    return sxpipe_ner_dom

def buildNer(sxpipe_ner_dom):
    # printerr('Building NER module")
    xml = '<module type="ner" id="sxpipener">\n<news_content>\n'
    xml += sxpipe_ner_dom.getElementsByTagName('news_head')[0].toxml()
    xml += sxpipe_ner_dom.getElementsByTagName('news_text')[0].toxml() + '\n</news_content>\n'    
    xml += '</module>\n'
    return xml

def getNewsAnalytics(newsxml_dom, tokens2frequency_dict):
    metadata = {}
    for x in ['news_id', 'date', 'time', 'dateline', 'headline', 'lang', 'genre', 'subject_codes', 'location', 'keywords']:
        metadata[x] = None
    # WARNING: TYPE OF DICT VALUE IS DIFFERENT FOR EACH KEY
    # DON'T ITERATE AS HASH OF HASH...
    metadata['subjects'] = {}
    metadata['location'] = {}
    metadata['keywords'] = []
    ##########################################################################
    # elif options.file_type == 'newscorpus':
    metadata['news_id'] = newsxml_dom.getElementsByTagName('news_item')[0].getAttribute('ref')
    metadata['date'] = newsxml_dom.getElementsByTagName('meta')[0].getAttribute('date')
    metadata['time'] = newsxml_dom.getElementsByTagName('meta')[0].getAttribute('time')
    metadata['lang'] = newsxml_dom.getElementsByTagName('meta')[0].getAttribute('lang')
    # TODO: USE DATELINE LOCATION?
    # TODO: CONVERT AFP LOCS TO ALEDA IDS
    metadata['country'] = newsxml_dom.getElementsByTagName('meta')[0].getAttribute('country')
    metadata['city'] = newsxml_dom.getElementsByTagName('meta')[0].getAttribute('city')
    metadata['dateline'] = newsxml_dom.getElementsByTagName('news_head')[0].getAttribute('dateline')
    metadata['headline'] = newsxml_dom.getElementsByTagName('news_head')[0].getAttribute('headline')
    metadata['genre'] = newsxml_dom.getElementsByTagName('news_item')[0].getAttribute('genre')
    metadata['keywords'] = newsxml_dom.getElementsByTagName('news_slugs')[0].getAttribute('text').split('-') #<news_slugs text="Santé-maladies-prévention"/>
    for subject in map((lambda node: node.getAttribute('value')), newsxml_dom.getElementsByTagName('iptc_code')):
        metadata['subjects'][subject] = True
    # metadata['swords'] = getSwords(tokens2frequency_dict)
    if options.noswords:
        metadata['swords'] = []
    else:
        metadata['swords'] = computeSalience(tokens2frequency_dict, total_words_population, words_connection)
    analytics_xml = analyticsToXml(metadata)
    return analytics_xml

def analyticsToXml(metadata):
    # printerr('Building Analytics module')
    xml = '<module type="news_analytics">\n'
    xml += '<meta doc_id="'+metadata['news_id']+'" lang="'+metadata['lang']+'" country="'+metadata['country']+'" city="'+metadata['city']+'"/>\n'
    xml += '<category>\n'
    ######################################################
    for subject in metadata['subjects']:
        xml += '<iptc value="'+subject+'"/>\n'
    # TODO
    xml += '<domain/>\n'
    ######################################################
    for keyword in metadata['keywords']:
        xml += '<keyword value="'+keyword+'"/>\n'
    xml += '</category>\n'
    xml += '<context>\n'
    try: xml += '<spatial_context country="'+metadata['location']['country']+'" city="'+metadata['location']['city']+'"'
    except KeyError: xml += '<spatial_context'# country="'+metadata['location']['country']+'" city="'+metadata['location']['city']+'"'
    if metadata['location'].has_key('area'): xml += ' area="'+metadata['location']['area']+'"'
    xml += '/>\n'
    xml += '<temporal_context date="'+metadata['date']+'" time="'+metadata['time']+'"/>\n'
    xml += '</context>\n'
    xml += '<salient_words>\n'
    i = 0
    # threshold = 1.2
    if metadata['swords'] != []:
        for (word, z) in metadata['swords'][0:21]:
            # if z >= threshold:
            i += 1
            xml += '<salient_word value="'+word+'" rank="'+str(i)+'" z="'+str(z)+'"/>\n'
    xml += '</salient_words>\n'
    xml += '</module>\n'
    return xml


def validPara(para):
    signature_pattern = re.compile("^\s*[a-zé&A-Z]{2,4}((-|/)[a-zé&A-Z]{2,4}/?)+( +[a-z\.]{1,7})?\s*$")
    if signature_pattern.search(para):
        # print >> sys.stderr, 'Delete signature %s'%(para)
        return False
    return True


def convertIptcCode(code):
    pattern = re.compile('^(\d\d)\d*$')
    match = pattern.match(code)
    if match != None:
        code = match.group(1)
    else:
        return None
    code2subject = {'01':'CLT', '02':'CLJ', '03':'DIS', '04':'ECO', '05':'EDU', '06':'ENV', '07':'HTH', '08':'HUM', '09':'SOC', '10':'LIF', '11':'POL', '12':'REL', '13':'SCI', '14':'SOI', '15':'SPO', '16':'UNR', '17':'WEA', '20':'NULL'}
    subject2code = {'CLT':'01', 'CLJ':'02', 'DIS':'03', 'ECO':'04', 'EDU':'05', 'ENV':'06', 'HTH':'07', 'HUM':'08', 'SOC':'09', 'LIF':'10', 'POL':'11', 'REL':'12', 'SCI':'13', 'SOI':'14', 'SPO':'15', 'UNR':'16', 'WEA':'17', 'NULL':'20'}
    if code in code2subject:
        return code2subject[code]
    else:
        return None

def getTokens(dom):
    # HANDLE AMBIGUOUS DAGS => DONT COUNT SEVERAL TIMES SAME TOKEN
    unique_id_tokens = dict(map(lambda t:tuple([t.getAttribute('id'), getNodeData(t)]), dom.getElementsByTagName('token'))).values()
    tokens = selectTokens(unique_id_tokens)
    tokens2frequency_dict = defaultdict(int)
    for t in tokens:
        tokens2frequency_dict[t] += 1
    return tokens2frequency_dict

def main():
    sys.stdin = codecs.getreader("utf-8")(sys.stdin)
    sys.stdout = codecs.getwriter('utf8')(sys.stdout)
    global options
    options, args = getOptions()
    global total_words_population; total_words_population = options.total_words_population
    # MODIFY WORDS DB AND CONNECTION
    global words_connection
    words_connection = DBConnection(options.words, getWordsDBtables())
    # HANDLE OUTPUT
    if options.outputfile == None:
        G = sys.stdout # AFP
    else:
        G = codecs.open(options.outputfile, 'w', 'utf-8')
    # DATA = ARG OR STREAM
    try:
        arg = args[0]
    except IndexError:
        data = sys.stdin.readlines()
        arg = None # AFP
    # BUILD OR GET NEWS-XML DOM (EQUIVALENT TO LIGHT NEWSML FORMAT)
    if options.file_type == 'newsml':
        if options.format == 'newsml': # AFP
            if arg is not None:
                newsxml_dom, error = initXml(arg, 'file')
                if error is not None and error != "":
                    sys.exit("buildNomos: Building Nomos document for %s has failed: %s\n"%(arg, error))
            else:
                print >> sys.stderr, "build 370"
                newsxml_dom, error = initXml("".join(data), 'data')
                if error is not None and error != "":
                    sys.exit("buildNomos: Building Nomos document for STDIN input has failed: %s\n"%(error))
                print >> sys.stderr, "build 374"
        elif options.format == 'light': # AFP
            if arg is not None:
                try:
                    newsxml_dom = parse(arg)
                except Exception as the_exception:
                    sys.exit("buildNomos: Building Nomos document for %s has failed: %s (%s)\n"%(arg, str(type(the_exception)), " ".join(the_exception.args)))
            else:
                try:
                    newsxml_dom = parseString("".join(data).encode('utf-8'))
                except Exception as the_exception:
                    sys.exit("buildNomos: Building Nomos document for STDIN input has failed: %s (%s)\n"%(str(type(the_exception)), " ".join(the_exception.args)))
        #  DO ALL AND WRITE OUTPUT  # AFP
        # if options.outputfile != None:
        G.write(doAll(newsxml_dom).decode('utf-8'))
    # END
    # THINGS ARE DONE HERE IF DATA PROVIDED IS A BIG FILE OF MANY NEWS IN LIGHT FORMAT (INCOMPATIBLE WITH PREVIOUS BLOCKS)
    # ONLY FOR WORK ON NOMOS DEVELOPMENT
    # DELETE WHEN RELEASED FOR REAL USE
    if options.file_type == 'newscorpus':
        if arg is not None:
            F = codecs.open(arg, 'r', 'utf-8')# ; print >> sys.stderr, "Argument newscorpus: %s"%(arg)
        else:
            F = data
        string_to_parse = ""; in_news = False
        cpt = 0
        G.write('<documents>\n')
        for line in F:
            if re.match('^\s*<news_item ', line):
                in_news = True; string_to_parse += line
            elif in_news and re.match('^\s*</news_item', line):
                in_news = False; string_to_parse += line
                newsxml_dom = parseString(string_to_parse.encode('utf-8'))
                if options.goldner or options.lianener or options.mergener:
                    print >> sys.stderr, "Getting paras..."
                    print >> sys.stderr, "Done."
                    ref = newsxml_dom.getElementsByTagName('news_item')[0].getAttribute('ref')
                    # DIRTY SWITCH FOR DIFFERENT NOMOS BUILDINGS AND NER CONFIGS
                    # gold_dom = getOtherNerParas(options.goldner, ref) # options.goldner is dir!!
                    other_dom = getOtherNerParas(options.lianener, ref) # options.goldner is dir!!
                    # if gold_dom is None:
                    if other_dom is None:
                        print "%s has been removed"%(ref)
                        string_to_parse = ""
                        continue
                    # G.write(doAll(newsxml_dom, gold_dom).decode('utf-8'))
                    G.write(doAll(newsxml_dom, other_dom).decode('utf-8'))
                else:
                    G.write(doAll(newsxml_dom).decode('utf-8'))
                # G.write('\n</document>\n')
                G.write('\n')
                string_to_parse = ""; cpt += 1
                message = "%d documents converted"%(cpt)
                sys.stderr.write(message + '\b'*(len(message)))
            elif in_news:
                string_to_parse += line
        G.write('\n</documents>\n')
        try: F.close()
        except: pass
    # END OF BLOCK FOR DEVELOPMENT WORK
    if options.outputfile != None: G.close()
    words_connection.close()

def getOtherNerParas(otherParasDir, ref):
    otherparas = {}
    print "Looking for %s"%(ref)
    for root, dirs, files in os.walk(otherParasDir):
        for f in files:
            if re.search('afp', f): fref = convertAfpFileName(f)
            else: fref = re.sub('\.[a-z1]+\.xml$', '', f)
            if fref == ref:
                dom = parse(os.path.join(root, f))
                print "DOM built for %s"%(f)
                return dom
    print "DOM not built for %s"%(ref)

def convertAfpFileName(f):
    fpattern = re.compile('^afp\.com-(\d{8}T\d{6}Z)TX-PAR-([A-Z]{3}\d{2})')
    datetime, refid = fpattern.search(f).groups()
    return "urn:newsml:afp.com:"+datetime+":TX-PAR-"+refid+":1"
    
def getOptions():
    usage = '%s [options] <news_item>\n'%sys.argv[0]
    parser = OptionParser(usage)
    file_types = ['newsml', 'newscorpus']
    formats = ['newsml', 'light']
    parser.add_option('--file', '-f', action='store', dest='file_type', type="choice", choices=file_types, default='newsml', help='Input type: newsml or newscorpus (many light docs) (default: newsml)')
    parser.add_option('--format', action='store', dest='format', type="choice", choices=formats, default='newsml', help='Input format type: newsml or light (default: newsml)')
    parser.add_option('--sxpipener', action='store_true', dest='sxpipener', default=True, help='Build SxPipe NER module (default True)')
    parser.add_option('--nosxpipener', action='store_false', dest='sxpipener', help='Dont build SxPipe NER module (default False)')
    parser.add_option('--goldner', action='store', dest='goldner', default=None, help='Gold NER file with root <paras> (or dir of such files if newscorpus file_type)')
    parser.add_option('--lianener', action='store', dest='lianener', default=None, help='Liane NER file with root <paras> (or dir of such files if newscorpus file_type)')
    parser.add_option('--disamb', action='store_true', dest='disamb', default=False, help='True: disambiguate NER (default False)')
    # IF MERGE NER: ARG IS FILE WITH MERGED SXPIPE AND LIANE <NES> (DISAMB OR NOT IS NO BUSINESS OF THIS SCRIPT)
    parser.add_option('--mergener', action='store', dest='mergener', default=None, help='Merge NER file with root <paras> (or dir of such files if newscorpus file_type)')
    parser.add_option('--nerid', action='store', dest='nerid', default='sxpipe_ner', help='NER id for NER module')
    # parser.add_option('--intersect', action='store_true', dest='intersect', default=False, help='True: Sxpipe and Liane NER intersection (False: union)')
    # parser.add_option('--multiplener', action='store_true', dest='multiplener', default=False, help='True: Output several NER modules')
    parser.add_option('--words', action='store', dest='words', default="__prefix__/share/nomos/frwikiwords.dat", help='<path to words data base> (default __prefix__/share/nomos/frwikiwords.dat)')
    parser.add_option('--norm', action='store_true', dest='normalize', default=False, help='Activate entity resolution by np_normalizer (SxPipe module) (default False)')
    # parser.add_option('--totalwords', action='store', dest='total_words_population', type='int', default='232083620', help='<total words population - default 232,083,620>')
    parser.add_option('--totalwords', action='store', dest='total_words_population', type='int', default='175401109', help='<total words population - default 175,401,109>')
    parser.add_option('--noswords', action='store_true', dest='noswords', default=False, help='deactivate salient words (default: False)')
    parser.add_option('--output', '-o', action='store', dest='outputfile', default=None, help='<outputfile name>')
    try: (options, args) = parser.parse_args()
    except OptionError: sys.stderr.write('OptionError'); parser.print_help(); sys.exit()
    except: sys.stderr.write('An error occured during options parsing\n'); parser.print_help(); sys.exit()
    if options.normalize and not options.sxpipener: print >> sys.stderr, "Incompatible options: "; parser.print_help(); sys.exit()
    if options.disamb and not options.sxpipener: print >> sys.stderr, "Incompatible options"; parser.print_help(); sys.exit()
    return options, args

if __name__ == '__main__':
    print >> sys.stderr, "build: main"
    main()





