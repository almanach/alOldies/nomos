<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="xml"/>
    <!-- INPUT: GFTB CORPUS XML (OBTAINED FROM FTB DOCS WITH XSL ftb62gftbCorpus.xml)
        ======================================================
        <?xml version="1.0" encoding="UTF-8"?>
        <gold_corpus>
        <item ref="id248980">
        <para rank="1">
        Certes, rien ne dit qu'une seconde motion de censure sur son projet de loi, reprenant l'accord du 10 avril, n'aurait pas été la bonne mais cette probabilité, reconnaissent les socialistes, n'était pas la plus plausible. 
        </para>
        <para rank="2">
        Toujours est-il que le gouvernement a cédé alors que ses adversaires politiques ne proposent aucune solution alternative et considèrent, avec un bel ensemble, que la défense des intérêts des malades passe par le maintien d'un système qui aboutit à multiplier les actes inutiles et au dépassement généralisé des honoraires de la "<ENAMEX type="Organization" sub_type="InstitutionalOrganization" eid="null" name="Sécurité sociale en France" uri="null">Sécu</ENAMEX>". 
        </para>
        
        OUTPUT: FORMAT NEWSML2TEXT OUTPUT CORPUS
        ======================================================
        <?xml version="1.0" encoding="utf-8"?>
        <news>
        <news_item rank="1" ref="urn:newsml:afp.com:20090520T164214Z:TX-PAR-FVU98:1" genre="null">
        <meta lang="fr" date="20090520" time="230103" country="FRA" city="Paris"/>
        <news_head dateline="Paris, 20 mai 2009 (AFP) -" headline="Le pays organisateur des JO récompensé par plusieurs moissons de médailles (étude)"/>
        <news_slugs text="Science-sport-JO-2008-2012-GB-"/>
        <iptc_code value="SCI"/>
        <iptc_code value="SPO"/>
        <news_text>
        <para rank="1">
        Les pays hôtes des Jeux olympiques engrangent davantage de médailles non seulement l'année où ils organisent les compétitions, mais aussi aux JO qui précèdent et qui suivent, selon une étude publiée jeudi dans le British Journal of Sports Medicine.
        </para>
        <para rank="2">
        Le nombre record de médailles décrochées par les athlètes britanniques à Pékin en 2008 était ainsi prévisible, soulignent les chercheurs, optimistes sur une nouvelle moisson exceptionnelle à Londres en 2012.
        </para>
        ...
    -->
    <xsl:template match="gold_corpus">
        <news>
            <xsl:apply-templates select="./item"/>
        </news>
    </xsl:template>
    <xsl:template match="item">
        <news_item>
            <xsl:attribute name="ref"><xsl:value-of select="./@ref"/></xsl:attribute>
            <xsl:attribute name="rank"><xsl:value-of select="1+count(preceding-sibling::item)"/></xsl:attribute>
            <xsl:attribute name="genre">lemonde_article</xsl:attribute>
            <meta>
                <xsl:attribute name="lang">fr</xsl:attribute>
                <xsl:attribute name="date">null</xsl:attribute>
                <xsl:attribute name="country">null</xsl:attribute>
                <xsl:attribute name="city">null</xsl:attribute>
            </meta>
            <news_head>
                <xsl:attribute name="dateline">null</xsl:attribute>
                <xsl:attribute name="headline">null</xsl:attribute>
            </news_head>
            <news_slugs><xsl:attribute name="text">null</xsl:attribute></news_slugs>
            <iptc_code><xsl:attribute name="value">null</xsl:attribute></iptc_code>
            <news_text>
                <xsl:apply-templates select="./para"/>
            </news_text>
        </news_item>
    </xsl:template>
    <xsl:template match="item/para">
        <para>
            <xsl:attribute name="rank"><xsl:value-of select="./@rank"/></xsl:attribute>
            <xsl:apply-templates/>
        </para>
    </xsl:template>
</xsl:stylesheet>