#!/usr/bin/python
# -*- coding: utf-8 -*-
import re
import os
import sys
# sys.path.append('@common_src@')
sys.path.append('/usr/local/share/nomos/build/common_src/')
# sys.path.append('@kb_src@')
from utils import *
# from kbwp_utils import *
from collections import defaultdict
# from subprocess import *
# from optparse import *
import codecs
from xml.dom.minidom import *


def main(corpus):
    sys.stdout = codecs.getwriter('utf8')(sys.stdout)
    sys.stdin = codecs.getreader("utf-8")(sys.stdin)
    dom = parse(corpus)

    itemsdata = defaultdict(dict)
    items = dom.getElementsByTagName('item')
    noenamex = 0
    if items == []:
        items = dom.getElementsByTagName('document')
    # items.extend(dom.getElementsByTagName('news_item'))
    l_paras = 0; minparas = 100; maxparas = 0
    minenamex = 100; minenamex2 = 100; maxenamex = 0
    minentities = 100; minentities2 = 100; maxentities = 0
    for item in items:
        paras = filter(lambda p: p.getAttribute('signature') == "", item.getElementsByTagName('para'))
        if paras == []:
            paras = item.getElementsByTagName('sentence')
        l_paras += len(paras)
        if len(paras) < minparas: minparas = len(paras)
        if len(paras) > maxparas: maxparas = len(paras)
        itemsdata[item.getAttribute('rank')]['paras'] = len(paras)

        eids = map(lambda e: getUniqueId(e), filter(lambda e: isPerLocOrg(e), item.getElementsByTagName('ENAMEX')))
        if eids == []: noenamex += 1
        itemsdata[item.getAttribute('rank')]['enamex'] = len(eids)
        if len(eids) < minenamex: minenamex = len(eids)
        if len(eids) < minenamex2 and len(eids) > 0: minenamex2 = len(eids)
        if len(eids) > maxenamex: maxenamex = len(eids)        
        itemsdata[item.getAttribute('rank')]['entities'] = len(set(eids))
        if len(eids) < minentities: minentities = len(set(eids))
        if len(eids) < minentities2 and len(eids) > 0: minentities2 = len(set(eids))
        if len(eids) > maxentities: maxentities = len(set(eids))
        itemsdata[item.getAttribute('rank')]['entitieskno'] = len(set(filter(lambda e: e != "null", eids)))
        itemsdata[item.getAttribute('rank')]['entitiesunk'] = len(set(filter(lambda e: e == "null", eids)))

    print >> sys.stderr, "NEWS"
    print >> sys.stderr, "News items\tTotal: %d"%(len(items))
    if l_paras != 0: print >> sys.stderr, "Paras\tTotal: %d\tAverage #paras/item: %d\tMin #paras/item: %d\tMax #paras/item: %d"%(l_paras, int(l_paras/len(items)), minparas, maxparas)
    print >> sys.stderr, "ENTITES"
    enamex = filter(lambda e: isPerLocOrg(e), dom.getElementsByTagName('ENAMEX'))
    uniqueenamex = set(map(lambda e: getNodeData(e), enamex))
    uniqueenamexkno = set(map(lambda e: getNodeData(e), filter(lambda e: isKnown(e), enamex)))
    uniqueenamexunk = set(map(lambda e: getNodeData(e), filter(lambda e: not isKnown(e), enamex)))
    print >> sys.stderr, "ENAMEX total: %d\tKnown: %d\tUnknown: %d"%(len(enamex), len(filter(lambda e: isKnown(e), enamex)), len(filter(lambda e: not isKnown(e), enamex)))
    print >> sys.stderr, "ENAMEX Per: %d\tLoc: %d\tOrg: %d\tOther: %d"%(len(filter(lambda e: isPer(e), enamex)), len(filter(lambda e: isLoc(e), enamex)), len(filter(lambda e: isOrg(e), enamex)), len(filter(lambda e: isMisc(e), enamex)))
    print >> sys.stderr, "ENAMEX KNOWN Per: %d\tLoc: %d\tOrg: %d\tOther: %d"%(len(filter(lambda e: isPer(e) and isKnown(e), enamex)), len(filter(lambda e: isLoc(e) and isKnown(e), enamex)), len(filter(lambda e: isOrg(e) and isKnown(e), enamex)), len(filter(lambda e: isMisc(e) and isKnown(e), enamex)))
    print >> sys.stderr, "ENAMEX UNKNOWN Per: %d\tLoc: %d\tOrg: %d\tOther: %d"%(len(filter(lambda e: isPer(e) and not isKnown(e), enamex)), len(filter(lambda e: isLoc(e) and not isKnown(e), enamex)), len(filter(lambda e: isOrg(e) and not isKnown(e), enamex)), len(filter(lambda e: isMisc(e) and not isKnown(e), enamex)))
    print >> sys.stderr, "ENAMEX UNIQUE total: %d\tKnown: %d\tUnknown: %d"%(len(uniqueenamex), len(uniqueenamexkno), len(uniqueenamexunk))
    uniquekno = set(map(lambda e: e.getAttribute('eid'), filter(lambda e: isKnown(e), enamex)))
    uniqueunk = set(map(lambda e: e.getAttribute('name'), filter(lambda e: not isKnown(e), enamex)))
    print >> sys.stderr, "len uniquekno: %d"%(len(uniquekno))
    
    # uniqueper = set(map(lambda e: e.getAttribute('eid'), filter(lambda e: isPer(e), enamex)))
    # uniqueloc = set(map(lambda e: e.getAttribute('eid'), filter(lambda e: isLoc(e), enamex)))
    # uniqueorg = set(map(lambda e: e.getAttribute('eid'), filter(lambda e: isOrg(e), enamex)))

    uniqueperkno = set(map(lambda e: e.getAttribute('eid'), filter(lambda e: isPer(e) and isKnown(e), enamex)))
    uniquelockno = set(map(lambda e: e.getAttribute('eid'), filter(lambda e: isLoc(e) and isKnown(e), enamex)))
    uniqueorgkno = set(map(lambda e: e.getAttribute('eid'), filter(lambda e: isOrg(e) and isKnown(e), enamex)))

    # print >> sys.stderr, "inter(per-kno, loc-kno)"
    # print >> sys.stderr, uniqueperkno & uniquelockno
    # print >> sys.stderr, "inter(per-kno, org-kno)"
    # print >> sys.stderr, uniqueperkno & uniqueorgkno
    # print >> sys.stderr, "inter(org-kno, loc-kno)"
    # print >> sys.stderr, uniqueorgkno & uniquelockno

    uniqueperunk = set(map(lambda e: e.getAttribute('name'), filter(lambda e: isPer(e) and not isKnown(e), enamex)))
    uniquelocunk = set(map(lambda e: e.getAttribute('name'), filter(lambda e: isLoc(e) and not isKnown(e), enamex)))
    uniqueorgunk = set(map(lambda e: e.getAttribute('name'), filter(lambda e: isOrg(e) and not isKnown(e), enamex)))

    uniquemisc = set(map(lambda e: e.getAttribute('eid'), filter(lambda e: isMisc(e), enamex)))

    print >> sys.stderr, "EntitiesUnique total: %d\tKnown: %d\tUnknown: %d"%(len(uniquekno)+len(uniqueunk), len(uniquekno), len(uniqueunk))
    # print >> sys.stderr, "EntitiesUnique Per: %d\tLoc: %d\tOrg: %d\tOther: %d"%(len(uniqueper), len(uniqueloc), len(uniqueorg), len(uniquemisc))
    print >> sys.stderr, "EntitiesUnique KNOWN Per: %d\tLoc: %d\tOrg: %d (total %d)"%(len(uniqueperkno), len(uniquelockno), len(uniqueorgkno), len(uniqueperkno)+len(uniquelockno)+len(uniqueorgkno))
    print >> sys.stderr, "EntitiesUnique UNKNOWN Per: %d\tLoc: %d\tOrg: %d (total %d)"%(len(uniqueperunk), len(uniquelocunk), len(uniqueorgunk), len(uniqueperunk)+len(uniquelocunk)+len(uniqueorgunk))
    
    print >> sys.stderr, "ENTITIES / NEWS"
    print >> sys.stderr, "ENAMEX/news Average: %d\tMin: %d / Min2: %d\t\tMax: %d"%(int(len(enamex)/(len(items)-noenamex)), minenamex, minenamex2, maxenamex)
    print >> sys.stderr, "EntitiesUnique/news Average: %d\tMin: %d / Min2: %d\t\tMax: %d"%(int((len(uniquekno)+len(uniqueunk))/(len(items)-noenamex)), minentities, minentities2, maxentities)
    print >> sys.stderr, "ENAMEX/news Known Average: %d"%(int(len(filter(lambda e: isKnown(e), enamex))/(len(items)-noenamex)))
    print >> sys.stderr, "ENAMEX/news Unknown Average: %d"%(int(len(filter(lambda e: not isKnown(e), enamex))/(len(items)-noenamex)))
    print >> sys.stderr, "EntitiesUnique/news Known Average: %d"%(int(len(uniquekno)/(len(items)-noenamex)))
    print >> sys.stderr, "EntitiesUnique/news Unknown Average: %d"%(int(len(uniqueunk)/(len(items)-noenamex)))

    print >> sys.stderr, "Items wo/ mentions: %d"%(noenamex)

    # print >> sys.stderr, "\n".join(map(lambda e: e.toxml(), filter(lambda e: isMisc(e), enamex)))

    refsdb = DBConnection('/usr/local/share/aleda/ne_refs.fr.dat', ['data'])
    varsdb = DBConnection('/usr/local/share/aleda/ne_vars.fr.dat', ['data'])
    refsdbc = refsdb.cursor
    varsdbc = varsdb.cursor

    entities = defaultdict(int)
    datarec = {}
    mincands = 100000; maxcands = 0; candscount = []
    mincand = ""; maxcand = ""
    for kno in filter(lambda e: isKnown(e), enamex):
        data = getNodeData(kno)
        if data not in datarec:
            varsdb.tables['data'].selectFromTable(varsdbc, ['key'], [('variant', '=', data)])
            res = varsdbc.fetchall()
            if res == None or res == []:
                l = 0
            else:
                l = len(res)
            if l < mincands: mincands = l; mincand = data
            if l > maxcands: maxcands = l; maxcand = data
            candscount.append(l)
            datarec[data] = True
        eid = kno.getAttribute('eid')
        entities[eid] += 1

    nilamb = {}; nilnotamb = 0
    for unk in filter(lambda e: not isKnown(e), enamex):
        data = getNodeData(unk)
        if data not in datarec:
            varsdb.tables['data'].selectFromTable(varsdbc, ['key'], [('variant', '=', data)])
            res = varsdbc.fetchall()
            if res == None or res == []:
                l = 0
                nilnotamb += 1
            else:
                l = len(res)
                nilamb[data] = l
            if l < mincands: mincands = l; mincand = data
            if l > maxcands: maxcands = l; maxcand = data
            candscount.append(l)
            datarec[data] = True

    print >> sys.stderr, "Mentions distinct - #candidates: Min: %d (%s)\tMax: %d (%s)\tAverage: %d"%(mincands, mincand, maxcands, maxcand, int(sum(candscount)/len(candscount)))
    print >> sys.stderr, "\tAt least 1 candidate: %d / %d"%(len(filter(lambda x: x>0, candscount)), len(candscount))
    print >> sys.stderr, "\t>1 candidate: %d"%(len(filter(lambda x: x>1, candscount)))
    print >> sys.stderr, "\t1 candidate: %d"%(len(filter(lambda x: x==1, candscount)))
    print >> sys.stderr, "\t0 candidate: %d"%(len(filter(lambda x: x==0, candscount)))
    print >> sys.stderr, ">>Mentions distinct - #candidates: Min: %d (%s)\tMax: %d (%s)\tAverage wo/0: %d"%(mincands, mincand, maxcands, maxcand, int(sum(candscount)/len(filter(lambda x: x>0, candscount))))
    print >> sys.stderr, "Mentions distinct denoting NIL amb with Aleda cands: %d\tMin amb: %d (%d)\tMax amb: %d (%d)\tAverage amb: %d\two/0: %.4f\t>1amb: %d\n>>Not amb: %d"%(len(nilamb), min(nilamb.values()), len(filter(lambda x:nilamb[x]==min(nilamb.values()), nilamb)), max(nilamb.values()), len(filter(lambda x:nilamb[x]==max(nilamb.values()), nilamb)), sum(nilamb.values())/len(nilamb.values()), sum(map(lambda x:float(x), nilamb.values()))/(float(len(nilamb.values()))+float(nilnotamb)), len(filter(lambda x:nilamb[x]>1,nilamb)), nilnotamb)

    for eid in entities:
        refsdb.tables['data'].selectFromTable(refsdbc, ['type', 'name', 'link', 'weight'], [('key', '=', eid)])
        res = refsdbc.fetchone()
        if res == None or res == []:
            print >> sys.stderr, "WARNING: ALEDA ID %s IS NOT IN ALEDA"%(eid)
        else:
            t, name, link, w = res
        try: sys.stdout.write("|".join([str(entities[eid]), eid, name, link, t, str(w)])+"\n")
        except TypeError: sys.stdout.write("|".join([str(entities[eid]), eid, name, "nolink", t, str(w)])+"\n")

    refsdb.close()
    varsdb.close()



def isPerLocOrg(enamex):
    # t = enamex.getAttribute('type')
    # return t == "Person" or t == "Location" or t == "Organisation" or t == "Organization" or t == "Company"
    return isPer(enamex) or isLoc(enamex) or isOrg(enamex)

def isProduct(enamex):
    return enamex.getAttribute('type') == "Product"

def getUniqueId(enamex):
    eid = enamex.getAttribute('eid')
    if eid != "null": return eid
    else: return enamex.getAttribute('name')

def isKnown(enamex):
    return enamex.getAttribute('eid') != "null"

def isPer(enamex):
    return enamex.getAttribute('type') == "Person"

def isLoc(enamex):
    return enamex.getAttribute('type') == "Location"

def isOrg(enamex):
    return enamex.getAttribute('type') == "Organization" or enamex.getAttribute('type') == "Company" or enamex.getAttribute('type') == "Organisation"

# def getType(enamex):
#     return enamex.getAttribute('type')


def isMisc(enamex):
    # t = enamex.getAttribute('type')
    # return t != "Person" and t != "Organization" and t != "Location" and t != "Company" and t != "Product" and t != "Organisation"
    if isPerLocOrg(enamex): return False
    return True

if __name__ == "__main__":
    try: corpus = sys.argv[1]
    except IndexError: sys.exit("provide corpus as argument")
    if os.path.isfile(corpus):
        main(corpus)
    else:
        sys.exit('%s does not exist'%(corpus))
