#!/usr/bin/python
# -*- coding: utf-8 -*-
import codecs
import re
import os
import sys
from subprocess import *
from optparse import *
from shutil import copyfile
import glob
from xml.dom.minidom import *
sys.path.append('@common_src@')
from utils import *

def convertCorpus2news(file, outputdir, big): #dir/all
    total_items = 0
    print >> sys.stderr, "Parse gold..."
    if not big:
        all_dom = parse(file)
        items = all_dom.getElementsByTagName('item')
        total_items = len(items)
        print >> sys.stderr, "%d items"%(total_items)
        for news_item in items:
            ref = news_item.getAttribute('ref')
            if re.search('afp\.com', ref):
                ref = convertRef(ref)
            print >> sys.stderr, "Get text"
            text = '<?xml version="1.0" encoding="UTF-8"?>\n<paras>\n'
            for para in news_item.getElementsByTagName('para'):
                text += para.toxml()
            text += '\n</paras>\n'
            writeFile(text, ref, outputdir)
    else:
        refpattern = re.compile('ref="([^"]+)"')
        F = codecs.open(file, 'r', 'utf-8')
        initem = False
        string = ""
        for line in F:
            if re.search('<item', line):
                initem = True
                string = ""
                ref = refpattern.search(line).group(1)
                if re.search('afp\.com', ref):
                    ref = convertRef(ref)
            elif re.search('</item', line):
                initem = False
                string = '<?xml version="1.0" encoding="UTF-8"?>\n<paras>\n'+string+'\n</paras>'
                dom = parseString(string.encode('utf-8'))
                writeFile(dom.toxml(), ref, outputdir)
                total_items += 1
            elif initem and (re.search('</?para', line) or re.search('<token', line)):
                string += line
        F.close()
    return total_items

def writeFile(text, ref, outputdir):
    # PUT FILE IN dir/all/
    file_name = outputdir+"/"+ref+".gold.xml"
    F = codecs.open(file_name, "w", "utf-8")
    print >> sys.stderr, "Output file: %s"%(file_name)
    dom = parseString(text.encode('utf-8'))
    print >> sys.stderr, "Reset tokens"
    # RESET SENTENCES AND TOKEN COUNTS
    seen_tokens = {}
    for token in dom.getElementsByTagName('token'):
        token_id = token.getAttribute('id')
        if not seen_tokens.has_key(token_id):
            token_count = len(seen_tokens.keys())+1
            seen_tokens[token_id] = token_count
        else:
            token_count = seen_tokens[token_id]
        token.setAttribute('id', "T"+str(token_count))
    # ADD LOCAL EIDS TO ENAMEXES
    print >> sys.stderr, "Add entity local ids"
    local_eid = 0
    for enamex in dom.getElementsByTagName('ENAMEX'):
        local_eid += 1
        addAttribute2Element(dom, enamex, 'local_eid', str(local_eid))
    print >> sys.stderr, "Validate & format XML"
    xmllint = Popen(["xmllint", "--format", "--encode", "UTF-8", "-"], stdin=PIPE, stdout=PIPE)
    valid_output = xmllint.communicate(dom.toxml("utf-8"))[0]
    print >> sys.stderr, "Write output"
    F.write(valid_output.decode("utf-8"))
    F.close()
    dom.unlink()



def buildFolding(outputdir, total_items, fold=10.0):
    # train_nb = int(float(total_items) * (1.0-2.0/fold))
    train_nb = int(float(total_items) * (1.0-1.0/fold))
    devtest_nb = total_items - train_nb
    # dev_nb = devtest_nb / 2
    # print >> sys.stderr, "Total: %s\nTrain: %d, Dev/Test: %d (%d)\n"%(total_items, train_nb, devtest_nb, train_nb+devtest_nb)
    print >> sys.stderr, "Total: %s\nTrain: %d, Test: %d (%d)\n"%(total_items, train_nb, devtest_nb, train_nb+devtest_nb)    
    # news_table = []
    for root, dirs, files in os.walk(outputdir+'/all/'):
        valid_files = map((lambda f2: os.path.join(root, f2)), filter((lambda f1: not(re.match('^\.', f1))), files))
        train_files = []
        devtest_files = []
        for p in range(1,int(fold)+1):
            print >> sys.stderr, "Making %s..."%(outputdir+"/"+str(p))
            try: os.mkdir(outputdir+"/"+str(p))
            except OSError: print >> sys.stderr, "%s already exists"%(outputdir+"/"+str(p))
            # floor = (p-1)*dev_nb
            # ceil = (p-1)*dev_nb + devtest_nb
            floor = (p-1)*devtest_nb
            ceil = (p-1)*devtest_nb + devtest_nb
            if (ceil-1) > len(valid_files):
                rest = ceil-len(valid_files)
                devtest_files = valid_files[floor:len(valid_files)+1]
                devtest_files.extend(valid_files[0:rest])
            else:
                devtest_files = valid_files[floor:ceil]
            for f in filter(lambda f: f not in devtest_files, valid_files):
                train_files.append(f)
            # for x in ["train", "dev", "test"]:
            for x in ["train", "test"]:
                try: os.mkdir(outputdir+"/"+str(p)+"/"+x); print >> sys.stderr, "Making %s"%(outputdir+"/"+str(p)+"/"+x)
                except OSError, error: print >> sys.stderr, "%s not made: %s"%(outputdir+"/"+str(p)+"/"+x, error[1])
            for train_file in train_files:
                filename = os.path.basename(train_file)
                copyfile(train_file, outputdir+"/"+str(p)+"/train/"+filename)
            # for i in range(0,dev_nb):
            #     # for dev_file in dev_files:
            #     devtest_file = devtest_files[i]
            #     filename = os.path.basename(devtest_file)
            #     copyfile(devtest_file, outputdir+"/"+str(p)+"/dev/"+filename)
            for i in range(0, devtest_nb):
                # for test_file in test_files:
                devtest_file = devtest_files[i]
                filename = os.path.basename(devtest_file)
                copyfile(devtest_file, outputdir+"/"+str(p)+"/test/"+filename)
            train_files = []
            devtest_files = []



def convertRef(ref):
    temp1 = re.sub("^urn:newsml:", "", ref)
    temp2 = re.sub(":1$", "", temp1)
    temp3 = re.sub(":", "-", temp2)
    return re.sub("-TX", "TX", temp3)
    
def main():
    sys.stdin = codecs.getreader("utf-8")(sys.stdin)
    sys.stdout = codecs.getwriter('utf8')(sys.stdout)

    usage = '%s [options] <gold-corpus>'%sys.argv[0]
    parser = OptionParser(usage)
    parser.add_option("--dir", action="store", dest="dir", default=None, help="output directory for news")
    parser.add_option("--big", action="store_true", dest="big", default=False, help="input file is a big xml file")
    # parser.add_option("--conf", action="store", dest="conf", default=None, help="<sxpipe configuration file>")    
    parser.add_option("--fold", action="store", dest="fold", type="float", default="10.0", help="n fold cross (default: 10)")
    # parser.add_option("--", action="store", dest="", default="", help="")
    try: (options, args) = parser.parse_args()
    except OptionError: sys.stderr.write('OptionError'); parser.print_help(); sys.exit()
    except: sys.stderr.write('An error occured during options parsing\n'); parser.print_help(); sys.exit()
    if len(args) != 1: parser.print_help(); sys.exit()

    arg = args[0]

    try: os.mkdir(options.dir); print >> sys.stderr, "Making %s"%(options.dir)
    except OSError: print >> sys.stderr, "%s already exists"%(options.dir)

    try: os.mkdir(options.dir+"/all"); print >> sys.stderr, "Making %s"%(options.dir+"/all")
    except OSError: print >> sys.stderr, "%s already exists"%(options.dir+"/all")
    total_items = convertCorpus2news(arg, options.dir+"/all", options.big)
    buildFolding(options.dir, total_items, options.fold)
    
if __name__ == "__main__":
    main()




        # print >> sys.stderr, "Format"
        # format_text = Popen(["xml2sxpipe"], stdin=PIPE, stdout=PIPE)
        # formatted_text = format_text.communicate(text.encode('utf-8'))[0]
        # # # print formatted_text; stdin;
        # print >> sys.stderr, "Annotate"
        # # # sxpipe = Popen(["lingpipe", "--config", configfile, "--skip", "etr,np,np_normalizer"], stdin=PIPE, stdout=PIPE)
        # sxpipe = Popen(["sxpipe-afp", "-t"], stdin=PIPE, stdout=PIPE)
        # tempdags = sxpipe.communicate(formatted_text)[0]
        # rebuild = Popen(["__prefix__/share/sxpipe/tok2cc/rebuild_easy_tags.pl"], stdin=PIPE, stdout=PIPE)
        # dags = rebuild.communicate(tempdags)[0]
        #  # dags = sxpipe.communicate(formatted_text)[0]
        # # # desamb = Popen(["shortest_path"], stdin=PIPE, stdout=PIPE)
        # # # udags = desamb.communicate(dags)[0]
        # # # udags = sxpipe.communicate(formatted_text)[0]
        # # # print >> sys.stderr, udags; stdin;
        # print >> sys.stderr, "Convert to XML"
        # # # sxpipe2xml = Popen(["sxpipe2xml", "--t", "--form"], stdin=PIPE, stdout=PIPE)
        # sxpipe2xml = Popen(["sxpipe2xml", "--t"], stdin=PIPE, stdout=PIPE)
        # # # output = sxpipe2xml.communicate(udags)[0]
        # output = sxpipe2xml.communicate(dags)[0]
        # # # print >> sys.stderr, output; stdin;
        # # print >> sys.stderr, "Parse output"
        # output = re.sub('</?(TIMEX|NUMEX|SMILEY|EMAIL|ADDRESS|ADRESSE|TEL|URL|HEURE|EPSILON|META_TEXTUAL_PONCT|SENT_BOUND|ETR|SPECWORD|TMP_[^ ]+)[^<>]*>', '', text)
        # print >> sys.stderr, "Parse content"
        # output_dom = parseString(output)
