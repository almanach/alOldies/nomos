#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import re
from collections import defaultdict
from xml.dom.minidom import *
sys.path.append('@common_src@')
from utils import *

def main():
    gold = parse('/Users/triantafillo/dev/alpc/sxpipe/evalnp/DATA/goldCorpus.xml')
    connection = DBConnection('/usr/local/lib/aleda/ne_refs.fr.dat')
    c = connection.cursor
    connection.addTables(['data'])
    data = connection.tables['data']
    eid2nodes = defaultdict(list)
    for e in gold.getElementsByTagName('ENAMEX'):
        eid = e.getAttribute('eid')
        if eid != 'null':
            eid2nodes[eid].append(e)
        else:
            eid2nodes[e.getAttribute('name')].append(e)
    for eid, nodes in eid2nodes.items():
        entity = Entity(nodes)
        if not entity.nil:
            entity.checkInAleda(c, data)
            if not entity.inaleda:
                entity.retrieveByName(c, data)
        else:
            entity.checkName(c, data)
        #stdin()
    connection.close()
    gold.unlink()

class Entity:
    def __init__(self, nodes):
        node = nodes[0]
        self.occs = len(nodes)
        self.eid = node.getAttribute('eid')
        if self.eid == 'null':
            self.nil = True
        else:
            self.nil = False
            try: self.eid = int(self.eid)
            except: print >> sys.stderr, ">>"+self.eid+"<<"
        self.name = node.getAttribute('name')
        self.data = getNodeData(node)
        self.type = node.getAttribute('type')
        self.subtype = node.getAttribute('sub_type')
        self.gninfo = node.getAttribute('gnfeature')

    def checkInAleda(self, c, table):
        table.selectFromTable(c, ['name', 'def', 'geonames_type'], [('key', '=', self.eid)])
        res = c.fetchone()
        if res != None:
            self.inaleda = True
            (name, descr, gnfeature) = res
            print "Entity %s in Aleda:\t%s (%s)\t%s"%(str(self.eid), name, self.name, gnfeature)
        else:
            self.inaleda = False
            print "Entity not in Aleda %s:\t%s"%(str(self.eid), self.name)
            sys.stderr.write("ID not in Aleda: %d|%s\n"%(self.eid, self.name.encode('utf-8')))

    def retrieveByName(self, c, table):
        table.selectFromTable(c, ['key', 'def', 'link'], [('name', '=', self.name)])
        res = c.fetchall()
        # print 'Possible IDs:'
        if res != []:
            for r in res:
                print "Alternate ID:",;print int(r[0]),; print r[1],; print r[2]
                sys.stderr.write("Alternate ID:\t%d|%s|%s\n"%(int(r[0]), r[1].encode('utf-8'), r[2].encode('utf-8')))
        sys.stderr.write("=====\n")
            # sys.stderr.write(str(int(r[0]))+'|'+r[1].encode('utf-8')+'|'+r[2].encode('utf-8')+'|')
        # sys.stderr.write('\n')

    def checkName(self, c, table):
        table.selectFromTable(c, ['key', 'def', 'link'], [('name', '=', self.name)])
        res = c.fetchall()
        print self.name,; print '(NIL)'
        if res != []:
            sys.stderr.write("%s (NIL)\n"%(self.name.encode('utf-8')))
            for r in res:
                sys.stderr.write('Name in Aleda: %d|%s|%s\n'%(int(r[0]), r[1].encode('utf-8'), r[2].encode('utf-8')))
            sys.stderr.write("=====\n")
    
if __name__ == '__main__':
    main()

