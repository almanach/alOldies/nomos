<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="xml"/>
    <!-- INPUT: Format ftb6_all.EN.docs.xml
        =============================
        <root>
        <corpus id="ftb6_1">
        <document id="id248980">
        <sentence id="E1">
        Certes, rien ne dit qu'une seconde motion de censure sur son projet de loi, reprenant l'accord du 10 avril, n'aurait pas été la bonne mais cette probabilité, reconnaissent les socialistes, n'était pas la plus plausible. 
        </sentence>
        <sentence id="E2">
        Toujours est-il que le gouvernement a cédé alors que ses adversaires politiques ne proposent aucune solution alternative et considèrent, avec un bel ensemble, que la défense des intérêts des malades passe par le maintien d'un système qui aboutit à multiplier les actes inutiles et au dépassement généralisé des honoraires de la "<ENAMEX type="Organization" sub_type="InstitutionalOrganization" eid="null" name="Sécurité sociale en France" uri="null">Sécu</ENAMEX>". 
        </sentence>
        ...
    -->
    <!-- OUTPUT: Format gafpCorpus.xml
        =============================
        <gold_corpus>
        <item rank="1" eval_type="dev" ref="urn:newsml:afp.com:20090520T164214Z:TX-PAR-FVU98:1">
            <para rank="1">
                Les pays hôtes des Jeux olympiques engrangent davantage de médailles non seulement l'année où ils organisent les compétitions, mais aussi aux JO qui précèdent et qui suivent, selon une étude publiée jeudi dans le <ENAMEX eid="null" type="Organization" sub_type="MediaOrganization" name="British Journal of Sports Medicine">British Journal of Sports Medicine</ENAMEX>.            
    -->
    <xsl:template match="child::*">
        <gold_corpus>
            <xsl:apply-templates select="//document"/>
        </gold_corpus>
    </xsl:template>
    <xsl:template match="document">
        <item>
            <xsl:attribute name="ref"><xsl:value-of select="./@id"/></xsl:attribute>
            <xsl:apply-templates select="./sentence"/>
        </item>
    </xsl:template>
    <xsl:template match="sentence">
        <para>
            <xsl:attribute name="rank">
                <xsl:value-of select="1+count(preceding-sibling::sentence)"/>
            </xsl:attribute>
            <xsl:apply-templates select="./ENAMEX|text()"/>
        </para>
    </xsl:template>
    <xsl:template match="sentence/ENAMEX">
        <xsl:copy-of select="."/>
    </xsl:template>
    
    
</xsl:stylesheet>