#+Title: Nomos

* Knowledge acquisition
** Knowledge description
** Steps
   1. items (news / articles) tokenization (for Wikipedia: [[file:linking/build_data/wikipedia2sxpipe.sh][wikipedia2sxpipe.sh]])
   2. init DB entities (enamex, WL) + words ([[file:linking/build_data/initDB.py][initDB.py]])
   3. entity tagging (enamex / WL) (for AFP: sxpipe-afp with np_normalizer, later with linker trained on Wikipedia data; for WP: <WL>)
   4. read corpus
      1. store entities K with no computing ([[file:linking/build_data/annotation2db.py][annotation2db.py]], classes [[file:entities/build_entities/Extraction.py][Extraction.py]]); progressively
         fill DB with each corpus segment == state 1
	 - words 
	   + print text files items2words
	   + store in wordsDB#words2frequency
	   + store in wordsDB#words_population (total)
	 - entities
	   + case: AFP news (with gold or errorful baseline linking or less erroful wp-linker linking)
	     - store in entitiesDB#entities2(frequency|news|date|iptc|slugs|mentions|entities)
	     - store in entitiesDB#entities_population (total by type - kno vs. unk wrt. Aleda)
	   + case: Wikipedia articles
	     - store in entitiesDB#WL2(frequency|articles|categories|mentions|WL)
	     - store in entitiesDB#WL_population (total by type - kno. vs unk. wrt. Aleda)
      2. compute mentions co-occurrences == state 2
	 - read entitiesDB#entities2news|entitiesDB#WL2articles
	   - get entities|wl in each news|article
	   - get their mentions
	 - fill new table entity|wl mentions => entity|wl mentions + count (more than one co-occurrence)
      3. compute saillance ([[file:linking/build_data/swords2entities.py][swords2entities.py]])
	 1. read text files items2words
	 2. access wordsDB
	 3. compute items saillant words - select n most saillant
	 4. access entitiesDB#entities2items - select entities occuring at least n times in item
	 5. add swords to entities
	 6. print text files
	 7. select most frequent swords for entities
      4. store entities swords in entitiesDB#entities2swords ([[file:linking/build_data/db_entities2swords.py][db_entities2swords.py]]) == state 3
** Wikipedia Linker KB
*** Words
computing problems: total words not correct => ignore table words_population
# TOTAL WORDS
#1# 2 239 383 808
#2# 1 828 864 908
#3# 3 266 948 251
#4# 3 174 537 035
#5# 4 950 028 136
## 15 500 000 000 => not ok (nimp)
also ignore total tokens
# TOTAL WORDS COMPUTED FROM <TOKEN> IN FILES
# 398 655 352 => not ok because a lot of tokens are discarded during count
take total computed from each word occurrences #
# TOTAL WORDS COMPUTED FROM SQLITE DB WITH SCRIPT
#1# 44 004 326
#2# 36 970 276
#3# 48 803 593
#4# 55 044 596
#5# 47 260 829
## 232 083 620
to be merged
# TOTAL DISTINCT WORDS
#1# 1 128 167
#2# 1 085 784
#3# 1 214 115
#4# 1 271 759
#5# 1 182 458
## 3 261 984
*** Entities / WikiLinks
forgot to fetch all data in wl_populations => same info now in table control
problem: almost all titles were not matched to aleda entities even when they should have
need to rewalk through entities, check if in aleda, move unk to kno if it is
done: table control known=1|0
* Training data
** Methodology
-for each article
--for each "entity" mention
---generate mention features
----(1)number of candidates
---find candidates aleda entries having this mention as a variant - mark the winner
----for each wl-candidate
-----generate wl features (+normalize 0=>1)
------(2)doc-sim-features: (a)cats, (b)slugs, (c)swords, (d)mentions-bag
------(3)mention-sim-features: (a)string resemblance
------(4)own-features: (a)frequency, (b)relative frequency, (c)type in Aleda, (d)weight in Aleda
------add features combinations:
-------(1)x(2)x(3)x(4) = 392
----add training example: (mention) winner+features=1, others+features=2 for all others

* Notes
** KB data
   - swords: filter swords where sword == entity
** Problems
   - tokenization
     - in version eswc/v3-after, token discrepancy in
       7/afp.com-20090521T123500ZTX-PAR-FYM15.gold.xml: <nationaliste-> in gold - changed manually
       in resolved.xml
* Results
** Nomos I
   1. Merge entities KB-2009-2010 and KB-2009-2010-2011S1: improvement
   2. New features configuration (cf. [[file:linking/resolution/LinkingInstance.py][LinkingInstance.py]]) - only atomic features: super improvement
   3. New Nomos building: all enamex are ambiguous with non-enamex

|                     |       v0 | 1. merged KB | 2. new features | 3. new nomos |
|---------------------+----------+--------------+-----------------+--------------|
| Detection-P-Dev     | 0.699301 |     0.721429 |        0.732394 |     0.769841 |
| Detection-R-Dev     | 0.757576 |     0.765152 |        0.787879 |     0.734848 |
| Detection-F-Dev     | 0.727273 |     0.742647 |        0.759124 |     0.751938 |
| Resolution-Acc-Dev  | 0.730000 |     0.811881 |        0.855769 |     0.917526 |
|---------------------+----------+--------------+-----------------+--------------|
| Detection-P-Test    | 0.595376 |     0.616279 |        0.662791 |     0.722222 |
| Detection-R-Test    | 0.751825 |     0.773723 |        0.832117 |     0.759124 |
| Detection-R-Test    | 0.664516 |     0.686084 |        0.737864 |     0.740214 |
| Resolution-Acc-Test | 0.553398 |     0.613208 |        0.710526 |     0.778846 |
|                     |          |              |                 |              |
