<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

  <xsl:output indent="yes" method="html" encoding="utf-8"
    doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"
    doctype-public="-//W3C//DTD XHTML 1.1//EN"/>
  <xsl:template match="comment()">
    <div><xsl:value-of select="."/></div>
  </xsl:template>

  <xsl:param name="mytype"/>
  <xsl:param name="status"/>
  

  <xsl:template match="/child::*">
    <xsl:call-template name="parent"/>
  </xsl:template>
  <xsl:template name="parent">
    <html>
      <head>
	<title><xsl:value-of select="concat('Entities (', $mytype, ')')"/></title>
      </head>
      <body>
      	<table border="1" cellspacing="0" cellpadding="0" width="100%" style="border: 0.5px;font-family:Verdana;font-size:10px;">
          <tr><th width="10%">ORIG.ID</th><th width="25%">OCCS</th><th width="20%">NAME</th><th width="10%">TYPE</th><th width="5%">TOTAL</th><th width="30%">IPTC</th></tr>	     
<!--      	  <tr><td></td><td></td><td></td><td><xsl:value-of select=".//total_enamex"/></td><td></td></tr>-->
<!--      	  <tr>
      	    <td>Pers. > 24 : </td><td><xsl:value-of select="count(.//ENAMEX[@type='Person' and ./total > 24])"/></td>
      	    <td>Org > 24 : </td><td><xsl:value-of select="count(.//ENAMEX[@type='Organization' and ./total > 24])"/></td>
      	    <td>Loc > 50 : </td><td><xsl:value-of select="count(.//ENAMEX[@type='Location' and not(@sub_type) and ./total > 49])"/></td></tr>
      	  <xsl:apply-templates select=".//comment()"/>
      	  <xsl:apply-templates select=".//ENAMEX[@type='Person' and ./total > 24]"><xsl:sort select="./@id"/></xsl:apply-templates>
          <xsl:apply-templates select=".//ENAMEX[@type='Organization' and not(@sub_type) and ./total > 24]"><xsl:sort select="./@id"/></xsl:apply-templates>
      	  <xsl:apply-templates select=".//ENAMEX[@type='Organization' and @sub_type='Company' and ./total > 24]"><xsl:sort select="./@id"/></xsl:apply-templates>
      	  <xsl:apply-templates select=".//ENAMEX[@type='Location' and not(@sub_type) and ./total > 49]"><xsl:sort select="./@id"/></xsl:apply-templates>-->
      	  <xsl:apply-templates select=".//ENAMEX"><xsl:sort select="./@type"/><xsl:sort select="./total/@value" data-type="number" order="descending"/></xsl:apply-templates>
      	</table>
      </body>
    </html>
  </xsl:template>

  <!--<xsl:template match="ENAMEX[@type='Person' and ./total > 24]">      
      <xsl:if test="not(contains(./info, 'UNKNOWN'))">        
        <xsl:call-template name="build"/>
      </xsl:if>  
  </xsl:template>
  <xsl:template match="ENAMEX[@type='Organization' and not(@sub_type) and ./total > 24]">
      <xsl:if test="not(contains(./info, 'UNKNOWN'))">        
        <xsl:call-template name="build"/>
      </xsl:if>  
  </xsl:template>
  <xsl:template match="ENAMEX[@type='Organization' and @sub_type='Company' and ./total > 24]">
      <xsl:if test="not(contains(./info, 'UNKNOWN'))">        
        <xsl:call-template name="build"/>
      </xsl:if>  
  </xsl:template>
  <xsl:template match="ENAMEX[@type='Location' and not(@sub_type) and ./total > 49]">
      <xsl:if test="not(contains(./info, 'UNKNOWN'))">        
        <xsl:call-template name="build"/>
      </xsl:if>  
  </xsl:template>-->
  
  <xsl:template match="ENAMEX">
<!--    <ENAMEX id="Financial Times" type="Organization">
        <name value="Financial Times"/>
        <info value="quotidien économique et financier britannique, mais dont la majorité des lecteurs résident dans d'autres pays depuis 1998"/>
        <a href="http://fr.wikipedia.org/wiki/Financial Times"/>
        <occ nb="2" value="Financial Times"/>
        <total value="2"/>
        <iptc_code nb="2" value="ECO"/>
</ENAMEX>
-->
    <xsl:if test="./@type=$mytype">
      <xsl:choose>
        <xsl:when test="$status='kno'">
        <xsl:if test="./@known='true'">
          <tr>
            <td><xsl:value-of select="./@eid"/></td>
              <td>
                 <xsl:for-each select="./occ">
                    <xsl:sort select="./@nb" data-type="number" order="descending"/>
                    <xsl:choose>
                      <xsl:when test="position()=last()">
                        <span><xsl:value-of select="./@value"/></span><small><xsl:value-of select="concat(' (', ./@nb, ')')"/></small>
                      </xsl:when>
                      <xsl:otherwise>
                        <span><xsl:value-of select="./@value"/></span><small><xsl:value-of select="concat(' (', ./@nb, '), ')"/></small>                  
                      </xsl:otherwise>
                    </xsl:choose>                  
                </xsl:for-each>
              </td>
              <td>
               	<a>
               	  <xsl:attribute name="title"><xsl:value-of select="./info/@value"/></xsl:attribute>
               	  <xsl:attribute name="target">_blank</xsl:attribute>
               	  <xsl:attribute name="href"><xsl:value-of select="./a[1]/@href"/></xsl:attribute>
               	  <xsl:value-of select="./name/@value"/>
               	</a>
            </td>
            <xsl:choose>
      	     <xsl:when test="./@sub_type">
      	       <td><xsl:value-of select="concat(./@type, ' [', ./@sub_type, ']')"/></td>
      	     </xsl:when>
      	     <xsl:otherwise>
      	       <td><xsl:value-of select="./@type"/></td>
      	     </xsl:otherwise>
            </xsl:choose>
            <td><xsl:value-of select="./total/@value"/></td>
            <td>
              <xsl:for-each select="./iptc_code">
                <xsl:sort select="./@nb" data-type="number" order="descending"/>
                <span style='color:green'><xsl:value-of select="./@value"/></span><small><xsl:value-of select="concat(' (', ./@nb, ')')"/></small>
              </xsl:for-each>
            </td>           
          </tr>
          </xsl:if>
          </xsl:when>
          <xsl:when test="$status='unk'">
          <xsl:if test="./@known='false'">
            <tr>
                <td>null</td>
                <td>
                <xsl:for-each select="./occ">
                    <xsl:sort select="./@nb" data-type="number" order="descending"/>
                    <xsl:choose>
                      <xsl:when test="position()=last()">
                        <span><xsl:value-of select="./@value"/></span><small><xsl:value-of select="concat(' (', ./@nb, ')')"/></small>
                      </xsl:when>
                      <xsl:otherwise>
                        <span><xsl:value-of select="./@value"/></span><small><xsl:value-of select="concat(' (', ./@nb, '), ')"/></small>                  
                      </xsl:otherwise>
                    </xsl:choose>
                  
                </xsl:for-each>
              </td>
              <td>
<!--              	<a style="color:black">
              	  <xsl:attribute name="title"><xsl:value-of select="./info/@value"/></xsl:attribute>
              	  <xsl:attribute name="target">_blank</xsl:attribute>-->
              	  <xsl:value-of select="./name/@value"/><!--
              	</a>-->
              </td>
              <xsl:choose>
        	     <xsl:when test="./@sub_type">
        	       <td><xsl:value-of select="concat(./@type, ' [', ./@sub_type, ']')"/></td>
        	     </xsl:when>
        	     <xsl:otherwise>
        	       <td><xsl:value-of select="./@type"/></td>
        	     </xsl:otherwise>
              </xsl:choose>
              <td><xsl:value-of select="./total/@value"/></td>
              <td>
                <xsl:for-each select="./iptc_code">
                  <xsl:sort select="./@nb" data-type="number" order="descending"/>
                  <span style='color:green'><xsl:value-of select="./@value"/></span><small><xsl:value-of select="concat(' (', ./@nb, ')')"/></small>
                </xsl:for-each>
              </td>                
            </tr>        
            </xsl:if>
          </xsl:when>
        </xsl:choose>
      </xsl:if>
  </xsl:template>

</xsl:stylesheet>

