<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [
<!ENTITY xsd-string  "http://www.w3.org/2001/XMLSchema#string">
<!ENTITY xsd-anyURI "http://www.w3.org/2001/XMLSchema#anyURI">
<!ENTITY xsd-id "http://www.w3.org/2001/XMLSchema#ID">
<!ENTITY xsd-idref "http://www.w3.org/2001/XMLSchema#IDREF">
<!ENTITY owl "http://www.w3.org/2002/07/owl">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
            xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
            xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            xmlns:owl="http://www.w3.org/2002/07/owl#"
            xmlns:AMO="http://www.semanticweb.org/ontologies/2010/2/AMO.owl#"
            exclude-result-prefixes="rdf xsd owl AMO">
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Jun 21, 2010</xd:p>
            <xd:p><xd:b>Author:</xd:b> stern</xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:output indent="yes" method="xml" encoding="utf-8"/>
    


<!--    #    <ENAMEX gender="f" id="Maria Nowak" type="Person">
#         <name value="Maria Nowak"/>
#         <info value="économiste spécialiste du microcrédit, présidente de l'Association pour le droit à l'initiative économique (1935"/>
#         <a href="http://fr.wikipedia.org/wiki/Maria Nowak"/>
#         <occ nb="1" value="Maria Nowak"/>
#         <occ nb="1" value="Nowak"/>
#         <total value="2"/>
#         <iptc_code nb="2" value="SOI"/>
#    </ENAMEX>-->
    
<!--    #    <Person rdf:about="#test_instance">
#        <rdf:type rdf:resource="&owl;Thing"/>
#        <hasReferentialID rdf:datatype="&xsd;ID"
#            >AFP_AMO_000000001</hasReferentialID>
#        <hasLabel rdf:datatype="&xsd;string">Hector Sarrasin</hasLabel>
#        <hasURI rdf:datatype="&xsd;anyURI"
#            >http://anypedia.fr/HectorSarrasin</hasURI>
#        <isInNewsItem rdf:datatype="&xsd;anyURI"
#            >urn:newsml:afp.com:20090129:TX-CAFP-SDSP08-185152-026:1</isInNewsItem>
#        <hasShortDescription rdf:datatype="&xsd;string"
#            >&#233;minent savant sp&#233;cialiste du concept de bootstrapping de M&#252;nchausen &#224; nos jours</hasShortDescription>
#        <hasGender>m</hasGender>
#        <hasTheme rdf:resource="#Sci"/>
#        <hasMainTheme rdf:resource="#Pol"/>
#    </Person>-->
    
    
    <xsl:template match="/child::*">
        <rdf:RDF>
            <xsl:apply-templates select="./ENAMEX"></xsl:apply-templates>
        </rdf:RDF>
    </xsl:template>
    <xsl:template match="ENAMEX">
        <xsl:param name="element_name">
            <xsl:choose>
                <xsl:when test="./@sub_type='Company'"><xsl:value-of select="./@sub_type"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="./@type"/></xsl:otherwise>
            </xsl:choose>
        </xsl:param>
        <xsl:param name="owl_type">
            <xsl:choose>
                <xsl:when test="./@type='Person'">Per</xsl:when>
                <xsl:when test="./@type='Location'">Loc</xsl:when>
                <xsl:when test="./@type='Organization'">Org</xsl:when>
            </xsl:choose>
        </xsl:param>
        <xsl:element name="AMO:{$element_name}">
            <xsl:attribute name="rdf:about"><xsl:value-of select="concat('#',./@id)"/></xsl:attribute>
            <!--<xsl:element name="rdf:type"><xsl:attribute name="rdf:resource"><xsl:value-of select="concat('&owl;', '#Thing')"/></xsl:attribute></xsl:element>-->
            <xsl:element name="AMO:hasReferentialID">
                <xsl:attribute name="rdf:datatype"><xsl:value-of select="'&xsd-id;'"/></xsl:attribute>
                <!--<xsl:value-of select="concat($owl_type, '#', ./@id)"/>-->
                <xsl:value-of select="concat($owl_type, '#', generate-id(.))"/>
            </xsl:element>
            <xsl:element name="AMO:hasNormalName">
                <xsl:attribute name="rdf:datatype"><xsl:value-of select="'&xsd-string;'"/></xsl:attribute>
                <xsl:value-of select="./name/@value"/>
            </xsl:element>
            <xsl:for-each select="./occ">
                <xsl:element name="AMO:hasLabel">
                    <xsl:attribute name="rdf:datatype"><xsl:value-of select="'&xsd-string;'"/></xsl:attribute>
                    <xsl:value-of select="./@value"/>
                </xsl:element>
            </xsl:for-each>
            <xsl:element name="AMO:hasURI">
                <xsl:attribute name="rdf:datatype"><xsl:value-of select="'&xsd-anyURI;'"/></xsl:attribute>
                <xsl:value-of select="./a./@href"/>
            </xsl:element>
            <xsl:element name="AMO:hasShortDescription">
                <xsl:attribute name="rdf:datatype"><xsl:value-of select="'&xsd-string;'"/></xsl:attribute>
                <xsl:value-of select="./info/@value"/>
            </xsl:element>
            <xsl:for-each select="./iptc_code">                
                <xsl:sort select="./@nb" data-type="number" order="descending"/>
                <xsl:choose>
                    <xsl:when test="position()=1">
                        <xsl:element name="AMO:hasMainTheme">
                            <xsl:attribute name="rdf:resource"><xsl:value-of select="concat('#', ./@value)"/></xsl:attribute>
                        </xsl:element>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:element name="AMO:hasTheme">
                            <xsl:attribute name="rdf:resource"><xsl:value-of select="concat('#', ./@value)"/></xsl:attribute>
                        </xsl:element>                    
                    </xsl:otherwise>
                </xsl:choose>                      
            </xsl:for-each>
        </xsl:element>            
    </xsl:template>
</xsl:stylesheet>
