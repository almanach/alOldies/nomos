declare option saxon:output "indent=yes";

<Misc_known>
<Work>
{
for $entity in //ENAMEX
where ($entity[@type="Work" and ./@known='true'])
return $entity
}
</Work>
<Product>
{
for $entity in //ENAMEX
where ($entity[@type="Product" and ./@known='true'])
return $entity
}
</Product>
</Misc_known>



