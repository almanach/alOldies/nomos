#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
import re
from xml.dom.minidom import *
from optparse import *
from subprocess import *
import codecs
from operator import *
import commands
import time
from collections import defaultdict
sys.path.append('@common_src@')
from utils import *
from Extraction import *



def main():
    options, args = getOptions()
    global enamex_table
    enamex_table = {}
    global unk_enamex_table
    unk_enamex_table = {}
    global counts
    counts = {}
    for x in ['refs', 'mentions']:
        counts[x] = {}
        for y in ['all', 'per', 'loc', 'org', 'com', 'pro', 'wor']:
            counts[x][y] = 0
    ueid = 0
    # oldIds2newIds = getNewIds(options.ids)
    if os.path.isfile(args[0]):
        file = args[0]
        # merge(file, oldIds2newIds)
        merge(file)
    elif os.path.isdir(args[0]):
        for root, dirs, files in os.walk(args[0]):
            for file in files:
                full_name = os.path.join(root, file)
                if full_name.endswith('.xml'):
                    # merge(full_name, oldIds2newIds)
                    merge(full_name)
    print >> sys.stderr, 'Total Enamex: '+str(len(enamex_table.keys()))
    # print >> sys.stderr, time.asctime()

    impl = getDOMImplementation()
    newdoc = impl.createDocument(None, "some_tag", None)
    top_element = newdoc.documentElement
    
    G = codecs.open(options.out, 'w', 'UTF-8')
    G.write('<?xml version="1.0" encoding="UTF-8" ?>\n<ENAMEX_ROOT>\n')
    print >> sys.stderr, "Writing merged Enamex"
    for eid in enamex_table.keys():
        counts['refs']['all'] += 1
        type = enamex_table[eid]['features']['type']
        if type == 'Person': counts['refs']['per'] += 1
        elif type == 'Location': counts['refs']['loc'] += 1
        elif type == 'Organization': counts['refs']['org'] += 1
        elif type == 'Company': counts['refs']['com'] += 1
        elif type == 'Product': counts['refs']['pro'] += 1
        elif type == 'Work': counts['refs']['wor'] += 1
        features = {}
        for feature in ['type', 'known', 'gender']:
            features[feature] = enamex_table[eid]['features'][feature]
        features['eid'] = eid
        features['name'] = enamex_table[eid]['features']['name']
        new_node = createElementNode(newdoc, 'ENAMEX', features, None)
        # info = enamex_table[eid]['features']['info']
        # href = enamex_table[eid]['features']['href']
        # new_node.appendChild(createElementNode(newdoc, 'name', {'value':name}, None))
        # new_node.appendChild(createElementNode(newdoc, 'info', {'value':info}, None))
        # new_node.appendChild(createElementNode(newdoc, 'a', {'href':href}, None))

        total = 0
        for occ in enamex_table[eid]['occs_record'].keys():
            count = enamex_table[eid]['occs_record'][occ]
            total += count
            counts['mentions']['all'] += count
            if type == 'Person': counts['mentions']['per'] += count
            elif type == 'Location': counts['mentions']['loc'] += count
            elif type == 'Organization': counts['mentions']['org'] += count
            elif type == 'Company': counts['mentions']['com'] += 1
            elif type == 'Product': counts['mentions']['pro'] += 1
            elif type == 'Work': counts['mentions']['wor'] += 1
            new_node.appendChild(createElementNode(newdoc, 'occurrence', {'count':str(count), 'value':occ}, None))
            new_node.appendChild(createElementNode(newdoc, 'total', {'value':str(total)}, None))
            
        for code in enamex_table[eid]['iptc_codes_record'].keys():
            count = enamex_table[eid]['iptc_codes_record'][code]
            new_node.appendChild(createElementNode(newdoc, 'iptc_code', {'count':str(count), 'value':code}, None))
            
        if total > options.filter:
            G.write(new_node.toprettyxml())
            new_node.unlink()

    G.write('\n</ENAMEX_ROOT> \n')
    # print >> sys.stderr, time.asctime()
    G.close()            
    print >> sys.stderr, "Done"

    NEWS = codecs.open(options.news, "w", "utf-8")
    DATES = codecs.open(options.dates, "w", "utf-8")
    IPTC = codecs.open(options.iptc, "w", "utf-8")
    for eid in enamex_table.keys():
        type = enamex_table[eid]['features']['type']
        for link in enamex_table[eid]['news_links_record'].keys():
            count = enamex_table[eid]['news_links_record'][link]
            NEWS.write(eid+"\t"+type+"\t"+link+"\t"+str(count)+"\n")
        for iptc, count in enamex_table[eid]['iptc_codes_record'].items():
            IPTC.write(eid+"\t"+type+"\t"+iptc+"\t"+str(count)+"\n")
        for date, count in enamex_table[eid]['dates_record'].items():
            DATES.write(eid+"\t"+type+"\t"+date+"\t"+str(count)+"\n")
    NEWS.close()
    IPTC.close()
    DATES.close()

    for key_1 in counts.keys():
        sys.stderr.write(key_1+"\n")
        for key_2 in counts[key_1].keys():
            sys.stderr.write("\t"+key_2+": "+str(counts[key_1][key_2])+"\n")
            
def merge(file, oldIds2newIds=None):                
    # enamex_nodes = parseLargeXml(file, "ENAMEX")
    # print >> sys.stderr, time.asctime()
    # print 'Got dom for '+file
    dom = parse(file)
    enamex_nodes = dom.getElementsByTagName('ENAMEX')
    print >> sys.stderr, file+' has '+str(len(enamex_nodes))+' Enamex'
    for enamex_node in enamex_nodes:
        # try:
        #     eid = oldIds2newIds[enamex_node.getAttribute('eid')]
        # except KeyError:
        #     eid = "null"
        eid = enamex_node.getAttribute('id')
        if eid == "": print >> sys.stderr, enamex_node.toxml(); sys.exit()
        name = enamex_node.getAttribute('name')
        known = enamex_node.getAttribute('known')
        # if known == "false":
        #     eid = name
        if not enamex_table.has_key(eid):
            enamex_table[eid] = {}
            enamex_table[eid]['features'] = {}
            enamex_table[eid]['occs_record'] = defaultdict()
            enamex_table[eid]['news_links_record'] = defaultdict()
            enamex_table[eid]['iptc_codes_record'] = defaultdict()
            enamex_table[eid]['dates_record'] = defaultdict()
            if enamex_node.getAttribute('gender') != "":
                gender = enamex_node.getAttribute('gender')
            else:
                gender = None
            enamex_table[eid]['features']['type'] = enamex_node.getAttribute('type')
            enamex_table[eid]['features']['name'] = name
            enamex_table[eid]['features']['gender'] = gender
            enamex_table[eid]['features']['known'] = known
            # enamex_table[eid]['features']['info'] = enamex_node.getElementsByTagName('info')[0].getAttribute('value')
            # enamex_table[eid]['features']['href'] = enamex_node.getElementsByTagName('a')[0].getAttribute('href')

        occs = enamex_node.getElementsByTagName('occurrence')
        for occ in occs:
            string = occ.getAttribute('value')
            count = occ.getAttribute('count')
            try: enamex_table[eid]['occs_record'][string] += int(count)
            except KeyError: enamex_table[eid]['occs_record'][string] = int(count)

        news_links = enamex_node.getElementsByTagName('news')
        for news_link in news_links:
            link = news_link.getAttribute('value')
            nb = news_link.getAttribute('count')
            try: enamex_table[eid]['news_links_record'][link] += int(nb)
            except KeyError: enamex_table[eid]['news_links_record'][link] = int(nb)

        iptc_codes = enamex_node.getElementsByTagName('theme_code')
        for iptc_code in iptc_codes:
            code = iptc_code.getAttribute('value')
            count = iptc_code.getAttribute('count')      
            try: enamex_table[eid]['iptc_codes_record'][code] += int(count)
            except KeyError: enamex_table[eid]['iptc_codes_record'][code] = int(count)
            
        dates = enamex_node.getElementsByTagName('date')
        for date_node in dates:
            date = date_node.getAttribute('value')
            count = date_node.getAttribute('count')
            try: enamex_table[eid]['dates_record'][date] += int(count)
            except KeyError: enamex_table[eid]['dates_record'][date] = int(count)
            
    dom.unlink()

def getNewIds(file):
    oldIds2newIds = {}
    F = open(file)
    pattern = re.compile("^(\d+)\t+(\d+|unknown)\s*$")
    for line in F:
        match = re.match(pattern, line)
        if match != None:
            old = match.group(1)
            new = match.group(2)
            if new == "unknown": new = "null"
            oldIds2newIds[old] = new
    F.close()
    return oldIds2newIds

def getOptions():
    usage = '\n\n\t%prog [options] <enamex_dir|enamex_file>\n'
    parser = OptionParser(usage)
    parser.add_option("-o", "--out", action="store", dest="out", default="enamex.mrg.xml", help="output file for merged annotations")
    parser.add_option("--news", action="store", dest="news", default="extraction_news_index.csv", help="index record of enamex => news")
    parser.add_option("--iptc", action="store", dest="iptc", default="extraction_iptc_index.csv", help="index record of enamex => IPTC")
    parser.add_option("--dates", action="store", dest="dates", default="extraction_dates_index.csv", help="index record of enamex => date")
    parser.add_option("--filter", action="store", dest="filter", default=0, type="int", help="filter by min frequency (default 1)")
    # parser.add_option("--ids", action="store", dest="ids", default=None, help="matching file old Ids => new Ids")       
    try: (options, args) = parser.parse_args()
    except OptionError: parser.print_help();sys.exit('OptionError')
    except: parser.print_help();sys.exit('An error occured during options parsing\n')
    if len(args) < 1: parser.print_help();sys.exit()
    return options, args



if __name__ == "__main__":
    main()    
        # <ENAMEX eid="2000000003194884" id="2000000003194884" known="true" name="Republic of Montenegro" total="8" type="Location">
        #         <occurrence count="8" value="Monténégro"/>
        #         <theme_code count="2" value="ECO"/>
        #         <theme_code count="1" value="SOI"/>
        #         <theme_code count="1" value="POL"/>
        #         <theme_code count="1" value="UNR"/>
        #         <theme_code count="2" value="ENV"/>
        #         <slug count="2" value="environnement"/>
        #         <slug count="1" value="conflit"/>
        #         <slug count="1" value="USA"/>
        #         <slug count="1" value="Afghanistan"/>
        #         <slug count="1" value="Serbie"/>
        #         <slug count="2" value="Monténégro"/>
        #         <slug count="1" value="recensement"/>
        #         <slug count="1" value="défense"/>
        #         <slug count="1" value="doc"/>
        #         <slug count="2" value="tourisme"/>
        #         <date count="1" value="20110103"/>
        #         <date count="2" value="20110104"/>
        #         <date count="1" value="20110106"/>
        # </ENAMEX>
