#!/bin/bash

if [ -z $1 ]; then
    echo "Usage: <xml_file> <output_dir>"
else
    if [ -z $2 ]; then
	echo "Usage: <xml_file> <output_dir>"
    else
	xml=$1
	outputdir=$2
	if [ ! -e $outputdir ]; then
	    mkdir $outputdir
	    mkdir $outputdir/xml
	    mkdir $outputdir/html
	    mkdir $outputdir/owl
	fi
	echo "========== XQuery: Split Known / Unknown by type (All) =========="
	java -Xmx1024m -cp /Volumes/Data/apps/saxonhe9-2-0-6j/saxon9he.jar net.sf.saxon.Query -t -s:$xml -q:/usr/local/share/nomos/entities/xquery/entities.ALL.kno.xquery > $outputdir/xml/enamex.ALL.kno.xml
	echo "=========="
	java -Xmx1024m -cp /Volumes/Data/apps/saxonhe9-2-0-6j/saxon9he.jar net.sf.saxon.Query -t -s:$xml -q:/usr/local/share/nomos/entities/xquery/entities.ALL.unk.xquery > $outputdir/xml/enamex.ALL.unk.xml
	for TYPE in "Person" "Organization" "Company" "Location" "Product"
	do
	    kno_xml=$outputdir/xml/enamex.$TYPE.kno.xml
	    unk_xml=$outputdir/xml/enamex.$TYPE.unk.xml
	    kno_html=$outputdir/html/enamex.$TYPE.kno.html
	    unk_html=$outputdir/html/enamex.$TYPE.unk.html
	    kno_owl=$outputdir/owl/enamex.$TYPE.kno.owl
	    unk_owl=$outputdir/owl/enamex.$TYPE.unk.owl
	    echo "========== XQuery: Split Known / Unknown for $TYPE =========="
	    java -Xmx1024m -cp /Volumes/Data/apps/saxonhe9-2-0-6j/saxon9he.jar net.sf.saxon.Query -t -s:$xml -q:/usr/local/share/nomos/entities/xquery/entities.$TYPE.kno.xquery > $kno_xml
	    echo "=========="
	    java -Xmx1024m -cp /Volumes/Data/apps/saxonhe9-2-0-6j/saxon9he.jar net.sf.saxon.Query -t -s:$xml -q:/usr/local/share/nomos/entities/xquery/entities.$TYPE.unk.xquery > $unk_xml
	    echo "========== XSLT: Convert $TYPE to HTML =========="
	    echo "$TYPE / Known to HTML"
	    xsltproc --stringparam mytype $TYPE --stringparam status kno /usr/local/share/nomos/entities/xsl/entities2html.xsl $kno_xml > $kno_html
	    echo "=========="
	    echo "$TYPE / Unknown to HTML"
	    xsltproc --stringparam mytype $TYPE --stringparam status unk /usr/local/share/nomos/entities/xsl/entities2html.xsl $unk_xml > $unk_html
	    echo "========== XSLT: Convert $TYPE to OWL =========="
	    echo "$TYPE / Known to OWL"
	    /usr/local/share/nomos/entities/xsl/entities2owl.py -o $kno_owl --fmin 25 --split 1000 --db /usr/local/lib/aleda/ne_refs.fr.dat $kno_xml
	    echo "=========="
	    echo "$TYPE / Unknown to OWL"
	    /usr/local/share/nomos/entities/xsl/entities2owl.py -o $unk_owl --fmin 25 --split 1000 --db /usr/local/lib/aleda/ne_refs.fr.dat $unk_xml 
	done	
	echo "XML split, HTML and OWL conversions finished"
	# countEntities.pl $outputdir
    fi
fi
