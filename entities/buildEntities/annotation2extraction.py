#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
import re
import collections
from xml.dom.minidom import *
from optparse import *
from subprocess import *
import codecs
from operator import *
import commands
sys.path.append('@common_src@')
from utils import *
from Extraction import *

def main():
    options, args = getOptions()
    if os.path.isdir(args[0]):
        for root, dirs, files in os.walk(args[0]):
            for file in files:
                full_file = os.path.join(root, file)
                if full_file.endswith('.xml'):
                    populateExtractionsFromCorpus(full_file, options.doctype, options.element_name, options.attribute_name)
    elif os.path.isfile(args[0]):
        populateExtractionsFromCorpus(args[0], options.doctype, options.element_name, options.attribute_name)
    else:
        sys.exit('Argument provided is not a valid file / directory')

    if options.out != None:
        print >> sys.stderr, 'Writing '+options.element_name+' XML Extraction in %s...\n'%(options.out)
        writeXmlExtraction(options.element_name, options.out)
    else:
        print >> sys.stderr, 'Writing '+options.element_name+' XML Extraction to STDOUT\n'
        writeXmlExtraction(options.element_name)
    
def writeXmlExtraction(element_name, out=None):
    impl = getDOMImplementation()
    root = element_name+'_ROOT'
    new_dom = impl.createDocument(None, root, None)
    # root_node = new_dom.documentElement
    if out != None: G = codecs.open(out, 'w', 'UTF-8')
    else: G = sys.stdout
    G.write('<?xml version="1.0" encoding="utf-8"?>\n<ENAMEX_ROOT>\n')
    for extraction_id, extraction in Extraction.instances[element_name].items():
        #if isinstance(extraction.id, int):
        # root_node.appendChild(extraction.toXmlNode(new_dom))
        node = extraction.toXmlNode(new_dom)
        # G.write(node.toprettyxml('utf-8'))
        G.write(node.toprettyxml())
        node.unlink()
        G.write('\n')
    new_dom.unlink()
    G.write('</ENAMEX_ROOT>\n')
    if out != None: G.close()
    print >> sys.stderr, '%d extractions written.'%(len(Extraction.instances[element_name].keys()))

def getOptions():
    usage = '\n\n\t%prog [options] <dir|file>\n'
    parser = OptionParser(usage)
    parser.add_option("--doc", action="store", dest="doctype", type="string", default="news", help="Document type (default: news_item)")
    parser.add_option("--tag", action="store", dest="element_name", type="string", default=None, help="Information type to extract")
    parser.add_option("--attr", action="store", dest="attribute_name", type="string", default=None, help="XML attribute for extraction ids")
    parser.add_option("-o", "--out", action="store", dest="out", default=None, help="Output file for annotations - root = tag+_ROOT")
    try: (options, args) = parser.parse_args()
    except OptionError: parser.print_help(); sys.exit('OptionError')
    except: parser.print_help(); sys.exit('An error occured during options parsing\n')
    if len(args) < 1: parser.print_help(); sys.exit()
    elif options.element_name == None or options.attribute_name == None: parser.print_help(); sys.exit()
    return options, args

if __name__ == "__main__":
    main()    
