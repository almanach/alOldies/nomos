#!/usr/bin/perl -w

use strict;
use Encode;
use DBI;
use Getopt::Long;
my $handle_known = 0;
my $handle_unknown = 0;
my $known_status;
my $limit = 0;
my $threshold = 1;
my $javascript = 1;
my $type = "";
GetOptions ('k' => \$handle_known,
	    'u' => \$handle_unknown,
	    'l:i' => \$limit,
	    't:i' => \$threshold,
	    'j' => \$javascript,
	   'type:s' => \$type);

if($type eq "" or $type !~ /^Person|Organization|Company$/){die "Usage: -k | -u [known or unknown entities] --type <Person|Organization|Company> -j <generate javascript> -l <limit for number of entities> -t <threshold for entities frequency> <iptc-index_file> <output_dir>";}
if($handle_known){$known_status = "KNO";}
if($handle_unknown){$known_status = "UNK";}

my $index_file = shift or die "Usage: -k | -u [known or unknown entities] --type <Person|Organization|Company> <iptc-index_file> <output_dir>";
my $output_dir = shift or die "Usage: <iptc-index_file> <output_dir>";

print STDERR "Get frequencies for entities $known_status ($type) in $index_file\nLimit set to [$limit]\nFrequency threshold set to [$threshold]\n";

open(ENTITIES, "<:encoding(utf-8)", $index_file) or die "Can't open $index_file: $!\n";

my %entities2date = ();
my %entities_record = ();
my %inits = ();

my $refs_dbh;
my $refs_sth;
if($handle_known)
  {    
    $refs_dbh = DBI->connect("dbi:SQLite:@aledalibdir@/ne_refs.dat", "", "", {RaiseError => 1, AutoCommit => 1});
    $refs_sth = $refs_dbh->prepare('select name, link from data where key=?;');
  }

my $cpt_entities = 0;
while(<ENTITIES>)#read index file of entities => iptc
# Michel_Garrandaux       Person  UNK     SOC     20100106        1
# 684234  Person  KNO     HTH     20090722        2
  {
    chomp;
    my $line = $_;
    if($line =~ /^(\S+)\s+($type)\s+$known_status\s+([A-Z]+)\s+(\d{8})\s+(\d+)\s*$/)
      {	
	my $entity_id = $1;
	my $type = $2;
	my $iptc = $3;
	my $date = $4;
	my $occ_nb = $5;
	my $name = "";
	my $link = "";
	my $init = "";
	if(not(exists($entities_record{$entity_id})))
	  {
	    $cpt_entities++;
	    print STDERR "Entities seen: $cpt_entities\r";	    
	    if($handle_known)
	      {
		$refs_sth->execute($entity_id);
		while(my @t = $refs_sth->fetchrow_array)
		  {
		    $name = Encode::decode("utf8", $t[0]);
		    $link = Encode::decode("utf8", $t[1]);
		    if(not($link)){$link =  "<a/>"}
		    $entities_record{$entity_id}{"name"} = $name;
		    $entities_record{$entity_id}{"link"} = $link;
		    # print STDERR ">>$name<< >>$link<<";<STDIN>;
		  }
		$refs_sth->finish;
		$name =~ /^(.)/; $init = $1;
	      }
	    else{$entity_id =~ /^(.)/; $init = $1;}
	    $entities_record{$entity_id}{"init"} = $init;
	    $inits{$init}{$entity_id}++;
	  }	
	# ici : transformer la date
	$entities2date{$entity_id}{$date} += $occ_nb;
	$entities_record{$entity_id}{"freq"} += $occ_nb;
	# print "$entity_id (<$name>, <$link>) => $date + $occ_nb\n";
      }
  }
close(ENTITIES);
print STDERR "Entities seen: $cpt_entities\n";
if($handle_known)
  {
    $refs_dbh->disconnect;
  }

$cpt_entities = 0;
my %written_entities = ();
for my $entity_id(sort{$entities_record{$b}{"freq"} <=> $entities_record{$a}{"freq"}}(keys(%entities_record)))
  {
    my $freq = $entities_record{$entity_id}{"freq"};
    if($freq >= $threshold)
      {
	if(($limit and $cpt_entities < $limit) or $limit == 0)
	  {
	    $cpt_entities++;
	    my $name = $entity_id;
	    if($handle_known){$name = $entities_record{$entity_id}{"name"};}
	    my $init = $entities_record{$entity_id}{"init"};	    
	    if(not(-d $output_dir."/".$init)){mkdir $output_dir."/".$init;}
	    if($javascript)
	      {
		$written_entities{$entity_id}++;
		my $temp = $name; $temp =~ s/\//_SLASH_/g;
		my $html = $output_dir."/".$init."/".$temp.".html";
		my $js = $output_dir."/".$init."/".$temp.".js";
		my $js_in_html = "./".$temp.".js";
		$html =~ s/ +/_/g;
		$js =~ s/ +/_/g;
		$js_in_html =~ s/ +/_/g;
		# print STDERR "Writing file $html for $name [$freq]\n";
		# print STDERR "Writing file $js ($js_in_html) for $name [$freq]\n";
		print STDERR "Writing $cpt_entities entities\r";
		open(HTML, ">:encoding(utf8)", $html) or die "Can't open $html (w): $!\n";
		print HTML "<html><head><META http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n";
		print HTML "<script type='text/javascript' src='https://www.google.com/jsapi'></script>\n";
		print HTML "<script type='text/javascript' src='".$js_in_html."'></script>\n";
		print HTML "</head><body>";
		print HTML "<h1>".$name."</h1>\n";
		print HTML "<div id='chart_div' style=\"width:1200px;height:400px\"></div>\n";
		my $link = $entities_record{$entity_id}{"link"};
		print HTML "<div>".$link."</div>\n";
		print HTML "</body></html>\n";
		close(HTML);
		open(JS, ">:encoding(utf8)", $js) or die "Can't open $js (w): $!\n";
		print JS "google.load('visualization', '1', {'packages':['annotatedtimeline']});\n";
		print JS "google.setOnLoadCallback(drawChart);\n";
		print JS "function drawChart() {\n";
		print JS "var data = new google.visualization.DataTable();\n";
		print JS "data.addColumn('date', 'Date');\n";
		print JS "data.addColumn('number', 'Frequency');\n";
		print JS "data.addRows([\n";
		my $len = scalar(keys(%{ $entities2date{$entity_id} }));
		my $cpt = 0;
		for my $date(sort(keys(%{ $entities2date{$entity_id} })))
		  {
		    $cpt++;
		    my $date2print = $date;
		    $date2print =~ s/^(\d\d\d\d)(\d\d)(\d\d)$/$1, $2, $3/;
		    $date2print =~ s/ 0+/ /g;
		    if($cpt == $len){print JS "[new Date(".$date2print."), ".$entities2date{$entity_id}{$date}."]\n";}
		    else{print JS "[new Date(".$date2print."), ".$entities2date{$entity_id}{$date}."],\n";}
		  }
		print JS "]);\n";
		print JS "var chart = new google.visualization.AnnotatedTimeLine(document.getElementById('chart_div'));\n";
		print JS "chart.draw(data, {displayAnnotations: true});}\n";
		close(JS);
	      }
	    else
	      {
		my $outfile = $output_dir."/".$init."/".$name;
		$outfile =~ s/ +/_/g;
		# print STDERR "Writing file $outfile for $name [$freq]\n";
		print STDERR "Writing $cpt_entities entities\r";
		open(F, ">:encoding(utf-8)", $outfile);
		if($handle_known)
		  {
		    print F "#$name\tsxpipeID:".$entity_id."\t".$entities_record{$entity_id}{"link"}."\t$freq\n";
		  }
		else
		  {
		    print F "#$name\tundef\tundef\t$freq\n";
		  }
		for my $date(sort(keys(%{ $entities2date{$entity_id} })))
		  {
		    my $date2print = $date;
		    $date2print =~ s/^(\d\d\d\d)(\d\d)(\d\d)$/$1-$2-$3/;
		    print F $date2print."\t".$entities2date{$entity_id}{$date}."\n";
		  }
		close(F);
	      }
	  }
      }
  }
if($javascript)
  {
    my $html_index = $output_dir."/index.html";
    print STDERR "Writing $html_index\n";
    open(INDEX, ">:encoding(utf-8)", $html_index) or die "Can't open $html_index (w): $!\n";
    print INDEX "<html><head><META http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n</head>\n";
    print INDEX "<title>AFP Metadata Reference Base 2009-2010.T1+T2 | ".uc($type)."</title>\n";
    print INDEX "<style type=\"text/css\">\n";
    print INDEX " span.initlist{font-size: 18pt }\n";
    print INDEX " span.init{font-size: 16pt}\n";
    print INDEX " div.name{margin-left: 40px ; font-size: 14pt}\n";
    print INDEX "</style>\n";
    print INDEX "<body>\n";
    print INDEX "<h2>".uc($type)."</h2>\n";
    print INDEX "<a name=\"top\"></a>";
    print INDEX "<div><span>|</span>";
    foreach my $init(sort(keys(%inits)))
      {
	print INDEX "<span class=\"initlist\"><a href=\"#".$init."\">  $init  </a></span><span>|</span>";
      }
    print INDEX "</div>";
    foreach my $init(sort(keys(%inits)))
      {
	print INDEX "<div><span class=\"init\"><a name=\"".$init."\">$init</a></span><span>   |   </span><span><a href=\"#top\">top</a></span></div>";
	foreach my $entity_id(sort(keys(%{ $inits{$init} })))
	  {
	    if(exists($written_entities{$entity_id}))
	      {
		my $name = $entities_record{$entity_id}{"name"};
		my $temp = $name; $temp =~ s/\//_SLASH_/g;
		my $href = "./".$init."/".$temp.".html";
		$href =~ s/ +/_/g;
		print INDEX "<div class=\"name\"><a href=\"".$href."\">".$name."</a></div>\n";
	      }
	  }
	print INDEX "</div></div>";
      }
    print INDEX "</body></html>\n";
    print STDERR "$cpt_entities entities plotted.\n";
  }


