#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
import sqlite3
import re
from xml.dom.minidom import *
from optparse import *
from subprocess import *
import codecs
from operator import *
import commands
sys.path.append('@common_src@')
from utils import *
#INPUT: SxPipe Annotations XML ENAMEX format
###############################

class Entity:
    """Named Entity extracted from SxPipe annotation in NewsML items from AFP. To be selected for population of AFP OWL-ontology AMO.
    """
    # id_creator = ID_creator()
    
    def __init__(self, enamex_node, cursor):
        """Entity constructed with features from XML Enamex node
        """
        self.dom = enamex_node
        # WE SUPPOSE WE HAVE ONLY KNOWN ENTITITES (PREVIOUSLY SELECTED) | OTHERWISE CUSTOMIZATION IS NEEDED ON IDS WHICH IS JUSTE "THE NAME" FOR UNKNOWN
        self.eid = enamex_node.getAttribute('eid')
        self.name = enamex_node.getElementsByTagName('name')[0].getAttribute('value')
        # BEURK PATCH FOR BACKQUOTE IN NAME (YES, THIS HAPPENS...)
        if re.search(u'`', self.name): sys.stderr.write(self.name.encode('utf-8')+" changed to ");self.name = re.sub('`', '', self.name); sys.stderr.write(self.name.encode('utf-8')+"\n")
        # if re.search('Aqabah', name): sys.stderr.write(name.encode('utf-8')+" rejected";)#name = re.sub('`', '', name)        
        self.type = enamex_node.getAttribute('type')
        self.gender = enamex_node.getAttribute('gender')
        # self.referential_id = Entity.id_creator.createNewID()
        if self.type == "Location": self.display_type = "Loc"
        elif self.type == "Person": self.display_type = "Per"
        elif self.type == "Organization": self.display_type = "Org"
        elif self.type == "Company": self.display_type = "Com"
        elif self.type == "Product": self.display_type = "Pro"
        elif self.type == "Work": self.display_type = "Wor"
        else: self.display_type = self.type
        temp = self.display_type+'_'+self.name
        temp = re.sub(' ', '_', temp)
        temp = re.sub('&amp;', '&', temp); temp = re.sub('&', '&amp;', temp)
        self.display_id = temp
        origin = "sxp"
        self.referential_id = origin+"_"+self.eid
        self.max_iptc_subject_instance = "NULL"
        self.occs = []
        for occ in enamex_node.getElementsByTagName('occ'):
            self.occs.append(occ.getAttribute('value'))
        # self.items = []
        self.total = enamex_node.getElementsByTagName('total')[0].getAttribute('value')
        self.topics = {}
        max = 0
        regex1 = re.compile(r"^[\s\n]+", re.MULTILINE)
        regex2 = re.compile(r"[\s\n]+$", re.MULTILINE)                  
        for iptc_code_node in enamex_node.getElementsByTagName('iptc_code'):
            try: nb = int(iptc_code_node.getAttribute('nb'))
            except: nb = 0
            if nb > max: max = nb
            # iptc_code = convertIptcCodes(iptc_code_node.firstChild.data) # codes should already be converted to Topic strings
            # CLEAN BLANK LINES FROM NODE DATA         
            temp = iptc_code_node.getAttribute('value')
            temp_ = re.sub(regex1, '', temp)
            iptc_code = re.sub(regex2, '', temp_)
            if not re.search('null|NULL', iptc_code):
                iptc_subject_instance = "IPTCSubject_"+iptc_code
                if not self.topics.has_key(iptc_subject_instance):
                    self.topics[iptc_subject_instance] = nb
                if max == nb: self.max_iptc_subject_instance = iptc_subject_instance

        self.known = enamex_node.getAttribute('known')
        if self.known == "true":
            # GET INFO AND HREF IN NE.DAT
            # SXPIPE/NP DATABASE SCHEMA: data(key INTEGER,name,type,weight,def,link)
            if self.type == "Location":
                query = """select def from data where key = %s""" %(str(self.eid))
                cursor.execute(query)
                try: self.uri = cursor.fetchone()[0]
                except: self.uri = "null" # AN URI WILL BE ADDED FOR LOCATIONS
                self.descr = None
                # TO BE COMING
                # query = """select long, lat, zoom from data where key = %s""" %(str(self.eid))
                # cursor.execute(query)
                # try: (self.long, self.lat, self.zoom) = cursor.fetchone()
                # except: (self.long, self.lat, self.zoom) = (None, None, None)
                # AJOUTER RELATIONS VILLES/CAPITALES/PAYS
            else:
                query = """select def, link from data where key = %s""" %(str(self.eid))
                cursor.execute(query)
                infos = cursor.fetchone()
                try:
                    descr = infos[0]
                    # name='naissance'
                    pattern = re.compile(r"\s*name='([^']+)'")
                    descr = pattern.sub(r"\1", descr)                    
                    descr = re.sub("&lt;\S+", "", descr)
                    descr = re.sub("\/?&gt; *", "", descr)
                    self.descr = descr
                except Exception, error:
                    sys.stderr.write(error.args[0]+"\n")
                    self.descr = "null"
                try:
                    link = infos[1].encode('utf-8')
                    link_node = parseString(link).documentElement 
                    self.uri = link_node.getAttribute('href')
                except:
                    self.uri = "null"
            self.status = "candidate"
            query = """select weight from data where key=%s"""%(str(self.eid))
            cursor.execute(query)
            try: self.weight = cursor.fetchone()[0]
            except: self.weight = 0.0
        elif self.known == "false":
            # NO INFO NOR HREF TO GET
            # SET REFERENTIAL STATUS ATTRIBUTE AND VALUE TO "CANDIDATE"
            self.status = "incomplete"
            self.uri = None
            self.descr = None
            self.weight = None
            
    def display(self):
        """Display self as XML node
        """
        print self.dom.toxml()
        raw_input()

    def getName(self):
        return self.name

    def xml2owl(self):
        """Transform Entity XML node into OWL instance
        """
        if self.name != "":
            dom = xml.dom.minidom.Document()
            ontology_namespace = "http://www.semanticweb.org/ontologies/2010/2/AMO.owl"        
            owl_instance_node = createElementNode(dom, 'owl:Thing', {'rdf:about':self.display_id})
            owl_instance_node.appendChild(createElementNode(dom, 'rdf:type', {'rdf:resource':ontology_namespace+'#'+self.type}))
            owl_instance_node.appendChild(createElementNode(dom, 'AMO:hasReferentialID', {'rdf:datatype':'http://www.w3.org/2001/XMLSchema#ID'}, self.referential_id))
            owl_instance_node.appendChild(createElementNode(dom, 'AMO:hasNormalName', {'rdf:datatype':'http://www.w3.org/2001/XMLSchema#string'}, self.name))        
            for occ in self.occs:
                owl_instance_node.appendChild(createElementNode(dom, 'AMO:hasLabel', {'rdf:datatype':'http://www.w3.org/2001/XMLSchema#string'}, occ))
            owl_instance_node.appendChild(createElementNode(dom, 'AMO:hasReferenceFrequency', {'rdf:datatype':'http://www.w3.org/2001/XMLSchema#integer'}, self.total))
            if self.weight != None:
                owl_instance_node.appendChild(createElementNode(dom, 'AMO:hasReferenceWeight', {'rdf:datatype':'http://www.w3.org/2001/XMLSchema#integer'}, str(self.weight)))
            if self.uri != None:
                owl_instance_node.appendChild(createElementNode(dom, 'AMO:hasURI', {'rdf:datatype':'http://www.w3.org/2001/XMLSchema#anyURI'}, self.uri))
            if self.descr != None:
                owl_instance_node.appendChild(createElementNode(dom, 'AMO:hasShortDescription', {'rdf:datatype':'http://www.w3.org/2001/XMLSchema#string'}, self.descr))
            if self.status != None:
                owl_instance_node.appendChild(createElementNode(dom, 'AMO:hasReferentialStatus', {}, self.status))
            owl_instance_node.appendChild(createElementNode(dom, 'AMO:hasReferentialStatusChangeLog', {}, ""))
            for topic in self.topics.keys():
                if topic != self.max_iptc_subject_instance:
                    owl_instance_node.appendChild(createElementNode(dom, 'AMO:hasTheme', {'rdf:resource':ontology_namespace+'#'+topic}, None))
                else:
                    owl_instance_node.appendChild(createElementNode(dom, 'AMO:hasMainTheme', {'rdf:resource':ontology_namespace+'#'+topic}, None))
            if self.gender != 'null':
                owl_instance_node.appendChild(createElementNode(dom, 'AMO:hasGender', {'rdf:datatype':'http://www.w3.org/2001/XMLSchema#string'}, self.gender))
            return owl_instance_node, dom
        else:
            return None



def main():
    options, args = getOptions()
    # WRITE INSTANCES FILE ONLY - DON'T LOAD ONTOLOGY
    # ontology_dom = parse(options['ontology'])
    # sys.stderr.write('Got OWL dom\n')
    # ontology_root = ontology_dom.documentElement    
    # instances = {}
    # OWL KB IS SUPPOSED TO BE EMPTY (INITIAL ACQUISITION)
    # for uri_node in ontology_dom.getElementsByTagName('hasURI'):
    #     uri = getData(uri_node)
    #     instances[uri] = True
    # max_id = len(ontology_dom.getElementsByTagName('hasReferentialID'))
    # Entity.id_creator.setMaxID(max_id)

    # CREATE DOM FOR INSTANCES FILE
    # impl = getDOMImplementation()
    # rdf_dom = xml.dom.minidom.Document()
    # rdf_root_node = rdf_dom.createElementNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "rdf:RDF")    
    # addAttribute2Element(rdf_dom, rdf_root_node, 'xmlns:rdf', "http://www.w3.org/1999/02/22-rdf-syntax-ns#")
    # addAttribute2Element(rdf_dom, rdf_root_node, 'xmlns:owl', "http://www.w3.org/2002/07/owl#")
    # addAttribute2Element(rdf_dom, rdf_root_node, 'xmlns:AMO', "http://www.semanticweb.org/ontologies/2010/2/AMO.owl#")    
    
    # rdf_dom.appendChild(rdf_root_node)
    
    # rdf_dom = impl.createDocument(None, 'RDF', None)
    # rdf_root_node = rdf_dom.documentElement    

    owl_doc_header = '<?xml version="1.0" ?>\n'
    owl_doc_header += '<rdf:RDF xmlns:AMO="http://www.semanticweb.org/ontologies/2010/2/AMO.owl#" xmlns:owl="http://www.w3.org/2002/07/owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">\n'

    # OPEN CONNEXION TO SXPIPE/NP DATABASE TO GET SOME INFOS ON KNOWN ENTITIES WHEN DOING ENTITIES OBJECTS
    connexion = sqlite3.connect(options.db)
    connexion.row_factory = sqlite3.Row
    cursor = connexion.cursor()    

    owl_output_file = options.owl_output_file
    if options.split == None:
        G = codecs.open(owl_output_file, 'w', 'UTF-8')
        G.write(owl_doc_header)
    else:
        string_to_write = ""
        owl_output_file = re.sub('\.owl$', '', owl_output_file)
        cpt_files = 0
    cpt_split_enamex = 0        

    # ENAMEX XML FILE
    # TOO BIG TO PARSE DOM
    sys.stderr.write('Reading %s...\n' %(args[0]))
    F = codecs.open(args[0], 'r', 'utf-8')
    string_to_parse = ''
    in_enamex = False
    cpt_enamex = 0
    cpt_all_enamex = 0
    for line in F:
        if re.match('\s*<ENAMEX', line):
            in_enamex = True
            string_to_parse = line
        elif re.match('\s*</ENAMEX', line) and in_enamex:
            string_to_parse += line;
            in_enamex = False
            # print string_to_parse; raw_input()
            enamex_dom = parseString(string_to_parse.encode('utf-8'))   
            enamex_node = enamex_dom.documentElement
            # href = enamex_node.getElementsByTagName('a')[0].getAttribute('href')
            # if not int(enamex_node.getAttribute('eid')) in valid_ids:
            #     string_to_parse = ''
            #     continue
            total = int(enamex_node.getElementsByTagName('total')[0].getAttribute('value'))
            if (options.frequencymin != None and total >= options.frequencymin) or (options.frequencymax != None and total <= options.frequencymax) or (options.frequencymin == None and options.frequencymax == None):
                # printerr("Entity has a frequency of "+str(total)+", above -f set to "+str(options['frequency']))
                instance = Entity(enamex_node, cursor)
                instance.display(); raw_input()
                # owl_instance_node = instance.xml2owl(rdf_dom)
                # rdf_root_node.appendChild(owl_instance_node)
                try: owl_instance_node, owl_instance_dom = instance.xml2owl()
                except Exception, error: printerr("Did not convert "+enamex_node.getElementsByTagName('name')[0].getAttribute('value')+" ("+error.args[0]+")"); continue
                owl_instance_string = node2string(owl_instance_node)
                owl_instance_dom.unlink()
                if options.split == None:
                    cpt_enamex += 1
                    cpt_all_enamex += 1                                        
                    if options.limit and cpt_all_enamex > options.limit:
                        break
                    G.write(owl_instance_string)                    
                else:
                    cpt_enamex += 1
                    cpt_all_enamex += 1                                        
                    if options.limit and cpt_all_enamex > options.limit:
                        break
                    string_to_write += owl_instance_string
                sys.stderr.write(' '+str(cpt_all_enamex)+'\b'*(len(str(cpt_all_enamex))+1))
                if options.split != None and cpt_enamex == options.split:
                    # printerr(str(cpt_enamex)+" enamex seen")
                    cpt_files += 1
                    G = codecs.open(owl_output_file+"_"+str(cpt_files)+".owl", 'w', 'UTF-8')
                    G.write(owl_doc_header)
                    G.write(string_to_write)
                    G.write('</rdf:RDF>\n')
                    G.close()
                    sys.stderr.write('\n%s OWL instances written in file: %s\n'%(str(cpt_enamex), owl_output_file+"_"+str(cpt_files)+".owl"))
                    string_to_write = ""
                    cpt_enamex = 0
        elif in_enamex:
            string_to_parse += line
    F.close()
    cursor.close()
    # sys.stderr.write('Finished reading %s\n' %(args[0]))
    if options.split != None and string_to_write != "":
        cpt_files += 1
        G = codecs.open(owl_output_file+"_"+str(cpt_files)+".owl", 'w', 'UTF-8')
        G.write(owl_doc_header)
        G.write(string_to_write)
        G.write('</rdf:RDF>\n')
        G.close()
        sys.stderr.write('%s OWL instances written in %s\n'%(str(cpt_enamex), owl_output_file+"_"+str(cpt_files)+".owl"))
        string_to_write = ""

    if options.split == None:
        G.write('</rdf:RDF>\n')
        G.close()
        sys.stderr.write('%s OWL instances written in %s\n'%(str(cpt_all_enamex), owl_output_file))
        
    if cpt_all_enamex > 0:
        sys.stderr.write(str(cpt_all_enamex) + ' entities converted from XML-Enamex to OWL instances\n')
    else:
        if options.split == None:
            G.close()
        os.unlink(owl_output_file)
        sys.exit('No entities converted from XML-Enamex to OWL instances\n')
        
    #sys.stderr.write('Start writing %s\n' %(options['owl_output_file']))

    # ontology_dom.writexml(F, indent="", addindent="\t")
    # F.write(ontology_dom.toxml())
    #G.write(rdf_dom.toprettyxml())

    # sys.stderr.write('Cleaning format...\n')    
    # H = codecs.open(owl_file, 'w', 'UTF-8')
    # cat_infile = Popen(["cat", temp_file], stdout=PIPE)
    # cleaner = Popen(["formatOwl.pl"], stdin=cat_infile.stdout, stdout=PIPE)
    # clean_owl_output = cleaner.communicate()[0]
    # H.write(clean_owl_output.decode('utf-8'))
    # H.close()
                                                            
    

def node2string(node):
    # SPECIAL FOR OWL INSTANCE NODE (FOR NOW)
    pretty_xml_string = "\t<"+node.tagName+" "
    attributes = node.attributes
    for i in range(attributes.length):
        attribute = attributes.item(i)
        pretty_xml_string += attribute.name+'="'+attribute.value
    pretty_xml_string += '">\n'
    children = node.childNodes
    for child in children:
        data = getData(child)
        pretty_xml_string += "\t\t<"+child.tagName
        attributes = child.attributes
        for i in range(attributes.length):
            attribute = attributes.item(i)
            pretty_xml_string += " "+ attribute.name+'="'+attribute.value+'"'
        if data == None:
            pretty_xml_string += "/>\n"
        else:
            pretty_xml_string += ">"+data+"</"+child.tagName+">\n"
    pretty_xml_string += "\t</"+node.tagName+">\n"
    return pretty_xml_string
            
def getOptions():
    usage = '\n\n\t%prog [options] <enamex file>\n'
    parser = OptionParser(usage)
    # parser.add_option("--onto", action="store", dest="ontology", default=None, help="OWL ontology (empty) file")
    parser.add_option("--out", "-o", action="store", dest="owl_output_file", default=None, help="OWL output file")
    parser.add_option("--limit", action="store", dest="limit", type="int", default=None, help="Stop converting after n entities")
    parser.add_option("--split", action="store", dest="split", type="int", default=None, help="Split in files with max n instances")    
    parser.add_option("--fmin", action="store", dest="frequencymin", type="int", default=None, help="Convert only entities with frequency over n")
    parser.add_option("--fmax", action="store", dest="frequencymax", type="int", default=None, help="Convert only entities with frequency under n")        
    parser.add_option("--db", action="store", dest="db", default="/usr/local/lib/aleda/ne_refs.dat", help="Path to data base to get infos on known entities")
    # parser.add_option("--valid", action="store", dest="valid", default=None, help="Path to data base to get AFP validated entities")      
    try: (options, args) = parser.parse_args()
    except OptionError: parser.print_help();sys.exit('OptionError')
    except: parser.print_help();sys.exit('An error occured during options parsing\n')
    if len(args) < 1: parser.print_help();sys.exit()
    # if opts.ontology == None: parser.print_help();sys.exit('OWL ontology input file must be specified by option --onto')
    if options.owl_output_file == None: parser.print_help();sys.exit('OWL output file must be specified by option --out')    
    # options = {'ontology':opts.ontology, 'owl_output_file':opts.owl_output_file}
    # options = {'owl_output_file':opts.owl_output_file, 'limit':opts.limit, 'db':opts.db, 'frequency':opts.frequency, 'split':opts.split}
    return options, args

if __name__ == "__main__":
    main()    


# class ID_creator:
#     def __init__(self, max_id=0):
#         self.counter = max_id

#     def setMaxID(self, max_id):
#         self.counter = max_id

#     def createNewID(self):
#         self.counter += 1
#         # REFERENTIAL ID CONVENTION IN AMO: AFP_AMO_000000000
#         prefix = '0' * (9-len(str(self.counter)))
#         new_id = 'AFP_AMO_' + prefix + str(self.counter)
#         return new_id
