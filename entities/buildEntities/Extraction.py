#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
import re
import time
import collections
from xml.dom.minidom import *
from optparse import *
from subprocess import *
import codecs
from operator import *
import commands
sys.path.append('@common_src@')
from utils import *

class Doc:
    docs = {}
    def __init__(self, doc_node, doc_id):
        self.id = doc_id
        self.node = doc_node
        Doc.docs[self.id] = self
        self.extractions = {}

    def display(self):
        print >> sys.stderr, "Document: %s - ID %s"%(self.__class__.__name__, self.id)

    def addWords(self, tag):
        self.words2count = collections.defaultdict(int)
        if tag == 'form':
            words = map((lambda form_node: form_node.getAttribute('value')), self.node.getElementsByTagName('form'))
        elif tag == 'token':
            words = map((lambda token_node: getNodeData(token_node)), self.node.getElementsByTagName('token'))
        for word in words:
            word = re.sub('^\s+', '', word)
            word = re.sub('\s+$', '', word)
            if acceptWordInDB(word, 3):
                self.words2count[fixStringForDB(word)] += 1
                if not Word.words.has_key(fixStringForDB(word)):
                    word_instance = Word(fixStringForDB(word))
                else:
                    word_instance = Word.words[fixStringForDB(word)]
                word_instance.addOccurrence(1)

    def getExtractions(self, entity_tag, attribute_name, aleda=None):
       pass
   
class News(Doc):        
    def __init__(self, news_node):
        Doc.__init__(self, news_node, news_node.getElementsByTagName('news_item')[0].getAttribute('ref'))
        self.type = "news"

    def getExtractions(self, entity_tag, attribute_name, aleda=None):
        nodes = self.node.getElementsByTagName(entity_tag)
        extraction_ids2nodes = zip(map((lambda node: fixStringForDB(getExtractionId(node, attribute_name))), nodes), nodes)
        for (extraction_id, node) in extraction_ids2nodes:
            if not Extraction.instances[entity_tag].has_key(extraction_id):
                extraction = Entity(node, extraction_id)
                extraction.recordExtraction()
            else:
                extraction = Extraction.instances[entity_tag][extraction_id]
            if not self.extractions.has_key(extraction_id):
                self.extractions[extraction_id] = True
            extraction.addOccurrence(getNodeData(node))
            extraction.addOccInDoc(self.id)
            extraction.addDocData(self)
        
class Article(Doc):        
    def __init__(self, article_node):
        Doc.__init__(self, article_node, article_node.getElementsByTagName('article')[0].getAttribute('id'))
        self.type = "article"

    def getExtractions(self, entity_tag, attribute_name, aleda=None):
        nodes2mentions = selectWL(filter((lambda entity_node: isValidWl(entity_node, attribute_name, aleda)), self.node.getElementsByTagName(entity_tag)))
        # extraction_ids2nodes = zip(map((lambda node: fixStringForDB(getExtractionId(node, attribute_name))), nodes), nodes)
        # for (extraction_id, node) in extraction_ids2nodes:
        for node, mentions in nodes2mentions.items():
            extraction_id = re.sub('^ +', '', re.sub(' +$', '', node.getAttribute(attribute_name)))
            if not Extraction.instances[entity_tag].has_key(extraction_id):
                extraction = Wlink(node, extraction_id, aleda)
                extraction.recordExtraction()
            else:
                extraction = Extraction.instances[entity_tag][extraction_id]
            for mention in mentions:
                extraction.addOccurrence(mention)
                extraction.addOccInDoc(self.id)
            self.extractions[extraction_id] = True
            extraction.addDocData(self)

def selectWL(nodes):
    # CHECK THAT ALL MENTIONS ARE CAPITALIZED
    selected_nodes = collections.defaultdict(list)
    nodes2mentions = collections.defaultdict(list)
    for node in nodes:
        nodes2mentions[node].append(getNodeData(node))
    for node, mentions in nodes2mentions.items():
        flag = True
        for mention in mentions:
            if re.match('^[A-ZÀÂÉÈÊËIÎÏOÔÖUÛÜÙÇ]', re.sub('^ +', '', re.sub(' +$', '', mention))) == None:
                flag = False; break
        if flag: selected_nodes[node] = mentions
    return selected_nodes
                            

class Word:
    words = {}
    def __init__(self, word):
        self.count = 0
        Word.words[word] = self
    def addOccurrence(self, count):
        self.count += count

class Extraction:
    instances = collections.defaultdict(dict)
    def __init__(self, node, param):
        self.id = param
        self.attributes = {}
        self.tag_name = node.tagName
        self.total = 0
        self.data = {}
        self.data_types = ['occurrences', 'docs']
        self.data_types = self.setDataTypes()
        self.addAttributes(node, {'id':fixIdToString(self.id)})

    def setDataTypes(self):
        for data_type in self.data_types:
            self.data[data_type] = collections.defaultdict(int)

    def addAttributes(self, node, additional_attributes):
        attributes_list = map( (lambda attribute: attribute.name), [node.attributes.item(i) for i in range(node.attributes.length)] )
        for attribute in attributes_list:
            value = node.getAttribute(attribute)
            self.attributes[attribute] = value
        for attribute, value in additional_attributes.items():
            self.attributes[attribute] = value

    def recordExtraction(self):
        Extraction.instances[self.tag_name][self.id] = self

    def addOccInDoc(self, doc_id):
        # doc_id = doc_node.getElementsByTagName('news_item')[0].getAttribute('ref')
        self.data['docs'][doc_id] += 1

    def addDocData(self, doc_instance):
        pass

    def addOccurrence(self, data):
        self.data['occurrences'][data] += 1
        self.total += 1

    def addPreviousData(self, data_type, data, count):
        self.data[data_type][data] += count

    def addPreviousTotal(self, frequency):    
        self.total += frequency
        
    def display(self):
        print >> sys.stderr, "ID: "+fixIdToString(self.id)
        print >> sys.stderr, self.attributes
        for data_type in Extraction.data_types:
            print >> sys.stderr, data_type+': '+", ".join(self.data[data_type].keys())
        # print >> sys.stderr, self.data['news'].keys()
        # print >> sys.stderr, self.data['dates'].keys()
        # print >> sys.stderr, self.data['iptc'].keys()
        # print >> sys.stderr, self.data['slugs'].keys()
        raw_input()
        
    def toXmlNode(self, dom):
        new_node = createElementNode(dom, self.tag_name, self.attributes, None)
        addAttribute2Element(dom, new_node, 'total', str(self.total))
        if self.tag_name == 'ENAMEX':
            for occ, count in self.data['occurrences'].items():
                new_node.appendChild(createElementNode(dom, 'occurrence', {'count':str(count), 'value':occ}, None))
            for theme_code, count in self.data['themes'].items():
                new_node.appendChild(createElementNode(dom, 'theme_code', {'count':str(count), 'value':theme_code}, None))
            for slug, count in self.data['keywords'].items():
                new_node.appendChild(createElementNode(dom, 'slug', {'value':slug, 'count':str(count)}, None))
            for date, count in self.data['dates'].items():
                new_node.appendChild(createElementNode(dom, 'date', {'value':date, 'count':str(count)}, None))
            for doc_id, count in self.data['docs'].items():
                new_node.appendChild(createElementNode(dom, 'news', {'value':doc_id, 'count':str(count)}, None))
        return new_node

class PreviousExtraction(Extraction):
    def __init__(self, param, total, tag_name, aleda=None):
        self.id = param
        self.total = total
        self.tag_name = tag_name
        self.data = {}
        self.data_types = ['occurrences', 'docs']
        self.data_types = self.setDataTypes()
        self.attributes = {}
        #self.addAttributes({'id':fixIdToString(self.id)})
        self.setKnown(aleda)

    def setKnown(self, aleda=None):
        if self.tag_name == 'ENAMEX':
            if isinstance(self.id, int): self.attributes['known'] = 1
            else: self.attributes['known'] = 0
        elif self.tag_name == 'nicewl':
            if aleda.has_key(self.id): self.attributes['known'] = 1
            else: self.attributes['known'] = 0

    def setDataTypes(self):
        if self.tag_name == 'ENAMEX':
            self.data_types.extend(['dates', 'keywords', 'themes'])
        elif self.tag_name == 'nicewl':
            self.data_types.extend(['categories', 'slugs'])
        for data_type in self.data_types:
            self.data[data_type] = collections.defaultdict(int)

    def addAttributes(self, additional_attributes):
        for attribute, value in additional_attributes.items():
            self.attributes[attribute] = value
        
class Entity(Extraction):
    def __init__(self, node, entity_id):
        try: entity_id = int(entity_id)
        except ValueError: pass
        Extraction.__init__(self, node, entity_id)
        self.attributes['known'] = 'true'
        if self.tag_name == 'ENAMEX':
            if isinstance(self.id, int): self.attributes['known'] = 'true'
            else: self.attributes['known'] = 'false'

    def setDataTypes(self):
        self.data_types.extend(['dates', 'keywords', 'themes'])
        for data_type in self.data_types:
            self.data[data_type] = collections.defaultdict(int)

    def addDocData(self, doc_instance):
        # news item id = urn:newsml:afp.com:20101115T182546Z:TX-PAR-ELV39:1 or urn:newsml:afp.com:20090101:TX-CAFP-EAER10-135340-048:1
        # news_id = news_node.getElementsByTagName('news_item')[0].getAttribute('ref')
        if doc_instance.type == 'news':
            date = re.sub(":.+$", "", re.sub("T\d{6}Z", "", re.sub("^urn:newsml:afp.com:", "", doc_instance.id)))
            self.data['dates'][date] += 1
            for iptc_code in map( (lambda iptc_node: iptc_node.getAttribute('value')), doc_instance.node.getElementsByTagName('iptc_code') ):
                self.data['themes'][iptc_code] += 1
            for slug in doc_instance.node.getElementsByTagName('news_slugs')[0].getAttribute('text').split('-'):
                self.data['keywords'][slug] += 1

class Wlink(Entity):
    def __init__(self, node, entity_id, aleda=None):
        Extraction.__init__(self, node, entity_id)
        if aleda != None and aleda.has_key(entity_id):
            self.attributes['known'] = 1
        else:
            self.attributes['known'] = 0

    def setDataTypes(self):
        self.data_types.extend(['categories', 'slugs'])
        for data_type in self.data_types:
            self.data[data_type] = collections.defaultdict(int)

    def addDocData(self, doc_instance):
        for category in filter((lambda cat: cat != 'null'), map((lambda node: node.getAttribute('normalized_title')), doc_instance.node.getElementsByTagName('category'))):
            self.data['categories'][category] += 1
        for slug in filter((lambda s: s!= 'null'), map((lambda node: node.getAttribute('afp_slug')), doc_instance.node.getElementsByTagName('category'))):
            self.data['slugs'][slug] += 1


def fixIdToString(param):
    if isinstance(param, int): return str(param)
    elif isinstance(param, str) or isinstance(param, unicode): return param
    else: return param
    
def populateExtractionsFromCorpus(file, corpustype, entity_tag, attribute_name, get_words_with_tag=None, aleda=None):
    docroot = corpustype2docroot(corpustype)
    F = codecs.open(file, "r", "utf-8")
    in_doc = False
    string = ""
    lines = 0
    docs = 0
    message = 'File not parsed\n'
    for line in F:
        lines += 1
        if re.match("^\s*<%s"%(docroot), line):
            in_doc = True
            string += line
        elif re.match("^\s*</%s"%(docroot), line):
            in_doc = False
            string += line
            try:
                doc_node = parseString(string.encode('utf-8'))
            except Exception, error:
                sys.stderr.write("Parsing doc failed (line %d)\n"%(lines))
                print >> sys.stderr, error
                string = ""
                continue
            docs += 1
            message = ' '+str(docs)+' docs handled'
            # sys.stderr.write(message+'\r'*(len(str(message))+1))
            if corpustype == 'news' or corpustype == 'afp':
                doc_instance = News(doc_node)
            elif corpustype == 'wikipedia' or corpustype == 'wp':
                doc_instance = Article(doc_node)
            else: #if doctype == 'twit':
                doc_instance = Doc(doc_node, str(docs))
            # doc_instance.display()
            doc_instance.getExtractions(entity_tag, attribute_name, aleda)
            if get_words_with_tag:
                doc_instance.addWords(get_words_with_tag)
            doc_node.unlink()
            string = ""
        elif in_doc:
            string += line
    F.close()
    sys.stderr.write(message+'\n')

def getExtractionId(node, attribute_name):
    if not node.hasAttribute(attribute_name):
        sys.exit("Can't continue: getExtractionId can't find attribute %s in node %s\n"%(attribute, node.tagName))
    extraction_id = node.getAttribute(attribute_name)
    if node.tagName == 'ENAMEX':
        if extraction_id == 'null':
            extraction_id = node.getAttribute('name')
        else:
            extraction_id = int(extraction_id)
    elif node.tagName == 'nicewl':
        extraction_id = re.sub('^ +', '', re.sub(' +$', '', extraction_id))
    return extraction_id

def corpustype2docroot(corpustype):
    t = {'news':'news_item', 'afp':'news_item', 'twit':'twit', 'wikipedia':'article', 'wp':'article'}
    return t[corpustype]


def isValidWl(node, attribute_name, aleda=None):
    # if starts with Cap, if second word starts with cap if more than one word
    title = node.getAttribute(attribute_name)
    data = getNodeData(node)
    if aleda:
        if aleda.has_key(title): return True
    title = re.sub('^ +', '', re.sub(' +$', '', title))
    title_split = title.split(' ')
    # (ALL CAPS) OR (STARTS WITH CAP AND HAS ONE WORD) OR (STARTS WITH CAP AND MULTI WORD AND ALL WORDS START WITH CAP)
    if re.match('^[A-ZÀÂÉÈÊËIÎÏOÔÖUÛÜÙÇ]+$', title) or (re.match('^[A-ZÀÂÉÈÊËIÎÏOÔÖUÛÜÙÇ]+', title) and len(title_split) == 1) or (re.match('^[A-ZÀÂÉÈÊËIÎÏOÔÖUÛÜÙÇ]', title) and len(title_split) > 1 and allWordsCap(title)):
        return True
    return False

def allWordsCap(string):
    parts = filter((lambda part: re.match('^(du|de|da|del|delle|della)$', part) == None), string.split(' '))
    for part in parts:
        if not re.match('^[A-ZÀÂÉÈÊËIÎÏOÔÖUÛÜÙÇ]', part): return False
    return True

# Bunescu & Pasca:
# 1. If multiword: all cap => entity
# 2. If one word: at least two caps => entity; else go to 3
# 3. If at least 75% of wl data for this title is cap => entity
