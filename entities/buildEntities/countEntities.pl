#!/usr/bin/perl -w

use strict;

my $outputdir = shift or die "Provide directory with entities files\n";
$outputdir .= "/xml/";
my %table = ();
########## ALL
my $allkno = `grep -c "<ENAMEX" $outputdir/enamex.ALL.kno.xml`;
my $allunk = `grep -c "<ENAMEX" $outputdir/enamex.ALL.unk.xml`;
chomp($allkno);
chomp($allunk);
my $allrefs = $allkno + $allunk;
my @allmentionskno = `grep "<total" $outputdir/enamex.ALL.kno.xml`;
my $allmentionskno = 0;
foreach my $total(@allmentionskno)
  {
    $total =~ s/<total value="(.+?)"\/>/$1/;
    $allmentionskno += $total;
  }
@allmentionskno = ();
my @allmentionsunk = `grep "<total" $outputdir/enamex.ALL.unk.xml`;
my $allmentionsunk = 0;
foreach my $total(@allmentionsunk)
  {
    $total =~ s/<total value="(.+?)"\/>/$1/;
    $allmentionsunk += $total;
  }
@allmentionsunk = ();
my $allmentions = $allmentionskno + $allmentionsunk;
$table{'totalmentions'}{'All'} = $allmentions;
$table{'knomentions'}{'All'} = $allmentionskno;
$table{'unkmentions'}{'All'} = $allmentionsunk;
$table{'totalrefs'}{'All'} = $allrefs;
$table{'knorefs'}{'All'} = $allkno;
$table{'unkrefs'}{'All'} = $allunk;
########## PER
my $perkno = `grep -c "<ENAMEX" $outputdir/enamex.Person.kno.xml`;
my $perunk = `grep -c "<ENAMEX" $outputdir/enamex.Person.unk.xml`;
chomp($perkno);
chomp($perunk);
my $perrefs = $perkno + $perunk;
my @permentionskno = `grep "<total" $outputdir/enamex.Person.kno.xml`;
my $permentionskno = 0;
foreach my $total(@permentionskno)
  {
    $total =~ s/<total value="(.+?)"\/>/$1/;
    $permentionskno += $total;
  }
@permentionskno = ();
my @permentionsunk = `grep "<total" $outputdir/enamex.Person.unk.xml`;
my $permentionsunk = 0;
foreach my $total(@permentionsunk)
  {
    $total =~ s/<total value="(.+?)"\/>/$1/;
    $permentionsunk += $total;
  }
@permentionsunk = ();
my $permentions = $permentionskno + $permentionsunk;
$table{'totalmentions'}{'Per'} = $permentions;
$table{'knomentions'}{'Per'} = $permentionskno;
$table{'unkmentions'}{'Per'} = $permentionsunk;
$table{'totalrefs'}{'Per'} = $perrefs;
$table{'knorefs'}{'Per'} = $perkno;
$table{'unkrefs'}{'Per'} = $perunk;
########## ORG
my $orgkno = `grep -c "<ENAMEX" $outputdir/enamex.Organization.kno.xml`;
my $orgunk = `grep -c "<ENAMEX" $outputdir/enamex.Organization.unk.xml`;
chomp($orgkno);
chomp($orgunk);
my $orgrefs = $orgkno + $orgunk;
my @orgmentionskno = `grep "<total" $outputdir/enamex.Organization.kno.xml`;
my $orgmentionskno = 0;
foreach my $total(@orgmentionskno)
  {
    $total =~ s/<total value="(.+?)"\/>/$1/;
    $orgmentionskno += $total;
  }
@orgmentionskno = ();
my @orgmentionsunk = `grep "<total" $outputdir/enamex.Organization.unk.xml`;
my $orgmentionsunk = 0;
foreach my $total(@orgmentionsunk)
  {
    $total =~ s/<total value="(.+?)"\/>/$1/;
    $orgmentionsunk += $total;
  }
@orgmentionsunk = ();
my $orgmentions = $orgmentionskno + $orgmentionsunk;
$table{'totalmentions'}{'Org'} = $orgmentions;
$table{'knomentions'}{'Org'} = $orgmentionskno;
$table{'unkmentions'}{'Org'} = $orgmentionsunk;
$table{'totalrefs'}{'Org'} = $orgrefs;
$table{'knorefs'}{'Org'} = $orgkno;
$table{'unkrefs'}{'Org'} = $orgunk;
########## COM
my $comkno = `grep -c "<ENAMEX" $outputdir/enamex.Company.kno.xml`;
my $comunk = `grep -c "<ENAMEX" $outputdir/enamex.Company.unk.xml`;
chomp($comkno);
chomp($comunk);
my $comrefs = $comkno + $comunk;
my @commentionskno = `grep "<total" $outputdir/enamex.Company.kno.xml`;
my $commentionskno = 0;
foreach my $total(@commentionskno)
  {
    $total =~ s/<total value="(.+?)"\/>/$1/;
    $commentionskno += $total;
  }
@commentionskno = ();
my @commentionsunk = `grep "<total" $outputdir/enamex.Company.unk.xml`;
my $commentionsunk = 0;
foreach my $total(@commentionsunk)
  {
    $total =~ s/<total value="(.+?)"\/>/$1/;
    $commentionsunk += $total;
  }
@commentionsunk = ();
my $commentions = $commentionskno + $commentionsunk;
$table{'totalmentions'}{'Com'} = $commentions;
$table{'knomentions'}{'Com'} = $commentionskno;
$table{'unkmentions'}{'Com'} = $commentionsunk;
$table{'totalrefs'}{'Com'} = $comrefs;
$table{'knorefs'}{'Com'} = $comkno;
$table{'unkrefs'}{'Com'} = $comunk;
########## LOC
my $lockno = `grep -c "<ENAMEX" $outputdir/enamex.Location.kno.xml`;
my $locunk = `grep -c "<ENAMEX" $outputdir/enamex.Location.unk.xml`;
chomp($lockno);
chomp($locunk);
my $locrefs = $lockno + $locunk;
my @locmentionskno = `grep "<total" $outputdir/enamex.Location.kno.xml`;
my $locmentionskno = 0;
foreach my $total(@locmentionskno)
  {
    $total =~ s/<total value="(.+?)"\/>/$1/;
    $locmentionskno += $total;
  }
@locmentionskno = ();
my @locmentionsunk = `grep "<total" $outputdir/enamex.Location.unk.xml`;
my $locmentionsunk = 0;
foreach my $total(@locmentionsunk)
  {
    $total =~ s/<total value="(.+?)"\/>/$1/;
    $locmentionsunk += $total;
  }
@locmentionsunk = ();
my $locmentions = $locmentionskno + $locmentionsunk;
$table{'totalmentions'}{'Loc'} = $locmentions;
$table{'knomentions'}{'Loc'} = $locmentionskno;
$table{'unkmentions'}{'Loc'} = $locmentionsunk;
$table{'totalrefs'}{'Loc'} = $locrefs;
$table{'knorefs'}{'Loc'} = $lockno;
$table{'unkrefs'}{'Loc'} = $locunk;
########## PRO
my $prokno = `grep -c "<ENAMEX" $outputdir/enamex.Product.kno.xml`;
my $prounk = `grep -c "<ENAMEX" $outputdir/enamex.Product.unk.xml`;
chomp($prokno);
chomp($prounk);
my $prorefs = $prokno + $prounk;
my @promentionskno = `grep "<total" $outputdir/enamex.Product.kno.xml`;
my $promentionskno = 0;
foreach my $total(@promentionskno)
  {
    $total =~ s/<total value="(.+?)"\/>/$1/;
    $promentionskno += $total;
  }
@promentionskno = ();
my @promentionsunk = `grep "<total" $outputdir/enamex.Product.unk.xml`;
my $promentionsunk = 0;
foreach my $total(@promentionsunk)
  {
    $total =~ s/<total value="(.+?)"\/>/$1/;
    $promentionsunk += $total;
  }
@promentionsunk = ();
my $promentions = $promentionskno + $promentionsunk;
$table{'totalmentions'}{'Pro'} = $promentions;
$table{'knomentions'}{'Pro'} = $promentionskno;
$table{'unkmentions'}{'Pro'} = $promentionsunk;
$table{'totalrefs'}{'Pro'} = $prorefs;
$table{'knorefs'}{'Pro'} = $prokno;
$table{'unkrefs'}{'Pro'} = $prounk;

########## DISPLAY

# |ENS-Mentions | ENs-Mentions KNO    | ENs-Mentions UNK | ENs-Refs | incl. KNO refs | incl. UNK refs |
# |-------------+---------------------+------------------+----------+----------------+----------------|
# |             |                     |      2691771     |   117126 |                |          80993 |
# |             |    -1.2             |      2695738     |   117126 |                |          80953 |
# |             |                -1.3 |      2695706     |   118139 |          36256 |          81883 |
# |             |                -2.1 |       455303     |    40469 |          17610 |          22859 |
# |             |                -3   |                  |   154152 |          45779 |         108373 |

my $delim = "-"x15;
my @types = qw(All Per Org Com Loc Pro);
foreach my $type(@types)
  {
    #printf("|%s+%s+%s+%s+%s+%s|\n", $delim, $delim, $delim, $delim, $delim, $delim);
    printf("|%-15s|%-15s|%-15s|%-15s|%-15s|%-15s|\n", "$type Mentions", "incl. KNO", "incl. UNK", "$type Refs", "incl. KNO", "incl. UNK");
    printf("|%s+%s+%s+%s+%s+%s|\n", $delim, $delim, $delim, $delim, $delim, $delim);
    printf("|%-15s|%-15s|%-15s|%-15s|%-15s|%-15s|\n", $table{'totalmentions'}{$type}, $table{'knomentions'}{$type}, $table{'unkmentions'}{$type}, $table{'totalrefs'}{$type}, $table{'knorefs'}{$type}, $table{'unkrefs'}{$type});
    print "\n";
  }
# printf("| %-15s | %-10s | %-10s | %-10s | %-10s | %-10s | %-10s | %-10s | %-10s |\n", "Retr.-Rel. Type", "Relevant", "Missed", "False", "Retr.", "Retr.-Rel.", "Precision", "Recall", "Fscore");
# printf("|%s+%s+%s+%s+%s+%s+%s+%s+%s|\n", "-----------------", $delim, $delim, $delim, $delim, $delim, $delim, $delim, $delim);
# printf("| %15s | %10d | %10d | %10d | %10d | %10d | %10f | %10f | %10f |", $task, $rel, $mis, $fm, $ret, $rr, $p, $r, $f);
