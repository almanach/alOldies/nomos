#!/usr/bin/perl -w
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

use strict;
use Encode;
use DBI;
use Getopt::Long;

my $refs_sth; my $refs_dbh;
my $vars_sth; my $vars_dbh;

my $lang = "fr";
GetOptions ('l:s' => \$lang,
	   'lang:s' => \$lang);

$refs_dbh = DBI->connect("dbi:SQLite:__aledalibdir__/ne_refs.".$lang.".dat", "", "", {RaiseError => 1, AutoCommit => 1});
$refs_sth = $refs_dbh->prepare('select link from data where key=?;');
$vars_dbh = DBI->connect("dbi:SQLite:__aledalibdir__/ne_vars.".$lang.".dat", "", "", {RaiseError => 1, AutoCommit => 1});
$vars_sth = $vars_dbh->prepare('select lastname from data where key=?;');

my %links = ();

while(<>)
  {
    if($_ !~ /<ENAMEX/){print; next;}
    # <ENAMEX type="Organization" known="true" eid="605786" name="Parti_socialiste" offsetStart="714" offsetEnd="716">PS</ENAMEX>
    my $temp = $_;
    my $line = "";
    my $before = "";
    my $after = "";
    # while($temp =~ /(<ENAMEX )([^>]+)(>[^<]+<\/ENAMEX>)/)
    while($temp =~ /(<ENAMEX )([^>]+)(>)/)#[^<]+<\/ENAMEX>)/)
      {
	$before = $`;
	$after = $';
	my $attr = $2;
	my $enamex = $1."#ENAMEX#".$3;
	$line .= $before;
	$temp = $after;
	# if(not($wiki)){
	if($attr =~ /eid="(\d+)"/)
	  {
	    my $eid = $1; my $link = "";
	    if(exists($links{$eid})){$link = $links{$eid};}
	    else
	      {
		$refs_sth->execute($eid);
		while(my @t = $refs_sth->fetchrow_array){$link = Encode::decode("utf8", $t[0]);}
		$refs_sth->finish;	
		# if(not($link)){$link = "not_found";}
		if(not($link)){$link = "";}
		else
		  {
		    if($link =~ /href="(.+?)"/ or $link =~ /^(http.+)$/)
		      {
			$link = $1;
			$link = &replaceXmlEntities($link);
			$link =~ s/ +/_/g;
		      }
		    else{$link = "";}
		    $links{$eid} = $link;
		  } 
	      }	    
	    if($link ne ""){$attr .= " href=\"".$link."\"";}
	    $attr =~ /type="([^"]+)"/; my $type = $1;
	    if($type eq 'Person')
	      {
		$vars_sth->execute($eid);
		while(my @t = $vars_sth->fetchrow_array)
		  {
		    my $lastname = Encode::decode("utf8", $t[0]);
		    if($lastname ne ""){$attr .= " last_name=\"$lastname\""; last;}
		  }
	      }
	  }
	# else{$attr .= " href=\"http://www.afp.com/medialab/unknown_entity\"";}
	# print $line;
	$enamex =~ s/#ENAMEX#/$attr/;
	# print STDERR $enamex."\n";
	$line .= $enamex;
      }
    $line .= $after;
    print $line;
  }

undef($refs_sth);
$refs_dbh->disconnect;
undef($vars_sth);
$vars_dbh->disconnect;

sub replaceXmlEntities
    {
      my $string = shift;
      $string =~ s/&amp;/;;amp;/g;
      $string =~ s/&apos;/;;apos;/g;
      $string =~ s/&quot;/;;quot;/g;
      $string =~ s/&/&amp;/g;
      $string =~ s/'/&apos;/g;
      $string =~ s/"/&quot;/g;
      $string =~ s/;;amp;/&amp;/g;
      $string =~ s/;;apos;/&apos;/g;
      $string =~ s/;;quot;/&quot;/g;
      return $string;
    }



__END__
}else
	  {
	    my $type = "";
	    if($attr =~ /type="Person/){$type = "Person";}
	    elsif($attr =~ /type="Location"/){$type = "Location";}
	    if($attr =~ /eid="(\d+)"/)
	      {
		my $eid = $1;
		my $wikiname = "";
		if(exists($wikinames{$eid}))
		  {
		    $wikiname = $wikinames{$eid};		 
		  }
		else
		  {
		    if($type eq "Person")
		      {
			$bio_sth->execute($eid);
			while(my @t = $bio_sth->fetchrow_array)
			  {
			    $wikiname = Encode::decode("utf8", $t[0]);
			  }
			$bio_sth->finish;	
			if(not($wikiname)){$wikiname = "not_found";}
		      }
		    elsif($type eq "Location")
		      {
			$loc_sth->execute($eid);
			while(my @t = $loc_sth->fetchrow_array)
			  {
			    $wikiname = Encode::decode("utf8", $t[0]);
			  }
			$loc_sth->finish;	
			if(not($wikiname)){$wikiname = "not_found";}
		      }
		  }	    
		$attr .= " wikiname=\"".$wikiname."\"";
	      }
	    else{$attr .= " wikiname=\"none\"";}	    
	  }



# else{
# $doc_dbh = DBI->connect("dbi:SQLite:@nomoslibdir@/entities/afp2aleda.dat", "", "", {RaiseError => 1, AutoCommit => 1});
# # OLD STYLE
# # ('create table bio (doc_id text, doc_name text, aleda_id integer, aleda_name text)')
# # NEW STYLE
# # ('create table bio (aleda_id integer, name text)')
# $bio_sth = $doc_dbh->prepare('select name from bio where aleda_id=?;');
# # ('create table pays (doc_name text, aleda_id integer, aleda_name text)')
# $loc_sth = $doc_dbh->prepare('select aleda_name from pays where aleda_id=?;');}


# GetOptions ('w' => \$wiki, 'p:s' => \$prefix);
# GetOptions ('w' => \$wiki);
# if($prefix eq ""){die "Use -p option to specify prefix of data base location\n";}

# my $doc_dbh; my $bio_sth; my $loc_sth;

# my %wikinames = ();
