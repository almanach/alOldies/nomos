#!/usr/bin/perl -w

# SOME DAG RECTIFICATIONS AFTER XSL STYLE SHEET NOMOS2DAGS_2

while(<>)
  {
    chomp;
    if(/ *_XML\s*$/){print $_."\n"; next;}
    s/([^\\])%/$1\\%/g;
    s/(\} *[^\\]*)(\+|\?|\*)/$1\\$2/g;
    s/({<F id="[^"]+">(?:-|\.)<\/F>} )(SENT_BOUND)/$1_META_TEXTUAL_PONCT $1$2/g;
    s/({<F id="[^"]+">\(<\/F><F id="[^"]+">\.\.\.<\/F><F id="[^"]+">\)<\/F>}) *(\S+)/$1 \\(...\\)/g;
    print $_."\n";
  }
