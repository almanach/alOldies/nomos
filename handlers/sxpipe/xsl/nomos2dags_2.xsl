<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    <xsl:output encoding="UTF-8" method="xml" indent="no" omit-xml-declaration="yes"/>
    
    <xsl:template match="/child::*">
<xsl:text>{</xsl:text><root><xsl:attribute name="ref"><xsl:value-of select="./module[@type='news_analytics']/meta/@doc_id"/></xsl:attribute><xsl:text>} _XML
{_PAR_BOUND} _PAR_BOUND</xsl:text>
<xsl:text>
</xsl:text>
<xsl:text>{</xsl:text><news_head>
    <xsl:attribute name="dateline"><xsl:value-of select="./module[@type='resolution']//news_head/@dateline"/></xsl:attribute>
    <xsl:attribute name="headline"><xsl:value-of select="./module[@type='resolution']//news_head/@headline"/></xsl:attribute>
</news_head><xsl:text>} _XML
{_PAR_BOUND} _PAR_BOUND
</xsl:text>
<xsl:apply-templates select="./module[@type='news_analytics']/salient_words"/>
<xsl:text>
</xsl:text>
<xsl:text>{</xsl:text><body.content><xsl:text>} _XML
{_PAR_BOUND} _PAR_BOUND</xsl:text>
<xsl:apply-templates select="./module[@type='resolution']"/>
<xsl:text>
</xsl:text>
<xsl:text>{</xsl:text></body.content><xsl:text>} _XML
{_PAR_BOUND} _PAR_BOUND</xsl:text>
<xsl:text>
</xsl:text>
<xsl:text>{</xsl:text></root><xsl:text>} _XML
{_PAR_BOUND} _PAR_BOUND</xsl:text>
    </xsl:template>
    
    <xsl:template match="salient_words">
<xsl:text>{</xsl:text><swords><xsl:text>} _XML
{_PAR_BOUND} _PAR_BOUND</xsl:text>
<xsl:text>
</xsl:text>
<xsl:apply-templates select="salient_word[position() &lt; 10]"/>
<xsl:text>{</xsl:text></swords><xsl:text>} _XML
{_PAR_BOUND} _PAR_BOUND</xsl:text>
    </xsl:template>
    
    <xsl:template match="salient_word">
<xsl:text>{</xsl:text><sword>
<xsl:attribute name="rank"><xsl:value-of select="./@rank"/></xsl:attribute>
<xsl:attribute name="value"><xsl:value-of select="./@value"/></xsl:attribute>
<xsl:attribute name="z"><xsl:value-of select="./@z"/></xsl:attribute>
</sword><xsl:text>} _XML
{_PAR_BOUND} _PAR_BOUND</xsl:text>
<xsl:text>
</xsl:text>
    </xsl:template>
    
    <xsl:template match="module[@type='resolution']">
<xsl:apply-templates select=".//para"/> 
    </xsl:template>
    
    <xsl:template match="module[@type='resolution']//para">
        <xsl:choose>
            <xsl:when test="./@signature">
<xsl:text>
</xsl:text>
<xsl:text>{</xsl:text><p><xsl:attribute name="signature"><xsl:value-of select="./@signature"/></xsl:attribute></p><xsl:text>} _XML
{_PAR_BOUND} _PAR_BOUND</xsl:text>
            </xsl:when>
            <xsl:otherwise>
<xsl:text>
</xsl:text>
<xsl:text>{</xsl:text><p><xsl:text>} _XML
{_PAR_BOUND} _PAR_BOUND</xsl:text>
<xsl:text>
</xsl:text>
<xsl:apply-templates/>
<xsl:text>
</xsl:text>
<xsl:text>{</xsl:text></p><xsl:text>} _XML
{_PAR_BOUND} _PAR_BOUND</xsl:text>                
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="module[@type='resolution']//para/SENT">
<xsl:apply-templates/>
<xsl:text>
</xsl:text>
<xsl:text>
</xsl:text>
    </xsl:template>
    
    <xsl:template match="module[@type='resolution']//para//ENAMEX">
        <xsl:variable name="type">
            <xsl:choose>
                <xsl:when test="./@type='Person'">_PERSON</xsl:when>
                <xsl:when test="./@type='Location'">_LOCATION</xsl:when>
                <xsl:when test="./@type='Organization'">_ORGANIZATION</xsl:when>
                <xsl:when test="./@type='Company'">_COMPANY</xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="name">
            <xsl:value-of select="translate(./@name, ' ', '_')"/>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="./@gender">
                <xsl:text>{</xsl:text><xsl:apply-templates/><xsl:value-of select="concat('} ', $type, ' [|eid=', ./@eid, ';name=', $name, ';gender=', ./@gender, '|] ')"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>{</xsl:text><xsl:apply-templates/><xsl:value-of select="concat('} ', $type, ' [|eid=', ./@eid, ';name=', $name, ';gender=null|] ')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="module[@type='resolution']//para//TIMEX|module[@type='resolution']//para//NUMEX|module[@type='resolution']//para//TEL|module[@type='resolution']//para//URL|module[@type='resolution']//para//EMAIL">
        <xsl:variable name="type"><xsl:value-of select="concat('_', ./@type)"/></xsl:variable>
        <xsl:text>{</xsl:text><xsl:apply-templates/><xsl:value-of select="concat('} ', $type, ' ')"/>
    </xsl:template>
    
    <xsl:template match="module[@type='resolution']//para/SENT/form">
        <xsl:text>{</xsl:text><xsl:apply-templates/><xsl:value-of select="concat('} ', ./@value, ' ')"/>
    </xsl:template>
    
    <xsl:template match="module[@type='resolution']//para/SENT/form/token|module[@type='resolution']//para//ENAMEX/form/token|module[@type='resolution']//para//TIMEX/form/token|module[@type='resolution']//para//NUMEX/form/token|module[@type='resolution']//para//TEL/form/token|module[@type='resolution']//para//URL/form/token|module[@type='resolution']//para//EMAIL/form/token">
    <xsl:choose>
            <xsl:when test="position()=count(../token)">
                <F><xsl:attribute name="id"><xsl:value-of select="./@id"/></xsl:attribute><xsl:value-of select="."/></F><xsl:text> </xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <F><xsl:attribute name="id"><xsl:value-of select="./@id"/></xsl:attribute><xsl:value-of select="."/></F>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!--<xsl:template match="module[@type='resolution']//para/ALT/CHOICE[@rank='1']/ENAMEX/form">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="module[@type='resolution']//para/ALT/CHOICE[@rank='1']/ENAMEX/form/token">
        <xsl:choose>
            <xsl:when test="position()=count(../token)"><xsl:value-of select="concat(., ' ')"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>-->
    
    
    
    <xsl:template match="text()"/>
</xsl:stylesheet>