#!/usr/bin/perl -w

use strict;
use Getopt::Long;

my $lang = "fr";
my $annotator = "afp";
my $token = 0;
my $form = 0;
my $quotes = 0;
my $sent = 0;
my $hrefs = 0;
my $dags = 0;
my $debug = 0;

GetOptions ('l:s' => \$lang,
	    'a:s' => \$annotator,
	    't' => \$token,
	    'f' => \$form,
	    'q' => \$quotes,
	    'sent' => \$sent,
	    's' => \$sent,
	    'h' => \$hrefs,
	    'dags' => \$dags,
	    'd' => \$debug);


if(($token or $form) and $hrefs){warn "Incompatible options \"token\" (-t) or \"form\" (-f) and \"hrefs\" (-h) - entities hrefs will not be added\n";}


my $sxpipe_annotator = "sxpipe-";
if($quotes)
  {
    $sxpipe_annotator .= "afp-quotes";
  }
else
  {
    $sxpipe_annotator .= $annotator;
  }
$sxpipe_annotator .= " -u -l ".$lang;


my $sxpipe2xml = "sxpipe2xml";
if($token){$sxpipe2xml .= " -t";}
if($form){$sxpipe2xml .= " -form";}
if($sent){$sxpipe2xml .= " --sent";}

my $addEntitiesInfos = "cat";
if($hrefs and not($token) and not($form)){$addEntitiesInfos = "__nomosshare__/handlers/sxpipe/sxpipeXmlAddAledaInfos";}

my $command = "";

my $toxml_commands = "";
if($dags){$toxml_commands = "";}
else{$toxml_commands = $sxpipe2xml." | ".$addEntitiesInfos}

$command = $sxpipe_annotator." | ".$toxml_commands;

if($debug){print STDERR "COMMAND: $command\n";}

exec($command);

