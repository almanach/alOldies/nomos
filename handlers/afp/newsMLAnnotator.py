#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
import re
sys.path.append('@common_src@')
sys.path.append('@nomosshare@/handlers/afp')
from utils import *
from newsMLGetter import *
from newsMLHandler import *
from xml.dom.minidom import *
from optparse import *
from subprocess import *
import codecs
from operator import *
import commands

     
def main():

    pwd = commands.getoutput('pwd')
    if os.path.isdir(pwd+'/out'):
        output_file = pwd + '/out/out.xml'
    else:
        output_file = pwd + '/out.xml'
    # ###GET OPTIONS, CONTROL ARGS
    usage = '\n\n\t%prog [options] <dir|file>\n'
    parser = OptionParser(usage)
    mode_choices = ['newsml', 'xml', 'text']
    parser.add_option("-m", "--mode", action="store", dest="mode", default="text", type="choice", choices=mode_choices, help="Mode newsml: writes annotated xml files in _OUT directory\nMode xml: writes a single xml file (out.xml in .) with annotated text + some newsML infos\nMode text: writes a single text file with annotated text + dateline and title [default xml]")
    parser.add_option("--out", "-o", action="store", dest="out", default=output_file, type="string", help="Specify name of output file (only in TEXT mode)")
    parser.add_option("--noout", action="store_true", dest="noout", default=False, help="Write text output on stdout - incompatible with --out option")
    parser.add_option("--noannot", action="store_true", dest="noannot", default=False, help="Only make news selection, don't pass to sxpipe")
    parser.add_option("--annot", action="store", dest="annotator", default="afp", help="Annotator: sxpipe-afp | sxpipe-edylex [default: afp]")    
    parser.add_option("--slugfilter", action="store", dest="slugfilter", default="", type="string", help='Path to slugfilter file location - if "" : no slugs to filter')
    parser.add_option("--lang", "--lg", action="store", dest="lang", default="fr", type="string", help='SxPipe language')
    parser.add_option("--noiptc", action="append", dest="noiptc", default=[], help='IPTC categories number to filter (ex. SPORT = SPO POLITICS = POL ...')
    parser.add_option("-f", "--filter", action="store", dest="filter_short_items", default="0", type="int", help="If set, include only news with at least FILTER-1 paragraphs of text")
    parser.add_option("-l", "--limit", action="store", dest="limit", default="0", type="int", help="If set, stop after selecting LIMIT news")
    # parser.add_option("--xml_display", action="store", dest="xml_display", default="full", type="string", help="Output is partial xml (only in mode TEXT)")
    parser.add_option("--ext", action="store", dest="extension", default=".xml", type="string", help="Define extension for output files (only in mode NEWSML)")
    parser.add_option("-d", "--debug", action="store", dest="debug", default="0", type="int", help="Print some debug (verbose from 1 to 4) (press Enter to continue)")


    # ### HANDLE ARGUMENT (INDIR|ORFILE) AND OPTIONS
    try:
        (prog_options, args) = parser.parse_args()
#        print prog_options
    except OptionError:
        sys.stderr.write('OptionError')
        parser.print_help()
        sys.exit()
    except:
        sys.stderr.write('An error occured during options parsing\n')
        parser.print_help()
        sys.exit()
    # if len(args) != 1:
    #     parser.print_help()
    #     sys.exit()
    # else:
    try:
        arg = args[0]
        if os.path.isdir(arg):
            if prog_options.noannot:
                if prog_options.debug: printerr("Filtering dir. %s" %(arg))
            else:
                if prog_options.debug: printerr("Filtering and Annotation of dir %s" %(arg))
        elif os.path.isfile(arg):
            if prog_options.noannot == False:
                if prog_options.debug: printerr("Annotation of file %s" %(arg))
    except IndexError:
        file_data = sys.stdin.readlines()
        "".join(file_data)

    if prog_options.slugfilter != "":
        try:
            SLUGS = open(prog_options.slugfilter)
        except IOError:
            printerr("Can't open slug file (%s)" %(prog_options.slugfilter))
            sys.exit()        

    # ### SET DEBUG LEVEL (DEFAULT 0)
    debug_level = prog_options.debug
    filter_short_items = prog_options.filter_short_items
    limit = prog_options.limit

    options = {'noannot': prog_options.noannot, 'filter_short_items': prog_options.filter_short_items, 'mode': prog_options.mode, 'limit': prog_options.limit, 'noout': prog_options.noout, 'debug': prog_options.debug, 'out': prog_options.out, 'slugfilter': prog_options.slugfilter, 'extension':prog_options.extension, 'noiptc':prog_options.noiptc, 'lang':prog_options.lang, 'annotator':prog_options.annotator}


    if prog_options.debug: printerr("Mode: %s" %(prog_options.mode))
    # ### XML MODE: TAKES NEWS ITEMS AND GIVES BACK A SELECTION OF THEM, WITH OR WITHOUT ANNOTATION
    if prog_options.mode == 'newsml':
        # ### IF OPTION 'NO ANNOT', WE WANT ONLY NEWS SELECTION (NO SXPIPE CALL)
        if prog_options.noannot:
            selected_news_list = getFiles(arg, options)
            resulting_list = selected_news_list
        # ### ELSE WE WANT THE WHOLE THING TO GO
        else:
            enriched_news_list = newsML2enrichedNewsML(arg, options)
            debug('newsML => annotated newsML done\n', 3, debug_level)
            resulting_list = enriched_news_list        
        # ###MAKE OUT DIR (FLAT - SAME LEVEL AS ROOT OF INPUT DIR)
        if os.path.isfile(arg):
            out_dir = "ANNOT_OUT"
        elif os.path.isdir(arg):
            dir_name = re.sub('/$', '', arg)
            out_dir = dir_name + "_OUT"
        else:
            sys.stderr.write('Argument is not a file or directory\n')
            parser.print_help()
            sys.exit()
        try: os.mkdir(out_dir)
        except: pass
        if prog_options.debug: printerr("Output directory: %s" %(out_dir))
        for news_dom in resulting_list:
            writeNewFile(news_dom, out_dir, options)

    # ### TEXT MODE: TAKES NEWS ITEMS AND GIVES BACK ONE TEXT WITH THA BODY OF THE NEWS, WITH OR WITHOUT ANNOTATIONS
    elif prog_options.mode == 'text' or prog_options.mode == 'xml':
        out = prog_options.out
        # ### IF OPTION 'NO ANNOT', WE WANT ONLY NEWS SELECTION (NO SXPIPE CALL)
        if prog_options.noannot:
            selected_news_dicts_list = getFiles(arg, options)
            resulting_text = newsDicts2xmltext(selected_news_dicts_list, options)
            resulting_text = resulting_text.encode('utf-8')
        # ### ELSE WE WANT THE WHOLE THING TO GO
        else:
            enriched_news_text = newsML2enrichedText(arg, options)
            resulting_text = enriched_news_text.encode('utf-8')
        if prog_options.noout:
            if prog_options.debug: printerr("Output written to STDOUT")
            sys.stdout.write(resulting_text)
        else:
            if prog_options.debug: printerr("Output: "+out)
            OUT = codecs.open(out, "w", encoding = "utf-8")
            OUT.write(resulting_text.decode('utf-8'))
            OUT.close()

if __name__ == "__main__":
    main()
