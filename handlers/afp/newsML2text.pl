#!/usr/bin/perl -w

use strict;
use Getopt::Long;
# my $arg = shift or die "Can't shift arg\n";
my $arg = "";
my $scripts = "";
my $out = "";
my $mode = "";
my $iptclist = "";
my $slugfile = "";
my $limit = 0;
my $debug = 0;
# my $usage = "Usage: --scripts <path to newsML handlers scripts> -i <newsML file|dir> -o <output file> --mode <xml|text> -l <limit> --slugfilter <slug filter file path> --iptcfilter <iptc to exclude separated by colons \":\">";
my $usage = "Usage: -i <newsML file|dir> -o <output file> --mode <xml|text> -l <limit> --slugfilter <slug filter file path> --iptcfilter <iptc to exclude separated by colons \":\">";
GetOptions ('scripts:s' => \$scripts,
	    'i:s' => \$arg,
	    'o:s' => \$out,
	    'mode:s' => \$mode,
	    'l:i' => \$limit,
	    'slugfilter:s' => \$slugfile,
	    'iptcfilter:s' => \$iptclist,
	    'd' => \$debug);


#if(not(-d $dir)){die$usage;}
if($mode ne "xml" and $mode ne "text"){die $usage;}
if($arg eq ""){die $usage;}

my $command = "";
if($out eq "")
  {
    $command = "@nomosshare@/handlers/afp/newsMLAnnotator.py $arg --noannot --limit $limit --mode $mode --noout";
  }
else
  {
    $command = "@nomosshare@/handlers/afp/newsMLAnnotator.py $arg --noannot --limit $limit --mode $mode --out $out";
  }

if(-f $slugfile){$command .= " --slugfilter $slugfile";}
if($iptclist ne "")
  {
    my @iptclist = split(":", $iptclist);
    foreach my $iptc(@iptclist)
      {
	$command .= " --noiptc $iptc";
      }
  }

if($debug){print "Execute: ".$command."\n";}
exec($command);
