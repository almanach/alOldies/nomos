#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
import re
sys.path.append('@common_src@')
# sys.path.append('@nomosshare@/handlers/afp')
# sys.path.append('/usr/local/share/nomos/build/common_src/')
from utils import *
from xml.dom.minidom import *
from optparse import *
from subprocess import *
import codecs
# from operator import *

def main():
    sys.stdout = codecs.getwriter('utf8')(sys.stdout)
    sys.stdin = codecs.getreader("utf-8")(sys.stdin)

    options, args = getOptions()
    # INIT  MIN SIZE
    global filter_short_items; filter_short_items = options.filter_short_items
    # INIT FILTERS
    global slugs; global genres; slugs = []; genres = []; global iptcfilter; iptcfilter = options.iptcfilter
    if options.slugfilter != None and options.slugfilter != "":
        slugs = getSlugsOrGenres(options.slugfilter)
    if options.genrefilter != None and options.genrefilter != "":
        genres = getSlugsOrGenres(options.genrefilter)

    # CREATE CORPUS <news> OF <news_item>
    # 1. CREATE XML ROOT
    # impl = getDOMImplementation()
    # dom = impl.createDocument(None, "news", None)
    # root = dom.documentElement
    sys.stdout.write('<?xml version="1.0" encoding="utf-8"?>\n<news>\n')

    # 2. IF ARG = FILE = NEWSML: DOM - FILTER (SLUGS, GENRE; #PARAS) - TRANSFORM TO <news_item> AND APPEND, PRINT
    # 2. IF ARG = DIR OF NEWSML: FOR EACH NEWSML => DOM - FILTER (SLUGS, GENRE; #PARAS) - TRANSFORM TO <news_item> AND APPEND, PRINT
    # 2. IF ARG = STDIN: SPLIT STDIN ON <NewsML> => DOM - ETC.

    if options.infile != None:
        try: 
            news_dom = parse(options.infile)
        except Exception as the_exception:
            stderr_news(options.infile, str(type(the_exception))+" ("+" ".join(the_exception.args)+")")
            # EXIT
        try: 
            news2write, filtertype = newsml2news(news_dom, 1, options.robust, options.annot)
            if filtertype is None:
                sys.stdout.write(news2write)
            else:
                stderr_news(options.infile, filtertype)
                # EXIT
        except Exception as the_exception: 
            stderr_news(options.infile, str(type(the_exception))+" ("+" ".join(the_exception.args)+")")
            # EXIT

    elif len(args) == 0: # READ FROM STDIN, ONE OR MORE NEWS ITEMS 
        in_news = False
        news_content = ""
        id_pattern = re.compile('<PublicIdentifier>([^<]+)</PublicIdentifier>')
        news_id = ""
        cpt = 0
        for line in sys.stdin:
            if re.search(id_pattern, line) and in_news:
                news_id = id_pattern.search(line).group(1)
            if re.search('<NewsML', line):
                in_news = True
                news_content += line
                # IN CASE WHOLE NEWS ITEM IS ON ONE LINE
                if re.search('</NewsML', line):
                    in_news = False
                    try:
                        news_dom = parseString(news_content.encode('utf-8'))
                    except Exception as the_exception:
                        stderr_news(news_id, str(type(the_exception))+" ("+" ".join(the_exception.args)+")", False)
                        news_content = ""
                        continue
            elif re.search('</NewsML', line) and in_news:
                in_news = False
                news_content += line
                try:
                    news_dom = parseString(news_content.encode('utf-8'))
                except Exception as the_exception:
                    stderr_news(news_id, str(type(the_exception))+" ("+" ".join(the_exception.args)+")", False)
                    news_content = ""
                    continue
                cpt += 1
                news2write, filtertype = newsml2news(news_dom, cpt, options.robust, options.annot)
                if filtertype is None:
                    sys.stdout.write(news2write)
                else:
                    stderr_news(news_id, filtertype, False)
                news_content = ""
                news_dom.unlink()
            elif in_news:
                news_content += line
    elif os.path.isdir(args[0]):
        extpattern = re.compile('\.([^\.]+)$')
        cpt = 0
        for root, dirs, files in os.walk(args[0]):
            for file in files:
                full_file = os.path.join(root, file)
                if file.startswith('\.'): 
                    continue
                m = extpattern.search(file)
                if m == None:
                    stderr_news(fullfile, "No extension found", False)
                elif m.group(1) not in options.extensions: 
                    stderr_news(fullfile, "Bad extension", False)
                try:
                    news_dom = parse(full_file)
                except Exception as the_exception:
                    stderr_news(fullfile, str(type(the_exception))+" ("+" ".join(the_exception.args)+")", False)
                    continue
                try:
                    cpt += 1
                    news2write, filtertype = newsml2news(news_dom, cpt, options.robust, options.annot)
                    if filtertype is None:
                        sys.stdout.write(news2write)
                    else:
                        stderr_news(fullfile, filtertype)
                except Exception as the_exception:
                    stderr_news(fullfile, str(type(the_exception))+" ("+" ".join(the_exception.args)+")", False)
                    continue
    elif os.path.isfile(args[0]):
        try: 
            news_dom = parse(args[0])
        except Exception as the_exception:
            stderr_news(args[0], str(type(the_exception))+" ("+" ".join(the_exception.args)+")")
        try:
            news2write, filtertype = newsml2news(news_dom, 1, options.robust, options.annot)
            if filtertype is None:
                sys.stdout.write(news2write)
            else:
                stderr_news(args[0], filtertype)
        except Exception as the_exception:
            stderr_news(args[0], str(type(the_exception))+" ("+" ".join(the_exception.args)+")")

    sys.stdout.write('</news>\n')


def newsml2news(newsmldom, rank, robust, annot=None):
    # EXTRACT IPTC CODES FOR FILTER
    iptc_codes = set([])
    for node in mergeLists([newsmldom.getElementsByTagName('SubjectMatter'), newsmldom.getElementsByTagName('SubjectDetail'), newsmldom.getElementsByTagName('SubjectQualifier'), newsmldom.getElementsByTagName('Subject')]):
        iptc_codes.add(convertIptcCodes(node.getAttribute('FormalName')))
    # FILTER
    flag, filtertype = filterNews(newsmldom, iptc_codes, robust)
    if not flag: 
        return None, filtertype
    # BUILD: RANK AND NEWS ID
    news_id = getNodeData(newsmldom.getElementsByTagName('PublicIdentifier')[0])
    try: genre = newsmldom.getElementsByTagName('Genre')[0].getAttribute('FormalName')
    except IndexError: genre = "null"
    news = '<news_item rank="%d" ref="%s" genre="%s">\n'%(rank, news_id, genre)
    # BUILD: LANGUAGE
    news += '<meta lang="%s"'%(newsmldom.getElementsByTagName('Language')[0].getAttribute('FormalName'))
    # BUILD: DATE & TIME
    datetimepattern = re.compile('^(\d+)T(\d+)Z$')
    date = datetimepattern.match(getNodeData(newsmldom.getElementsByTagName('DateAndTime')[0])).group(1)
    time = datetimepattern.match(getNodeData(newsmldom.getElementsByTagName('DateAndTime')[0])).group(2)
    news += ' date="%s" time="%s"'%(date, time)
    # BUILD SPATIAL CONTEXT (TODO)
    # <Location><Property FormalName="Country" Value="FRA"/>
    # <Property FormalName="City" Value="paris"/></Location>
    try:
        country = selectNodes(newsmldom.getElementsByTagName('Location')[0].getElementsByTagName('Property'), 'attr_value', ('FormalName', 'Country'))[0].getAttribute('Value')
        news += ' country="%s"'%(country)
    except IndexError:
        pass
    try:
        city = selectNodes(newsmldom.getElementsByTagName('Location')[0].getElementsByTagName('Property'), 'attr_value', ('FormalName', 'City'))[0].getAttribute('Value')
        news += ' city="%s"'%(city)
    except IndexError:
        pass
    news += '/>\n'
    # BUILD: HEADLINE + DATELINE
    try:
        dateline = getNodeData(newsmldom.getElementsByTagName('DateLine')[0])
    except IndexError:
        if robust:
            dateline = "NULL"
        else:
            return None, "dateline"
    try:
        headline = getNodeData(newsmldom.getElementsByTagName('HeadLine')[0])
    except IndexError:
        if robust:
            headline = "NULL"
        else:
            return None, "headline"
    news += '<news_head dateline="%s" headline="%s"/>\n'%(convertXmlEntities(dateline), convertXmlEntities(headline))
    # BUILD: SLUGS
    news += '<news_slugs text="%s"/>\n'%(getNodeData(newsmldom.getElementsByTagName('NameLabel')[0]))
    # BUILD: IPTC
    for iptc in iptc_codes:
        news += '<iptc_code value="%s"/>\n'%(iptc)
    # BUILD: PARAS
    news += '<news_text>\n'
    pcpt = 0; signature_pattern = re.compile("^\s*[a-zé&A-Z]{2,4}((-|/)[a-zé&A-Z]{2,4}/?)+( +[a-z\.]{1,7})?\s*$")
    paras = ""
    for node in newsmldom.getElementsByTagName('p'):
        data = convertXmlEntities(getNodeData(node), 'light')
        data = re.sub('--', ' - ', re.sub('porte-Parole', 'porte-parole', data))
        if signature_pattern.search(data):
            pcpt += 1
            paras += '<para rank="%d" signature="%s"/>\n'%(pcpt, re.sub('[-/]', '_', data))
        elif not re.match('^\s*$', data):
            pcpt += 1
            paras += '<para rank="%d">\n'%(pcpt)
            paras += data
            paras += '\n</para>\n'
    # ANNOTATE PARAS IF OPTION
    if annot != None:
        config = 'sxpipe-'+annot
        sxpipe = Popen([config], stdin=PIPE, stdout=PIPE)
        temp1 = sxpipe.communicate(paras.encode('utf-8'))[0]
        shp = Popen(['shortest_path'], stdin=PIPE, stdout=PIPE)
        temp2 = shp.communicate(temp1)[0]
        sxpipe2xml = Popen(['sxpipe2xml'], stdin=PIPE, stdout=PIPE)
        temp3 = sxpipe2xml.communicate(temp2)[0]
        paras = temp3
    try:
        news += paras.decode('utf-8')+'</news_text>\n</news_item>\n'
    except UnicodeEncodeError:
        news += paras+'</news_text>\n</news_item>\n'
    return news, None

def filterNews(newsmldom, iptc_codes, robust):
    for iptc_code in iptc_codes:
        if iptc_code in iptcfilter:
            return False, "iptc"
    if not robust:
        try:
            if re.search('BSW', getNodeData(newsmldom.getElementsByTagName('DateLine')[0])): 
                return False, "BSW"
        except IndexError:
            return False, "dateline"
        if newsmldom.getElementsByTagName('DateLine') == []:
            return False, "dateline"
        if newsmldom.getElementsByTagName('HeadLine') == []:
            return False, "headline"
    if filter_short_items != 0 and len(newsmldom.getElementsByTagName('p')) < filter_short_items: 
        return False, "paragraphs"
    for slug in getNodeData(newsmldom.getElementsByTagName('NameLabel')[0]).split('-'):
        if replaceDiacr(slug).upper().encode('utf-8') in slugs: 
            return False, "slugs"
    if newsmldom.getElementsByTagName('Genre') != []:
        if replaceDiacr(getNodeData(newsmldom.getElementsByTagName('Genre')[0])).upper().encode('utf-8') in genres: 
            return False, "genre"
    return True, None

def convertIptcCodes(full_code):
    table = {'01':'CLT', '02':'CLJ', '03':'DIS', '04':'ECO', '05':'EDU', '06':'ENV', '07':'HTH', '08':'HUM', '09':'SOC', '10':'LIF', '11':'POL', '12':'REL', '13':'SCI', '14':'SOI', '15':'SPO', '16':'UNR', '17':'WEA'}
    topic = None
    for code in table.keys():
        if full_code.startswith(code):
            topic = table[code]
            break
    return topic

def getSlugsOrGenres(file):
    t = []; pattern = re.compile('^([^#\t]+)')
    F = open(file)
    for line in F:
        match = pattern.match(line)
        if(match): t.append(replaceDiacr(match.group(0)).upper().encode('utf-8'))
    F.close()
    return t

def stderr_news(newsid, reason, stop=True):
    sys.stderr.write("newsML2text: No output written for %s: %s\n"%(newsid, reason))
    if stop:
        sys.exit()

def getOptions():
    usage = '\n\n\t%prog [options] <newsml dir|file>\n'
    parser = OptionParser(usage)
    parser.add_option("--out", "-o", action="store", dest="out", default=None, type="string", help="Name of output file")
    parser.add_option("--infile", action="store", dest="infile", default=None, help="input file for bug in nomos chain")
    parser.add_option("--annot", action="store", dest="annot", default=None, help="Annotator: afp | afp-quotes")    
    parser.add_option("--slugfilter", action="store", dest="slugfilter", default=None, type="string", help='Path to slugfilter file')
    parser.add_option("--genrefilter", action="store", dest="genrefilter", default=None, type="string", help='Path to genrefilter file')
    parser.add_option("--iptcfilter", action="append", dest="iptcfilter", default=[], help='IPTC codes to filter (ex. SPORT = SPO POLITICS = POL ...); concat. with ":"; ex.: --iptcfilter SPO or --iptcfilter SPO:POL')    
    # parser.add_option("--lang", "--lg", "-l", action="store", dest="lang", default="fr", type="string", help='SxPipe language')
    parser.add_option("--filter", action="store", dest="filter_short_items", default="0", type="int", help="If set, include only news with at least FILTER-1 paragraphs of text")
    parser.add_option("--ext", action="append", dest="extensions", default=['xml', 'newsml'], help="Extension for files to process")
    parser.add_option("--robust", action="store_true", dest="robust", default=False, help="Don't stop building even if HeadLine or DateLine missing")
    parser.add_option("-d", "--debug", action="store", dest="debug", default="0", type="int", help="Print some debug (verbose from 1 to 4) (press Enter to continue)")
    try: (options, args) = parser.parse_args()
    except OptionError: sys.stderr.write('OptionError'); parser.print_help(); sys.exit()
    except: sys.stderr.write('An error occured during options parsing\n'); parser.print_help(); sys.exit()
    if len(args) > 1: sys.stderr.write('Argument error'); parser.print_help(); sys.exit()
    return options, args

if __name__ == '__main__':
    main()
