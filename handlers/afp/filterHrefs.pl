#!/usr/bin/perl -w

use strict;

# ON CONSIDERE QUE CE SCRIPT EST APPELE EN PIPE : ... | SCRIPT -f arg| ... > ...
# AVEC COMME SEUL ARG POSSIBLE -f arg ; SI DIFFERENT : NE FAIT RIEN

use Encode;
use DBI;
use Getopt::Long;

my $refs_sth; my $refs_dbh;
my $vars_sth; my $vars_dbh;

$refs_dbh = DBI->connect("dbi:SQLite:__aledalibdir__/ne_refs.fr.dat", "", "", {RaiseError => 1, AutoCommit => 1});
$refs_sth = $refs_dbh->prepare('select key from data where link like ?;');

my @filter_data = ();

my $opt = shift; 
if($opt eq '-f')
  {
    my $file = shift;
    if($file ne "")
      {
	open(F, $file) or die "Can't open $file: $!\n";
	@filter_data = <F>;
	close(F);
      }
  }


my $there_is_no_data = 1;
if(@filter_data){$there_is_no_data = 0;}

# FOR ACTIONS ON HREFS WHATEVER THE DATA:
my %hrefs = ();
# FOR ACTIONS ON HREFS DEPENDING ON THE DATA:
my %data = ();

foreach my $line(@filter_data)
  {
    if($line =~ /^([^\|]*)\|([^\|]+)\|([^\|]*)$/)
      {
	my $data = $1; my $href1 = $2; my $href2 = $3; chomp($href2);
	if($data eq '')
	  {
	    if($href2 eq '' or $href2 =~ /^\s*$/){$hrefs{$href1}{'action'} = 'delete';}
	    else{$hrefs{$href1}{'action'} = $href2;}
	  }
	else
	  {
	    $data{$data}{'href'} = $href1;
	    if($href2 eq '' or $href2 =~ /^\s*$/)
	      {
		$data{$data}{'action'} = 'delete';
	      }
	    else
	      {
		$data{$data}{'action'} = $href2;
	      }
	  }
      }
  }

while(<>)
  {
    # IF DELETE: DON'T DELETE AUT AND ENAMEX, JUST HREF ATTRIBUTE
    # while(s/<AUT>([^<])/$1/){}
    # while(s/([^>])<\/AUT>/$1/){}
    if($there_is_no_data){print; next;}
    if($_ !~ /<ENAMEX/){print; next;}
    chomp;
    my $line = "";
    while($_ =~ /<ENAMEX[^<>]+>[^<>]+<\/ENAMEX>/)
      {
	my $before = $`; my $after = $'; my $enamex = $&;
	$line .= $before;
	$enamex =~ /<ENAMEX([^<>]+)>([^<>]+)<\/ENAMEX>/;
	my $attr = $1; my $content = $2;
	$attr =~ /href="([^"]+)"/;
	my $href = $1; my $href_apos = $href; $href_apos =~ s/&apos;/'/g;
	if(exists($hrefs{$href}))
	  {
	    # IF DELETE: DON'T DELETE ENAMEX, JUST HREF ATTRIBUTE
	    # if($hrefs{$href}{'action'} eq 'delete'){$line .= $content;}
	    if($hrefs{$href}{'action'} eq 'delete')
	      {
		# $attr =~ s/ $href//; $line .= "<ENAMEX".$attr.">".$content."<\/ENAMEX>";
		$attr =~ s/ href="[^"]+"//; $attr=~ s/eid="[^"]+"/eid="null"/g;
		$line .= "<ENAMEX".$attr.">".$content."<\/ENAMEX>";
	      }
	    else
	      {
		my $href2 = $hrefs{$href}{'action'}; 
		$attr =~ s/href="[^"]+"/href="$href2"/;
		# FETCH RIGHT EID IN ALEDA ACCORDING TO LINK
		my $eid = "null";
		my $query = $href2; $query =~ s/_/ /g; $query =~ s/&apos;/'/g;
		$refs_sth->execute($query);
		if(my @t = $refs_sth->fetchrow_array)
		  {
		    $eid = sprintf("%.0f", $t[0]);
		  }
		$refs_sth->finish;
		$attr =~ s/eid="[^"]+"/eid="$eid"/;
		$line .= "<ENAMEX".$attr.">".$content."<\/ENAMEX>";
	      }
	  }
	elsif(exists($hrefs{$href_apos}))
	  {
	    # IF DELETE: DON'T DELETE ENAMEX, JUST HREF ATTRIBUTE
	    # if($hrefs{$href_apos}{'action'} eq 'delete'){$line .= $content;}
	    if($hrefs{$href_apos}{'action'} eq 'delete')
	      {
		$attr =~ s/ href="[^"]+"//; $attr=~ s/eid="[^"]+"/eid="null"/g;
		$line .= "<ENAMEX".$attr.">".$content."<\/ENAMEX>";}
	    else
	      {
		my $href2 = $hrefs{$href_apos}{'action'}; 
		$attr =~ s/href="[^"]+"/href="$href2"/; 
		# FETCH RIGHT EID IN ALEDA ACCORDING TO LINK
		my $eid = "null";
		my $query = $href2; $query =~ s/_/ /g; $query =~ s/&apos;/'/g;
		$refs_sth->execute($query);
		if(my @t = $refs_sth->fetchrow_array)
		  {
		    $eid = sprintf("%.0f", $t[0]);
		  }
		$refs_sth->finish;
		$attr =~ s/eid="[^"]+"/eid="$eid"/;
		$line .= "<ENAMEX".$attr.">".$content."<\/ENAMEX>";
	      }
	  }
	elsif(exists($data{$content}))
	  {
	    if($data{$content}{'href'} eq $href)
	      {
		# IF DELETE: DON'T DELETE ENAMEX, JUST HREF ATTRIBUTE
		# if($data{$content}{'action'} eq 'delete'){$line .= $content;}
		if($data{$content}{'action'} eq 'delete')
		  {
		    $attr =~ s/ href="[^"]+"//; $attr=~ s/eid="[^"]+"/eid="null"/g;
		    $line .= "<ENAMEX".$attr.">".$content."<\/ENAMEX>";
		  }
		else
		  {
		    my $href2 = $data{$content}{'action'};
		    $attr =~ s/href="[^"]+"/href="$href2"/; 
		    # FETCH RIGHT EID IN ALEDA ACCORDING TO LINK
		    my $eid = "null";
		    my $query = $href2; $query =~ s/_/ /g; $query =~ s/&apos;/'/g;
		    $refs_sth->execute($query);
		    if(my @t = $refs_sth->fetchrow_array)
		      {
			$eid = sprintf("%.0f", $t[0]);
		      }
		    $refs_sth->finish;
		    $attr =~ s/eid="[^"]+"/eid="$eid"/;
		    $line .= "<ENAMEX".$attr.">".$content."<\/ENAMEX>";
		  }
	      }
	    elsif($data{$content}{'href'} eq $href_apos)
	      {
		# IF DELETE: DON'T DELETE ENAMEX, JUST HREF ATTRIBUTE
		# if($data{$content}{'action'} eq 'delete'){$line .= $content;}
		if($data{$content}{'action'} eq 'delete')
		  {$attr =~ s/ href="[^"]+"//; $attr=~ s/eid="[^"]+"/eid="null"/g;
		   $line .= "<ENAMEX".$attr.">".$content."<\/ENAMEX>";
		 }
		else
		  {
		    my $href2 = $data{$content}{'action'}; 
		    $attr =~ s/href="[^"]+"/href="$href2"/; 
		    # FETCH RIGHT EID IN ALEDA ACCORDING TO LINK
		    my $eid = "null";
		    my $query = $href2; $query =~ s/_/ /g; $query =~ s/&apos;/'/g;
		    $refs_sth->execute($query);
		    if(my @t = $refs_sth->fetchrow_array)
		      {
			$eid = sprintf("%.0f", $t[0]);
		      }
		    $refs_sth->finish;
		    $attr =~ s/eid="[^"]+"/eid="$eid"/;
		    $line .= "<ENAMEX".$attr.">".$content."<\/ENAMEX>";}
	      }
	    else
	      {
		$line .= $enamex;
	      }
	  }
	else
	  {
	    $line .= $enamex;
	  }
	$_ = $after;
      }
    $line .= $_;
    print $line."\n"; 
  }




