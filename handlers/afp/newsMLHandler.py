#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
import re
import string
sys.path.append('@common_src@')
sys.path.append('@nomosshare@/handlers/afp')
from utils import *
from newsMLGetter import *
from xml.dom.minidom import *
from optparse import OptionParser
from subprocess import *
import codecs
from operator import *

# ENCODING WARNING:
# ### TEXT RETURNED BY XML.DOM MODULE = ALREADY UTF-8
# ### TEXT RETURNED BY A SYSTEM COMMAND = DON'T FORGET TO PASS IT TO ENCODE OR DECODE (???...)
# ### ENCODE BEFORE PASSING TO XML PARSER (ALWAYS?)

def newsML2enrichedText(dir, options):
    news_list_dir = getFiles(dir, options)    # LIST OF DICTS 
    tempin = 'tempin'
    tempout = 'tempout'
    TEMPIN = codecs.open(tempin, "w", encoding = "utf-8")
    news_text = newsDicts2xmltext(news_list_dir, options)
    TEMPIN.write(news_text)
    TEMPIN.close()
    output = pass2sxpipe(tempin, options)
    return output

def newsML2enrichedNewsML(dir, options):
    # GETFILES => GET A LIST OF NEWSML-DOM
    # IF FAILS => DOM ABSENT FROM RETURNED LIST
    news_list = getFiles(dir, options)    # LIST OF DOMS
    enriched_news_list = []
    # COUNTER FOR NUMBER OF FILES PROCESSED
    cpt_all = 0
    cpt_good = 0
    for news_dom in news_list:
        lines = []
        # GET TEXT NODES
        lines = getText(news_dom, 'p')
        # GETTEXT FUNCTION MAY RETURN AN EMPTY LIST IF FAILED
        if(lines):
            cpt_all += 1
            # DISPLAY COUNTING
            paras_parents = deleteParasNodes(news_dom)
            input_file = newsParas2input(lines, paras_parents[0], "tempin")
            # output_file = "tempout"
            output = pass2sxpipe(input_file, options)
            debug(output, 4, options['debug'])
            if(output):    
                # GET DOM OF PARAGRAPHS RETURNED BY SXPIPE
                try:
                    output_dom = getStringDom(output.encode("utf-8"))
                    cpt_good += 1
                except xml.parsers.expat.ExpatError, error:
                    # printerr('ExpatError: %s' % str(error))
                    # raw_input("Press Enter to continue\n")
                    continue
                except:
                    # printerr('Some error else than ExpatError: no output')
                    continue
                if(output_dom):
                    enriched_news_dom = buildNewDom(output_dom, news_dom, paras_parents[1], options)
                    enriched_news_list.append(enriched_news_dom)
                    printerr(str(cpt_good) + " news processed")
                else:
#                    printerr("No output returned from getStringDom for file %s in selection - ignored" %(str(cpt_good)))
                    continue
            else:
#                printerr("No output returned from SxPipe for file %s in selection\n - ignored" %(str(cpt_all)))
                continue

        # IF LINES[] IS EMPTY
        else:
#            printerr("There was a problem in getText for file %s in selection - ignored" %(str(cpt)))
            continue
    printerr("Total: " + str(cpt_good) + " news processed\n")
    return enriched_news_list

def deleteParasNodes(news_dom):
    # DELETE ORGINAL PARAGRAPHS' PARENT  IN NEWS DOM (<body.content><p>+</body.content>)
    # GET NAME OF PARAGRAPHS' PARENT (<body.content>)
    para = news_dom.getElementsByTagName('p')[0]
    paras_parent = para.parentNode
    paras_parent_name = paras_parent.tagName
    # REMOVE PARAGRAPHS' PARENT FROM ITS PARENT (<body>)
    paras_grand_parent = paras_parent.parentNode
    paras_grand_parent.removeChild(paras_parent)
    paras_parent.unlink()
    paras_parents = []
    paras_parents.append(paras_parent_name)
    paras_parents.append(paras_grand_parent)
    return paras_parents

def newsParas2input(lines, paras_parent_name, file_name):
    # PREPARE INPUT FOR SXPIPE
    # ADD TAGS FOR PARAGRAPHS ; COMMENT XML FOR SXPIPE
    text = paras2sxpipeInput(lines, paras_parent_name)
    # OPEN TEMP FILE TO WRITE NEWS DOM AS SXPIPE INPUT-TEXT
    TEMPIN = codecs.open(file_name, "w", encoding = "utf-8")
    # WRITE TEXT FROM NEWS DOM IN TEMP FILE
    TEMPIN.write(text)
    TEMPIN.close()
    return file_name

def paras2sxpipeInput(lines, paras_parent_name):
    text = "\n<"+paras_parent_name+">\n"
    start_tag = "<p>\n"
    end_tag = "\n</p>\n"
    i = 1
    for line in lines:
        if(i == lines.index(lines[-1])):
            if re.match("[^/]+/[^/]+", line):
                i += 1
                text = text + start_tag + "<signature string=\"" + line + "\"/>\n" + end_tag + "</"+paras_parent_name+">\n"
            else:
                text = text + start_tag + line + end_tag + "</"+paras_parent_name+">\n"
                i += 1
        elif(i < lines.index(lines[-1])):
            text = text + start_tag + line + end_tag
            i += 1
    return text

def buildNewDom(output_dom, news_dom, paras_grand_parent, options):
    # GET ROOT ELEMENT FROM PARAS' DOM
    new_paras_parent = output_dom.documentElement    
    # APPEND IT TO PARAS' GRAND PARENT IN NEWS DOM
    paras_grand_parent.appendChild(new_paras_parent)
    debug(news_dom.toxml(), 4, options['debug'])
    properties_dict = getProperties(news_dom, options)
    # INSERT INFOS RETURNED BY SXPIPE IN NEWSML (<PROPERTY>...)
    news_dom_with_properties = insertProperties(news_dom, properties_dict, options)
    return news_dom_with_properties


def getProperties(news_dom, options):
    # ### PARSE NEW <P>s AND GET SXPIPE-TAGS
    properties_dict = {}
    for enamex_node in news_dom.getElementsByTagName('ENAMEX'):
        # TODO CHANGE WHEN NEW SXPIPE OUTPUT
        entity_name = enamex_node.getAttribute('name')
        eid = enamex_node.getAttribute('eid')
        debug(entity_name, 3, options['debug'])
        # FILL IN DICT OF ENTITIES TO PUT AFTER IN PROPERTIES
        if not(properties_dict.has_key(entity_name)):
            properties_dict[eid] = {}
            properties_dict[eid]['name'] = entity_name
            properties_dict[eid]['type'] = enamex_node.getAttribute('type')
    return properties_dict

def insertProperties(news_dom, properties_dict, options):
#    printerr("In insert properties\n")
#    print properties_dict
    # GET ELEMENT CONTAINING <PROPERTY>s
    properties_parent = news_dom.getElementsByTagName('DescriptiveMetadata')[0]
    # WALK DICT AND CREATE PROPERTY ELEMENTS + APPEND
    for eid in properties_dict:
        type = properties_dict[eid]['type']
        name = properties_dict[eid]['name']
        property = news_dom.createElement('Property')
        formal_name_attr = news_dom.createAttribute('FormalName')
        value_attr = news_dom.createAttribute('Value')
        eid_attr = news_dom.createAttribute('Id')
        property.setAttributeNode(formal_name_attr)
        property.setAttributeNode(value_attr)
        property.setAttributeNode(eid_attr)
        property.setAttribute('FormalName', type)
        property.setAttribute('Value', name)
        property.setAttribute('Id', eid)
        debug(property.toxml(), 3, options['debug'])
        # ### APPEND THEM TO THEIR PARENT NODE
        properties_parent.appendChild(property)
        debug(news_dom.toprettyxml(indent=" "), 3, options['debug'])
    return news_dom



def writeNewFile(enriched_news_dom, outdir, options):
    # GET ORIGINAL FILE NAME
    date = enriched_news_dom.getElementsByTagName('DateId')[0].firstChild.data
    id = enriched_news_dom.getElementsByTagName('NewsItemId')[0].firstChild.data
    full_file_name = outdir + "/afp.com-" + date + '-' +id + options['extension']
    debug('File ' + full_file_name + ' should be written', 3, options['debug'])
    # WRITE XML IN NEW FILE, IN NEW DIR
    NEW_XML_FILE = codecs.open(full_file_name, "w", encoding = "utf-8")
    enriched_news_dom.writexml(NEW_XML_FILE, encoding="utf-8")
    NEW_XML_FILE.close()



def pass2sxpipe(input_file, options):
    # COMMANDS
    log_file = "newsML2sxpipe.log"
    LOG = open(log_file, "w")
    cat_infile = Popen(["cat", input_file], stdout=PIPE)
    if options['lang'] == "fr":
        if options['annotator'] == "afp":
            printerr('Annotation: sxpipeAnnotator -a afp -l fr')
            annotator = Popen(["sxpipeAnnotator", "-a", "afp"], stdin=cat_infile.stdout, stdout=PIPE, stderr=LOG)
        elif options['annotator'] == "edylex":
            printerr('Annotation: sxpipeAnnotator -a edylex -l fr')            
            annotator = Popen(["sxpipeAnnotator", "-a", "edylex"], stdin=cat_infile.stdout, stdout=PIPE, stderr=LOG)
    else:
        if options['annotator'] == "afp":
            printerr('Annotation: sxpipeAnnotator -a afp -l %s' %(options['lang']))          
            annotator = Popen(["sxpipeAnnotator", "-l", options['lang'], "-a", "afp"], stdin=cat_infile.stdout, stdout=PIPE, stderr=LOG)
        elif options['annotator'] == "edylex":
            printerr('Annotation: sxpipeAnnotator -a edylex -l %s' %(options['lang']))
            annotator = Popen(["sxpipeAnnotator", "-l", options['lang'], "-a", "edylex"], stdin=cat_infile.stdout, stdout=PIPE, stderr=LOG)
    output = annotator.communicate()[0]
    output = output.decode("utf-8")
    LOG.close()
    return output

def selectTag(tag_name):
    flag = 0
    for type in ["LOCATION", "PERSON", "PERSON_m", "PERSON_f", "ORGANIZATION", "COMPANY", "PRODUCT"]:
        if re.match(tag_name, type, re.IGNORECASE):
            flag = 1
            break
    return flag

def newsDicts2xmltext(news_dicts_dir, options):
    cpt_news = 0
    all_text = ""
    if options["mode"] == "xml":
        all_text = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<news>\n'
    news_dicts_indexes = news_dicts_dir.keys()
    news_dicts_indexes.sort()
    for index in news_dicts_indexes:
        cpt_news += 1
        news_dict = news_dicts_dir[index]
        title = news_dict['Title'] # il faut enlever tous les retours chariots dans le titre
        dateline = news_dict['Dateline']
        try: title_text = dateline + ' ' + title
        except:  title_text = dateline + ' ' + title[0]        
        if options["mode"] == "xml":        
            text = '<news_item rank="%s" ref="%s">\n' %(str(cpt_news), news_dict['ID'])
            # title_text = re.sub('&', '&amp;', title_text)            
            # title_text = re.sub('"', '&quot;', title_text)
            # title_text = re.sub("'", '&apos;', title_text)
            text += '<news_head text="' + convertXmlEntities(title_text) + '"/>\n'
            text += '<news_slugs text="' + convertXmlEntities(news_dict['Slugs']) + '"/>\n'
            for code in news_dict['IPTC']:
                text += '<iptc_code value="' + code + '"/>\n'
        elif options["mode"] == "text":
            text = ""
            text += '<news_item ref="'+news_dict["ID"]+'">\n'
            text += title_text+"\n\n"
        cpt_paras = 1
        paras = ''
        debug(text, 4, options['debug'])
        paras_list = news_dict['Body']
        last = False
        for p in paras_list:
            if options["mode"] == "xml":
                para = p.lstrip()
                if not re.search('^\s*$', para):
                    para = re.sub('&', '&amp;', para)
                    # s = re.compile('--([^ ])'); m = re.search(s, para)
                    # if m != None: para = re.sub(s, '- '+m.group(1), para)
                    # s = re.compile('([^ ])--'); m = re.search(s, para)
                    # if m != None: para = re.sub(s, m.group(1)+' -', para)
                    para = re.sub('--', '-', para)
                    para = re.sub(' -', ' - ', para)
                    para = re.sub('- ', ' - ', para)
                    # s = re.compile(' -([^ ])'); m = re.search(s, para)
                    # if m != None: para = re.sub(s, ' - '+m.group(1), para)
                    # s = re.compile('([^ ])- '); m = re.search(s, para)
                    # if m != None: para = re.sub(s, m.group(1)+' - ', para)
                    # if(cpt_paras == len(paras_list)):
                    signature_pattern = re.compile("^\s*[a-zé&A-Z]{2,4}((-|/)[a-zé&A-Z]{2,4}/?)+( +[a-z\.]{1,7})?\s*$")
                    match = signature_pattern.match(para)
                    if match != None:
                        para = re.sub('-', '_', para)
                        para = re.sub('/', '__', para)
                        paras += '<para rank="' + str(cpt_paras) + '" signature="'+convertXmlEntities(para)+'"/>\n'
                        last = True
                    elif not last:
                        # paras += '<para rank="' + str(cpt_paras) + '">\n' + convertXmlEntities(para) + '\n</para>\n'
                        paras += '<para rank="' + str(cpt_paras) + '">\n' + para + '\n</para>\n'     
                    elif last:
                        paras += '<para rank="' + str(cpt_paras) + '" ignore="true" content="'+convertXmlEntities(para)+'"/>\n'
                    cpt_paras += 1
            elif options["mode"] == "text":
                paras += p+"\n"
        if options["mode"] == "xml":
            text += '<news_text>\n' + paras + '\n</news_text>\n' + '</news_item>\n'
        elif options["mode"] == "text":
            text += paras+"\n</news_item>\n"
        debug(text, 4, options['debug'])
        all_text += text

    if options["mode"] == "xml":
        all_text += '</news>\n'
    return all_text



# __END__
