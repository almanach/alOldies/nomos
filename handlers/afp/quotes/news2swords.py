#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
import re
import codecs
import collections
from optparse import *
from xml.dom.minidom import *
import cPickle
import math
from operator import itemgetter


# READ NEWS IN CORPUS
#######################################################################
# <news>
# <news_item rank="1" ref="urn:newsml:afp.com:20110401T010326Z:TX-PAR-FFY24:1">
# <news_head text="PARIS, 1 avr 2011 (AFP) - François Hollande, un candidat &quot;de poids&quot; pour la presse régionale (REVUE DE PRESSE)"/>
# <news_slugs text="Cantonales-collectivités-primaire-présidentielle-partis-PS"/>
# <iptc_code value="POL"/>
# <news_text>
# <para rank="1">
# <form value="l'">L'</form><form value="entrée">entrée</form> ...
# </para>
# </news_text>
# </news_item>
#######################################################################
# AJOUTER AVANT <news_text> UNE METADONNEE SAILLANT-WORDS :
# <swords><sword value="" rank="" score=""/></swords>


# READ ALL CORPUS
# READ LINES
# FOR EACH NEWS ITEM
# AGGREGATE CONTENT
# COUNT WORDS
# GET WORDS2COUNT
# FOR EACH WORD GET CORPUS COUNT AND COMPUTE SALIENCE IN NEWS
# FOR 3 FIRST SWORDS ADD TAG IN <swords>

def main():
    options, args = getOptions()
    print "Fetching words 2 frequency from pickle file %s..."%(options.words2freq)
    # read python dict from pickle file
    pkl_file = open(options.words2freq, 'rb')
    global words2frequency
    words2frequency = cPickle.load(pkl_file)
    pkl_file.close()
    print "Done."
    print "words2frequency dict has %d items"%(len(words2frequency.keys()))
    global stop_list
    stop_list = stopList(options.stop)
    if len(args) == 0: F = sys.stdin
    else: F = codecs.open(args[0], 'r', 'utf-8'); print >> sys.stderr, 'Reading file %s...\n'%(args[0])
    # READ DOCS TO WORDS FILE
    message = ' 0 docs processed'
    in_doc = False
    docs_done = 0
    content = ""
    if options.out == None: F = sys.stdout
    else: G = codecs.open(options.out, 'w', 'utf-8')
    for line in F:
        if re.search('</?news>', line):
            G.write(line)
        if re.search('<news_item', line):
            in_doc = True
            G.write(line)
        elif re.search('</news_item', line) and in_doc:
            in_doc = False
            docs_done += 1
            news_words2count = countWords(content)
            # news_words2count = sorted(countWords(content, stop_list).items(), key=itemgetter(1), reverse=True)
            # print >> sys.stderr, news_words2count[0:4]; raw_input()
            ranked_words2salience = computeSaillance(news_words2count, float(options.wordspop))
            new_tag = '<swords>\n'
            i = 0
            for (word, salience) in ranked_words2salience[0:4]:
            # for (word, freq) in news_words2count[0:4]:
                i += 1
                new_tag += '<sword value="%s" rank="%d" score="%.3f" freq="%d"/>\n'%(word, i, salience, news_words2count[word])
                # new_tag += '<sword value="%s" rank="%d" freq="%d"/>\n'%(word, i, freq)
            new_tag += '</swords>\n'
            content = re.sub('<news_text>', new_tag+'<news_text>', content)
            if options.clean:
                content = re.sub('</?(form|token)[^>]*>', '', content)
            G.write(content+line)
            content = ""
            sys.stderr.write(' %d docs handled'%(docs_done)+'\r'*(len(str(docs_done))))
        elif in_doc and re.search('<\?news_head|<\?news_slugs|<\?iptc', line):
            G.write(line)
        elif in_doc: # <news_text> | <para rank="x"> | </para> | </news_text> | all para lines
            content += line
    F.close()
    G.close()
    message = ' %d docs processed'%(docs_done)
    sys.stderr.write(message+'\n')

def countWords(content):
    words2count = {}#collections.defaultdict(int)
    # p = re.compile('<token[^<>]+>([^<>]+)</token>')
    p = re.compile('<form value="([^"]+)"')
    content_without_enamex = re.sub('<ENAMEX[^>]+>(<form[^>]+>)?( *<token[^>]+>[^<]+</token> *)+(</form>)? *</ENAMEX>', '', content)
    found = p.findall(content_without_enamex)
    tokens = filter((lambda t: selectToken(t, found.index(t), stop_list)), found)
    for token in tokens:
        # token = lemmatize(token)
        try: words2count[token] += 1
        except KeyError: words2count[token] = 1
    return words2count

# def lemmatize(words2count):
#     new_words2count = {}
#     words = words2count.keys()
#     new_words = []
#     for word in words:
#         lemma = ""
#         # is a part.passé
#         if re.search('ée?$', word):
#             lemma = re.sub('ée?$', '', word)
#         elif re.search('[st]e?$'):
#             lemma = re.sub('[st]e?$', '', word)
#         if lemma in words:
#             new_words.append(lemma)
#         else:
#             new_words.append(word)
#             continue
#         # is plural
#         elif re.search('e?s$', word):
#             lemma = re.sub('e?s$', '', word)
#             if lemma in words:
#                 new_words.append(lemma)
#             else:
#                 lemma = re.sub('s$', '', word)
#                 if lemma in words:
#                     new_words.append(lemma)
#                 else:
#                     new_words.append(word)
#                     continue
#     return new_words2count

def selectItem(item):
    (word, frequency) = item
    return True
    

def computeSaillance(doc_words2frequency, words_population):
    rho = 0.0
    z = 0.0
    words2saillance = {}
    sub_population = float(sum(doc_words2frequency.values()))
    for (word, frequency) in filter((lambda item: selectItem(item)), doc_words2frequency.items()):
        try: temp_positive_in_population = float(words2frequency[word])
        except KeyError: temp_positive_in_population = 0.0
        # NORMALISATION DES POPULATIONS (TEST BEN)
        positive_in_population = temp_positive_in_population * sub_population / float(words_population)
        new_population = sub_population
        #new_population = words_population
        positive_in_sub_population = frequency
        rho = (positive_in_population + positive_in_sub_population) / (new_population + sub_population)
        #truc = ((rho * (1-rho)) / sub_population) + (rho * (1-rho) / words_population)
        truc = ((rho * (1-rho)) / sub_population) + (rho * (1-rho) / new_population)
        z = ((positive_in_sub_population / sub_population) - (positive_in_population / new_population)) / math.sqrt(truc)
        words2saillance[word] = z
    ranked_words2saillance = sorted(zip(words2saillance.keys(), words2saillance.values()), key=lambda x: x[1], reverse=True)
    return ranked_words2saillance


def _computeSaillance(doc_words2frequency, words_population):
    print doc_words2frequency
    rho = 0.0
    z = 0.0
    words2saillance = {}
    sub_population = sum(doc_words2frequency.values())
    print "Sub population: %d"%(sub_population)
    for (word, frequency) in doc_words2frequency.items():
        print word
        print frequency
        try: temp_positive_in_population = float(words2frequency[word])
        except KeyError: temp_positive_in_population = 0.0
        # NORMALISATION DES POPULATIONS (TEST BEN)
        print "positive in population = %f * %f / %f"%(temp_positive_in_population, sub_population, words_population)
        positive_in_population = temp_positive_in_population * sub_population / words_population
        new_population = sub_population
        positive_in_sub_population = frequency
        print "pos in pop: %f pos in sub pop: %f"%(positive_in_population, positive_in_sub_population)
        print "rho = (%f + %f) / (%f + %f)"%(positive_in_population, positive_in_sub_population, new_population, sub_population)
        rho = (positive_in_population + positive_in_sub_population) / (new_population + sub_population)
        print "rho = %f"%(rho)
        truc = ((rho * (1-rho)) / sub_population) + (rho * (1-rho) / words_population)
        z = ((positive_in_sub_population / sub_population) - (positive_in_population / new_population)) / math.sqrt(((rho * (1-rho)) / sub_population) + (rho * (1-rho) / new_population))
        # print >> sys.stderr, "Word: %s Frequency: %f"%(word, frequency)
        # print >> sys.stderr, 'z = ((positive_in_sub_population / sub_population) - (positive_in_population / new_population)) / math.sqrt(((rho * (1-rho)) / sub_population) + (rho * (1-rho) / new_population))'
        # print >> sys.stderr, 'z = (( %f / %f ) - (%f / %f)) / math.sqrt(((%f * (1-%f)) / %f) + (%f * (1-%f) / %f))'%(positive_in_sub_population, sub_population, positive_in_population, new_population, rho, rho, sub_population, rho, rho, new_population); raw_input('Z: %f'%(z))
        words2saillance[word] = z
        raw_input('z: %f'%(z))
    ranked_words2saillance = sorted(zip(words2saillance.keys(), words2saillance.values()), key=lambda x: x[1], reverse=True)
    # print >> sys.stderr, ranked_words2saillance[0:4]; raw_input()
    return ranked_words2saillance


def stopList(file):
    S = codecs.open(file, 'r', 'utf-8')
    stop_list = map((lambda line: line.rstrip()), S.readlines())
    return stop_list


def selectToken(token, rank, stop_list):
    token = re.sub(' +', '_', token)
    punctfilter = [u".", u";", u":", u"!", u"?", u",", u"-", u'"', u"«", u"»", u"\/", u"'", u"...", u"(", u")", u"{", u"}", u"[", u"]", u"<", u">", u"&lt;", u"&gt;", u"&apos;", u"&quot;", u"&amp;"]
    if re.match('^[A-ZÀÂÉÈÊËIÎÏOÔÖUÛÜÙÇ0-9_]', token): return False
    if len(token.encode('utf-8')) < 5: return False
    if re.search('_$', token): return False
    # if rank == 0: token = token.lower()
    # if token.lower() == token: return False
    elif re.match('^[-\d+%\. ,]+$', token): return False
    elif re.search('__', token): return False
    elif re.match('^_', token): return False
    # elif re.search('SENT_BOUND|META_TEXTUAL', token): return False
    try:
        if token.decode('utf-8') in punctfilter: return False
    except UnicodeEncodeError:
        if token in punctfilter: return False
    for punct in map((lambda p: re.escape(p)), punctfilter):
        if re.search(punct, token):
            return False
    if token in stop_list: return False
    return True


def getOptions():
    corpus_choices = ['afp', 'wikipedia']
    usage = '\n\n\t%prog [options]\n'
    parser = OptionParser(usage)
    parser.add_option("--words2freq", action="store", dest="words2freq", default=None, help="words2frequency pickle")
    # parser.add_option("--wordspop", action="store", dest="wordspop", type="int", default=27352823, help="words population (default 27,352,823)")
    parser.add_option("--wordspop", action="store", dest="wordspop", type="int", default=922081, help="words population (default 922,081)")
    parser.add_option("--stop", action="store", dest="stop", default=None, help="stop list")
    parser.add_option("--out", "-o", action="store", dest="out", default=None, help="output file <doc => saillant words counts>")
    parser.add_option("--clean", action="store_true", dest="clean", default=False, help="Output XML without <token> and <form>")
    try: (options, args) = parser.parse_args()
    except OptionError: parser.print_help(); sys.exit('OptionError')
    except: parser.print_help(); sys.exit('An error occured during options parsing\n')
    if options.wordspop == None or options.words2freq == None or options.stop == None: parser.print_help(); sys.exit()
    return options, args

if __name__ == "__main__":
    main()    

