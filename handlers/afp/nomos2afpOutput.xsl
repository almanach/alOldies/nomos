<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    <xsl:output encoding="UTF-8" method="xml" indent="no" omit-xml-declaration="yes"/>
    <xsl:template match="/child::*">
<xsl:text>{_PAR_BOUND} _PAR_BOUND</xsl:text>
<xsl:text>
</xsl:text>
<xsl:text>{</xsl:text><root><xsl:attribute name="ref"><xsl:value-of select="./module[@type='news_analytics']/meta/@doc_id"/></xsl:attribute><xsl:text>} _XML
{_PAR_BOUND} _PAR_BOUND</xsl:text>
<xsl:text>
</xsl:text>
<xsl:text>{</xsl:text><news_head>
<xsl:attribute name="dateline"><xsl:value-of select="./module[@type='resolution']//news_head/@dateline"/></xsl:attribute>
<xsl:attribute name="headline"><xsl:value-of select="./module[@type='resolution']//news_head/@headline"/></xsl:attribute></news_head><xsl:text>} _XML
{_PAR_BOUND} _PAR_BOUND
</xsl:text>
<xsl:apply-templates select="./module[@type='news_analytics']/salient_words"/>
<xsl:text>
</xsl:text>
<xsl:text>{</xsl:text><body.content><xsl:text>} _XML
{_PAR_BOUND} _PAR_BOUND</xsl:text>
<xsl:apply-templates select="./module[@type='resolution']"/>
<xsl:text>
</xsl:text>
<xsl:text>{</xsl:text></body.content><xsl:text>} _XML
{_PAR_BOUND} _PAR_BOUND</xsl:text>
<xsl:text>
</xsl:text>
<xsl:text>{</xsl:text></root><xsl:text>} _XML
{_PAR_BOUND} _PAR_BOUND</xsl:text>
    </xsl:template>
    <xsl:template match="salient_words">
        <xsl:text>{</xsl:text><swords><xsl:text>} _XML
{_PAR_BOUND} _PAR_BOUND</xsl:text>
<xsl:text>
</xsl:text>
            
            <xsl:apply-templates select="salient_word[position() &lt; 10]"/>
            <xsl:text>{</xsl:text></swords><xsl:text>} _XML
{_PAR_BOUND} _PAR_BOUND</xsl:text>
    </xsl:template>    
    
    <xsl:template match="salient_word">
        <xsl:text>{</xsl:text><sword>
            <xsl:attribute name="rank"><xsl:value-of select="./@rank"/></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="./@value"/></xsl:attribute>
            <xsl:attribute name="z"><xsl:value-of select="./@z"/></xsl:attribute>
        </sword><xsl:text>} _XML
{_PAR_BOUND} _PAR_BOUND</xsl:text>
        <xsl:text>
</xsl:text>
    </xsl:template>
    
    <xsl:template match="module[@type='resolution']">
        <xsl:apply-templates select=".//para"/> 
    </xsl:template>
    
    <xsl:template match="module[@type='resolution']//para">
        <xsl:choose>
            <xsl:when test="./@signature">
                <xsl:text>
</xsl:text>
<xsl:text>{</xsl:text><p><xsl:attribute name="signature"><xsl:value-of select="./@signature"/></xsl:attribute></p><xsl:text>} _XML
{_PAR_BOUND} _PAR_BOUND</xsl:text>
            </xsl:when>
            <xsl:otherwise>
<xsl:text>
</xsl:text>
<xsl:text>{</xsl:text><p><xsl:text>} _XML
{_PAR_BOUND} _PAR_BOUND</xsl:text>
<xsl:text>
</xsl:text>
                    <xsl:apply-templates/>
<xsl:text>
</xsl:text>
<xsl:text>{</xsl:text></p><xsl:text>} _XML
{_PAR_BOUND} _PAR_BOUND</xsl:text>                
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>    
    
   
    
    <xsl:template match="module[@type='resolution']//para/SENT/token|module[@type='resolution']//para/token|module[@type='resolution']//para//CHOICE[@rank='1' or @rank='0']/token">
    <xsl:value-of select="concat('{', ., '} ', ., ' ')"/>
    </xsl:template>
    
    <xsl:template match="module[@type='resolution']//para//CHOICE[@rank='1' or @rank='0']">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="module[@type='resolution']//para//CHOICE[@rank='1' or @rank='0']/ENAMEX|module[@type='resolution']//para/ENAMEX|module[@type='resolution']//para/SENT/ENAMEX">
        <xsl:variable name="name">
            <xsl:choose>
                <xsl:when test="./@candidate_name">
                    <xsl:value-of select="translate(./@candidate_name, ' ', '_')"/>
                </xsl:when>
                <xsl:when test="./@name">
                    <xsl:value-of select="translate(./@name, ' ', '_')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:for-each select="./token">
                        <xsl:value-of select="concat(., ' ')"/>
                </xsl:for-each></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="type">
            <xsl:choose>
                <xsl:when test="./@candidate_type='Person'">_PERSON</xsl:when>
                <xsl:when test="./@candidate_type='Location'">_LOCATION</xsl:when>
                <xsl:when test="./@candidate_type='POI'">_LOCATION</xsl:when>
                <xsl:when test="./@candidate_type='Organization'">_ORGANIZATION</xsl:when>
                <xsl:when test="./@candidate_type='Company'">_COMPANY</xsl:when>
                <xsl:when test="./@candidate_type='Work'">_WORK</xsl:when>
                <xsl:when test="./@candidate_type='Product'">_PRODUCT</xsl:when>
                <xsl:when test="./@candidate_type='Animal'">_ANIMAL</xsl:when>
                <xsl:when test="./@candidate_type='FictionChar'">_FICTIONCHAR</xsl:when>
                <xsl:when test="./@cand_type='NAE'">_NAE</xsl:when>
                <xsl:when test="./@cand_type='NIL'">
                    <xsl:choose>
                        <xsl:when test="./@type='Person'">_PERSON</xsl:when>
                        <xsl:when test="./@type='Location'">_LOCATION</xsl:when>
                        <xsl:when test="./@type='Organization'">_ORGANIZATION</xsl:when>
                        <xsl:when test="./@type='Company'">_COMPANY</xsl:when>
                        <xsl:when test="./@type='POI'">_POI</xsl:when>
                        <xsl:when test="./@type='Work'">_WORK</xsl:when>
                        <xsl:when test="./@type='Product'">_PRODUCT</xsl:when>
                        <xsl:when test="./@type='Animal'">_ANIMAL</xsl:when>
                        <xsl:when test="./@type='FictionChar'">_FICTIONCHAR</xsl:when>
                    </xsl:choose>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
<xsl:text>{</xsl:text>
            <xsl:for-each select="./token">
                <xsl:value-of select="concat(., ' ')"/>
            </xsl:for-each>
        <xsl:text>} </xsl:text><xsl:value-of select="concat($type, ' [|eid=', ./@resolution_id, ';name=', $name, '|] ')"/>
    </xsl:template>
    
    <xsl:template match="module[@type='resolution']//para/SENT/TIMEX|module[@type='resolution']//para/SENT/NUMEX|module[@type='resolution']//para/SENT/EMAIL|module[@type='resolution']//para/SENT/URL|module[@type='resolution']//para/SENT/TEL|module[@type='resolution']//para/TIMEX|module[@type='resolution']//para/NUMEX|module[@type='resolution']//para/EMAIL|module[@type='resolution']//para/URL|module[@type='resolution']//para/TEL|module[@type='resolution']//para//CHOICE[@rank='1' or @rank='0']/TIMEX|module[@type='resolution']//para//CHOICE[@rank='1' or @rank='0']/NUMEX|module[@type='resolution']//para//CHOICE[@rank='1' or @rank='0']/EMAIL|module[@type='resolution']//para//CHOICE[@rank='1' or @rank='0']/URL|module[@type='resolution']//para//CHOICE[@rank='1' or @rank='0']/TEL">
        <xsl:variable name="tokens">
            <xsl:for-each select="./token"><xsl:value-of select="concat(., '_')"/></xsl:for-each>
        </xsl:variable>
        <xsl:text>{</xsl:text>
        <xsl:for-each select="./token">
            <xsl:value-of select="concat(., ' ')"/>
        </xsl:for-each>
        <xsl:text>} </xsl:text>
        <xsl:choose>
            <xsl:when test="local-name()!='TIMEX' or local-name()!='NUMEX'"><xsl:value-of select="concat('_', local-name())"/></xsl:when>
            <xsl:when test="local-name()='TIMEX'"><xsl:value-of select="_DATE"/></xsl:when>
            <xsl:when test="local-name()='NUMEX'"><xsl:value-of select="_NUM"/></xsl:when>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="text()"/>
</xsl:stylesheet>