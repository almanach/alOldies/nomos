#!/usr/bin/python
# -*- coding: utf-8 -*-
import codecs
import re
import os
import sys
from optparse import *
from xml.dom.minidom import *
from subprocess import *
sys.path.append('@common_src@')
from utils import *
from collections import defaultdict

def main():
    sys.stdout = codecs.getwriter('utf8')(sys.stdout)
    sys.stdin = codecs.getreader("utf-8")(sys.stdin)
    global options
    options, args = getOptions()
    verbose(options.verbose, "Format IN: %s."%(options.formatin))
    # GET INPUT - GET DOM
    try:
        arg = args[0]
        verbose(options.verbose, "Parsing file: %s."%(arg))
        newsml_dom = parse(arg)
        if options.formatin == "light":
            newsml_light_dom = newsml_dom
        else:
            verbose(options.verbose, "Getting NewsML-Light...")
            newsml_light_dom, error = getLightDom("file", arg)
            check_error(error, 27)
    except IndexError:
        file_data = sys.stdin.readlines()
        verbose(options.verbose, "Parsing STDIN data")
        try:
            newsml_dom = parseString("".join(file_data).encode('utf-8'))
            if options.formatin == "light":
                newsml_light_dom = newsml_dom
            else:
                verbose(options.verbose, "Getting NewsML-Light...")
                newsml_light_dom, error = getLightDom("data", "".join(file_data).encode('utf-8'))
        except Exception as the_exception:
            check_error(getError(the_exception), 38)
        arg = None

    verbose(options.verbose, "Getting valid content...")
    content = "\n".join(map(lambda p:p.toxml(), filter(lambda p: p.getAttribute('signature') == "", newsml_light_dom.getElementsByTagName('para'))))
    verbose(options.verbose, "Sending content to NER module...")
    ner_dags = getNerDags(content)
    verbose(options.verbose, "Sending NER content to Linking module...")
    linked_content = getSxpipeNerNormalization(ner_dags) # RETURNS XML CONTENT WITH ROOT <body.content>
    if options.quotes:
        verbose(options.verbose, "Sending linked content to Quotes module...")
        linked_content = getQuotes(linked_content)
    verbose(options.verbose, "Adding Hrefs to Enamex tags...")
    add_entities = Popen(['__prefix__/share/nomos/build/handlers/sxpipe/sxpipeXmlAddAledaInfos'], stdin=PIPE, stdout=PIPE)
    linked_content_dom, error = getDom(add_entities.communicate(linked_content)[0])
    check_error(error, 58)

    if not options.noswords:
        verbose(options.verbose, "Getting document salient words...")
        swords_element_temp = swords2element(getSwords(getTokens(ner_dags)))
        verbose(options.verbose, "Filtering salient words...")
        swords_filter = Popen(['__prefix__/share/nomos/build/handlers/afp/swordsFilter.pl'], stdin=PIPE, stdout=PIPE)
        swords_element = parseString(swords_filter.communicate(swords_element_temp)[0])
    else:
        swords = []

    if options.formatout == "newsml":
        verbose(options.verbose, "Inserting annotations and metadata to NewsML...")
        paras_parent_node = newsml_dom.getElementsByTagName('p')[0].parentNode
        paras_grandparent_node = paras_parent_node.parentNode
        paras_grandparent_node.replaceChild(linked_content_dom.getElementsByTagName('body.content')[0], paras_parent_node)
        # ADD PROPERTIES TO NEWSML: SWORDS
        metadata_node = newsml_dom.getElementsByTagName('DescriptiveMetadata')[0]
        if not options.noswords:
            verbose(options.verbose, "Adding swords to NewsML properties...")
            for sword_node in swords_element.getElementsByTagName("sword"):
                metadata_node.appendChild(createElementNode(newsml_dom, 'Property', {'FormalName':'Sword', 'Value':sword_node.getAttribute("value")}))
        verbose(options.verbose, "Adding Entities to NewsML properties...")
        # ADD PROPERTIES TO NEWSML: ENTITIES
        seen_enamex = set([])
        for enamex in newsml_dom.getElementsByTagName('ENAMEX'):
            if enamex.getAttribute('eid') not in seen_enamex:
                metadata_node.appendChild(createElementNode(newsml_dom, 'Property', {'FormalName':'Entity.'+enamex.getAttribute('type'), 'Value':enamex.getAttribute('href')}))
                seen_enamex.add(enamex.getAttribute('eid'))
        # WRITE OUTPUT
        verbose(options.verbose, "Writing output (NewsML)...")
        sys.stdout.write(newsml_dom.toxml()+"\n")
    elif options.formatout == "ws":
        impl = getDOMImplementation()
        ws_dom = impl.createDocument(None, "root", None)
        root_node = ws_dom.documentElement
        dateline = newsml_light_dom.getElementsByTagName('news_head')[0].getAttribute("dateline")
        headline = newsml_light_dom.getElementsByTagName('news_head')[0].getAttribute("headline")
        head_node = createElementNode(ws_dom, 'news_head', {'dateline':dateline, 'headline':headline})
        root_node.appendChild(head_node)
        if not options.noswords:
            swords_node = createElementNode(ws_dom, "swords")
        else:
            swords_node = swords_element
        root_node.appendChild(swords_node)
        root_node.appendChild(linked_content_dom.documentElement)
        sys.stdout.write(ws_dom.toxml())
        
        

def getSwords(tokens2frequency_dict):
    words_connection = DBConnection(options.words, getWordsDBtables())
    swords = computeSalience(tokens2frequency_dict, options.total_words_population, words_connection)
    return swords

def swords2element(swords):
    swords_element = '<swords>\n'
    i = 0
    # threshold = 1.2
    for (word, z) in swords[0:21]:
        # if z >= threshold:
        i += 1
        swords_element += '<sword value="%s" rank="%d" z="%.4f"/>\n'%(word, i, z)
    swords_element += '</swords>\n'
    return swords_element

def getTokens(dags):
    # HANDLE AMBIGUOUS DAGS => DONT COUNT SEVERAL TIMES SAME TOKEN
    p = re.compile('<F id="([^"]+)">([^<>]+)</F>')
    tokens = selectTokens(dict(p.findall(dags)).values())
    tokens2frequency_dict = defaultdict(int)
    for t in tokens:
        tokens2frequency_dict[t] += 1
    return tokens2frequency_dict

def getNerDags(content):
    cmd = Popen(['sxpipe-afp-nomos'], stdin=PIPE, stdout=PIPE)    
    ner_dags = cmd.communicate(content.encode('utf-8'))[0]
    return ner_dags

def getQuotes(content):
    # GET XML CONTENT WITH ROOT BODY.CONTENT, PUT TEXT BACK TO RAW (<ENAMEX> => _PERSON & CO) WITH XSL, GIVE TO SXPIPE CONF WITH QUOTES, GET BACK DAGS, CLEAN AND SXPIPE2XML
    # xslt
    xslt = Popen(['xsltproc', '@nomosshare@/handlers/sxpipe/bodycontent2dags.xsl', '-'], stdin=PIPE, stdout=PIPE)
    dags = xslt.communicate(content)[0]
    dags = re.sub('(\[\|[^\|]+\|\])', '{\\1} _SPECWORD', dags)# todo: hide enamex infos inside token comments
    quotes = Popen(['sxpipe-afp-quotes-nomos'], stdin=PIPE, stdout=PIPE)
    quotes_content = quotes.communicate(dags)[0]
    quotes_content = re.sub('\{<F id="[^"]+">([^<>]+)<\/F>\} _SPECWORD', '\\1', quotes_content)
    disamb = Popen(['dag2ddag'], stdin=PIPE, stdout=PIPE)
    quotes_content_disamb = disamb.communicate(quotes_content)[0]
    sxpipe2xml = Popen(['sxpipe2xml', '--sent'], stdin=PIPE, stdout=PIPE)
    return sxpipe2xml.communicate(quotes_content_disamb)[0]

def getSxpipeNerNormalization(sxpipe_ner_dags):
    np_normalizer = Popen(['@sxpipebin@/np_normalizer.pl', '-d', '@aledalibdir@', '-l', 'fr'], stdin=PIPE, stdout=PIPE)
    sxpipe_ner_dags_normalized = np_normalizer.communicate(sxpipe_ner_dags)[0]
    # disamb = Popen(['shortest_path'], stdin=PIPE, stdout=PIPE)
    disamb = Popen(['dag2ddag'], stdin=PIPE, stdout=PIPE)
    sxpipe_ner_dags_normalized_disamb = disamb.communicate(sxpipe_ner_dags_normalized)[0]
    clean = Popen(['@sxpipebin@/comments_cleaner.pl'], stdin=PIPE, stdout=PIPE)
    clean_sxpipe_ner_dags_normalized_disamb = clean.communicate(sxpipe_ner_dags_normalized_disamb)[0]
    sxpipe2xml = Popen(['sxpipe2xml', '--sent'], stdin=PIPE, stdout=PIPE)
    sxpipe_ner_normalized_xml = sxpipe2xml.communicate(clean_sxpipe_ner_dags_normalized_disamb)[0]
    return "<body.content>\n" + sxpipe_ner_normalized_xml + "</body.content>\n"

def getDom(xml_content):
    try:
        dom = parseString(xml_content)
    except Exception as the_exception:
        return None, getException(the_exception)
    return dom, None

def getLightDom(mode, data):
    if mode == 'file':
        newsml2text = Popen(['newsML2text', '--infile', data], stdout=PIPE, stderr=PIPE)
        output, error = newsml2text.communicate()
    elif mode == 'data':
        newsml2text = Popen(['newsML2text'], stdin=PIPE, stdout=PIPE, stderr=PIPE)
        output, error = newsml2text.communicate(data)
    if error is not None and error != "":
        return None, error
    try:
        output_dom = parseString(output)
    except Exception as the_exception:
        return None, getException(the_exception)
    return output_dom, None


def getOptions():
    usage = '%s [options] <news_item>\n'%sys.argv[0]
    parser = OptionParser(usage)
    formatsin = ['newsml', 'ws']
    parser.add_option('--formatin', action='store', dest='formatin', default="newsml", type="choice", choices=formatsin, help='<format input:  newsml or light (newsml-light) - default: newsml>')
    formatsout = ['newsml', 'light']
    parser.add_option('--formatout', action='store', dest='formatout', default="newsml", type="choice", choices=formatsin, help='<format output: newsml or ws (web service) - default newsml ; formatout "newsml" incompatible with formatin "light">')
    parser.add_option('--words', action='store', dest='words', default="__prefix__/share/nomos/frwikiwords.dat", help='<path to words data base>')
    parser.add_option('--aledadir', action='store', dest='aledadir', default="__aledalibdir__", help='<path to dir with aleda data bases (default __aledalibdir__)>')
    parser.add_option('--weights', action='store', dest='weights', default='__prefix__/share/nomos/feature_weights', help='<path to feature weights for Nomos resolution (default "__prefix__/share/nomos/feature_weights")>')
    parser.add_option('--fclasses', action='store', dest='fclasses', default='__prefix__/share/nomos/feature_classes', help='<path to feature classes for Nomos resolution (default "__prefix__/share/nomos/feature_classes")>')
    parser.add_option('--features', action='store', dest='fclasses', default='__prefix__/share/nomos/features', help='<path to files with features to be used (default "__prefix__/share/nomos/features")>')
    parser.add_option('--quotes', '-q', action='store_true', dest='quotes', default=False, help='Activate quotation detection')
    parser.add_option('--totalwords', action='store', dest='total_words_population', type='int', default='232083620', help='<total words population - default 232,083,620>')    
    parser.add_option('--noswords', action='store_true', dest='noswords', default=False, help='deactivate salient words (default: False)')
    parser.add_option('--verbose', '-v', '-d', action='store_true', dest='verbose', default=False, help='verbose')
    parser.add_option('--lang', '-l', action='store', dest='lang', default="fr", help='default: fr')
    try: (options, args) = parser.parse_args()
    except OptionError: sys.stderr.write('OptionError'); parser.print_help(); sys.exit()
    except: sys.stderr.write('An error occured during options parsing\n'); parser.print_help(); sys.exit()
    if options.formatin == "light" and options.formatout == "newsml": parser.print_help(); sys.exit('Incompatible option values')
    return options, args

if __name__ == '__main__':
    main()

				
	    
				
