#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
import re
import commands
import codecs
sys.path.append('@common_src@')
from utils import *
from xml.dom.minidom import *

###########################################################################################
# CONTENTS
# getFiles(dir, mode)[fileList[xml:doms|text:dicts]], getFileDom(file)[dom], 
# getStringDom(string)[dom], process(dom, mode)[xml:dom|text:dict],
# getText(dom, nodeName)[linesList], getSlugs(slug_file)[slug_list], selectFiles(dom,slug_file, 
# filter_short_items=0)[flag], slugFilter(file, mode, slug_file)[flag], copyDir(dir, out_name)[makedirs]
###########################################################################################

###########################################################################################

#def getFiles(dir, mode, main_debug_level, filter_short_items=0, limit=0):
def getFiles(arg, options):
    global debug_level
    debug_level = options['debug']
    function_name = sys._getframe().f_code.co_name
    function_name + ' is in mode ' + options['mode']
    if options['mode'] == 'newsml': fileList = []
    elif options['mode'] == 'xml' or options['mode'] == 'text': fileList = {}
    cpt = 0
    global slugs
    if options['slugfilter'] != "":
        slugs = getSlugs(options['slugfilter'])
        debug('SlugFile is ' + options['slugfilter'] + '\n', 2, debug_level)
    else:
        slugs = None
    if os.path.isfile(arg):
        got_file = getFile(arg, options)
        if got_file != None:
            cpt += 1
            if options['mode'] == 'text' or options['mode'] == 'xml': fileList[got_file['ID']] = got_file
            elif options['mode'] == 'newsml': fileList.append(got_file)
            debug(arg + ' dom was appended to the list\n', 2, debug_level)            
    elif os.path.isdir(arg):
        dir = arg
        stop_all = False
        debug('Arg is Dir\n', 3, debug_level)
        debug('Limit is set to ' + str(options['limit']) + '\n', 3, debug_level)
        for root, dirs, files in os.walk(dir):
            if not stop_all:
                for file in files:
                    debug('Counter is ' + str(cpt) + '\n', 3, debug_level)                    
                    if options['limit'] and cpt >= options['limit']:
                        debug('Counter is greater than limit: stop\n', 3, debug_level)                                            
                        stop_all = True
                        break
                    full_file = os.path.join(root, file)
                    if os.path.isfile(full_file) and not file.startswith('.'):
                        debug('Getting file dom for ' + full_file + '\n', 2, debug_level)
                        got_file = getFile(full_file, options)
                        if got_file != None:
                            cpt += 1
                            if options['mode'] == 'text' or options['mode'] == 'xml': fileList[got_file['ID']] = got_file
                            elif options['mode'] == 'newsml': fileList.append(got_file)
                            debug(file + ' dom was appended to the list\n', 2, debug_level)                                
                    else:
                        debug(full_file + ' is not a file\n', 2, debug_level)                    
    # FILELIST IS LIST OF XML-DOMS OR LIST OF TEXT-DICTIONARIES
    if options['mode'] == 'text' or options['mode'] == 'xml':
        if len(fileList.keys()) >= 1 and options['debug'] > 0: printerr(str(cpt) + ' File(s) selected')
    elif options['mode'] == 'newsml':
        if len(fileList) >= 1 and options['debug'] > 0: printerr(str(cpt) + ' File(s) selected')
    return fileList

########################################################################################### 
def getFile(file, options):
    try:
        newsDom = getFileDom(file)
#        print newsDom.toxml();raw_input();
#        filter: if 1 keep; else don't
        if slugs:
            debug(file + " is being selected...\n", 4, debug_level)
            debug("Selecting ... (filter_short_items is set to: %s)\n" %(str(options['filter_short_items'])), 4, debug_level)
            select_flag = selectFiles(newsDom, options)
            debug(file + '\nFile selected ? => ' + str(select_flag) + '\n', 2, debug_level)
        else:
            select_flag = 1
        if(select_flag):
            news_item = process(newsDom, options) # news_item = dom (xml) or dict (text)
            if news_item == "IPTC filtered":
                debug(file + " is not appended to the list (IPTC filtering)\n", 2, debug_level)
                return None
            elif news_item == "BSW filtered":
                debug(file + " is not appended to the list (BSW filtering)\n", 2, debug_level)
                return None
            if news_item:
#                print news_item;raw_input();
                debug('Got '+file + ' dom\n', 2, debug_level)
                return news_item            
            else:
                debug(file + " has no dom appended to the list (pb in process function)\n", 2, debug_level)
                return None
        else:
            return None
    except xml.parsers.expat.ExpatError:
        printerr("Couldn't get dom - pass %s\n" %(file))
        return None

###########################################################################################        
def getSlugs(slug_file):
    try:
        SLUGS = open(slug_file)
    except IOError:
        printerr("Can't open slug file - exit\n")
        sys.exit()

    slug_list = []
    pattern = re.compile('^([^#\t]+)')
    for line in SLUGS:
        # debug(line, 5, debug_level)
        match = pattern.match(line)
        if(match):
            slug = match.group(0)
            slug = slug.encode("utf8")
            slug = replaceDiacr(slug)
            slug = slug.upper()
            # debug(slug, 5, debug_level)
            slug_list.append(slug)        
    SLUGS.close()
    return slug_list

###########################################################################################

def getFileDom(file):
    fileDom = parse(file)
    return fileDom

###########################################################################################        

def getStringDom(string):
    stringDom = parseString(string)
    return stringDom

###########################################################################################

def getText(dom, nodeName):
    lines = []
    for node in dom.getElementsByTagName(nodeName):
        data = ''
        try:
            for child in node.childNodes:
                data += child.nodeValue
            lines.append(data)
        except:
            pass
        
    if len(lines) > 1:
        return lines
    elif len(lines) == 1:
        if re.search('^\s*$', lines[0]):
            return None
        else:
            return lines[0]
    else:
        return None

def getRecursiveText(dom, nodeName):
    lines = []
    for node in dom.getElementsByTagName(nodeName):
        for child in node.childNodes:
            if child.nodeType == child.TEXT_NODE:
                data += child.data
            elif child.nodeType == child.ELEMENT_NODE:
                for sub_child in child.childNodes:
                    if sub_child.nodeValue:
                        data += child.data
                       
###########################################################################################

def process(dom, options):
    #process dom and return various elements accodring to defined mode
    #modes : all xml dom, text-dictionary (NameLabel+HeadLine+p>text_content)

    # GET IPTC CAT: GET ALL SubjectCode CHILDREN AND ALL CODES; KEEP MAIN CODE(S) => LIST
    # <SubjectCode>
    # <Subject
    # <SubjectMatter    
    # <SubjectDetail
    # <SubjectQualifier
    iptc_code = []; codes = {}
    try:
        for subject in dom.getElementsByTagName('SubjectCode'):
            for child in subject.childNodes:
                # CONVERT IPTC NUMERIC CODES INTO LETTER CODES SUSCH AS "SPO" OR "ECO"
                try: code = convertIptcCodes(child.getAttribute('FormalName'))
                except AttributeError: continue
                if not codes.has_key(code): codes[code] = 1
        for code in codes.keys(): iptc_code.append(code); debug('Code '+code+' appended to IPTC codes', 3, debug_level)
    except IndexError:        
        iptc_code = ["NullIPTC"]
        debug('IndexError on getting SubjectCode Element in current dom\n', 2, debug_level)
    except:        
        iptc_code = ["NullIPTC"] 
        debug('Error on getting SubjectCode Element in current dom\n', 2, debug_level) 
        
    return_flag = 1
    if options['noiptc'] != []:
        for code in options['noiptc']:
            if code in iptc_code: return_flag = 0; break
    if (options['mode'] == 'text' or options['mode'] == 'xml') and return_flag:
        dateline = getText(dom, 'DateLine') or "DateLineNull"
        if dateline == None: dateline = "DateLineNull"
        if isinstance(dateline, list): dateline = dateline[0]
        if re.match('\(\s*\BSW\s*\)', dateline):
            return "BSW filtered"
        else:
            news_slugs = getText(dom, 'NameLabel') or "SlugsNull"
            title = getText(dom, 'HeadLine') or "TitleNull"
            body = getText(dom, 'p') or ["BodyNull"]
            file_id = getText(dom, 'PublicIdentifier') or "IDNull"
            text_dict = {"Dateline":dateline, "ID":file_id, "Slugs":news_slugs, "IPTC":iptc_code, "Title":title, "Body":body}
            return text_dict
    elif options['mode'] == 'newsml' and return_flag:
        return dom
    elif not return_flag:
        return "IPTC filtered"
    else:
        function_name = sys._getframe().f_code.co_name
        message_error = "Problem in " %(function_name)
#        printerr(message_error)
        return None

###########################################################################################

def selectFiles(dom, options):
    debug('In selectFiles - calling slugFilter\n', 4, debug_level)
    slug_flag = slugFilter(dom)
    debug('slugFilter returned flag: ' + str(slug_flag) + "\n", 3, debug_level)
    if slug_flag == 0:
        flag = 0
    elif slug_flag == 1:
        if options['filter_short_items']:
            debug("Applying filter_short_items\n", 4, debug_level)
            paras = getText(dom, 'p')
            if not paras:
                debug("Couldn't get paras' length (pb in getText function) - flag is 1 anyway\n", 2, debug_level)
                length_flag = 1
            if len(paras) < options['filter_short_items']:
                length_flag = 0
            else:
                length_flag = 1
            debug("Length flag is: %s" %(str(length_flag)), 4, debug_level)
        else:
            length_flag = 1

        flag = slug_flag * length_flag
    else:
        debug("No selection on that file - flag is 1 anyway\n", 2, debug_level)
        flag = 1

    debug("selectFiles returns flag: " + str(flag) + "\n", 2, debug_level)
    return flag

###########################################################################################

def slugFilter(dom):
    debug('In slugFilter\n', 2, debug_level)
    # ### GET NEWS CONTENT
    # ### IF ONE OF SLUGS IN SLUGS2FILTER => FLAG = 0 - ELSE 1 - RETURNS FLAG
    flag = 1
    temp = getText(dom, 'NameLabel')
    if temp == None:
        debug("Getting slugLine failed\n", 2, debug_level)
    else:
        debug('NameLabel is: ' + temp + "\n", 2, debug_level)
        slug_list = slugs
        if debug_level == 5: print >> sys.stderr, slug_list
        # debug("Will use list:\n", 5, debug_level)
        # for s in slug_list:
        #     debug(s, 5, debug_level)
    
        try:
            slug_line = temp.rstrip()
            slugs_from_slug_line = re.split('-', slug_line)
        
            for slug in slugs_from_slug_line:
                if(not(flag)):
                    break
                else:
                    # slug = slug.encode("utf8") !!!
                    slug = replaceDiacr(slug)
                    slug = slug.upper()
                    debug('Comparing slug: ' + slug + "\n", 5, debug_level)
                    # for slug2filter in slug_list:
                    #     if(slug == slug2filter):
                    if slug in slug_list:
                        flag = 0
                        debug("file is rejected because of: %s\n" %(slug), 2, debug_level)
                        break
                    else:
                        debug("=> Slug OK\n", 2, debug_level)
            return flag
        except:
            function_name = sys._getframe().f_code.co_name
            message_error = "problem in %s" %function_name
            return message_error
        
###########################################################################################        

def convertIptcCodes(full_code):
    table = {'01':'CLT', '02':'CLJ', '03':'DIS', '04':'ECO', '05':'EDU', '06':'ENV', '07':'HTH', '08':'HUM', '09':'SOC', '10':'LIF', '11':'POL', '12':'REL', '13':'SCI', '14':'SOI', '15':'SPO', '16':'UNR', '17':'WEA'}
    topic = None
    for code in table.keys():
        if full_code.startswith(code):
            topic = table[code]
            break
    if topic:
        return topic
    else:
        return 'NULL'
###########################################################################################

def copyDir(dir, out_name):
    # GET ROOT DIR NAME
    search = re.search("/([^/]+)/?$", dir)
    dir_name = search.group(1)
    # WALK THROUGH DIR
    for dirpath, dirs, files in os.walk(dir):
        for this_dir in dirs:
            out_dir = os.path.join(dirpath, this_dir)
            out_dir = re.sub(dir_name, out_name, out_dir)
            try:
                os.makedirs(out_dir)
            except:
                debug('Anonymous exception caught in copyDir', 1, debug_level)
                pass

###########################################################################################
        
