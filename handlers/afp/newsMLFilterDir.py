#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
import re
sys.path.append('@common_src@')
from utils import *
from xml.dom.minidom import *
from optparse import *
from subprocess import *
import codecs
from operator import *
import shutil

def main():
    options, args = getOptions()
    # INIT  MIN SIZE
    global filter_short_items; filter_short_items = options.filter_short_items
    # INIT FILTERS
    global slugs; global genres; slugs = []; genres = []; global iptcfilter; iptcfilter = options.iptcfilter
    if options.slugfilter != None and options.slugfilter != "":
        slugs = getSlugsOrGenres(options.slugfilter)
    if options.genrefilter != None and options.genrefilter != "":
        genres = getSlugsOrGenres(options.genrefilter)


    if os.path.isdir(args[0]):
        extpattern = re.compile('\.([^\.]+)$')
        cpt = 0
        for root, dirs, files in os.walk(args[0]):
            for file in files:
                # print >> sys.stderr, "File: %s"%(file)
                if cpt > options.limit and options.limit != 0: print >> sys.stderr, "Limit reached"; break
                if file.startswith('\.'): continue
                m = extpattern.search(file)
                if m == None: print >> sys.stderr, "No extension found (%s)"%(file); continue
                elif m.group(1) not in options.extensions: print >> sys.stderr, "Bad extension (%s)"%(file); continue
                full_file = os.path.join(root, file)
                try:
                    news_dom = parse(full_file)
                except Exception as the_exception:
                    print >> sys.stderr, "Can't parse news content (mark 2) - Exception %s (%s)\nFile %s\n"%(type(the_exception), the_exception.args, full_file)
                    continue
                iptc_codes = set([])
                for node in mergeLists([news_dom.getElementsByTagName('SubjectMatter'), news_dom.getElementsByTagName('SubjectDetail'), news_dom.getElementsByTagName('SubjectQualifier'), news_dom.getElementsByTagName('Subject')]):
                    iptc_codes.add(convertIptcCodes(node.getAttribute('FormalName')))
                if filterNews(news_dom, iptc_codes):
                    newfile = options.out+"/"+file
                    print >> sys.stderr, "Copy %s to %s"%(full_file, newfile)
                    shutil.copy(full_file, newfile)
    else:
        print >> sys.stderr, "Argument %s is not a directory"%(args[0])


def filterNews(newsmldom, iptc_codes):
    for iptc_code in iptc_codes:
        if iptc_code in iptcfilter: return False
    if newsmldom.getElementsByTagName('DateLine') == [] or newsmldom.getElementsByTagName('HeadLine') == []: return False
    if re.search('BSW', getNodeData(newsmldom.getElementsByTagName('DateLine')[0])): return False
    if filter_short_items != 0 and len(newsmldom.getElementsByTagName('p')) < filter_short_items: return False
    for slug in getNodeData(newsmldom.getElementsByTagName('NameLabel')[0]).split('-'):
        if replaceDiacr(slug).upper().encode('utf-8') in slugs: return False
    if newsmldom.getElementsByTagName('Genre') != []:
        if replaceDiacr(getNodeData(newsmldom.getElementsByTagName('Genre')[0])).upper().encode('utf-8') in genres: return False
    return True

def convertIptcCodes(full_code):
    table = {'01':'CLT', '02':'CLJ', '03':'DIS', '04':'ECO', '05':'EDU', '06':'ENV', '07':'HTH', '08':'HUM', '09':'SOC', '10':'LIF', '11':'POL', '12':'REL', '13':'SCI', '14':'SOI', '15':'SPO', '16':'UNR', '17':'WEA'}
    topic = None
    for code in table.keys():
        if full_code.startswith(code):
            topic = table[code]
            break
    return topic

def getSlugsOrGenres(file):
    t = []; pattern = re.compile('^([^#\t]+)')
    F = open(file)
    for line in F:
        match = pattern.match(line)
        if(match): t.append(replaceDiacr(match.group(0)).upper().encode('utf-8'))
    F.close()
    return t

def getOptions():
    usage = '\n\n\t%prog [options] <newsml dir>\n'
    parser = OptionParser(usage)
    parser.add_option("--out", "-o", action="store", dest="out", default=None, type="string", help="Path to output directory")
    parser.add_option("--slugfilter", action="store", dest="slugfilter", default=None, type="string", help='Path to slugfilter file')
    parser.add_option("--genrefilter", action="store", dest="genrefilter", default=None, type="string", help='Path to genrefilter file')
    parser.add_option("--iptcfilter", action="append", dest="iptcfilter", default=[], help='IPTC codes to filter (ex. SPORT = SPO POLITICS = POL ...); concat. with ":"; ex.: --iptcfilter SPO or --iptcfilter SPO:POL')    
    # parser.add_option("--lang", "--lg", "-l", action="store", dest="lang", default="fr", type="string", help='SxPipe language')
    parser.add_option("--filter", action="store", dest="filter_short_items", default="0", type="int", help="If set, include only news with at least FILTER-1 paragraphs of text")
    parser.add_option("--limit", action="store", dest="limit", default="0", type="int", help="If set, stop after selecting LIMIT news")
    parser.add_option("--ext", action="append", dest="extensions", default=['xml', 'newsml'], help="Extension for files to process")
    parser.add_option("-d", "--debug", action="store", dest="debug", default="0", type="int", help="Print some debug (verbose from 1 to 4) (press Enter to continue)")
    try: (options, args) = parser.parse_args()
    except OptionError: sys.stderr.write('OptionError'); parser.print_help(); sys.exit()
    except: sys.stderr.write('An error occured during options parsing\n'); parser.print_help(); sys.exit()
    if len(args) > 1: sys.stderr.write('Argument error'); parser.print_help(); sys.exit()
    return options, args

if __name__ == '__main__':
    main()
