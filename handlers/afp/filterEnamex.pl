#!/usr/bin/perl -w

use strict;

my $filter_loc = 0;
my $filter_org = 0;

while(my $opt = shift)
  {
    if($opt =~ /^-?-?loc$/){$filter_loc = 1;}
    elsif($opt =~ /^-?-?org$/){$filter_org = 1;}
    else{next;}
  }

if($filter_loc == 0 and $filter_org == 0)
  {
    while(<>){print;}
  }

else
  {
    while(<>)
      {
	if($_ !~ /<ENAMEX/){print; next;}
	chomp;
	my $line = "";
	while($_ =~ /<ENAMEX[^<>]+>[^<>]+<\/ENAMEX>/)
	  {
	    my $before = $`; my $after = $'; my $enamex = $&;
	    $line .= $before;
	    $enamex =~ /<ENAMEX([^<>]+)>([^<>]+)<\/ENAMEX>/;
	    my $attr = $1; my $content = $2;
	    # if($attr =~ /type="Location"/ or $attr =~ /type="Organization"/ or $attr =~ /type="Company"/)
	    if(($attr =~ /type="Location"/ and $filter_loc) or (($attr =~ /type="Organization"/ or $attr =~ /type="Company"/) and $filter_org))
	      {
		$line .= $content;
	      }
	    elsif($attr =~ /type="Person"/ and $attr =~ /eid="null"/)
	      {
		# DELETE ENAMEX UNLESS IT IS AN AUTHOR
		if($before =~ /<AUT>$/){$line .= $enamex;}
		else{$line .= $content;}
	      }
	    else
	      {
		$line .= $enamex;
	      }
	    $_ = $after;
	  }
	$line .= $_;
	print $line."\n";
      }
  }

